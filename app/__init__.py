from flask import Flask, render_template, jsonify, g, request,url_for
from werkzeug.datastructures import CombinedMultiDict, MultiDict
from werkzeug.exceptions import HTTPException
import sys
import commons
import wrappers
import json
import logging
import logging.handlers
import config
from app.commons.resources import my_logger
import datetime
import urllib
import traceback


# Define the WSGI application object
app = Flask(__name__)

# trying to config parameters
try:
    app.config.from_object('config')
except Exception as e:
    print 'environment config not found'
    sys.exit(1)

if app.debug is not True:
    import logging
    from logging.handlers import RotatingFileHandler
    file_handler = RotatingFileHandler('flask_error.log', maxBytes=1024 * 1024 * 100, backupCount=20)
    file_handler.setLevel(logging.ERROR)
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    file_handler.setFormatter(formatter)
    app.logger.addHandler(file_handler)

@app.before_request
def pre():
    json =  request.get_json() if type(request.get_json()) is dict else {}
    g.incoming = CombinedMultiDict([request.args, request.form, json])
    g.request_start_time = commons.Toolkit.get_timestamp()
    g.processing_time = lambda: int(g.request_end_time - g.request_start_time)
    g.incoming_files = request.files.keys()
    print g.incoming
    print g.incoming_files

    #my_logger.info(timestamp+' '+str(request.url))
    # output = []
    # for rule in app.url_map.iter_rules():
    #     print rule
    #     options = {}
    #     for arg in rule.arguments:
    #         options[arg] = "[{0}]".format(arg)

    #     methods = ','.join(rule.methods)
    #     url = url_for(rule.endpoint, **options)
    #     line = urllib.unquote("{:50s} {:20s} {}".format(rule.endpoint, methods, url))
    #     output.append(line)

    # for line in sorted(output):
    #     print "*$*"+str(line)


@app.after_request
def after_request(response):
    time=str(datetime.datetime.now())
    timestamp=time.split('.')[0]
    my_logger.info(timestamp+' '+str(g.incoming))
    my_logger.info(timestamp+' '+str(g.incoming_files))
    if response.status_code==200:
        my_logger.info(timestamp+' '+str(request.url)+' - '+str(response.status_code))
    else:
        my_logger.error(timestamp+' '+str(request.url)+' - '+str(response.status_code))
    my_logger.info('========================================================================================')

    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS,HEAD')
    return response

# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    time=str(datetime.datetime.now())
    timestamp=time.split('.')[0]
    my_logger.error(timestamp+' '+str(error))
    return jsonify({'status' : 'error', 'message' : "Page found, but we'll not show you."}), 404

@app.errorhandler(405)
def not_found(error):
    time=str(datetime.datetime.now())
    timestamp=time.split('.')[0]
    my_logger.error(timestamp+' '+str(error))
    return jsonify({'status' : 'error', 'message' : 'Is this the right HTTP method?'}), 405

@app.errorhandler(500)
def internal_error(exception):
    time=str(datetime.datetime.now())
    timestamp=time.split('.')[0]
    my_logger.error(timestamp+' '+str(error))
    #app.logger.exception(exception)
    return render_template('500.html'), 500

@app.errorhandler(Exception)
def handle_error(exception):
    print exception
    trace = traceback.format_exc()
    code = 418
    if isinstance(exception, HTTPException):
        code = exception.code

    try:
        db_e = wrappers.Postgres.init()
        cursor_e = wrappers.Postgres.get_cursor(db_e)
        cursor_e.execute("SELECT * FROM error_token")
        res = cursor_e.fetchone()
        token = str(res[0])
        my_logger.error("Token ID %s : %s" % (token, str(exception)))
        my_logger.error(trace)
        cursor_e.execute("UPDATE error_token SET last_used=%(last_used)s",{'last_used':res[0]+1})
        db_e.commit()
        db_e.close()
    except Exception, e:
        print e
        token = "AMMO-ERROR"
        my_logger.error("Token ID : " + token)
        my_logger.error("--------OUTER ERROR--------")
        my_logger.error(exception)
        my_logger.error(trace)
        my_logger.error("--------INNER ERROR--------")
        my_logger.error(e)
        trace = traceback.format_exc()
        my_logger.error(trace)

    response = {'status': 'error',
                'message': 'High on caffeine... Token no. ',
                'token_id': token}

    return jsonify(response), code

# set up static file serving endpoint in debug mode
if app.debug is True:
    @app.route('/<path:path>')
    def static_proxy(path):
        # send_static_file will guess the correct MIME type
        return app.send_static_file(path)

# Import a module / component using its blueprint handler variable (mod_auth)
from app.utility.controllers import api as util_api
from app.testing.controllers import test_api
from app.hooks.controllers import post_hooks
from app.home.controllers import home_api
from app.content.controllers import api as content_api
from app.channel.controllers import api as channel_api
from app.sponsor.controllers import api as sponsor_api
from app.version.controllers import api as version_api
from app.feed.controllers import api as feed_api
from app.tag.controllers import api as tag_api
from app.raw_content_registry.controllers import api as raw_content_registry_api
from app.provider.controllers import api as provider_api
from app.bloodhound.controllers import api as bloodhound_api


# Register blueprint(s)
app.register_blueprint(util_api)
app.register_blueprint(test_api)
app.register_blueprint(home_api)
app.register_blueprint(content_api)
app.register_blueprint(channel_api)
app.register_blueprint(sponsor_api)
app.register_blueprint(version_api)
app.register_blueprint(feed_api)
app.register_blueprint(post_hooks)
app.register_blueprint(tag_api)
app.register_blueprint(raw_content_registry_api)
app.register_blueprint(provider_api)
app.register_blueprint(bloodhound_api)
