"""
    Controller to dictate working of __URL__/tag/ and all associated APIs.
"""
from flask import Blueprint, request, g
from app.commons.CORS import crossdomain
from app.commons.Toolkit import respond, get_timestamp
import models
import app.commons.modules as modules
from werkzeug.datastructures import CombinedMultiDict, MultiDict

# Setting up access to this controller under '<OUR_AWESOME_DOMAIN>/tag/*'
api = Blueprint('bloodhound', __name__, url_prefix='/bloodhound')

@api.route('/tag', methods=['GET'])
def bloudhound_tag():
    search = request.args.get('query')
    response = modules.bloodhound.tag(search)
    return respond(response)

@api.route('/content', methods=['GET'])
def bloudhound_content():
    search = request.args.get('query')
    response = modules.bloodhound.content(search)
    return respond(response)

@api.route('/image_ad', methods=['GET'])
def bloudhound_image_ad():
    search = request.args.get('query')
    response = modules.bloodhound.image_ad(search)
    return respond(response)

@api.route('/preroll', methods=['GET'])
def bloudhound_preroll():
    search = request.args.get('query')
    response = modules.bloodhound.preroll(search)
    return respond(response)

@api.route('/splash', methods=['GET'])
def bloudhound_splash():
    search = request.args.get('query')
    response = modules.bloodhound.splash(search)
    return respond(response)

@api.route('/sponsor', methods=['GET'])
def bloudhound_sponsor():
    search = request.args.get('query')
    response = modules.bloodhound.sponsor(search)
    return respond(response)

@api.route('/collection', methods=['GET'])
def bloudhound_collection():
    search = request.args.get('query')
    response = modules.bloodhound.collection(search)
    return respond(response)

@api.route('/channel', methods=['GET'])
def bloudhound_channel():
    search = request.args.get('query')
    response = modules.bloodhound.channel(search)
    return respond(response)

@api.route('/provider', methods=['GET'])
def bloudhound_provider():
    search = request.args.get('query')
    response = modules.bloodhound.provider(search)
    return respond(response)
