import wrappers


def keep(preroll,image_ad,splash,sponsor_id,sponsor_dict,exist):
    
    __TRACKED_FIELDS = ["sponsor_id", "ad_id", "ad_type"]
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
 
    if exist is  True:  #sponsor entry does not exist
        index=0
        for preroll_obj in preroll:
            if preroll_obj !=None:
                query = "DELETE FROM ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s and ad_id=%(ad_id)s"
                cursor.execute(query,{'sponsor_id':int(str(sponsor_id).split('-')[-1]),'ad_type':'preroll','ad_id':index})
            index=index+1    
        index=0
        for splash_obj in splash:
            if splash_obj !=None:
                query = "DELETE FROM ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s and ad_id=%(ad_id)s"
                cursor.execute(query,{'sponsor_id':int(str(sponsor_id).split('-')[-1]),'ad_type':'splash','ad_id':index})
            index=index+1    
            
        index=0
        for image_ad_obj in image_ad:
            if image_ad_obj !=None:
                query = "DELETE FROM ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s and ad_id=%(ad_id)s"
                cursor.execute(query,{'sponsor_id':int(str(sponsor_id).split('-')[-1]),'ad_type':'image_ad','ad_id':index})
            index=index+1    

    index=0
    for preroll_obj in preroll:
        if preroll_obj !=None and preroll_obj!='':
            query="INSERT INTO ads_map(sponsor_id,ad_type,ad_id,resource_location) VALUES (%(sponsor_id)s,%(ad_type)s,%(ad_id)s,%(resource_location)s)"
            cursor.execute(query,{'sponsor_id':int(str(sponsor_id).split('-')[-1]),'ad_type':'preroll','ad_id':index,'resource_location':preroll_obj})
        index=index+1

    index=0
    for splash_obj in splash:
        if splash_obj != None and splash_obj!='':
            query="INSERT INTO ads_map(sponsor_id,ad_type,ad_id,resource_location) VALUES (%(sponsor_id)s,%(ad_type)s,%(ad_id)s,%(resource_location)s)"
            cursor.execute(query,{'sponsor_id': int(str(sponsor_id).split('-')[-1]),'ad_type':'splash','ad_id':index,'resource_location':splash_obj})
        index=index+1

    index=0
    for image_ad_obj in image_ad:
        if image_ad_obj !=None and image_ad_obj!='':
            query="INSERT INTO ads_map(sponsor_id,ad_type,ad_id,resource_location) VALUES (%(sponsor_id)s,%(ad_type)s,%(ad_id)s,%(resource_location)s)"
            cursor.execute(query,{'sponsor_id': int(str(sponsor_id).split('-')[-1]),'ad_type':'image_ad','ad_id':index,'resource_location':image_ad_obj})
        index=index+1

    db.commit()
    db.close()