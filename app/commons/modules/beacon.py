from app.commons.resources import beacon_redis_cli

def beacon_redis_del(item_type, item_id):
    try:
        item_type = str(item_type)
        item_id = str(item_id).split('-')[-1]
        redis_key = 'fetch:%s:%s' % (item_type, item_id)
        beacon_redis_cli.delete(redis_key)
    except Exception, e:
        print 'Beacon redis error -->', e
    finally:
        return True
