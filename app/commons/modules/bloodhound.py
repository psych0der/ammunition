from re import escape as rescape
import wrappers

def tag(search):
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    search = rescape(search)
    response = {}
    response['status'] = 'success'
    tag_list = []
    query = "SELECT array_agg(name) from tags where status = 'complete' AND (name ~* %(search)s or display_name ~* %(search)s)"
    cursor.execute(query, {'search': str(search)})
    result = cursor.fetchall()
    db.close()
    if len(result) > 0 and result[0][0] != None:
        tag_list = result[0][0]
    response['data'] = tag_list
    return response

def collection(search):
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    search = rescape(search)
    response = {}
    response['status'] = 'success'
    tag_list = []
    query = "SELECT id, name from tags where (name ~* %(search)s or display_name ~* %(search)s) AND collection = 'yes' AND status = 'complete'"
    cursor.execute(query, {'search': str(search)})
    result = cursor.fetchall()
    db.close()
    if len(result) > 0:
        for row in result:
            tag_object = {"id": "pp-tag-" + str(row[0]),
                            "name": str(row[1])}
            tag_list.append(tag_object)
    response['data'] = tag_list
    return response


def content(search):
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    search = rescape(search)
    response = {}
    response['status'] = 'success'
    content_list = []
    query = "SELECT id, name from content where status = 'complete' AND name ~* %(search)s"
    cursor.execute(query, {'search': str(search)})
    result = cursor.fetchall()
    db.close()
    if len(result) > 0:
        for row in result:
            content_object = {"id": "pp-content-" + str(row[0]),
                            "name": str(row[1])}
            content_list.append(content_object)
    response['data'] = content_list
    return response


def channel(search):
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    search = rescape(search)
    response = {}
    response['status'] = 'success'
    channel_list = []
    query = "SELECT id, name from channels where status = 'complete' AND (name ~* %(search)s OR display_name ~* %(search)s)"
    cursor.execute(query, {'search': str(search)})
    result = cursor.fetchall()
    db.close()
    if len(result) > 0:
        for row in result:
            channel_object = {"id": "pp-channel-" + str(row[0]),
                            "name": str(row[1])}
            channel_list.append(channel_object)
    response['data'] = channel_list
    return response


def image_ad(search):
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    search = rescape(search)
    response = {}
    response['status'] = 'success'
    ad_list = []
    query = "SELECT s.name, s.id, am.id, am.ad_id FROM ads_map as am LEFT JOIN sponsors as s on am.sponsor_id = s.id WHERE s.active = 'yes' AND am.ad_type = 'image_ad' AND s.status = 'complete' AND s.name ~* %(search)s"
    cursor.execute(query, {'search': str(search)})
    result = cursor.fetchall()
    db.close()
    if len(result) > 0:
        for row in result:
            ad_object = {"name": str(row[0]),
                            "sponsor_id": "pp-sponsor-" + str(row[1]),
                            "id": "pp-sponsor-" +str(row[1])+"-"+ str(row[3])
                            }
            ad_list.append(ad_object)
    response['data'] = ad_list
    return response


def preroll(search):
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    search = rescape(search)
    response = {}
    response['status'] = 'success'
    ad_list = []
    query = "SELECT s.name, s.id, am.id, am.ad_id FROM ads_map as am LEFT JOIN sponsors as s on am.sponsor_id = s.id WHERE s.active = 'yes' AND am.ad_type = 'preroll' AND s.status = 'complete' AND s.name ~* %(search)s"
    cursor.execute(query, {'search': str(search)})
    result = cursor.fetchall()
    db.close()
    if len(result) > 0:
        for row in result:
            ad_object = {"name": str(row[0]),
                            "sponsor_id": "pp-sponsor-" + str(row[1]),
                            "id": "pp-sponsor-" + str(row[1]) + "-" + str(row[3])
                            }
            ad_list.append(ad_object)
    response['data'] = ad_list
    return response


def splash(search):
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    search = rescape(search)
    response = {}
    response['status'] = 'success'
    ad_list = []
    query = "SELECT s.name, s.id, am.id, am.ad_id FROM ads_map as am LEFT JOIN sponsors as s on am.sponsor_id = s.id WHERE s.active = 'yes' AND am.ad_type = 'splash' AND s.status = 'complete' AND s.name ~* %(search)s"
    cursor.execute(query, {'search': str(search)})
    result = cursor.fetchall()
    db.close()
    if len(result) > 0:
        for row in result:
            ad_object = {"name": str(row[0]),
                            "sponsor_id": "pp-sponsor-" + str(row[1]),
                            "id": "pp-sponsor-" + str(row[1]) + "-" + str(row[3])
                            }
            ad_list.append(ad_object)
    response['data'] = ad_list
    return response


def sponsor(search):
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    search = rescape(search)
    response = {}
    response['status'] = 'success'
    ad_list = []
    query = "SELECT id, name FROM sponsors WHERE active = 'yes' AND status = 'complete' AND name ~* %(search)s"
    cursor.execute(query, {'search': str(search)})
    result = cursor.fetchall()
    db.close()
    if len(result) > 0:
        for row in result:
            ad_object = {"id": "pp-sponsor-" + str(row[0]),
                            "name": str(row[1])
                            }
            ad_list.append(ad_object)
    response['data'] = ad_list
    return response

def provider(search):
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    search = rescape(search)
    response = {}
    response['status'] = 'success'
    provider_list = []
    query = "SELECT id, name FROM provider WHERE active = 'yes' AND status = 'complete' AND name ~* %(search)s"
    cursor.execute(query, {'search': str(search)})
    result = cursor.fetchall()
    db.close()
    if len(result) > 0:
        for row in result:
            provider_object = {"id": "pp-provider-" + str(row[0]),
                            "name": str(row[1])
                            }
            provider_list.append(provider_object)
    response['data'] = provider_list
    return response
