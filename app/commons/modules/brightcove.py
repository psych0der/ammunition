import sys
import requests
import json
import argparse
import config

access_token_url = "https://oauth.brightcove.com/v3/access_token"
video_url="https://analytics.api.brightcove.com/v1/data"
def get_access_token():
    access_token = None
    r = requests.post(access_token_url, params="grant_type=client_credentials", auth=(
        config.BRIGHTCOVE_CLIENT_ID, config.BRIGHTCOVE_SECRET), verify=False)
    if r.status_code == 200:
        access_token = r.json().get('access_token')
    return access_token

def submit_pbi(video_id,src_video,callback_type=None):

    access_token=get_access_token()
    headers = {'Authorization': 'Bearer ' + access_token,"Content-Type": "application/json"}

    url = ("https://ingest.api.brightcove.com/v1/accounts/{pubid}/videos/{videoid}/ingest-requests").format(pubid=config.BRIGHTCOVE_PUB_ID, videoid=video_id)
    callback = config.API_PROTOCOL+'://'+config.API_SERVER_NAME+'/hooks/brightcove'

    if callback_type is not None and callback_type == 'trailer':
        callback = config.API_PROTOCOL+'://'+config.API_SERVER_NAME+'/hooks/brightcove/trailer'

    data = {"master": {"url": "s3://"+src_video}, "profile":"pp-default", "callbacks": [callback]}

    r = requests.post(url, headers=headers, json=data)

    return r.json()


def create_video(name):
    access_token = get_access_token()
    headers = {'Authorization': 'Bearer ' + access_token,
               "Content-Type": "application/json"}

    url = ("https://cms.api.brightcove.com/v1/accounts/{pubid}/videos/").format(pubid=config.BRIGHTCOVE_PUB_ID)
    data = {"name":name}
    r = requests.post(url, headers=headers, json=data)
    return r.json()
