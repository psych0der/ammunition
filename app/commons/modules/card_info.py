import wrappers
from models.Version_store import Version_store
import config


def update_status(row_id):
	response = {}
	db = wrappers.Postgres.init()
	cursor = wrappers.Postgres.get_cursor(db)
	if row_id is None:
		response['status'] = 'failed'
		response['http_status']=420
		response['message']='Kya update karun bhai?'
	else:
		try:
			print row_id
			cursor.execute("UPDATE version_box set status='completed' where id=%(row_id)s", {'row_id': str(row_id)})
			db.commit()
			response['status'] = 'success'
			response['http_status']=200
			response['message']='Sab badiya!'
		except Exception, e:
			print e
			response['status'] = 'failed'
			response['http_status']=420
			response['message']='Galat id mat bhej!'
		db.close()
	return response


def get_status():
	response={}
	db = wrappers.Postgres.init()
	cursor = wrappers.Postgres.get_cursor(db)
	cursor.execute("SELECT version_id, status, zip_location, id from version_box ORDER BY id DESC LIMIT 1")
	res=cursor.fetchone()
	db.close()
	if res!=None:
		response['process_status'] = str(res[1])
		response['zip_location'] = str(res[2])
		response['version_id'] = str(res[0])
		response['row_id'] = str(res[3])
		response['status'] = "success"
		return response
	else:
		response['status']='failed'
		response['http_status']=420
		response['message']='No version published yet'
		response['new-version']='no'
		return response


def query_stats(version_id):
	response={}
	db = wrappers.Postgres.init()
	cursor = wrappers.Postgres.get_cursor(db)
	cursor.execute("SELECT status,zip_location FROM version_box where version_id = %(version_id)s",{'version_id':int(version_id.split('-')[-1])})
	res=cursor.fetchone()
	db.close()
	if res != None:
		response['version_id']='pp-version-'+str(version_id)
		if res[0]=='pending':
			response['process_status']='pending'
			response['http_status']=420
			response['message']='The process is still pending'
			return response
		elif res[0]=='failed':
			response['process_status']='failed'
			response['http_status']=418
			response['message']='failed'
			return response

		elif res[0]=='success':
			response['process_status']='success'
			response['http_status']=200
			response['message']='process has completed'
			response['zip_location']=res[1]
			return response
	else:
		response['status']='failed'
		response['http_status']=420
		response['message']='version is not yet published'
		return response
