import wrappers
from app.commons.Toolkit import enumerate_table
from models.Channel import Channel
import os
import upload_s3 as fileops
from copy import deepcopy
from flask import request
import tags_map
import json
import time
import config
from datatable_adapter import generic_adapter
from slugify import slugify
from app.commons.modules.userlogs import log_activity
from app.commons.modules.beacon import beacon_redis_del

def fetch(channel_id, incoming):
    requested_channel = Channel(channel_id=channel_id)
    response = {'exists' : False, 'status': 'failure'}

    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    if requested_channel._exists == True:
        response['exists'] = True
        response['status'] = 'success'
        tags_list=[]
        cursor.execute("SELECT name from tags INNER JOIN tags_map ON tags_map.tag_id=tags.id where item_type=%(item_type)s and item_id=%(channel_id)s",{'item_type':'channel','channel_id':int(requested_channel.id.split('-')[-1])})
        tag_ids=cursor.fetchall()

        for tag_id in tag_ids:
            print tag_id[0]
            tags_list.append(tag_id[0])

        response['data'] = requested_channel.export()
        response['data']['tags'] = tags_list
        response['data']['icon_favicon'] = None
        response['data']['icon_small_favicon'] = None
        response['data']['cover_image_favicon'] = None
        if response['data']['provider_id'] !=None:
            cursor.execute("SELECT name from provider where id=%s" % str(response['data']['provider_id']))
            provider = cursor.fetchone()
            if provider is not None:
                response['data']['provider_name'] = provider[0]
            response['data']['provider_id']='pp-provider-'+str(response['data']['provider_id'])
        if response['data']['icon']!=None:
            response['data']['icon']=str(response['data']['icon'])+'?T='+str(int(round(time.time()*1000)))
            response['data']['icon_favicon'] = str(response['data']['icon']).replace('cloudfront.net/','cloudfront.net/small/')
        if response['data']['icon_small']!=None:
            response['data']['icon_small']=str(response['data']['icon_small'])+'?T='+str(int(round(time.time()*1000)))
            response['data']['icon_small_favicon'] = str(response['data']['icon_small']).replace('cloudfront.net/','cloudfront.net/small/')
        if response['data']['cover_image']!=None:
            response['data']['cover_image']=str(response['data']['cover_image'])+'?T='+str(int(round(time.time()*1000)))
            response['data']['icover_image_favicon'] = str(response['data']['cover_image']).replace('cloudfront.net/','cloudfront.net/small/')

    db.commit()
    db.close()
    return response

def keep(channel_id, incoming, incoming_files):
    activity_type = 'edit'
    if channel_id == None:
        channel_id = '0'
        activity_type = 'new'

    response = {}
    change_icon_flag=0
    change_icon_small_flag=0
    change_cover_image_flag=0
    a_channel = Channel(channel_id=channel_id)
    try:
        icon_path=a_channel.icon
        change_icon_flag=1

    except AttributeError:
        icon_path=None

    try:
        icon_small_path=a_channel.icon_small
        change_icon_small_flag=1

    except AttributeError:
        icon_small_path=None

    try:
        cover_image_path=a_channel.cover_image
        change_cover_image_flag=1

    except AttributeError:
        change_cover_image_flag=None

    a_channel.consume(incoming)


    if a_channel._exists is True:
        a_channel.date_modified= int(round(time.time() * 1000))

        if 'status' in incoming :
            if request.form['status']=='complete' or request.form['status']=='approved':
                db = wrappers.Postgres.init()
                cursor = wrappers.Postgres.get_cursor(db)
                cursor.execute("SELECT * from tags_map where item_id=%(item_id)s and item_type=%(item_type)s",{'item_id':int(a_channel.id.split('-')[-1]),'item_type':'channel'})
                tags=cursor.fetchall()
                db.commit()
                db.close()

                if a_channel.icon is None or a_channel.icon_small is None or tags is None or a_channel.name is None or a_channel.display_name is None :
                    response['status']='failed'
                    response['http_status']=412

                    if a_channel.icon is None:
                        response['message']='Please add a poster'
                        return response
                    elif a_channel.icon_small is None:
                        response['message']='Please add an icon'
                        return response
                    elif tags is None:
                        response['message']='Please add tags'
                        return response
                    elif a_channel.name is None:
                        response['message']='Please fill name field'
                        return response
                    elif a_channel.display_name is None:
                        response['message']='Please fill display name field'
                        return response

    else:
        a_channel.date_created= int(round(time.time() * 1000))
        if 'status' in incoming :
            if request.form['status']=='complete' or request.form['status']=='approved':
                 if 'icon_name' not in incoming or 'icon_small_name' not in incoming or 'tags' not in incoming or a_channel.name is None or a_channel.display_name is None :
                    response['status']='failed'
                    response['http_status']=412

                    if 'icon_name' not in incoming:
                        response['message']='Please add a poster'
                        return response
                    elif 'icon_small_name' not in incoming:
                        response['message']='Please add an icon'
                        return response
                    elif 'tags' not in incoming:
                        response['message']='Please add tags'
                        return response
                    elif a_channel.name is None:
                        response['message']='Please fill name field'
                        return response
                    elif a_channel.display_name is None:
                        response['message']='Please fill display name field'
                        return response

    if a_channel._exists ==True:
        if a_channel.provider_id!=None:
            try:
                a_channel.provider_id=int(str(a_channel.provider_id).split('-')[-1])
            except:
                response['http_status']=419
                response['status']='failed'
                response['message']='not a valid provider id'
                return response
    else:
        if 'provider_id' in incoming:
            if incoming['provider_id']!='null':
                try:
                    a_channel.provider_id=int(incoming['provider_id'].split('-')[-1])
                except:
                    response['http_status']=419
                    response['status']='failed'
                    response['message']='not a valid provider id'
                    return response
            else:
                a_channel.provider_id=None



    saved_id = a_channel.save()
    if saved_id is None:
        response['status'] = 'failed'
        response['message'] = 'There was a problem with saving this record. Please check your request!'
        response['http_status'] = 412
        if a_channel._unique_error is True:
            response['message'] = 'Error saving record. Please check if duplicate channel!'
        print "$$$"+str(a_channel._token_id)
        if a_channel._token_id!=-1:
           response['token_id'] = a_channel._token_id
        return response
    else:
        response['status'] = 'success'
        response['message'] = 'Record saved!'
        response['storage_id'] = saved_id

        if 'tags' in incoming:
            chann_id=saved_id.split('-')[-1]
            #******remove later
            tagsarr=json.loads(incoming['tags'])
            #print type(tagsarr)
            tags_map.keep(chann_id,'channel',tagsarr)   #mapping after successful storage

        #a_same_channel=Channel(channel_id=saved_id)

        if 'icon_name' in request.form:         #if logo_name is in request
            if 'null' not in request.form['icon_name']:
                if change_icon_flag:
                    fileops.manage_delete(icon_path)
                    fileops.manage_delete(icon_path,config.IMAGE_RESOURCE_BUCKET)
                file_name=saved_id+'-icon'
                path=fileops.save_file(request.files['icon'],file_name,'/tmp')
                fileops.manage_upload(path,request.files['icon'].content_type,file_name)
                temp_link= fileops.manage_upload(path,request.files['icon'].content_type,file_name,config.IMAGE_RESOURCE_BUCKET)
                a_channel.icon=config.CDN_PREFIX+os.path.basename(temp_link)
            else:
                a_channel.icon=None
                fileops.manage_delete(icon_path)
                fileops.manage_delete(icon_path,config.IMAGE_RESOURCE_BUCKET)

        if 'icon_small_name' in request.form:         #if logo_name is in request
            if 'null' not in request.form['icon_small_name']:
                if change_icon_small_flag:
                    fileops.manage_delete(icon_small_path)
                    fileops.manage_delete(icon_small_path,config.IMAGE_RESOURCE_BUCKET)
                file_name=saved_id+'-icon-small'
                path=fileops.save_file(request.files['icon_small'],file_name,'/tmp')
                fileops.manage_upload(path,request.files['icon_small'].content_type,file_name)
                temp_link = fileops.manage_upload(path,request.files['icon_small'].content_type,file_name,config.IMAGE_RESOURCE_BUCKET)
                a_channel.icon_small=config.CDN_PREFIX+os.path.basename(temp_link)
            else:
                a_channel.icon_small=None
                fileops.manage_delete(icon_small_path)
                fileops.manage_delete(icon_small_path,config.IMAGE_RESOURCE_BUCKET)

        if 'cover_image_name' in request.form:         #if logo_name is in request
            if 'null' not in request.form['cover_image_name']:
                if change_cover_image_flag:
                    fileops.manage_delete(cover_image_path)
                    fileops.manage_delete(cover_image_path,config.IMAGE_RESOURCE_BUCKET)
                file_name=saved_id+'-cover-image'
                path=fileops.save_file(request.files['cover_image'],file_name,'/tmp')
                fileops.manage_upload(path,request.files['cover_image'].content_type,file_name)
                temp_link = fileops.manage_upload(path,request.files['cover_image'].content_type,file_name,config.IMAGE_RESOURCE_BUCKET)
                a_channel.cover_image=config.CDN_PREFIX+os.path.basename(temp_link)
            else:
                a_channel.cover_image=None
                fileops.manage_delete(cover_image_path)
                fileops.manage_delete(cover_image_path,config.IMAGE_RESOURCE_BUCKET)

        saved_id=a_channel.save()

    if saved_id is None:
        response['status'] = 'failed'
        response['message'] = 'There was a problem with saving this record. Please check your request!'
        response['http_status'] = 412
        if a_channel._unique_error is True:
            response['message'] = 'Error saving record. Please check if duplicate channel!'
        if a_channel._token_id!=-1:
           response['token_id'] = a_channel._token_id
    else:
        log_activity(endpoint='channel/keep/', activity_type=activity_type,
                    item_id=saved_id, item_type='channel',
                    request=request)
        beacon_redis_del(item_id=saved_id, item_type='channel')
        response['status'] = 'success'
        response['message'] = 'Record saved!'
        response['storage_id'] = saved_id

    return response

def rain(incoming):
    response = {}
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    if 'status' in incoming:
        all_channel_ids = enumerate_table('channels', {'status': incoming['status']})
    else:
        all_channel_ids = enumerate_table('channels')

    if len(all_channel_ids) == 0:
        response['status'] = 'failed'
        response['message'] = 'No relevant data found!'
    else:
        response['status'] = 'success'
        response['data'] = []
    index=0
    for a_channel_id in all_channel_ids:
        a_channel = Channel(channel_id=a_channel_id)

        tags_list=[]
        cursor.execute("SELECT name from tags INNER JOIN tags_map ON tags_map.tag_id=tags.id where item_type=%(item_type)s and item_id=%(channel_id)s",{'item_type':'channel','channel_id':int(a_channel.id.split('-')[-1])})
        tag_ids=cursor.fetchall()

        for tag_id in tag_ids:
            tags_list.append(tag_id[0])
        response['data'].append(deepcopy(a_channel.export()))
        if response['data'][index]['provider_id'] !=None:
            response['data'][index]['provider_id']='pp-provider-'+str(response['data'][index]['provider_id'])

        if response['data'][index]['icon']!=None:
            response['data'][index]['icon']=str(response['data'][index]['icon'])+'?T='+str(int(round(time.time()*1000)))
        if response['data'][index]['icon_small']!=None:
            response['data'][index]['icon_small']=str(response['data'][index]['icon_small'])+'?T='+str(int(round(time.time()*1000)))

        response['data'][index]['tags']=tags_list
        index=index+1
    db.commit()
    db.close()
    return response

def datatable_rain(incoming):

    table='channels'
    query_dict=generic_adapter(incoming,table)
    query=query_dict['complete_query']
    search_query=query_dict['search_query']
    search_value=query_dict['search_value']

    print query

    response = {}
    response['draw']=incoming['draw']
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)

    a_query='SELECT column_name FROM information_schema.columns WHERE table_name =%(table_name)s'
    cursor.execute(a_query,{'table_name':table})
    res=cursor.fetchall()

    id_position=0

    if res:
        for each in res:
            if each[0] == 'id':
                break
            else:
                id_position+=1


    cursor.execute(query,{'search':search_value})
    data=cursor.fetchall()
    response['data']=[]
    for row in data:
        a_channel= Channel(channel_id=str(row[id_position]))
        response['data'].append(deepcopy(a_channel.export()))

    cursor.execute(search_query,{'search':search_value})
    data=cursor.fetchall()

    f_count=0
    for row in data:
        f_count+=1
    response['recordsFiltered']=f_count


    cursor.execute("SELECT count(*) from "+table)
    totalcnt=cursor.fetchone()
    response['recordsTotal']=totalcnt[0]
    response['http_status']=200

    db.commit()
    db.close()
    return response


def batch_fetch(field_csv, id_csv):
    response={}
    response['http_status']=200
    response['status']='success'
    db=wrappers.Postgres.init()
    data = {}
    cursor=wrappers.Postgres.get_cursor(db, 'dict')
    id_list = id_csv.split(',')
    field_list = field_csv.split(',')

    if len(id_list) > 0 and len(field_list) > 0:
        field_list = set(field_list)
        field_list.add('id')
        id_list = set(id_list)
        query = "SELECT %s from channels where id in (%s)" % (", ".join(map(str, field_list)),  ", ".join(map(str, id_list)))
        cursor.execute(query)
        results = cursor.fetchall()
        for row in results:
            row_obj = dict(row)
            key = "pp-channel-" + str(row_obj['id'])
            if 'icon' in row_obj:
                row_obj['icon_favicon'] = None
                if row_obj['icon'] != None:
                    row_obj['icon_favicon'] = str(row_obj['icon']).replace('cloudfront.net/','cloudfront.net/small/')
            if 'icon_small' in row_obj:
                row_obj['icon_small_favicon'] = None
                if row_obj['icon_small'] != None:
                    row_obj['icon_small_favicon'] = str(row_obj['icon_small']).replace('cloudfront.net/','cloudfront.net/small/')
            data[key] = row_obj

    db.commit()
    db.close()
    response['data'] = data
    return response

def check_slug(slug_url):
    trim_slug = str(slug_url).strip()
    if trim_slug == "None" or trim_slug == "":
        response = {"http_status": 418,
                    "status":"failed",
                    "message":"please provide a value"}
        return response
    response={}
    response['http_status']=200
    response['status']='success'
    response['available'] = True
    db=wrappers.Postgres.init()
    cursor=wrappers.Postgres.get_cursor(db, 'dict')
    slug_url = slugify(slug_url)
    response['slug_url'] = slug_url
    query = "SELECT id from channels where slug_url=%(slug_url)s"
    cursor.execute(query, {'slug_url': slug_url})
    result = cursor.fetchone()
    if result is not None:
        response['available'] = False
    return response
