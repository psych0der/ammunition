import wrappers
from app.commons.Toolkit import enumerate_table
from models.Collection_feed import Collection_feed
from models.Collection_feed_temp import Collection_feed_temp
from models.Content import Content
from models.Channel import Channel
from models.Tag import Tag
import os
import upload_s3 as fileops
from copy import deepcopy
from flask import request
import json
from slugify import slugify

def keep(collection_feeds,test=False):
    #if feed_id == None:
    #    feed_id = '0'

    response = {}
    if test==False:
        a_collection=Collection_feed()
    else:
        a_collection=Collection_feed_temp()

    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    if test==False:
        cursor.execute("SELECT tag_map_id,tag_id from tags_map_position ")
        prev_tag_res=cursor.fetchall()
        cursor.execute("DELETE FROM collection_feed")
        cursor.execute("DELETE FROM tags_map_position")
        db.commit()
    else:
        cursor.execute("SELECT tag_map_id,tag_id from tags_map_position_temp ")
        prev_tag_res=cursor.fetchall()
        cursor.execute("DELETE FROM collection_feed_temp")
        cursor.execute("DELETE FROM tags_map_position_temp")
        db.commit()



    saved_id=None
    response['status'] = 'success'
    response['http_status'] = 200
    response['message'] = 'Record saved!'
    response['storage_id'] = saved_id
    row=1
    col=1
    for collection_row in collection_feeds:		#arr_obj is an array of objects
        #print type(collection_row)
        a_tag=Tag(tag_id=collection_row['collection_id'])
        #print '####1'+str(a_tag.id)
        if a_tag._exists is not True:
            response['status']='failed'
            response['http_status'] = 500
            response['message'] = a_tag.id+' tag_id does not exist'
            return response
        if a_tag.status!='complete' and a_tag.status!='approved':
            response['status']='failed'
            response['http_status'] = 500
            response['message'] = a_tag.id+' tag_id is not complete'
            return response
        if a_tag.collection!='yes':
            response['status']='failed'
            response['http_status'] = 500
            response['message'] =a_tag.id+' tag_id is not a collection'
            return response

        db = wrappers.Postgres.init()
        cursor = wrappers.Postgres.get_cursor(db)

        tag_row=1
        tag_col=1

        for obj_row in collection_row['data']:

            if obj_row['type']=='channel':
                a_channel=Channel(channel_id=obj_row['id'])
                if a_channel._exists is not True:
                    response['status']='failed'
                    response['http_status'] = 500
                    response['message'] = obj_row['id']+' channel does not exist'
                    return response
                if a_channel.status!='complete' and a_channel.status!='approved':
                    response['status']='failed'
                    response['http_status'] = 500
                    response['message'] = obj_row['id']+' channel is not complete'
                    return response

                #update_query="UPDATE tags_map SET row=%(row)s,col=%(col)s where tag_id=%(tag_id)s and item_type=%(item_type)s and item_id=%(item_id)s"
                #cursor.execute(update_query,{'row':tag_row,'col':tag_col,'tag_id':int(collection_row['collection_id'].split('-')[-1]),'item_type':'channel','item_id':int(a_channel.id.split('-')[-1])})
                #db.commit()

                # a_query="SELECT id from tags_map where tag_id=%(tag_id)s and item_type=%(item_type)s and item_id=%(item_id)s"
                # cursor.execute(a_query,{'tag_id':int(collection_row['collection_id'].split('-')[-1]),'item_type':'channel','item_id':int(a_channel.id.split('-')[-1])})
                # entry=cursor.fetchone()
                # if test==False:
                #     try:
                #         if cursor!=None:
                #             cursor.execute("INSERT INTO tags_map_position(tag_id,tag_map_id,row,col) VALUES (%(tag_id)s,%(tag_map_id)s,%(row)s,%(col)s)",{'tag_id':int(a_tag.id.split('-')[-1]),'tag_map_id':entry[0],'row':tag_row,'col':tag_col})
                #         db.commit()
                #     except Exception, e:
                #         print e
                #         db.commit()
                #         continue
                # else:
                #     if cursor!=None:
                #         cursor.execute("INSERT INTO tags_map_position_temp(tag_id,tag_map_id,row,col) VALUES (%(tag_id)s,%(tag_map_id)s,%(row)s,%(col)s)",{'tag_id':int(a_tag.id.split('-')[-1]),'tag_map_id':entry[0],'row':tag_row,'col':tag_col})
                #     db.commit()
            else:
                a_content=Content(content_id=obj_row['id'])
                if a_content._exists is not True:
                    response['status']='failed'
                    response['http_status'] = 500
                    response['message'] = obj_row['id']+' content does not exist'
                    return response
                if a_content.status!='complete' and a_content.status!='approved':
                    response['status']='failed'
                    response['http_status'] = 500
                    response['message'] = obj_row['id']+' content is not complete'
                    return response

                if test==False:
                    cursor.execute('UPDATE content SET live=%(live)s where id=%(id)s',{'live':'yes','id':int(a_content.id.split('-')[-1])})
                    db.commit()
                #update_query="UPDATE tags_map SET row=%(row)s,col=%(col)s where tag_id=%(tag_id)s and item_type=%(item_type)s and item_id=%(item_id)s"
                #cursor.execute(update_query,{'row':tag_row,'col':tag_col,'tag_id':int(collection_row['collection_id'].split('-')[-1]),'item_type':'content','item_id':int(a_content.id.split('-')[-1])})
                #db.commit()

                # a_query="SELECT id from tags_map where tag_id=%(tag_id)s and item_type=%(item_type)s and item_id=%(item_id)s"
                #
                # cursor.execute(a_query,{'tag_id':int(collection_row['collection_id'].split('-')[-1]),'item_type':'content','item_id':int(a_content.id.split('-')[-1])})
                # entry=cursor.fetchone()
                # if test==False:
                #     if entry!=None:
                #         cursor.execute("INSERT INTO tags_map_position(tag_id,tag_map_id,row,col) VALUES (%(tag_id)s,%(tag_map_id)s,%(row)s,%(col)s)",{'tag_id':int(a_tag.id.split('-')[-1]),'tag_map_id':entry[0],'row':tag_row,'col':tag_col})
                #     db.commit()
                # else:
                #     if entry!=None:
                #         cursor.execute("INSERT INTO tags_map_position_temp(tag_id,tag_map_id,row,col) VALUES (%(tag_id)s,%(tag_map_id)s,%(row)s,%(col)s)",{'tag_id':int(a_tag.id.split('-')[-1]),'tag_map_id':entry[0],'row':tag_row,'col':tag_col})
                #     db.commit()
            tag_row=tag_row+1



        prev_coll_set=set()
        if prev_tag_res!=None:
            for each in prev_tag_res:
                if each[1]==int(a_tag.id.split('-')[-1]):
                    prev_coll_set.add(each[0])

        # if test == False:
        #     cursor.execute("SELECT tag_map_id from tags_map_position where tag_id=%(tag_id)s",{'tag_id':int(a_tag.id.split('-')[-1])})
        #     res=cursor.fetchall()
        # else:
        #     cursor.execute("SELECT tag_map_id from tags_map_position_temp where tag_id=%(tag_id)s",{'tag_id':int(a_tag.id.split('-')[-1])})
        #     res=cursor.fetchall()

        curr_coll_set=set()
        # if res!=None:
        #     for each in res:
        #         curr_coll_set.add(each[0])

        #print "****"+str(len(curr_coll_set - prev_coll_set))

        a_collection.row=row
        a_collection.col=col
        a_collection.diff=int(len(curr_coll_set - prev_coll_set))
        a_collection.tag_id=int(a_tag.id.split('-')[-1])
        saved_id=a_collection.save()
        if saved_id is None:
            response['status'] = 'failed'
            response['message'] = 'There was a problem with saving this record. Please check your request!'
            response['http_status'] = 412
            if a_collection._token_id!=-1:
                response['token_id']=a_collection._token_id
            return response
        else:
            response['status'] = 'success'
            response['http_status'] = 200
            response['message'] = 'Record saved!'
        row=row+1

    db.close()
    return response


def batch_fetch(field_csv, id_csv):
    response={}
    response['http_status']=200
    response['status']='success'
    db=wrappers.Postgres.init()
    data = {}
    cursor=wrappers.Postgres.get_cursor(db, 'dict')
    id_list = id_csv.split(',')
    field_list = field_csv.split(',')

    if len(id_list) > 0 and len(field_list) > 0:
        field_list = set(field_list)
        field_list.add('id')
        id_list = set(id_list)
        query = "SELECT %s from tags where id in (%s) and collection='yes'" % (", ".join(map(str, field_list)),  ", ".join(map(str, id_list)))
        cursor.execute(query)
        results = cursor.fetchall()
        for row in results:
            row_obj = dict(row)
            key = "pp-tag-" + str(row_obj['id'])
            if 'poster' in row_obj:
                row_obj['poster_favicon'] = None
                if row_obj['poster'] != None:
                    row_obj['poster_favicon'] = str(row_obj['poster']).replace('cloudfront.net/','cloudfront.net/small/')
            if 'poster_small' in row_obj:
                row_obj['poster_small_favicon'] = None
                if row_obj['poster_small'] != None:
                    row_obj['poster_small_favicon'] = str(row_obj['poster_small']).replace('cloudfront.net/','cloudfront.net/small/')
            data[key] = row_obj

    response['data'] = data
    return response


def check_slug(slug_url):
    trim_slug = str(slug_url).strip()
    if trim_slug == "None" or trim_slug == "":
        response = {"http_status": 418,
                    "status":"failed",
                    "message":"please provide a value"}
        return response
    response={}
    response['http_status']=200
    response['status']='success'
    response['available'] = True
    db=wrappers.Postgres.init()
    cursor=wrappers.Postgres.get_cursor(db, 'dict')
    slug_url = slugify(slug_url)
    response['slug_url'] = slug_url
    query = "SELECT id from tags where slug_url=%(slug_url)s"
    cursor.execute(query, {'slug_url': slug_url})
    result = cursor.fetchone()
    if result is not None:
        response['available'] = False
    return response
