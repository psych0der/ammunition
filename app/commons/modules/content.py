import wrappers
from app.commons.Toolkit import enumerate_table
import torrent
from models.Content import Content
from models.Raw_content_registry import Raw_content_registry
from models.Provider import Provider
import tags_map
import upload_s3 as fileops
from copy import deepcopy
from flask import request
import os
import config
from app.commons.resources import redis_cli, putio_client
from app.commons.modules.userlogs import log_activity
from app.commons.modules.beacon import beacon_redis_del
from werkzeug import secure_filename
from jobs import celery_tasks as tasks
import brightcove
import json
import requests
import hashlib
import time
import datetime
from datatable_adapter import generic_adapter
from slugify import slugify


def fetch(content_id, incoming):
    requested_content = Content(content_id=content_id)
    response = {'exists': False, 'status': 'failure'}

    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db, 'dict')
    if requested_content._exists == True:
        response['exists'] = True
        response['status'] = 'success'
        response['data'] = requested_content.export()
        response['data']['poster_favicon'] = None
        response['data']['poster_small_favicon'] = None
        response['data']['cover_image_favicon'] = None
        if response['data']['poster']!=None:
            response['data']['poster']=str(response['data']['poster'])+'?T='+str(int(round(time.time()*1000)))
            response['data']['poster_favicon'] = str(response['data']['poster']).replace('cloudfront.net/','cloudfront.net/small/')

        if response['data']['poster_small']!=None:
            response['data']['poster_small']=str(response['data']['poster_small'])+'?T='+str(int(round(time.time()*1000)))
            response['data']['poster_small_favicon'] = str(response['data']['poster_small']).replace('cloudfront.net/','cloudfront.net/small/')

        if response['data']['cover_image']!=None:
            response['data']['cover_image']=str(response['data']['cover_image'])+'?T='+str(int(round(time.time()*1000)))
            response['data']['cover_image_favicon'] = str(response['data']['cover_image']).replace('cloudfront.net/','cloudfront.net/small/')

        if response['data']['local_link'] !=None:
            headers={}
            if requested_content.type=='audio':
                headers={'response-content-type':'audio/mpeg3'}
            else:
                headers={'response-content-type':'video/mp4'}
            link_fragments = response['data']['local_link'].split('/')
            response['data']['local_link']=fileops.sign(bucket=config.RAW_CONTENT_BUCKET,path=link_fragments[-1],headers=headers)

        content_id = int(requested_content.id.split('-')[-1])

        cursor.execute("SELECT array_agg(tags.name) from tags INNER JOIN tags_map ON tags_map.tag_id=tags.id where item_type=%(item_type)s and item_id=%(content_id)s",{'item_type':'content','content_id':content_id})
        result=cursor.fetchone()
        tags_list = []

        if result is not None:
            tags_list = result[0]
        response['data']['tags'] = tags_list

        if response['data']['provider_id'] != None:
            cursor.execute("SELECT name from provider where id=%s" % str(response['data']['provider_id']))
            provider = cursor.fetchone()
            if provider is not None:
                response['data']['provider_name'] = provider[0]
            response['data']['provider_id']='pp-provider-'+str(response['data']['provider_id'])

        cursor.execute("select r.name, tm.raw_content_id, tm.media_processed, tm.local_link, r.media_type, tm.remote_id from trailers_map as tm left join raw_content_registry as r on r.id=tm.raw_content_id where content_id=%(content_id)s", {'content_id':content_id})
        result = cursor.fetchall()
        trailer_list = []
        if result is not None and len(result) > 0:
            for row in result:
                row_obj = dict(row)
                if row_obj['media_type']=='audio':
                    headers={'response-content-type':'audio/mpeg3'}
                else:
                    headers={'response-content-type':'video/mp4'}
                if row_obj['local_link'] is not None:
                    link_fragments = row_obj['local_link'].split('/')
                    row_obj['local_link'] = fileops.sign(bucket=config.RAW_CONTENT_BUCKET,path=link_fragments[-1],headers=headers)
                trailer_list.append(row_obj)

        response['data']['trailers'] = trailer_list

    db.close()
    return response

def keep(content_id, incoming, incoming_files):
    activity_type = 'edit'
    if content_id == None:
        content_id = '0'
        activity_type = 'new'

    response = {}
    change_poster_flag = 0
    change_poster_small_flag=0
    change_cover_image_flag=0
    ingest_trailer = False
    trailers = None
    a_content = Content(content_id=content_id)
    try:
        poster_path = a_content.poster
        change_poster_flag = 1
    except AttributeError:
        poster_path=None

    try:
        raw_id_old=a_content.raw_content_id
    except AttributeError:
        raw_id_old=None

    try:
        poster_small_path=a_content.poster_small
        change_poster_small_flag=1
    except AttributeError:
        poster_small_path=None

    try:
        cover_image_path=a_content.cover_image
        change_cover_image_flag=1
    except AttributeError:
        cover_image_path=None

    a_content.consume(incoming)
    if a_content._exists is True:
        a_content.date_modified=int(round(time.time() * 1000))
        if 'status' in incoming :
            if request.form['status']=='complete' or request.form['status']=='approved':
                db = wrappers.Postgres.init()
                cursor = wrappers.Postgres.get_cursor(db)
                cursor.execute("SELECT * from tags_map where item_id=%(item_id)s and item_type=%(item_type)s",{'item_id':a_content.id.split('-')[-1],'item_type':'content'})
                tags=cursor.fetchall()
                db.commit()
                db.close()
                if a_content.poster is None or a_content.poster_small is None or a_content.name is None or a_content.duration is None or a_content.type is None or tags is None:
                    response['status']='failed'
                    response['http_status']=412

                    if a_content.poster is None:
                        response['message']='Please select a poster.'
                        return response
                    elif a_content.poster_small is None:
                        response['message']='Please select an icon'
                        return response
                    elif a_content.name is None:
                        response['message']='Please fill the name field'
                        return response
                    elif a_content.duration is None:
                        response['message']='Please fill the duration'
                        return response
                    elif a_content.type is None:
                        response['message']='Please fill the type'
                        return response
                    elif tags is None:
                        response['message']='Please fill tags'
                        return response

    else:
        a_content.date_created=int(round(time.time() * 1000))
        if 'status' in incoming :
            if request.form['status']=='complete' or request.form['status']=='approved':

                if 'poster_name' not in incoming  or 'poster_small_name' not in incoming  or a_content.name is None or a_content.duration is None or a_content.type is None or 'tags' not in incoming:
                    response['status']='failed'
                    response['http_status']=412

                    if 'poster_name' not in incoming:
                        response['message']='Please select a poster.'
                        return response
                    elif 'poster_small_name' not in incoming:
                        response['message']='Please select an icon'
                        return response
                    elif a_content.name is None:
                        response['message']='Please fill the name field'
                        return response
                    elif a_content.duration is None:
                        response['message']='Please fill the duration'
                        return response
                    elif a_content.type is None:
                        response['message']='Please fill the type'
                        return response
                    elif 'tags' not in incoming:
                        response['message']='Please fill tags'
                        return response


    #to rename a file on local machine
    if 'raw_content_id' in incoming:

        if incoming['raw_content_id'] != "null":
            new_raw_content = incoming['raw_content_id']
            print 'new raw content id : '+incoming['raw_content_id']
            a_raw_content=Raw_content_registry(raw_content_id=incoming['raw_content_id'])
            if a_raw_content._exists is not True or a_raw_content.used == 'yes':
                response['status'] = 'failed'
                response['message'] = 'raw_content does not exist'
                response['http_status'] = 412
                return response
        else:
            new_raw_content = None
    else:
        # to skip ingestion process . ==> Bad hack. replace ASAP

        new_raw_content =  raw_id_old

    print 'new content id : '+str(new_raw_content)
    print 'old content id : '+str(raw_id_old)

    if a_content._exists ==True:
        if a_content.provider_id!=None:
            try:
                a_content.provider_id=int(str(a_content.provider_id).split('-')[-1])
            except:
                response['http_status']=419
                response['status']='failed'
                response['message']='not a valid provider id'
                return response
    else:
        if 'provider_id' in incoming:
            if incoming['provider_id']!='null':
                try:
                    a_content.provider_id=int(incoming['provider_id'].split('-')[-1])
                except:
                    response['http_status']=419
                    response['status']='failed'
                    response['message']='not a valid provider id'
                    return response
            else:
                a_content.provider_id=None

    #check for premium ,expiry and cost
    # if a_content.premium=='no' and (a_content.expiry != None or a_content.cost != None ):
    #     response['http_status']=418
    #     response['status']='failed'
    #     response['message']='check premium to yes'
    #     return response

    if a_content.premium=='yes' and (a_content.expiry == None or a_content.cost == None ):
        print "a_content.expiry "+str(a_content.expiry)
        print "a_content.cost "+str(a_content.expiry)
        response['http_status']=418
        response['status']='failed'
        response['message']='check expiry and cost field'
        return response



    saved_id = a_content.save()
    #a_content._exists=True
    if saved_id is None:
        response['status'] = 'failed'
        response['message'] = 'There was a problem with saving this record. Please check your request!'
        response['http_status'] = 412
        if a_content._token_id!=-1:
            response['token_id']=a_content._token_id
        if a_content._unique_error is True:
            response['message'] = 'Error saving record. Please check if duplicate content!'
        return response

    else:
        cont_id = saved_id.split('-')[-1]

        if 'trailers' in incoming:
            ingest_trailer = True
            trailers = incoming['trailers']
            if trailers is not None and trailers != 'null':
                trailer_object = json.loads(trailers)
                trailer_list = trailer_object.values()
            else:
                trailer_list = []
            trailers = trailer_cleanup(trailer_list, cont_id)

        #a_same_content=Content(content_id=saved_id)
        if 'tags' in incoming:
            tagsarr=json.loads(incoming['tags'])
            tags_map.keep(cont_id, 'content', tagsarr)

        if 'poster_name' in request.form:  # if logo_name is in request
            if 'null' not in request.form['poster_name']:
                if change_poster_flag:
                    fileops.manage_delete(poster_path)
                    fileops.manage_delete(poster_path,config.IMAGE_RESOURCE_BUCKET)
                # get part before .png/.avi
                file_name = saved_id+'-poster'
                path=fileops.save_file(request.files['poster'],file_name,'/tmp')
                fileops.manage_upload(path,request.files['poster'].content_type, file_name)
                temp_link= fileops.manage_upload(path,request.files['poster'].content_type, file_name,config.IMAGE_RESOURCE_BUCKET)
                a_content.poster=config.CDN_PREFIX+os.path.basename(temp_link)
            else:
                a_content.poster = None
                fileops.manage_delete(poster_path)
                fileops.manage_delete(poster_path,config.IMAGE_RESOURCE_BUCKET)


        if 'poster_small_name' in request.form:         #if logo_name is in request
            if 'null' not in request.form['poster_small_name']:
                if change_poster_small_flag:
                    fileops.manage_delete(poster_small_path)
                    fileops.manage_delete(poster_small_path,config.IMAGE_RESOURCE_BUCKET)
                file_name=saved_id+'-poster-small'   #get part before .png/.avi
                path=fileops.save_file(request.files['poster_small'],file_name,'/tmp')
                fileops.manage_upload(path,request.files['poster_small'].content_type,file_name)
                temp_link= fileops.manage_upload(path,request.files['poster_small'].content_type,file_name,config.IMAGE_RESOURCE_BUCKET)
                a_content.poster_small = config.CDN_PREFIX+os.path.basename(temp_link)
            else:
                a_content.poster_small=None
                fileops.manage_delete(poster_small_path)
                fileops.manage_delete(poster_small_path,config.IMAGE_RESOURCE_BUCKET)


        if 'cover_image_name' in request.form:         #if logo_name is in request
            if 'null' not in request.form['cover_image_name']:
                if change_cover_image_flag:
                    fileops.manage_delete(cover_image_path)
                    fileops.manage_delete(cover_image_path,config.IMAGE_RESOURCE_BUCKET)
                file_name=saved_id+'-cover-image'   #get part before .png/.avi
                path=fileops.save_file(request.files['cover_image'],file_name,'/tmp')
                fileops.manage_upload(path,request.files['cover_image'].content_type,file_name)
                temp_link= fileops.manage_upload(path,request.files['cover_image'].content_type,file_name,config.IMAGE_RESOURCE_BUCKET)
                a_content.cover_image = config.CDN_PREFIX+os.path.basename(temp_link)
            else:
                a_content.cover_image=None
                fileops.manage_delete(cover_image_path)
                fileops.manage_delete(cover_image_path,config.IMAGE_RESOURCE_BUCKET)



    saved_id = a_content.save()
    if raw_id_old != new_raw_content:
        a_content.digital_master_uploaded = 'no'
        a_content.media_processed = 'no'
        a_content.error = None
        a_content.last_failed_upload_attempt = None
        a_content.local_link = None
        a_content.save()
        tasks.content_ingestor.apply_async((str(saved_id), str(raw_id_old), str(new_raw_content)),queue=config.CELERY_QUEUE)

    if ingest_trailer == True:
        tasks.trailer_ingestor.apply_async((str(saved_id), trailers),queue=config.CELERY_QUEUE)

    if saved_id is None:
        response['status'] = 'failed'
        response['message'] = 'There was a problem with saving this record. Please check your request!'
        if a_content._token_id != -1:
            response['token_id']=a_content._token_id
        if a_content._unique_error is True:
            response['message'] = 'Error saving record. Please check if duplicate content!'
        response['http_status'] = 412
    else:
        log_activity(endpoint='content/keep/', activity_type=activity_type,
                    item_id=saved_id, item_type='content',
                    request=request)
        beacon_redis_del(item_type='content', item_id=saved_id)
        response['status'] = 'success'
        response['message'] = 'Record saved!'
        response['storage_id'] = saved_id


    return response


def rain(incoming):
    response = {}

    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    __TRACKED_FILEDS = ["name", "poster", "language", "box_only", \
                        "duration", "type", "status", "genre", "specifics", \
                        "video_url", "featured","remote_id","local_link","digital_master_uploaded","media_processed",
                        'raw_content_id',"poster_small","provider_id","live","description","premium","expiry","cost","seo",
                        "date_created","date_modified","allow_download", "video_content_type", "slug_url", "cover_image", "short_description", "id"]

    if 'status' in incoming:
        all_content_ids = enumerate_table(
            'content', {'status': incoming['status']})
    else:
        all_content_ids = enumerate_table('content')

    if len(all_content_ids) == 0:
        response['status'] = 'failed'
        response['message'] = 'No relevant data found!'
    else:
        response['status'] = 'success'
        response['data'] = []

    fields = ', '.join(__TRACKED_FILEDS)
    # a_query = "SELECT "+fields + \
    #    ", (SELECT channel_id from channel_content_mapping where id = ( SELECT max(id) from  channel_content_mapping where content_id = c.id )) FROM content c"
    a_query = "SELECT "+fields+" from content"
    cursor.execute(a_query)
    result = cursor.fetchall()

    index=0

    for a_content_id in all_content_ids:
        a_content = Content(content_id=a_content_id, precalresult=result)
        tags_list=[]
        cursor.execute("SELECT name from tags INNER JOIN tags_map ON tags_map.tag_id=tags.id where item_type=%(item_type)s and item_id=%(content_id)s",{'item_type':'content','content_id':int(a_content.id.split('-')[-1])})
        tag_ids=cursor.fetchall()

        for tag_id in tag_ids:
            #cursor.execute("SELECT name from tags where id=%(tag_id)s",{"tag_id":tag_id})
            #tags_list.append(cursor.fetchone()[0])
            tags_list.append(tag_id[0])

        response['data'].append(deepcopy(a_content.export()))
        if response['data'][index]['poster']!=None:
            response['data'][index]['poster']=str(response['data'][index]['poster'])+'?T='+str(int(round(time.time()*1000)))

        if response['data'][index]['poster_small']!=None:
            response['data'][index]['poster_small']=str(response['data'][index]['poster_small'])+'?T='+str(int(round(time.time()*1000)))

        if response['data'][index]['local_link'] !=None:
            headers={}
            if a_content.type=='audio':
                headers={'response-content-type':'audio/mpeg3'}
            else:
                headers={'response-content-type':'video/mp4'}
            link_fragments = response['data'][index]['local_link'].split('/')
            response['data'][index]['local_link']=fileops.sign(bucket=config.BRIGHTCOVE_SRC_BUCKET,path=link_fragments[-1],headers=headers)
        response['data'][index]['tags']=tags_list

        index=index+1
    db.commit()
    db.close()
    return response

def datatable_rain(incoming):

    table='content'
    query_dict=generic_adapter(incoming,table)
    query=query_dict['complete_query']
    search_query=query_dict['search_query']
    search_value=query_dict['search_value']

    print query

    response = {}
    response['draw']=incoming['draw']
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)

    a_query='SELECT column_name FROM information_schema.columns WHERE table_name =%(table_name)s'
    cursor.execute(a_query,{'table_name':table})
    res=cursor.fetchall()

    id_position=0

    if res:
        for each in res:
            if each[0] == 'id':
                break
            else:
                id_position+=1

    cursor.execute(query,{'search':search_value})
    data=cursor.fetchall()
    response['data']=[]
    for row in data:
        a_content= Content(content_id=str(row[id_position]))
        response['data'].append(deepcopy(a_content.export()))

    cursor.execute(search_query,{'search':search_value})
    data=cursor.fetchall()

    f_count=0
    for row in data:
        f_count+=1
    response['recordsFiltered']=f_count


    cursor.execute("SELECT count(*) from "+table)
    totalcnt=cursor.fetchone()
    response['recordsTotal']=totalcnt[0]
    response['http_status']=200

    db.commit()
    db.close()
    return response

def is_file_allowed(filename, type):
    if type == 'video':
        ALLOWED_FILE_EXTENSIONS = [
            'mp4', 'avi', 'wmv', 'flv', 'mpg', 'mov', 'ogv', 'mkv']
    elif type == 'audio':
        ALLOWED_FILE_EXTENSIONS = ['mp3']
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_FILE_EXTENSIONS


def magnetic_pull(raw_content_id, magneturi, db, cursor):
    '''
        Transfers magnet link to putio to start content fetch
    '''
    response = {}
    try:
        resp = putio_client.Transfer.add_url(
            magneturi, callback_url=config.API_PROTOCOL+'://'+config.API_SERVER_NAME+'/hooks/putio')
        attrs = vars(resp)
        if attrs.has_key('id'):
            cursor.execute('UPDATE raw_content_registry SET remote_id = %(remote_id)s WHERE id = %(id)s',
                           {'remote_id': attrs['id'], 'id': raw_content_id})
            db.commit()
            db.close()
        else:
            cursor.execute("UPDATE raw_content_registry SET status = %(status)s, message = %(status)s WHERE id = %(id)s",
                           {'status': 'error', 'message': 'non-valid tranfer object returned', 'id': raw_content_id})
            db.commit()
            db.close()
            response['status'] = 'failed'
            response['message'] = 'Unable to initiate torrent download'
            response['http_status'] = 499
            return response

        response['status'] = 'success'
        response['message'] = 'File fetch initiated'
        response['http_status'] = 200
        response['putio_resp'] = attrs
        return response
    except Exception, e:
        print e
        cursor.execute("UPDATE raw_content_registry SET status = %(status)s, remarks = %(remarks)s WHERE id = %(id)s",
                       {'status': 'error', 'remarks': str(e)[:50], 'id': raw_content_id})
        db.commit()
        db.close()
        response['status'] = 'failed'
        response['message'] = 'Unable to initiate torrent download'
        response['http_status'] = 499
        return response


def procure(incoming, incoming_files):
    '''
        Procures content based on post parameters.
        Procurement modes  : Download from youtube,facebook etc and torrent downloads
    '''
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    date_created=int(round(time.time() * 1000))
    response = {}
    procurement_method = incoming.get('procurement_method')
    media_type = incoming.get('media_type')
    media_name = incoming.get('media_name')
    media_url = incoming.get('media_url')

    if procurement_method == None:
        response['status'] = 'failed'
        response['message'] = 'Please be more precise, as what exactly you want?'
        response['http_status'] = 419
        db.close()
        return response

    if procurement_method == 'DOWNLOAD':
        if media_type == None or media_name == None or media_url == None:
            response['status'] = 'failed'
            response['message'] = 'Please be more precise, as to what exactly do you want?'
            response['http_status'] = 419
            db.close()
            return response

        cursor.execute('INSERT INTO raw_content_registry (name, status, media_type, source,date_created, source_link) VALUES (%(name)s, %(status)s, %(media_type)s, %(source)s,%(date_created)s,%(source_link)s) returning id',
                       {'name': media_name, 'status': 'started', 'media_type': media_type, 'source': procurement_method,'date_created':date_created, 'source_link':media_url})
        db.commit()
        raw_content_id = cursor.fetchone()[0]
        tasks.download_video.apply_async((str(raw_content_id), media_url,media_type, media_name),queue=config.CELERY_QUEUE)
        # redis_cli.publish(
        #     'pp-downloader', '$'.join(['DOWNLOAD', str(raw_content_id), media_type, media_name, media_url]))
        # log_data = {'user_name': request.form['user_name'], 'user_email':request.form['user_email'],'source_link': media_url, 'raw_content_id':raw_content_id, 'media_name': media_name, 'media_type': media_type, 'source': procurement_method}
        log_activity(endpoint='util/procure-content/', activity_type='new',
                    item_id=raw_content_id, item_type='raw_content',
                    request=request)
        response['status'] = 'success'
        response['message'] = 'Content download initiated'
        db.close()
        return response

    elif procurement_method == 'TORRENT':
        torrent_file = request.files[
            'torrent'] if 'torrent' in incoming_files else None
        if torrent_file and media_name and media_type:
            filename = secure_filename(torrent_file.filename)
            torrent_file.save(os.path.join(config.TORRENT_FILE_DIR, filename))
            magneturi = torrent.magnetize(
                os.path.join(config.TORRENT_FILE_DIR, filename))

            if not magneturi:
                response['status'] = 'failed'
                response['message'] = "Are you stupid or trying to fool me? Send me a valid torrent file"
                response['http_status'] = 419
                db.close()
                return response

            cursor.execute('INSERT INTO raw_content_registry (name, status, source, media_type,date_created,source_link) VALUES (%(name)s, %(status)s, %(source)s, %(media_type)s,%(date_created)s,%(source_link)s) returning id',
                           {'name': media_name, 'status': 'started', 'source': procurement_method, 'media_type': media_type,'date_created':date_created,'source_link':magneturi})
            db.commit()
            raw_content_id = cursor.fetchone()[0]
            # log_data = {'user_name': request.form['user_name'], 'user_email':request.form['user_email'],'source_link': magneturi, 'raw_content_id':raw_content_id, 'media_name': media_name, 'media_type': media_type, 'source': procurement_method}
            log_activity(endpoint='util/procure-content/', activity_type='new',
                        item_id=raw_content_id, item_type='raw_content',
                        request=request)
            response = magnetic_pull(raw_content_id, magneturi, db, cursor)
            db.close()
            return response
        else:
            response['status'] = 'failed'
            response['message'] = 'Please be more precise, as to what exactly do you want?'
            response['http_status'] = 419
            db.close()
            return response

    elif procurement_method == 'MAGNET':
        if media_url and media_name and media_type:
            cursor.execute('INSERT INTO raw_content_registry (name, status, source, media_type,date_created,source_link) VALUES (%(name)s, %(status)s, %(source)s, %(media_type)s,%(date_created)s,%(source_link)s) returning id',
                           {'name': media_name, 'status': 'started', 'source': procurement_method, 'media_type': media_type,'date_created':date_created,'source_link':media_url})
            db.commit()
            raw_content_id = cursor.fetchone()[0]
            # media url will be magnet link in this case
            magneturi = media_url
            response = magnetic_pull(raw_content_id, magneturi, db, cursor)
            # log_data = {'user_name': request.form['user_name'], 'user_email':request.form['user_email'],'source_link': media_url, 'raw_content_id':raw_content_id, 'media_name': media_name, 'media_type': media_type, 'source': procurement_method}
            log_activity(endpoint='util/procure-content/', activity_type='new',
                        item_id=raw_content_id, item_type='raw_content',
                        request=request)
            db.close()
            return response
        else:
            response['status'] = 'failed'
            response[
                'message'] = 'Please be more precise, as to what exactly do you want?'
            response['http_status'] = 419
            db.close()
            return response

    elif procurement_method == 'UPLOAD':
        media_file = request.files[
            'media_file'] if 'media_file' in incoming_files else None
        if media_file and media_name and media_type:
            filename = secure_filename(media_file.filename)

            if not is_file_allowed(filename, media_type):
                response['status'] = 'failed'
                response[
                    'message'] = 'invalid file type for media type specified'
                response['http_status'] = 419
                db.close()
                return response

            cursor.execute('INSERT INTO raw_content_registry (name, status, media_type, source,date_created,source_link) VALUES (%(name)s, %(status)s, %(media_type)s, %(source)s,%(date_created)s,%(source_link)s) returning id',
                           {'name': media_name, 'status': 'started', 'source': procurement_method, 'media_type': media_type,'date_created':date_created,'source_link':filename})
            db.commit()
            raw_content_id = cursor.fetchone()[0]
            basename, ext = os.path.splitext(filename)
            if ext in ['.mp3']:
                media_file.save(os.path.join(
                    config.MEDIA_DOWNLOAD_LOCATION, 'pp-raw-content-'+str(raw_content_id)+ext))
                # transfer to S3 and remove
                remote_link = fileops.transfer_s3(os.path.join(
                    config.MEDIA_DOWNLOAD_LOCATION, 'pp-raw-content-'+str(raw_content_id)+ext), target_bucket=config.RAW_CONTENT_BUCKET)
                os.remove(os.path.join(
                    config.MEDIA_DOWNLOAD_LOCATION, 'pp-raw-content-'+str(raw_content_id)+ext))
                response['status'] = 'success'
                response['message'] = 'Media successfully stored'
                cursor.execute("UPDATE raw_content_registry SET status = %(status)s , remote_link = %(remote_link)s WHERE id = %(id)s",
                               {'status': 'complete', 'id': raw_content_id, 'remote_link': remote_link})

                db.commit()
                db.close()
                # log_data = {'user_name': request.form['user_name'], 'user_email':request.form['user_email'],'source_link': filename, 'raw_content_id':raw_content_id, 'media_name': media_name, 'media_type': media_type, 'source': procurement_method}
                log_activity(endpoint='util/procure-content/', activity_type='new',
                            item_id=raw_content_id, item_type='raw_content',
                            request=request)
                return response
            else:
                if media_type == 'video':
                    media_file.save(os.path.join(
                        config.MEDIA_DOWNLOAD_LOCATION, 'pp-raw-content-'+str(raw_content_id)+'-test'+ext))
                    cursor.execute("UPDATE raw_content_registry SET status = %(status)s  WHERE id = %(id)s",
                                   {'status': 'converting', 'id': raw_content_id})

                    db.commit()
                    tasks.video_converter.apply_async((os.path.join(config.MEDIA_DOWNLOAD_LOCATION, 'pp-raw-content-'+str(
                        raw_content_id)+'-test'+ext), os.path.join(config.MEDIA_DOWNLOAD_LOCATION, 'pp-raw-content-'+str(raw_content_id)+'.mp4')),{'raw_content_id': str(raw_content_id)},queue=config.CELERY_QUEUE)

                    # redis_cli.publish('pp-downloader', '$'.join(['CONVERT', str(raw_content_id), os.path.join(config.MEDIA_DOWNLOAD_LOCATION, 'pp-raw-content-'+str(
                    #     raw_content_id)+'-test'+ext), os.path.join(config.MEDIA_DOWNLOAD_LOCATION, 'pp-raw-content-'+str(raw_content_id)+'.mp4'), ]))

                    response['status'] = 'success'
                    response['message'] = 'Media conversion started'
                    db.close()
                    # log_data = {'user_name': request.form['user_name'], 'user_email':request.form['user_email'],'source_link': filename, 'raw_content_id':raw_content_id, 'media_name': media_name, 'media_type': media_type, 'source': procurement_method}
                    log_activity(endpoint='util/procure-content/', activity_type='new',
                                item_id=raw_content_id, item_type='raw_content',
                                request=request)
                    return response

    elif procurement_method == 'S3':
        if media_url and media_name and media_type:
            cursor.execute('INSERT INTO raw_content_registry (name, status, source, media_type,date_created,source_link) VALUES (%(name)s, %(status)s, %(source)s, %(media_type)s,%(date_created)s,%(source_link)s) returning id',
                           {'name': media_name, 'status': 'started', 'source': procurement_method, 'media_type': media_type,'date_created':date_created,'source_link':media_url})
            db.commit()

            raw_content_id = cursor.fetchone()[0]

            tasks.transfer_from_s3.apply_async((str(raw_content_id), media_url, media_type),queue=config.CELERY_QUEUE)

            response['status'] = 'success'
            response['message'] = 'Media conversion started'
            db.close()
            # log_data = {'user_name': request.form['user_name'], 'user_email':request.form['user_email'], 'source_link': media_url, 'raw_content_id':raw_content_id, 'media_name': media_name, 'media_type': media_type, 'source': procurement_method}
            log_activity(endpoint='util/procure-content/', activity_type='new',
                        item_id=raw_content_id, item_type='raw_content',
                        request=request)
            return response

        else:
            response['status'] = 'failed'
            response[
                'message'] = 'Please be more precise, as to what exactly do you want?'
            response['http_status'] = 419

            db.close()
            return response

    else:
        response['status'] = 'failed'
        response['message'] = 'invalid file type for media type specified'
        response['http_status'] = 419

    db.close()
    return response


def hook(content_id, incoming):
    a_content = Content(content_id=content_id)
    response = {}
    if a_content._exists is True:
        if 'raw-content-id' in incoming:
            temp = fileops.rename_file(
                orig_name=request.form['raw-content-id']+'.png', new_name=content_id+'.png')
            a_content.local_link = temp

        # updating raw-content-registry
        a_raw_content = Raw_content_registry(
            raw_content_id=request.form['raw_content_id'])
        if a_raw_content._exists is True:

            a_raw_content.used = 'yes'
            saved_id = a_raw_content.save()

            if saved_id is None:
                response['status'] = 'failed'
                response['message'] = 'problem saving record in raw_content!'
                response['http_status'] = 412
                if a_raw_content._token_id!=-1:
                    response['token_id']=a_raw_content._token_id
                return response
            else:

                cont_saved_id = a_content.save()
                if cont_saved_id is None:
                    response['status'] = 'failed'
                    response['message'] = 'problem saving record in content!'
                    response['http_status'] = 412
                    if a_content._token_id!=-1:
                        response['token_id']=a_content._token_id
                    return response
                else:
                    response['status'] = 'success'
                    response['message'] = 'Record saved!'
                    response['storage_id'] = saved_id
                    return response
        else:
            response['status'] = 'failed'
            response['message'] = 'raw_id does not exist!'
            response['http_status'] = 412
            return response
    else:
        response['status'] = 'failed'
        response['message'] = 'content_id does not exist!'
        response['http_status'] = 412
        return response



def get_raw_content_list(content_type):
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db, 'dict')
    cursor.execute('SELECT id, name, media_type, remote_link FROM raw_content_registry WHERE status = %(status)s and used = %(used)s \
                        and media_type = %(type)s ', {'used': 'no', 'status': 'complete', 'type':content_type})
    result = cursor.fetchall()
    file_list = []
    for i, group in enumerate(result):
        link = group[3]
        if link == None:
            continue
        file_name = link.split('/')[-1]
        headers={}
        if group[2]=='audio':
            headers={'response-content-type':'audio/mpeg3'}
        else:
            headers={'response-content-type':'video/mp4'}
        signedurl=fileops.sign(bucket=config.RAW_CONTENT_BUCKET,path=file_name,headers=headers,access_key=config.AWS_CREDS['KEY'],secret_key=config.AWS_CREDS['SECRET'])
        file_list.append({'name':group[1],'link':signedurl,'type':group[2], 'id':str(group[0])})

    db.close()
    return {'status':'success', 'file-list':file_list}

def video_stats():
    response={}
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    access_token=brightcove.get_access_token()
    headers={'Authorization':'Bearer '+access_token,
               "Content-Type": "application/json"}
    url=("https://analytics.api.brightcove.com/v1/data")
    params={'accounts':'4722550562001','from':'1456835644000','to':'1458650044000','limit':'all','dimensions':'video','fields':'video,video_name,video_duration'}
    r=requests.get(url,params=params,headers=headers)
    video_data=[]
    for video_obj in r.json()['items']:
        cursor.execute("SELECT id from content where remote_id=%(remote_id)s",{'remote_id':video_obj['video']})
        res=cursor.fetchone()
        if res:
            video_obj['content_id']='pp-content-'+str(res[0])
        else:
            video_obj['content_id']=res
        video_data.append(video_obj)
    response['data']=video_data
    return response


def video_stats_complete(incoming):
    response={}
    response['http_status']=200
    response['status']='success'
    db=wrappers.Postgres.init()
    cursor=wrappers.Postgres.get_cursor(db)
    access_token=brightcove.get_access_token()

    if 'ids' not in incoming:
        response['http_status']=419
        response['status']='failed'
        response['message']='pass the ids in the parameter'
        return response
    else:
        temp_ids=incoming['ids'].split(',')

        for each_id in temp_ids:
            try:
                a_content=Content(content_id=each_id)
            except Exception ,e:
                response['http_status']=419
                response['status']='failed'
                response['message']='invalid url paramter passed'
                return response

        ids=incoming['ids']



    end_time=int(round(time.time()*1000))
    start_time=end_time-(30*86400000)

    internet_data=[]    #internet data for 30 days
    ctr=start_time
    while ctr<=end_time:
        headers={'Authorization':'Bearer '+access_token,
                   "Content-Type": "application/json"}
        url=("https://analytics.api.brightcove.com/v1/data")
        params={'accounts':'4722550562001','from':ctr,'to':ctr,'limit':'all','dimensions':'video','fields':'video,video_name,video_duration,video_seconds_viewed'}
        internet=requests.get(url,params=params,headers=headers)
        temp_data=internet.json()
        video_data=[]
        for video_obj in temp_data['items']:
            cursor.execute("SELECT id from content where remote_id=%(remote_id)s",{'remote_id':video_obj['video']})
            res=cursor.fetchone()
            if res:
                video_obj['content_id']='pp-content-'+str(res[0])
            else:
                video_obj['content_id']=res
            video_data.append(video_obj)
        internet_data.append({'stamp':ctr,'data':temp_data})
        ctr+=86400000




    string=str(ids)+str(start_time)+str(end_time)+'5d9c1fed1354f562c9e94bef9839831dbrokengallintend'
    signature = (hashlib.sha256(string)).hexdigest()
    url=("http://api-2.pressplaytv.in/data/content_stats/content")
    params={"ids":ids,"start_time":start_time,"end_time":end_time,"key":"5d9c1fed1354f562c9e94bef9839831d","signature":signature,"engagement":"false"}
    box=requests.get(url,params=params)
    box_data=box.json()

    if 'segregated_count' in box_data:
        net_3g_views=0
        net_3g_engagement=0
        for id_obj in box_data['segregated_count']:
            if 'content_views' in box_data['segregated_count'][id_obj]:
                total_3g_views=0
                total_3g_engagement=0

                #if box data exists for an id
                if box_data['segregated_count'][id_obj]['content_views']:
                    for view_obj in box_data['segregated_count'][id_obj]['content_views']:

                        if 'timestamp' in view_obj:
                            temp=datetime.datetime.strptime(str(view_obj['timestamp'].split('T')[0]), "%Y-%m-%d")
                            dt=temp.strftime("%Y-%m-%d")

                        for obj in internet_data:
                            s_d=datetime.datetime.fromtimestamp(obj['stamp']/1000).strftime('%Y-%m-%d')
                            found=0
                            if s_d==dt:
                                for content_obj in obj['data']['items']:
                                    if content_obj['content_id']==id_obj:
                                        print "id matched"+str(content_obj['content_id'])
                                        found=1
                                        view_obj['3g_views']=content_obj['video_view']
                                        view_obj['3g_engagement_time']=int(content_obj['video_seconds_viewed'])*1000
                                        total_3g_views=total_3g_views+int(view_obj['3g_views'])
                                        total_3g_engagement=total_3g_engagement+int(view_obj['3g_engagement_time'])

                                        break
                                if found==1:
                                    break
                            if found==0:
                                view_obj['3g_views']=0
                                view_obj['3g_engagement_time']=0


                # no box , check for internet data
                else :
                    for obj in internet_data:
                        s_d=datetime.datetime.fromtimestamp(obj['stamp']/1000).strftime('%Y-%m-%d')
                        found=0
                        for content_obj in obj['data']['items']:
                            if content_obj['content_id']==id_obj:
                                box_data['segregated_count'][id_obj]['content_views'].append({'timestamp':str(s_d)+'T00:00:00Z','3g_views':content_obj['video_view'],'3g_engagement_time':int(content_obj['video_seconds_viewed'])*1000,'views':0})
                                total_3g_views=total_3g_views+content_obj['video_view']
                                total_3g_engagement=total_3g_engagement+(content_obj['video_seconds_viewed']*1000)
                                found=1
                                break
                        if found==0:
                            box_data['segregated_count'][id_obj]['content_views'].append({'timestamp':str(s_d)+'T00:00:00Z','3g_views':0,'3g_engagement_time':0,'views':0})

                if 'total_content_views' in box_data['segregated_count'][id_obj]:
                    box_data['segregated_count'][id_obj]['total_content_views']['total_3g_engagement']=total_3g_engagement
                    box_data['segregated_count'][id_obj]['total_content_views']['total_3g_views']=total_3g_views

            net_3g_engagement=net_3g_engagement+box_data['segregated_count'][id_obj]['total_content_views']['total_3g_engagement']
            net_3g_views=net_3g_views+box_data['segregated_count'][id_obj]['total_content_views']['total_3g_views']

        if 'total' in box_data:
            box_data['total']['3g_views']=net_3g_views
            box_data['total']['3g_engagement']=net_3g_engagement
    response['data']=box_data
    #response['internet_data']=internet_data

    return response


def batch_fetch(field_csv, id_csv):
    response={}
    response['http_status']=200
    response['status']='success'
    db=wrappers.Postgres.init()
    data = {}
    cursor=wrappers.Postgres.get_cursor(db, 'dict')
    id_list = id_csv.split(',')
    field_list = field_csv.split(',')

    if len(id_list) > 0 and len(field_list) > 0:
        field_list = set(field_list)
        field_list.add('id')
        id_list = set(id_list)
        query = "SELECT %s from content where id in (%s)" % (", ".join(map(str, field_list)),  ", ".join(map(str, id_list)))
        cursor.execute(query)
        results = cursor.fetchall()
        for row in results:
            row_obj = dict(row)
            key = "pp-content-" + str(row_obj['id'])
            if 'poster' in row_obj:
                row_obj['poster_favicon'] = None
                if row_obj['poster'] != None:
                    row_obj['poster_favicon'] = str(row_obj['poster']).replace('cloudfront.net/','cloudfront.net/small/')
            if 'poster_small' in row_obj:
                row_obj['poster_small_favicon'] = None
                if row_obj['poster_small'] != None:
                    row_obj['poster_small_favicon'] = str(row_obj['poster_small']).replace('cloudfront.net/','cloudfront.net/small/')
            data[key] = row_obj

    db.commit()
    db.close()
    response['data'] = data
    return response


def check_slug(slug_url):
    trim_slug = str(slug_url).strip()
    if trim_slug == "None" or trim_slug == "":
        response = {"http_status": 418,
                    "status":"failed",
                    "message":"please provide a value"}
        return response
    response={}
    response['http_status']=200
    response['status']='success'
    response['available'] = True
    db=wrappers.Postgres.init()
    cursor=wrappers.Postgres.get_cursor(db, 'dict')
    slug_url = slugify(slug_url)
    response['slug_url'] = slug_url
    query = "SELECT id from content where slug_url=%(slug_url)s"
    cursor.execute(query, {'slug_url': slug_url})
    result = cursor.fetchone()
    if result is not None:
        response['available'] = False
    return response


def trailer_cleanup(trailer_list, content_id):
    db=wrappers.Postgres.init()
    cursor=wrappers.Postgres.get_cursor(db, 'dict')

    if len(trailer_list) == 0:
        query = "UPDATE raw_content_registry set used='no' where id in (select raw_content_id from trailers_map where content_id=%s)" % (content_id)
        cursor.execute(query)
        db.commit()
        query = "DELETE from trailers_map where content_id=" + content_id
        cursor.execute(query)
        db.commit()
        return trailer_list

    trailers_new = [int(x) for x in trailer_list]
    trailers_new = set(trailers_new)
    trailers_old = set()

    query = "SELECT array_agg(raw_content_id) from trailers_map where content_id=" + content_id
    cursor.execute(query)
    result = cursor.fetchone()
    db.commit()

    if result is not None and result[0] is not None:
        print result
        type(result)
        trailers_old = set(result[0])

    delete_trailers = trailers_old.difference(trailers_new)
    add_trailers = trailers_new.difference(trailers_old)
    print delete_trailers
    print add_trailers

    if len(delete_trailers) > 0:
        query = "UPDATE raw_content_registry set used='no' where id in (%s)" % (", ".join(map(str, delete_trailers)))
        cursor.execute(query)
        db.commit()

        query = "DELETE from trailers_map where content_id=%s and raw_content_id IN (%s)" % (content_id, ", ".join(map(str, delete_trailers)))
        cursor.execute(query)
        db.commit()

    if len(add_trailers) > 0:
        insertQuery = "INSERT into trailers_map (content_id, raw_content_id) VALUES"
        insert_val = []
        for raw_id in add_trailers:
            insert_val.append(" (%s, %s)" % (content_id, str(raw_id)))
        insertQuery += ", ".join(insert_val)
        print insertQuery
        cursor.execute(insertQuery)
        db.commit()

    return add_trailers
