import wrappers
import re
def generic_adapter(incoming,table):

    query='not found'
    if 'draw' in incoming:
        search_value = re.escape(incoming['search[value]'])
        query='SELECT * FROM '+table
        if 'search[value]' in incoming:
            db = wrappers.Postgres.init()
            cursor = wrappers.Postgres.get_cursor(db)
            search_query='SELECT column_name,data_type FROM information_schema.columns WHERE table_name =%(table_name)s and data_type=%(data_type)s'
            cursor.execute(search_query,{'table_name':table,'data_type':'character varying'})
            res=cursor.fetchall()
            if res:
                query+=' where '
                for each in res:
                    query+=each[0]+' ~* %(search)s or '
    			#
                try:
                    cursor.execute("SELECT id from "+table+" where id=%(id)s",{'id':int(search_value.split('-')[-1])})
                    res=cursor.fetchone()
                    if res:
    					query+='id='+search_value.split('-')[-1]
                    else:
                        query=query[:-4]
                except Exception ,e:
                    print e
                    print "not searching through id"
                    query=query[:-4]
                db.close()

			#add search query
        if table == 'raw_content_registry':
            query += " and used='no'"
        search_query=query
        query+=' order by '
        i=0
        while True:
            if 'order['+str(i)+'][column]' in incoming:
                query+=incoming['columns['+incoming['order['+str(i)+'][column]']+'][data]']+' '+incoming['order['+str(i)+'][dir]']+','
                i+=1
            else:
                break
            query=query[:-1]
        query+=' limit '+incoming['length']+' offset '+incoming['start']
	return {'search_query':search_query,'complete_query':query, 'search_value': search_value}
