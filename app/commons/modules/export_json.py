import wrappers
from copy import deepcopy
import json
from models.Channel import Channel
from models.Sponsor import Sponsor
from models.Content import Content
import subprocess

content_types = {"movie": 1, "audio": 2, "video": 3}
yes_no = {"yes": 1, "no": 0, None: 0}

def handle(incoming):
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)

    final_json = [{"channels": []}]

    cursor.execute('SELECT * FROM channel_content_mapping order by id')
    results = cursor.fetchall()

    all_channels = set()
    chan_arr = []

    channels_set = {}

    for a_result in results:
        if a_result[3] not in channels_set:
            channels_set[a_result[3]] = []

        channels_set[a_result[3]].append({'order': a_result[1], 'content_id': a_result[2]})

        if a_result[3] not in all_channels:
            chan_arr.append(a_result[3])

        all_channels.add(a_result[3])

    for a_channel_id in chan_arr:
        a_channel = Channel(channel_id=str(a_channel_id))
        channel_data = a_channel.export()
        channel_data["pre_roll_ad"] = ""
        channel_data["content"] = []

        for a_content_info in channels_set[a_channel_id]:
            a_content = Content(content_id='pp-content-' + str(a_content_info['content_id']))
            content_data = a_content.export(to_database=True)
            another_content_data = a_content.export()
            content_data['duration'] = str(content_data['duration'])
            content_data['specifics'] = another_content_data['specifics']
            content_data['featured'] = yes_no[content_data['featured']]
            content_data['type'] = content_types[content_data['type']]
            if content_data['poster'] is not None:
                content_data['poster'] = content_data['poster'].split('/')[-1]
            elif content_data['specifics'] is not None and 'old_poster' in content_data['specifics']:
                content_data['poster'] = content_data['specifics']['old_poster']

            if content_data['premium_only'] in ['no', None]:
                content_data['premium'] = 0
            else:
                content_data['premium'] = 1
            if content_data['video_url'] is not None:
                content_data['video_id'] = content_data['video_url'].split('=')[1].split('&')[0]
                if 'youtu.be' in content_data['video_id']:
                    content_data['video_id'] = content_data['video_id'].split('/')[-1]
            elif content_data['specifics'] is not None and 'old_video_id' in content_data['specifics']:
                content_data['video_id'] = content_data['specifics']['old_video_id']
            if content_data['featured'] == 0:
                del content_data['featured']
            channel_data['content'].append(deepcopy(content_data))


        #if a_channel.sponsor_id is not None:
         #   a_sponsor = Sponsor(sponsor_id='pp-sponsor-' + str(a_channel.sponsor_id))

            channel_data["sponsor"] = a_sponsor.export()
            #if 'dummy' in channel_data['sponsor']['name'].lower():
                #channel_data['sponsor']['id'] = channel_data['sponsor']['sponsor_id'].replace('sponsor', 'dummysponsor')
                #channel_data['sponsor']['sponsor_id'] = channel_data['sponsor']['id']
            channel_data['sponsor']['apps'] = [{}]
            if 'app_data' in channel_data['sponsor'] and 'apps_download' in channel_data['sponsor']['app_data']:
                channel_data['sponsor']['apps_download'] = channel_data['sponsor']['app_data']['apps_download']
            if 'app_data' in channel_data['sponsor'] and 'package' in channel_data['sponsor']['app_data']:
                channel_data['sponsor']['apps'][0]['package'] = channel_data['sponsor']['app_data']['package']

            #if 'app_data' in channel_data['sponsor'] and 'id' in channel_data['sponsor']['app_data']:
            channel_data['sponsor']['apps'][0]['id'] = 'pp-sponsor-app-10'
            channel_data['sponsor']['apps'][0]['description-small'] = channel_data['sponsor']['description_small']
            channel_data['sponsor']['apps'][0]['description-large'] = channel_data['sponsor']['description']
            channel_data['sponsor']['apps'][0]['tagline'] = channel_data['sponsor']['tagline']
            if channel_data['sponsor']['image'] is not None:
                channel_data['sponsor']['apps'][0]['image'] = channel_data['sponsor']['image'].split('/')[-1].split('.')[0]

            channel_data['sponsor']['apps'][0]['platform'] = 'ANDROID'
            if 'app_data' in channel_data['sponsor'] and 'remote' in channel_data['sponsor']['app_data']:
                channel_data['sponsor']['apps'][0]['remote'] = channel_data['sponsor']['app_data']['remote']
            else:
                channel_data['sponsor']['apps'] = []
            if channel_data['sponsor']['splash'] is not None:
                channel_data['sponsor']['promotional_video'] = [{'type': "3", 'id': channel_data['sponsor']['splash'].split('/')[-1].split('.')[0]}]
            if channel_data['sponsor']['logo'] is not None:
                channel_data['sponsor']['logo'] = channel_data['sponsor']['logo'].split('/')[-1].split('.')[0]
            incumbent_pr = a_sponsor.preroll
            if incumbent_pr == None:
                incumbent_pr = ''
            if '=' not in incumbent_pr:
                incumbent_pr =  '=' + incumbent_pr
            channel_data["pre_roll_ad"] = incumbent_pr.split('=')[1].split('&')[0]

        channel_data["icon"] = channel_data["icon"].split("/")[-1]
        final_json[0]['channels'].append(deepcopy(channel_data))

        if 'no_file' not in incoming:
            file_obj = open("/home/ubuntu/testing_l3g.json", 'w')

            print >> file_obj, json.dumps(final_json, indent=4)
            command = "scp -i /home/ubuntu/ppdev.pem /home/ubuntu/testing_l3g.json ubuntu@52.74.96.210:/home/ubuntu/crons/ytb/"
            process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
            #process2 = subprocess.Popen('scp -i /home/ubuntu/ppdev.pem ubuntu@52.74.96.210 "sudo bash /home/ubuntu/ytb_uploader.sh"'.split(), stdout=subprocess.PIPE)
    db.close()
    return {'result': final_json}
