import wrappers
from app.commons.Toolkit import enumerate_table
from models.Feed import Feed
from models.Feed_temp import Feed_temp
from models.Content import Content
from models.Channel import Channel
from models.Sponsor import Sponsor
from models.Tag import Tag
import os
import upload_s3 as fileops
from copy import deepcopy
from flask import request
import json



def keep(home_feed_obj,test=False):
    #if feed_id == None:
    #    feed_id = '0'

    response = {}
    if test==False:
        a_feed = Feed()
    else:
        a_feed = Feed_temp()

    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    if test==False:
        a_query ="DELETE FROM home_feed"
        cursor.execute(a_query)
    else:
        a_query ="DELETE FROM home_feed_temp"
        cursor.execute(a_query)

    saved_id=None
    response['status'] = 'success'
    response['http_status'] = 200
    response['message'] = 'Record saved!'
    response['storage_id'] = saved_id

    row=1
    for arr_obj in home_feed_obj:		#arr_obj is an array of objects
        col=1
        for obj in arr_obj:						#each obj within in the array
            a_feed.row=row
            a_feed.col=col
            if obj['type'] == 'content':
                #a_query="SELECT status FROM content where id = %(id)s" 
                #cursor.execute(a_query,{'id':obj['id']})
                #cont_res=cursor.fetchone()
                a_cont=Content(content_id=obj['id'])

                if a_cont._exists is not True:      
                    response['http_status'] = 500
                    response['message'] = obj['id']+' content_id does not exist'
                    return response

                if a_cont.status !='complete' and a_cont.status !='approved':     
                    response['http_status'] = 500
                    response['message'] = obj['id']+' content is not approved'
                    return response
                if test==False:
                    cursor.execute('UPDATE content SET live=%(live)s where id=%(id)s',{'live':'yes','id':int(a_cont.id.split('-')[-1])})
                    db.commit()
            elif obj['type'] == 'channel': 
                a_channel=Channel(channel_id=obj['id'])  

                if a_channel._exists is not True:      
                    response['http_status'] = 500
                    response['message'] =obj['id']+' channel_id does not exist'
                    return response

                if a_channel.status!='complete' and a_channel.status!='approved':
                    response['http_status'] = 500
                    response['message'] = obj['id']+' channel is not approved'
                    return response

            elif obj['type']=='collection':
                a_tag=Tag(tag_id=obj['id'])
                if a_tag._exists is not True:      
                    response['http_status'] = 500
                    response['message'] = obj['id']+' collection does not exist'
                    return response

                if a_tag.status!='complete' and a_tag.status!='approved':
                    response['http_status'] = 500
                    response['message'] = obj['id']+' collection is not complete'
                    return response

                if a_tag.collection!='yes':
                    response['http_status'] = 500
                    response['message'] = obj['id']+' is not a collection'
                    return response
            else:
                a_sponsor=Sponsor(sponsor_id=obj['sponsor_id'])

                if a_sponsor._exists is not True:      
                    response['http_status'] = 500
                    response['message'] = obj['id']+' sponsor does not exist'
                    return response

                if a_sponsor.status!='complete' and a_sponsor.status!='approved':
                    response['http_status'] = 500
                    response['message'] = obj['id']+' sponsor is not complete'
                    return response

            if obj['type']=='image_ad':

                cursor.execute('SELECT id FROM ads_map WHERE sponsor_id=%(sponsor_id)s AND ad_id=%(ad_id)s AND ad_type=%(ad_type)s',{'sponsor_id':int(obj['sponsor_id'].split('-')[-1]),'ad_id':int(obj['id'].split('-')[-1]),'ad_type':str(obj['type'])})
                item_id=cursor.fetchone()[0]
                a_feed.item_id=str(item_id)
                a_feed.item_type=obj['type']
                saved_id = a_feed.save()

            elif obj['type']=='splash':
                cursor.execute('SELECT id FROM ads_map WHERE sponsor_id=%(sponsor_id)s AND ad_id=%(ad_id)s AND ad_type=%(ad_type)s',{'sponsor_id':int(obj['sponsor_id'].split('-')[-1]),'ad_id':int(obj['id'].split('-')[-1]),'ad_type':str(obj['type'])})
                item_id=cursor.fetchone()[0]
                a_feed.item_id=str(item_id)
                a_feed.item_type=obj['type']
                saved_id = a_feed.save()
            elif obj['type']=='preroll':
                cursor.execute('SELECT id FROM ads_map WHERE sponsor_id=%(sponsor_id)s AND ad_id=%(ad_id)s AND ad_type=%(ad_type)s',{'sponsor_id':int(obj['sponsor_id'].split('-')[-1]),'ad_id':int(obj['id'].split('-')[-1]),'ad_type':str(obj['type'])})
                item_id=cursor.fetchone()[0]
                a_feed.item_id=str(item_id)
                a_feed.item_type=obj['type']
                saved_id = a_feed.save()
            else:
                #print "saving content and channel"
                a_feed.item_id=obj['id']
                a_feed.item_type=obj['type']
                saved_id = a_feed.save()
            #print saved_id
            col=col+1

        if saved_id is None:
            response['status'] = 'failed'
            response['message'] = 'There was a problem with saving this record. Please check your request!'
            response['http_status'] = 412
            return response

        else:
            response['status'] = 'success'
            response['http_status'] = 200
            response['message'] = 'Record saved!'
            response['storage_id'] = saved_id
        row=row+1

    db.commit()
    db.close()
    return response

