import re
import hashlib

def identify(incoming):
    response = {'authorised': False}
    if 'email' not in incoming:
        response['http_status'] = 412
        response['message'] = 'No email provided to identify!'
        return response

    if not re.match(r"[^@]+@[^@]+\.[^@]+", incoming['email']):
        response['http_status'] = 400
        response['message'] = 'Invalid email ID!'
        return response

    email_parts = incoming['email'].split('@')
    if email_parts[-1] not in ['pressplaytv.in', 'pressplaytabs.com']:
        response['http_status'] = 403
        return response

    response['user_key'] = hashlib.sha256(email_parts[0] + 'verysaltypepper').hexdigest()
    response['http_status'] = 200
    response['authorised'] = True
    return response
