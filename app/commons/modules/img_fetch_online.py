from models.Channel import Channel
from models.Sponsor import Sponsor
from models.Content import Content
import upload_s3 as fileops
import os
import shutil
import requests
import config

def fetch_img(imageurl):
    response = {}
    img = requests.get(imageurl, stream=True)
    temp=os.path.basename(imageurl)
    filename=temp.split(".")[0]
    with open(config.FILE_PATH+filename+'.png', 'wb') as out_file:
        shutil.copyfileobj(img.raw, out_file)
    img_url=config.API_PROTOCOL+'://'+config.API_SERVER_NAME+'/'+config.FILE_PATH.split('/')[-2]+'/'+filename+'.png'
    response['status'] = 'success'
    response['message'] = img_url

    return response
   
#def downloadvideo(filename):
#    response={}
#    response['http_status']=200
#    response['status']='success'
#    res=fileops.manage_download(filename,'pp-test-buck')

    # if res is None:
    #     response['http_status']=500
    #     response['status']='failure'
    #     return response
    # response['link']=res

    # return response
