import psycopg2
import psycopg2.extras
import json
import sys
import os
from pymongo import MongoClient
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)),os.pardir))
from config import POSTGRES_CREDS as PG
from config import MONGO_CREDS as MDB
from config import HIDDEN_CHANNELS
from config import MONGO_LOCKFILE
import time


def fetchDict(query, fetch=True):
    cursor.execute(query)
    if fetch is True:
        records = cursor.fetchall()
        dict_result = []
        for row in records:
            dict_result.append(dict(row))
        return dict_result
    else:
        conn.commit()
        return True


def getPreviousVersion(previous_version=None):
    # versionQuery = "SELECT version_data, id FROM version_store WHERE id=%s" % str(previous_version)
    versionQuery = "SELECT version_data FROM version_store WHERE environment='internet' AND live='no' AND id < %s ORDER BY id DESC LIMIT 7 OFFSET 3" % (GLOBAL_OBJECT['version_id'])
    records = fetchDict(versionQuery)
    for row in records:
        version_info = row['version_data']
        collections = version_info['collection-feed']
        channels = version_info['channel-feed']
        PREVIOUS_VERSION = {}

        for collection in collections:
            collection_id = "pp-collection-" + str(collection['collection_id'].split('-')[-1])
            if collection_id not in PREVIOUS_VERSION:
                PREVIOUS_VERSION[collection_id] = {'channel': set(),
                                                   'content': set()}
            for content in collection['data']:
                if content['type'] == "channel":
                    PREVIOUS_VERSION[collection_id]['channel'].add(content['id'])
                elif content['type'] == "content":
                    PREVIOUS_VERSION[collection_id]['content'].add(content['id'])

        for channel in channels:
            channel_id = channel['channel_id']
            if channel_id not in PREVIOUS_VERSION:
                PREVIOUS_VERSION[channel_id] = set()
            for content in channel['content']:
                if content['type'] in ["content"]:
                    PREVIOUS_VERSION[channel_id].add(content['id'])

        return PREVIOUS_VERSION


def getLatestVersion():
    versionQuery = "SELECT version_data, id FROM version_store WHERE live='yes' and environment='internet' ORDER BY id desc limit 1"
    records = fetchDict(versionQuery)
    version_info = records[0]
    GLOBAL_OBJECT['version_id'] = version_info['id']
    VERSION_DATA = version_info['version_data']

    for channel_feed_obj in VERSION_DATA['channel-feed']:
        channel_id = channel_feed_obj['channel_id']
        ALL_CHANNEL_CONTENT[channel_id] = []
        for content_obj in channel_feed_obj['content']:
            if content_obj['type']=='content':
                content_id = content_obj['id'].split('-')[-1]
                ALL_CHANNEL_CONTENT[channel_id].append(content_id)

            if content_obj['type']=='image_ad':
                a_query="SELECT id,resource_location from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s and ad_id=%(ad_id)s"
                cursor.execute(a_query,{'sponsor_id':int(content_obj['id'].split('-')[-2]),'ad_type':'image_ad','ad_id':int(content_obj['id'].split('-')[-1])})
                res=cursor.fetchone()
                if res != None:
                    content_obj['ad_id']='pp-image-ad-'+content_obj['id'].split('-')[-2]+'-'+str(res[0])

        if 'preroll' in channel_feed_obj['sellable_entities']:
            for sellable_obj in channel_feed_obj['sellable_entities']['preroll']:
                    a_query="SELECT id,resource_location from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s and ad_id=%(ad_id)s"
                    cursor.execute(a_query,{'sponsor_id':int(sellable_obj['preroll_id'].split('-')[-2]),'ad_type':'preroll','ad_id':int(sellable_obj['preroll_id'].split('-')[-1])})
                    res=cursor.fetchone()
                    if res!=None:
                        sellable_obj['ad_id']='pp-pre-roll-'+sellable_obj['preroll_id'].split('-')[-2]+'-'+str(res[0])

        if 'splash' in channel_feed_obj['sellable_entities']:
            for sellable_obj in channel_feed_obj['sellable_entities']['splash']:
                    a_query="SELECT id,resource_location from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s and ad_id=%(ad_id)s"
                    cursor.execute(a_query,{'sponsor_id':int(sellable_obj['splash_id'].split('-')[-2]),'ad_type':'splash','ad_id':int(sellable_obj['splash_id'].split('-')[-1])})
                    res=cursor.fetchone()
                    if res!=None:
                        sellable_obj['ad_id']='pp-splash-'+sellable_obj['splash_id'].split('-')[-2]+'-'+str(res[0])

    for channel_feed_obj in VERSION_DATA['home-feed']:
        for content_obj in channel_feed_obj:
            if content_obj['type']=='image_ad':
                a_query="SELECT id,resource_location from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s and ad_id=%(ad_id)s"
                cursor.execute(a_query,{'sponsor_id':int(content_obj['id'].split('-')[-2]),'ad_type':'image_ad','ad_id':int(content_obj['id'].split('-')[-1])})
                res=cursor.fetchone()
                if res != None:
                    content_obj['ad_id']='pp-image-ad-'+content_obj['id'].split('-')[-2]+'-'+str(res[0])

    for collection_obj in VERSION_DATA['collection-feed']:
        collection_id = collection_obj['collection_id'].replace('tag','collection')
        ALL_COLLECTION_CONTENT[collection_id] = []
        for content_obj in collection_obj['data']:
            if content_obj['type'] == 'channel':
                if content_obj['id'] in ALL_CHANNEL_CONTENT:
                    ALL_COLLECTION_CONTENT[collection_id] += ALL_CHANNEL_CONTENT[content_obj['id']]
            if content_obj['type'] == 'content':
                content_id = content_obj['id'].split('-')[-1]
                ALL_COLLECTION_CONTENT[collection_id].append(content_id)

    return VERSION_DATA

def dumpLatestVersion():
    outfile = open('latestdump.json', 'w')
    outfile.write(json.dumps(VERSION_DATA))
    outfile.close()

def getDurationString(content_duration):
    duration_list = str(content_duration).split(':')
    duration_len = len(duration_list) - 1
    if duration_list[0] in ['0', '00']:
        del duration_list[0]
    duration_string = ":".join(duration_list)
    return duration_string


def getDescriptionBool(content_type):
    bool_value = "no"
    if str(content_type).lower() in ['movie']:
        bool_value = "yes"
    return bool_value

# def getContentTrailers(content_id):
#     trailer_list = []
#     query = "SELECT raw_content_id, remote_id as trailers from trailers_map where content_id=" + content_id
#     cursor.execute(query)
#     result = cursor.fetchall
#     if result is not None:
#         for row in result:
#             temp_obj = {'remoteId': row[0],
#                         'rawId': row[1]}
#             trailer_list.append(temp_obj)
#     return trailer_list

def getGlobalPreroll():
    query = "SELECT s.name, s.tagline, s.id, am.id, am.resource_location from ads_map as am left join sponsors as s on s.id = am.sponsor_id where s.is_global = 'yes' and ad_type='preroll'"
    cursor.execute(query)
    result = cursor.fetchall()
    global_preroll_list = None
    if result is not None and len(result) > 0:
        global_preroll_list = []
        for row in result:
            ad_id = "pp-pre-roll-%s-%s" % (str(row[2]), str(row[3]))
            ad_object = {'status': True,
                         'name': row[0],
                         'tagline': row[1],
                         'sponsorId': "pp-sponsor-" + str(row[2]),
                         'ppId':ad_id,
                         'type':"pre_roll",
                         'playbackUrl': row[4]}
            global_preroll_list.append(ad_object)
    return global_preroll_list

def getChannelInfo(channel_id):
    global CHANNEL_POOL
    CHANNEL_POOL.add(channel_id)
    str_id = "pp-channel-" + str(channel_id)
    if str_id in ALL_CHANNEL:
        channel_object = ALL_CHANNEL[str_id]['itemData']
    else:
        channelQuery = "SELECT ch.display_name, ch.icon, ch.tagline, ch.show_title, ch.slug_url, ch.is_global, ch.cover_image, ch.icon_small, ch.description, ch.short_description, ch.id, p.name AS provider_name, array_agg(t.name) AS tag_list FROM tags_map AS tm LEFT JOIN tags AS t ON t.id = tm.tag_id LEFT JOIN channels AS ch ON ch.id = tm.item_id LEFT JOIN provider AS p ON p.id = ch.provider_id WHERE ch.id = %s AND tm.item_type = 'channel' GROUP BY ch.display_name, ch.icon, ch.tagline, ch.show_title, ch.slug_url, ch.is_global, ch.cover_image, ch.icon_small, ch.description, ch.short_description, ch.id, provider_name" % str(channel_id)
        results = fetchDict(channelQuery)
        channel_info = results[0]
        poster_favicon = str(channel_info['icon']).replace('cloudfront.net/','cloudfront.net/small/')
        poster_small_favicon = str(channel_info['icon_small']).replace('cloudfront.net/','cloudfront.net/small/')
        channel_object = {
        'ppId': "pp-channel-" + str(channel_info['id']),
        'type': "channel",
        'name': channel_info['display_name'],
        'poster': channel_info['icon'],
        'posterFavicon': poster_favicon,
        'posterSmall': channel_info['icon_small'],
        'posterSmallFavicon': poster_small_favicon,
        'status': True,
        'providerName': channel_info['provider_name'],
        'newCount': 0,
        'tags': channel_info['tag_list'],
        'slugUrl': channel_info['slug_url'],
        'coverImage': channel_info['cover_image'],
        'description': channel_info['description'],
        'shortDescription': channel_info['short_description'],
        'isGlobal': channel_info['is_global'],
        'tagline': channel_info['tagline'],
        'preRollPool': None,
        'showTitle': channel_info['show_title']
        }

        if channel_info['is_global'] == 'yes':
            channel_object['preRollPool'] = getGlobalPreroll()

        ALL_CHANNEL[channel_object['ppId']] = {}
        ALL_CHANNEL[channel_object['ppId']]['itemData'] = channel_object
        ALL_CHANNEL[channel_object['ppId']]['itemContents'] = []
    return channel_object

def getContentInfo(content_id):
    CONTENT_POOL.append(content_id)
    str_id = "pp-content-" + str(content_id)
    if str_id in ALL_CONTENT:
        content_object = ALL_CONTENT[str_id]['itemData']
    else:
        contentQuery = "SELECT c.name, c.filter, c.raw_content_id, c.allow_download, c.seo, c.slug_url, c.cover_image, c.video_content_type, c.local_link, c.poster, c.poster_small, c.id, c.remote_id, c.type, c.description, c.short_description, c.premium,c.expiry,c.cost, p.name AS provider_name, c.duration, EXTRACT(EPOCH FROM c.duration)*1000 AS duration_ms, array_agg(t.name) AS tag_list FROM content AS c LEFT JOIN provider AS p ON c.provider_id=p.id LEFT JOIN tags_map AS tm ON tm.item_id = c.id LEFT JOIN tags AS t ON t.id = tm.tag_id WHERE c.id = %s AND tm.item_type = 'content' GROUP BY c.name, c.filter, c.raw_content_id, c.allow_download, c.seo, c.slug_url, c.cover_image, c.local_link, c.poster, c.poster_small, c.id, c.remote_id, c.type, c.description, c.short_description, c.premium,c.expiry,c.cost, provider_name, c.duration, duration_ms, c.video_content_type" % str(content_id)
        results = fetchDict(contentQuery)
        content_info = results[0]
        content_id = str(content_id)
        poster_favicon = str(content_info['poster']).replace('cloudfront.net/','cloudfront.net/small/')
        poster_small_favicon = str(content_info['poster_small']).replace('cloudfront.net/','cloudfront.net/small/')
        content_object = {
            'ppId': "pp-content-" + content_id,
            'name': content_info['name'],
            'poster': content_info['poster'],
            'posterFavicon': poster_favicon,
            'posterSmall': content_info['poster_small'],
            'posterSmallFavicon': poster_small_favicon,
            'remoteId': content_info['remote_id'],
            'rawId': content_info['raw_content_id'],
            'localUrl': content_info['local_link'],
            'type': content_info['type'],
            'status': True,
            'description': content_info['description'],
            'providerName': content_info['provider_name'],
            'isPremium': content_info['premium'],
            'expiry': content_info['expiry'],
            'cost': content_info['cost'],
            'filter': content_info['filter'],
            'displayDuration': "",
            'duration': content_info['duration_ms'],
            'videoContentType': content_info['video_content_type'],
            'tags': content_info['tag_list'],
            'slugUrl': content_info['slug_url'],
            'coverImage': content_info['cover_image'],
            'shortDescription': content_info['short_description'],
            'seo': content_info['seo'],
            'download': content_info['allow_download']
            }

        content_object['displayDuration'] = getDurationString(content_info['duration'])
        content_object['showDescription'] = getDescriptionBool(content_info['video_content_type'])
        # content_object['trailers'] = getContentTrailers(content_id)

        ALL_CONTENT[content_object['ppId']] = {}
        ALL_CONTENT[content_object['ppId']]['itemData'] = content_object
    return content_object


def getSponsorInfo(sponsor_id):
    sponsorQuery = "SELECT id, name, logo, image, twitter_link, fb_link, slug_url, cover_image, description, description_small, tagline, url, app_package,app_package_ios,ios_store_url,play_store_url FROM sponsors WHERE id = %s" % str(sponsor_id)
    results = fetchDict(sponsorQuery)
    sponsor_info = results[0]
    image_favicon = str(sponsor_info['image']).replace('cloudfront.net/','cloudfront.net/small/')
    logo_favicon = str(sponsor_info['logo']).replace('cloudfront.net/','cloudfront.net/small/')
    sponsor_object = {
                            "status": True,
                            "description": sponsor_info['description'],
                            "image": sponsor_info['image'],
                            "imageFavicon": image_favicon,
                            "logo": sponsor_info['logo'],
                            "logoFavicon": logo_favicon,
                            "ppId": "pp-sponsor-" + str(sponsor_info['id']),
                            "descriptionSmall": sponsor_info['description_small'],
                            "name": sponsor_info['name'],
                            "url": sponsor_info['url'],
                            "tagline": sponsor_info['tagline'],
                            "packageName": sponsor_info['app_package'],
                            "type": "sponsor",
                            "iosPackageName": sponsor_info['app_package_ios'],
                            "playStoreUrl": sponsor_info['play_store_url'],
                            "iosStoreUrl": sponsor_info['ios_store_url'],
                            "slugUrl": sponsor_info['slug_url'],
                            "facebook": sponsor_info['fb_link'],
                            "twitter": sponsor_info['twitter_link'],
                            "coverImage": sponsor_info['cover_image']
                        }
    return sponsor_object

def getImageAd(ad_id):
    temp_id = ad_id.split('-')[-1]
    adQuery = "SELECT am.name, am.sponsor_id, s.tagline, s.name as sponsor_name, am.resource_location, am.id FROM ads_map as am LEFT JOIN sponsors as s on s.id = am.sponsor_id WHERE am.id = %s" % str(temp_id)
    results = fetchDict(adQuery)
    adInfo = results[0]
    poster_favicon = str(adInfo['resource_location']).replace('cloudfront.net/','cloudfront.net/small/')
    ad_object = {
                "status": True,
                "name": adInfo['sponsor_name'],
                "poster": adInfo['resource_location'],
                "posterFavicon": poster_favicon,
                "sponsorInfo": getSponsorInfo(adInfo['sponsor_id']),
                "ppId": ad_id,
                "tagline": adInfo['tagline'],
                "type": "image_ad",
            }
    return ad_object

def getPreRoll(ad_id):
    temp_id = ad_id.split('-')[-1]
    adQuery = "SELECT am.name, am.sponsor_id, s.tagline, s.name as sponsor_name, am.resource_location, am.id FROM ads_map as am LEFT JOIN sponsors as s on s.id = am.sponsor_id WHERE am.id = %s" % str(temp_id)
    results = fetchDict(adQuery)
    adInfo = results[0]
    ad_object = {
                "status": True,
                "name": adInfo['sponsor_name'],
                "playbackUrl": adInfo['resource_location'],
                "sponsorId": "pp-sponsor-" + str(adInfo['sponsor_id']),
                "ppId": ad_id,
                "tagline": adInfo['tagline'],
                "type": "pre_roll"
            }
    return ad_object

def getSplash(ad_id):
    temp_id = ad_id.split('-')[-1]
    adQuery = "SELECT am.name, am.sponsor_id, s.tagline, s.name as sponsor_name, am.resource_location, am.id FROM ads_map as am LEFT JOIN sponsors as s on s.id = am.sponsor_id WHERE am.id = %s" % str(temp_id)
    results = fetchDict(adQuery)
    adInfo = results[0]
    ad_object = {
                "status": True,
                "name": adInfo['sponsor_name'],
                "playbackUrl": adInfo['resource_location'],
                "sponsorId": "pp-sponsor-" + str(adInfo['sponsor_id']),
                "ppId": ad_id,
                "type": "splash",
                "tagline": adInfo['tagline']
            }
    return ad_object


def getCollectionInfo(collection_id):
    str_id = "pp-collection-" + str(collection_id)
    if str_id in ALL_COLLECTION:
        collection_object = ALL_COLLECTION[str_id]['itemData']
    else:
        collectionQuery = "SELECT display_name, poster, poster_small, show_title, tagline, id, slug_url, description, short_description FROM tags WHERE id = %s" % str(collection_id)
        results = fetchDict(collectionQuery)
        collection_info = results[0]
        poster_favicon = str(collection_info['poster']).replace('cloudfront.net/','cloudfront.net/small/')
        poster_small_favicon = str(collection_info['poster_small']).replace('cloudfront.net/','cloudfront.net/small/')
        collection_object = {
                    'ppId': "pp-collection-" + str(collection_id),
                    'name': collection_info['display_name'],
                    'poster': collection_info['poster'],
                    'posterFavicon': poster_favicon,
                    'posterSmall': collection_info['poster_small'],
                    'posterSmallFavicon': poster_small_favicon,
                    'type': "collection",
                    'status': True,
                    'slugUrl': collection_info['slug_url'],
                    'newCount': 0,
                    'description': collection_info['description'],
                    'shortDescription': collection_info['short_description'],
                    'tagline': collection_info['tagline'],
                    'showTitle': collection_info['show_title']
                  }
        ALL_COLLECTION[collection_object['ppId']] = {}
        ALL_COLLECTION[collection_object['ppId']]['itemData'] = collection_object
        ALL_COLLECTION[collection_object['ppId']]['itemContents'] = []
    return collection_object


def getCollectionContent(collection_id):
    collectionQuery = "SELECT item_id, item_type FROM tags_map where tag_id=%s" % str(collection_id)
    results = fetchDict(collectionQuery)
    content_list = []
    for content in results:
        content_object = {}
        if content['item_type'] == "channel" and content['item_type'] in CHANNEL_POOL:
            content_object = getChannelInfo(content['item_id'])
            content_list.append([content_object])
        elif content['item_type'] == "content" and content['item_type'] in CONTENT_POOL:
            content_object = getContentInfo(content['item_id'])
            content_list.append([content_object])
    return content_list


def getCollectionFeed():
    GLOBAL_OBJECT['feeds']['collection'] = []
    collection_feed = VERSION_DATA['collection-feed']

    for collection in collection_feed:
        collection_id = collection['collection_id']
        collection_id_new = "pp-collection-" + str(collection_id.split('-')[-1])
        collection_object = {}
        COLLECTION_CONTENT_SET = set()
        COLLECTION_CHANNEL_SET = set()

        collection_object = getCollectionInfo(collection_id.split('-')[-1])
        ALL_COLLECTION[collection_id_new]['itemContents'] = []

        for item in collection['data']:
            item_id = item['id']
            item_object = {}

            if item['type'] == 'content':
                item_object = getContentInfo(item_id.split('-')[-1])
                COLLECTION_CONTENT_SET.add(str(item_id))

            elif item['type'] == 'channel':
                item_object = getChannelInfo(item_id.split('-')[-1])
                COLLECTION_CHANNEL_SET.add(item_id)

            if item['type'] in ['content', 'channel']:
                item_object['isNew'] = False
                if collection_id_new in PREVIOUS_VERSION:
                    if item_id not in PREVIOUS_VERSION[collection_id_new]:
                        item_object['isNew'] = True

            ALL_COLLECTION[collection_id_new]['itemContents'].append([item_object])

        if collection_id_new in PREVIOUS_VERSION:
            new_count = len(COLLECTION_CONTENT_SET.difference(PREVIOUS_VERSION[collection_id_new]['content']))
            new_channels = COLLECTION_CHANNEL_SET.difference(PREVIOUS_VERSION[collection_id_new]['channel'])
            old_channels = COLLECTION_CHANNEL_SET.intersection(PREVIOUS_VERSION[collection_id_new]['channel'])

            for chid in new_channels:
                new_count += CHANNEL_COUNT[chid]

            for chid in old_channels:
                new_count += ALL_CHANNEL[chid]['itemData']['newCount']

            # collection_object['newCount'] = new_count
            # ALL_COLLECTION[collection_id_new]['itemData']['newCount'] = new_count

        GLOBAL_OBJECT['feeds']['collection'].append([collection_object])


def getHomeFeed():
    GLOBAL_OBJECT['feeds']['home'] = []
    home_feed = VERSION_DATA['home-feed']
    for aRow in home_feed:
        temp_list = []
        for counter, item in enumerate(aRow) :
            item_id = item['id'].split('-')[-1]
            item_type = item['type']
            item_info = {}
            if item_type == "channel":
                item_info = getChannelInfo(item_id)
            elif item_type == "content":
                item_info = getContentInfo(item_id)
            elif item_type == "collection":
                collection_id = "pp-collection-" + str(item_id)
                if collection_id in ALL_COLLECTION:
                    item_info = ALL_COLLECTION[collection_id]['itemData']
                else:
                    item_info = getCollectionInfo(item_id)
                    ALL_COLLECTION[collection_id]['itemContents'] = getCollectionContent(item_id)

            elif item_type == "image_ad":
                item_info = getImageAd(item['ad_id'])

            temp_list.append(item_info)
            if counter == len(aRow)-1:
                GLOBAL_OBJECT['feeds']['home'].append(temp_list)


def getChannelFeed():
    global CONTENT_DICT
    global CHANNEL_CONTENT_MAP
    global CHANNEL_COUNT

    GLOBAL_OBJECT['feeds']['channel'] = []
    channel_feed = VERSION_DATA['channel-feed']
    temp_list = []

    for counter, channel in enumerate(channel_feed):
        channel_id = channel['channel_id']
        CHANNEL_CONTENT_MAP[channel_id] = set()
        channel_object = {}
        CHANNEL_SET = set()
        CHANNEL_COUNT[channel_id] = 0
        if channel_id in ALL_CHANNEL:
            channel_object = ALL_CHANNEL[channel_id]['itemData']
        else:
            channel_object = getChannelInfo(channel_id.split('-')[-1])

        for content in channel['content']:
            content_id = content['id'].split('-')[-1]
            content_object = {}
            if content['type'] == 'content':
                content_object = getContentInfo(content_id)
                CONTENT_DICT[content_id] = channel_id
                CHANNEL_CONTENT_MAP[channel_id].add(content_id)
                content_object['isNew'] = False
                CHANNEL_SET.add(content['id'])
                CHANNEL_COUNT[channel_id] += 1

                if channel_id in PREVIOUS_VERSION:
                    if content['id'] not in PREVIOUS_VERSION[channel_id]:
                        content_object['isNew'] = True

            elif content['type'] == 'image_ad':
                content_object = getImageAd(content['ad_id'])

            ALL_CHANNEL[channel_id]['itemContents'].append([content_object])

        if channel_id in PREVIOUS_VERSION:
            # print channel_id
            new_count = len(CHANNEL_SET.difference(PREVIOUS_VERSION[channel_id]))
            # channel_object['newCount'] = new_count

        if len(temp_list) == 2:
            GLOBAL_OBJECT['feeds']['channel'].append(temp_list)
            temp_list = []

        if channel_id not in HIDDEN_CHANNELS:
            temp_list.append(channel_object)

        if counter == len(channel_feed)-1:
            if len(temp_list) > 0:
                GLOBAL_OBJECT['feeds']['channel'].append(temp_list)

        if len(channel['sellable_entities']) > 0:

            if 'top-bar-sponsor' in channel['sellable_entities']:
                sponsor_id = channel['sellable_entities']['top-bar-sponsor'].split('-')[-1]
                ALL_CHANNEL[channel_id]['itemData']['topBarSponsor'] = getSponsorInfo(sponsor_id)

            if 'preroll' in channel['sellable_entities'] and len(channel['sellable_entities']['preroll']) > 0:
                if ALL_CHANNEL[channel_id]['itemData']['preRollPool'] is None:
                    ALL_CHANNEL[channel_id]['itemData']['preRollPool'] = []
                for preroll in channel['sellable_entities']['preroll']:
                    preroll_object = getPreRoll(preroll['ad_id'])
                    ALL_CHANNEL[channel_id]['itemData']['preRollPool'].append(preroll_object)

            if 'splash' in channel['sellable_entities']:
                ALL_CHANNEL[channel_id]['itemData']['splashPool'] = []
                for splash in channel['sellable_entities']['splash']:
                    splash_object = getSplash(splash['ad_id'])
                    ALL_CHANNEL[channel_id]['itemData']['splashPool'].append(splash_object)

def setContentLive(CONTENT_POOL):
    if len(CONTENT_POOL) > 0:
        updateQuery = "UPDATE content SET live='no'"
        fetchDict(updateQuery, fetch=False)

        liveQuery = "UPDATE content SET live='yes' WHERE id IN (%s)" % ", ".join(map(str, CONTENT_POOL))
        fetchDict(liveQuery, fetch=False)

        publishQuery = "UPDATE content SET first_publish_timestamp=%s where live='yes' and first_publish_timestamp is NULL" % (str(int(round(time.time() * 1000))))
        fetchDict(publishQuery, fetch=False)
    return True

def setChannelLive(CHANNEL_POOL):
    if len(CHANNEL_POOL) > 0:
        updateQuery = "UPDATE channels SET live='no'"
        fetchDict(updateQuery, fetch=False)

        liveQuery = "UPDATE channels SET live='yes' WHERE id IN (%s)" % ", ".join(map(str, CHANNEL_POOL))
        fetchDict(liveQuery, fetch=False)
    return True

def getRelatedContent(content_id, content_list):
    return_list = []
    if len(content_list) > 0:
        relatedQuery = "SELECT c.name, c.filter, c.slug_url, c.allow_download, c.cover_image, c.seo, c.raw_content_id, c.video_content_type, c.local_link, c.poster, c.poster_small, c.id, c.remote_id, c.type, c.description, c.short_description, c.premium, c.expiry, c.cost, c.duration, EXTRACT(EPOCH from c.duration)*1000 as duration_ms, p.name AS provider_name FROM content AS c, provider AS p WHERE c.provider_id = p.id AND c.id IN (SELECT c.id FROM tags_map tm LEFT JOIN content c ON tm.item_id = c.id WHERE tm.item_type = 'content' AND c.live = 'yes' AND c.id != %s and c.id NOT IN (%s) AND tm.tag_id IN (SELECT tag_id FROM tags_map WHERE item_id = %s AND item_type='content')) AND c.status='complete'" % (str(content_id), ", ".join(map(str, content_list)), str(content_id))
    else:
        relatedQuery = "SELECT c.name, c.filter, c.slug_url, c.allow_download, c.cover_image, c.seo, c.raw_content_id, c.video_content_type, c.local_link, c.poster, c.poster_small, c.id, c.remote_id, c.type, c.description, c.short_description, c.premium, c.expiry, c.cost, c.duration, EXTRACT(EPOCH from c.duration)*1000 as duration_ms, p.name AS provider_name FROM content AS c, provider AS p WHERE c.provider_id = p.id AND c.id IN (SELECT c.id FROM tags_map tm LEFT JOIN content c ON tm.item_id = c.id WHERE tm.item_type = 'content' AND c.live = 'yes' AND c.id != %s AND tm.tag_id IN (SELECT tag_id FROM tags_map WHERE item_id = %s AND item_type='content')) AND c.status='complete'" % (str(content_id), str(content_id))

    results = fetchDict(relatedQuery)
    for content_info in results:
        str_id = "pp-content-" + str(content_info['id'])
        poster_favicon = str(content_info['poster']).replace('cloudfront.net/','cloudfront.net/small/')
        poster_small_favicon = str(content_info['poster_small']).replace('cloudfront.net/','cloudfront.net/small/')
        content_object = {
            'ppId': str_id,
            'name': content_info['name'],
            'poster': content_info['poster'],
            'posterFavicon': poster_favicon,
            'posterSmall': content_info['poster_small'],
            'posterSmallFavicon': poster_small_favicon,
            'remoteId': content_info['remote_id'],
            'rawId': content_info['raw_content_id'],
            'localUrl': content_info['local_link'],
            'type': content_info['type'],
            'status': True,
            'description': content_info['description'],
            'providerName': content_info['provider_name'],
            'isPremium': content_info['premium'],
            'expiry': content_info['expiry'],
            'cost': content_info['cost'],
            'filter': content_info['filter'],
            'duration': content_info['duration_ms'],
            'videoContentType': content_info['video_content_type'],
            'tags': [],
            'slugUrl': content_info['slug_url'],
            'coverImage': content_info['cover_image'],
            'shortDescription': content_info['short_description'],
            'seo': content_info['seo'],
            'trailers': [],
            'download': content_info['allow_download']
            }

        content_object['displayDuration'] = getDurationString(content_info['duration'])
        content_object['showDescription'] = getDescriptionBool(content_info['video_content_type'])

        if str_id in ALL_CONTENT:
            content_object['tags'] = ALL_CONTENT[str_id]['itemData']['tags']
            # content_object['trailers'] = ALL_CONTENT[str_id]['itemData']['trailers']

        return_list.append([content_object])

    return return_list


def attachRelatedContent():
    for content_id in CONTENT_POOL:

        ALL_CONTENT["pp-content-" + str(content_id)]['itemContents'] = []
        content_list = []

        if content_id in CONTENT_DICT:
            channel_id = CONTENT_DICT[content_id]
            content_list = list(CHANNEL_CONTENT_MAP[channel_id])

            if content_id in content_list: content_list.remove(content_id)

            for item in content_list:
                item_id = "pp-content-" + str(item)
                if item_id in ALL_CONTENT:
                    content_obj = ALL_CONTENT[item_id]['itemData']
                    ALL_CONTENT["pp-content-" + str(content_id)]['itemContents'].append([content_obj])

        ALL_CONTENT["pp-content-" + str(content_id)]['itemContents'] += getRelatedContent(content_id, content_list)


def getRelatedChannels(channel_id):
    channel_id = str(channel_id)
    return_list = []
    relatedQuery = "SELECT ch.id, ch.name, ch.display_name, ch.tagline, ch.show_title, ch.is_global, ch.icon, ch.icon_small, ch.slug_url, ch.cover_image, ch.description, ch.short_description, p.name as provider_name FROM channels AS ch LEFT JOIN provider as p on p.id = ch.provider_id WHERE ch.id IN (SELECT tm.item_id FROM tags_map AS tm WHERE tm.item_type = 'channel' and tm.tag_id IN (SELECT tmp.tag_id FROM tags_map AS tmp WHERE tmp.item_type='channel' AND tmp.item_id=%s) AND tm.item_id IN (%s) AND tm.item_id != %s )" % (channel_id, ", ".join(map(str, CHANNEL_POOL)), channel_id)
    cursor.execute(relatedQuery)
    results = cursor.fetchall()

    for row in results:
        channel_info = dict(row)
        ppid = "pp-channel-" + str(channel_info['id'])
        poster_favicon = str(channel_info['icon']).replace('cloudfront.net/','cloudfront.net/small/')
        poster_small_favicon = str(channel_info['icon_small']).replace('cloudfront.net/','cloudfront.net/small/')
        channel_object = {
                'ppId': ppid,
                'type': "channel",
                'name': channel_info['display_name'],
                'poster': channel_info['icon'],
                'posterFavicon': poster_favicon,
                'posterSmall': channel_info['icon_small'],
                'posterSmallFavicon': poster_small_favicon,
                'status': True,
                'tags': [],
                'providerName': channel_info['provider_name'],
                'slugUrl': channel_info['slug_url'],
                'coverImage': channel_info['cover_image'],
                'description': channel_info['description'],
                'shortDescription': channel_info['short_description'],
                'isGlobal': channel_info['is_global'],
                'preRollPool': None,
                'tagline': channel_info['tagline'],
                'showTitle': channel_info['show_title']
                }

        if ppid in ALL_CHANNEL:
            channel_object['tags'] = ALL_CHANNEL[ppid]['itemData']['tags']
            channel_object['preRollPool'] = ALL_CHANNEL[ppid]['itemData']['preRollPool']

        return_list.append([channel_object])
    return return_list

def attachRelatedChannels():
    global RELATED_CHANNEL
    for item in CHANNEL_POOL:
        channel_id = str(item)
        str_id = "pp-channel-" + channel_id
        RELATED_CHANNEL[str_id] = getRelatedChannels(channel_id)

def getCollectionTimestamp(content_list):
    return_list = []
    query = "SELECT array_agg(first_publish_timestamp) from content where id in (%s) and first_publish_timestamp is not NULL" % ", ".join(map(str, content_list))
    cursor.execute(query)
    result = cursor.fetchone()
    if result is not None and result[0] is not None:
        return_list = result[0]
    return return_list

def attachCollectionTimestamp():
    for key, value in ALL_COLLECTION_CONTENT.iteritems():
        ALL_COLLECTION_TIMESTAMP[key] = getCollectionTimestamp(value)

def loadMongo():

    for key, value in GLOBAL_OBJECT['channels'].iteritems():
        loaddata = {"item_type": "channel",
                    "item_id": key,
                    "content_version": GLOBAL_OBJECT['version_id'],
                    "itemData": value["itemData"],
                    "slug_url": value["itemData"]["slugUrl"],
                    "itemContents": value["itemContents"]}
        result =  db.beacontemp.insert_one(loaddata)
        # print result.inserted_id

    for key, value in GLOBAL_OBJECT['content'].iteritems():
        loaddata = {"item_type": "content",
                    "item_id": key,
                    "content_version": GLOBAL_OBJECT['version_id'],
                    "itemData": value["itemData"],
                    "slug_url": value["itemData"]["slugUrl"],
                    "itemContents": value["itemContents"]}
        result =  db.beacontemp.insert_one(loaddata)
        # print result.inserted_id

    for key, value in GLOBAL_OBJECT['collections'].iteritems():
        loaddata = {"item_type": "collection",
                    "item_id": key,
                    "content_version": GLOBAL_OBJECT['version_id'],
                    "itemData": value["itemData"],
                    "slug_url": value["itemData"]["slugUrl"],
                    "itemContents": value["itemContents"]}
        result =  db.beacontemp.insert_one(loaddata)
        # print result.inserted_id

    for key, value in GLOBAL_OBJECT['feeds'].iteritems():
        loaddata = {
        "item_type": "feed",
        "item_id": key,
        "content_version": GLOBAL_OBJECT['version_id'],
        "value": value
        }
        result =  db.beacontemp.insert_one(loaddata)
        # print result.inserted_id

    for key, value in GLOBAL_OBJECT['related_channels'].iteritems():
        loaddata = {"item_type": "related",
                    "item_id": key,
                    "slug_url": ALL_CHANNEL[key]['itemData']['slugUrl'],
                    "content_version": GLOBAL_OBJECT['version_id'],
                    "value": value}
        result =  db.beacontemp.insert_one(loaddata)
        # print result.inserted_id


    loaddata = {"item_type":"content-version",
                "value": str(GLOBAL_OBJECT['version_id'])}
    result =  db.beacontemp.insert_one(loaddata)
    # print result.inserted_id

    loaddata = {"item_type": "collection-time",
                "value": GLOBAL_OBJECT['collection_content_time']}
    result =  db.beacontemp.insert_one(loaddata)
    # print result.inserted_id

    db.drop_collection(name_or_collection="beaconbkp")
    db.beaconlive.rename(new_name="beaconbkp")
    db.beacontemp.rename(new_name="beaconlive")

    print 'mongo is filled!!'


def fireUpMongo(previous_version=None):
    # start = time.time()
    global cursor
    global db
    global conn
    global VERSION_DATA
    global GLOBAL_OBJECT
    global CONTENT_POOL
    global CHANNEL_POOL
    global ALL_CONTENT
    global ALL_COLLECTION
    global ALL_CHANNEL
    global RELATED_CHANNEL
    global PREVIOUS_VERSION
    global CONTENT_DICT
    global CHANNEL_CONTENT_MAP
    global CHANNEL_COUNT
    global ALL_CHANNEL_CONTENT
    global ALL_COLLECTION_CONTENT
    global ALL_COLLECTION_TIMESTAMP

    VERSION_DATA = {}
    GLOBAL_OBJECT = {}
    GLOBAL_OBJECT['feeds'] = {}
    CONTENT_POOL = []
    CHANNEL_POOL = set()
    ALL_CONTENT = {}
    ALL_COLLECTION = {}
    ALL_CHANNEL = {}
    ALL_CHANNEL_CONTENT = {}
    ALL_COLLECTION_CONTENT = {}
    ALL_COLLECTION_TIMESTAMP = {}
    RELATED_CHANNEL = {}
    PREVIOUS_VERSION = {}
    CONTENT_DICT = {}
    CHANNEL_CONTENT_MAP = {}
    CHANNEL_COUNT = {}
    return_bool = True

    try:
        PG_CONN = "host='%(HOST)s' dbname='%(DATABASE)s' user='%(USER)s' password='%(PASSWORD)s'" % PG
        conn = psycopg2.connect(PG_CONN)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        mongoConnString = "mongodb://%(USER)s:%(PASSWORD)s@%(HOST)s:%(PORT)s/%(DATABASE)s" % MDB
        client = MongoClient(mongoConnString)
        db = client.beacon

        VERSION_DATA = getLatestVersion()
        PREVIOUS_VERSION = getPreviousVersion(previous_version=None)
        getChannelFeed()
        getCollectionFeed()
        getHomeFeed()
        CONTENT_POOL = list(set(CONTENT_POOL))
        setContentLive(CONTENT_POOL)
        # setChannelLive(CHANNEL_POOL)
        attachRelatedContent()
        attachRelatedChannels()
        attachCollectionTimestamp()

        GLOBAL_OBJECT['channels'] = ALL_CHANNEL
        GLOBAL_OBJECT['collections'] = ALL_COLLECTION
        GLOBAL_OBJECT['content'] = ALL_CONTENT
        GLOBAL_OBJECT['related_channels'] = RELATED_CHANNEL
        GLOBAL_OBJECT['collection_content_time'] = ALL_COLLECTION_TIMESTAMP

        loadMongo()

        # end = time.time()
        # print end - start

    except Exception, e:
        print e
        return_bool = False

    finally:
        os.remove(MONGO_LOCKFILE)
        return return_bool
