import httplib
import json
import MySQLdb as mdb
import ConfigParser
from gcm import GCM
import config
from flask import request

emails=["saurabh.minute@gmail.com","djangopphiring@gmail.com"]
emails.append("gd.jiit@gmail.com")

auth_key = config.GCM_AUTH
LIMIT=1000
gcm = GCM(auth_key)

def send_notification(device_ids):
    conn = httplib.HTTPSConnection("android.googleapis.com")
    conn.connect()
    conn.set_debuglevel(1)
    body = {}
    global_success=0
    global_failure=0
    print len(device_ids)
    for i in range(0,len(device_ids),LIMIT):
        body['data'] = request.get_json()
        response=gcm.json_request(registration_ids=device_ids[i:i+LIMIT], data=body['data'])
        global_success+=len(response['success'])
        global_failure+=len(response['errors'])

    return {'success_count': global_success, 'failure_count': global_failure}

def to_all(incoming):
    omdb = {
        'ip': config.MYSQL_CREDS['HOST'],
        'username': config.MYSQL_CREDS['USER'],
        'password': config.MYSQL_CREDS['PASSWORD'],
        'name': config.MYSQL_CREDS['DB'],
    }

    smithy2 = mdb.connect(omdb['ip'], omdb['username'], omdb['password'], omdb['name'])
    cur = smithy2.cursor()
    test_query="SELECT device_id  FROM `app_notification_register` WHERE app_version >=20 and email in ('"+"','".join(list(set(emails)))+"')"
    print test_query
    live_query = "SELECT device_id  FROM `app_notification_register` WHERE app_version >=25"

    if incoming['environment'] == 'testing':
        cur.execute(test_query)
    elif incoming['environment'] == 'live':
        cur.execute(test_query)
    devices=cur.fetchall()
    device_ids=[]
    for device in devices:
        device_ids.append(device[0])
    results = send_notification(device_ids)

    response = {'device_count': len(device_ids), 'status': 'success', 'data': results}
    return response
