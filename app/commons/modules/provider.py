import wrappers
from app.commons.Toolkit import enumerate_table
from models.Provider import Provider
import os
import upload_s3 as fileops
from copy import deepcopy
from flask import request
import json
from datetime import datetime as dt
import time
from datatable_adapter import generic_adapter
from slugify import slugify
import config
from app.commons.modules.userlogs import log_activity
from app.commons.modules.beacon import beacon_redis_del


def fetch(provider_id, incoming):
    requested_provider = Provider(provider_id=provider_id)
    response = {'exists' : False, 'status': 'failure'}


    if requested_provider._exists == True:
        response['exists'] = True
        response['status'] = 'success'
        response['data'] = requested_provider.export()
        response['data']['cover_image_favicon'] = None
        if response['data']['campaign_start'] !=None:
            response['data']['campaign_start']=dt.strptime(response['data']['campaign_start'], '%Y-%m-%d').strftime('%d-%m-%Y')
        if response['data']['campaign_end'] !=None:
            response['data']['campaign_end']=dt.strptime(response['data']['campaign_end'], '%Y-%m-%d').strftime('%d-%m-%Y')
        if response['data']['cover_image'] != None:
            response['data']['cover_image_favicon'] = str(response['data']['cover_image']).replace('cloudfront.net/','cloudfront.net/small/')
        response['data']['content']=[]
        response['data']['channel']=[]
        db = wrappers.Postgres.init()
        cursor = wrappers.Postgres.get_cursor(db)
        cursor.execute("SELECT id,name from content where provider_id=%(provider_id)s",{'provider_id':int(provider_id.split('-')[-1])})
        res=cursor.fetchall()
        for content_obj in res:
            response['data']['content'].append({'id':'pp-content-'+str(content_obj[0]),'name':content_obj[1]})
        cursor.execute("SELECT id,name from channels where provider_id=%(provider_id)s",{'provider_id':int(provider_id.split('-')[-1])})
        res=cursor.fetchall()
        for channel_obj in res:
            response['data']['channel'].append({'id':'pp-channel-'+str(channel_obj[0]),'name':channel_obj[1]})

    return response


def keep(provider_id, incoming, incoming_files):
    activity_type = 'edit'
    if provider_id == None:
        provider_id = '0'
        activity_type='new'
    response = {}
    a_provider = Provider(provider_id=provider_id)
    change_cover_image_flag = 0

    if a_provider._exists==False:
        if 'name' not in incoming:
            response['http_status']=419
            response['message']='please enter a name'
            response['status']='failed'
            return response
        else:
            if incoming['name']=='':
                response['http_status']=419
                response['message']='please enter a name'
                response['status']='failed'
                return response

    try:
        cover_image_path=a_provider.image
        change_cover_image_flag=1
    except AttributeError:
        cover_image_path=None

    if 'status' in incoming:
        if incoming['status']!='complete' and incoming['status']!='approved' and incoming['status']!='incomplete' and incoming['status']!='staged':
            response['http_status']=419
            response['message']='status type invalid'
            response['status']='failed'
            return response

    if 'campaign_end' in incoming:
        if incoming['campaign_end']!='null':
            try:
                dt.strptime(str(incoming['campaign_end']), "%d-%m-%Y")
            except:
                response['http_status']=419
                response['message']='please enter the correct format of campaign end date(dd-mm-yyyy)'
                response['status']='failed'
                return response

    if 'campaign_start' in incoming:
        if incoming['campaign_start']!='null':
            try:
                dt.strptime(str(incoming['campaign_start']), "%d-%m-%Y")
            except :
                response['http_status']=419
                response['message']='please enter the correct format of campaign start date(dd-mm-yyyy)'
                response['status']='failed'
                return response

    if a_provider._exists == True:
        a_provider.date_modified= int(round(time.time() * 1000))

        if 'status' in incoming :
            if request.form['status']=='complete' or request.form['status']=='approved':
                if a_provider.name is None or a_provider.campaign_start is None:
                    response['status']='failed'
                    response['http_status']=412
                    if a_provider.name is None :
                        response['message']='please enter the name field'
                        return response
                    if a_provider.campaign_start is None:
                        response['message']='please enter the start date'
                        return response
    else:
        a_provider.date_created= int(round(time.time() * 1000))

        if 'status' in incoming :
            if request.form['status']=='complete' or request.form['status']=='approved':
                if 'name' not in incoming or 'campaign_start' not in incoming:
                    response['status']='failed'
                    response['http_status']=412
                    if 'name' not in incoming :
                        response['message']='please enter the name field'
                        return response
                    if 'campaign_start' not in incoming:
                        response['message']='please enter the start date'
                        return response




    #     if a>=dt.now():
    #         response['http_status']=412
    #         response['status']='failed'
    #         response['message']='invalid campaign end date'
    #         return response
    # if 'campaign_start' in incoming:
    #     a=dt.strptime(str(incoming['campaign_start']), "%d-%m-%Y")
    #     if a>=dt.now():
    #         response['http_status']=412
    #         response['status']='failed'
    #         response['message']='invalid campaign start date'
    #         return response


    #end campaign date check
    if a_provider._exists==True:
        if 'campaign_start' in incoming:
            if incoming['campaign_start']!='null':
                start=dt.strptime(str(incoming['campaign_start']), "%d-%m-%Y")
            else:
                start=None
        elif a_provider.campaign_start!=None:
            start=dt.strptime(str(a_provider.campaign_start), "%Y-%m-%d")
        else:
            start=None

        if 'campaign_end' in incoming:

            if incoming['campaign_end'] !='null':
                end=dt.strptime(str(incoming['campaign_end']), "%d-%m-%Y")

            else:
                end=None
        elif a_provider.campaign_end!=None:
            end=dt.strptime(str(a_provider.campaign_end), "%Y-%m-%d")
        else:
            end=None

    else:
        if 'campaign_start' in incoming:
            if incoming['campaign_start']!='null':
                start=dt.strptime(str(incoming['campaign_start']), "%d-%m-%Y")
            else:
                start=None
        else :
            start=None

        if 'campaign_end' in incoming:
            if incoming['campaign_end']!='null':
                end=dt.strptime(str(incoming['campaign_end']), "%d-%m-%Y")
            else:
                end=None
        else :
            end=None

    if end!=None and start!=None:
        if end<start:
            response['http_status']=412
            response['message']='Campaign end date cannot be before campaign start date'
            response['status']='failed'
            return response


    a_provider.consume(incoming)

    saved_id=a_provider.save()

    if saved_id is None:
        response['status'] = 'failed'
        response['message'] = 'There was a problem with saving this record. Please check your request!'
        response['http_status'] = 412
        if a_provider._unique_error is True:
            response['message'] = 'Error saving record. Please check if duplicate provider!'
        if a_provider._token_id!=-1:
            response['token_id']=a_provider._token_id
    else:
        log_activity(endpoint='provider/keep/', activity_type=activity_type,
                    item_id=saved_id, item_type='provider',
                    request=request)
        beacon_redis_del(item_id=saved_id, item_type='provider')
        response['status'] = 'success'
        response['message'] = 'Record saved!'
        response['storage_id'] = saved_id

        if 'cover_image_name' in request.form:         #if in request
            if 'null' not in request.form['cover_image_name']:
                if change_cover_image_flag:
                    fileops.manage_delete(cover_image_path)
                    fileops.manage_delete(cover_image_path,config.IMAGE_RESOURCE_BUCKET)
                file_name=saved_id+"-cover-image"
                path=fileops.save_file(request.files['cover_image'],file_name,'/tmp')
                fileops.manage_upload(path,request.files['cover_image'].content_type,file_name)
                temp_link= fileops.manage_upload(path,request.files['cover_image'].content_type,file_name,config.IMAGE_RESOURCE_BUCKET)
                a_provider.cover_image=config.CDN_PREFIX+os.path.basename(temp_link)
            else:
                a_provider.cover_image=None
                fileops.manage_delete(cover_image_path)
                fileops.manage_delete(cover_image_path,config.IMAGE_RESOURCE_BUCKET)

        saved_id=a_provider.save()

    return response

def rain(incoming):
    response = {}

    if 'status' in incoming:
        all_provider_ids = enumerate_table('provider', {'status': incoming['status']})
    else:
        all_provider_ids = enumerate_table('provider')

    if len(all_provider_ids) == 0:
        response['status'] = 'failed'
        response['message'] = 'No relevant data found!'
    else:
        response['status'] = 'success'
        response['data'] = []
    #print str(dt.now())
    index=0
    for a_provider_id in all_provider_ids:
        a_provider = Provider(provider_id=a_provider_id)
        response['data'].append(deepcopy(a_provider.export()))
        if response['data'][index]['campaign_start'] !=None:
            response['data'][index]['campaign_start']=dt.strptime(response['data'][index]['campaign_start'], '%Y-%m-%d').strftime('%d-%m-%Y')

        if response['data'][index]['campaign_end'] !=None:
            response['data'][index]['campaign_end']=dt.strptime(response['data'][index]['campaign_end'], '%Y-%m-%d').strftime('%d-%m-%Y')
        index=index+1

    return response

def datatable_rain(incoming):

    table='provider'
    query_dict=generic_adapter(incoming,table)
    query=query_dict['complete_query']
    search_query=query_dict['search_query']
    search_value=query_dict['search_value']

    print query

    response = {}
    response['draw']=incoming['draw']
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)

    a_query='SELECT column_name FROM information_schema.columns WHERE table_name =%(table_name)s'
    cursor.execute(a_query,{'table_name':table})
    res=cursor.fetchall()

    id_position=0

    if res:
        for each in res:
            if each[0] == 'id':
                break
            else:
                id_position+=1


    cursor.execute(query,{'search':search_value})
    data=cursor.fetchall()
    response['data']=[]
    for row in data:
        a_provider= Provider(provider_id=str(row[id_position]))
        response['data'].append(deepcopy(a_provider.export()))

    cursor.execute(search_query,{'search':search_value})
    data=cursor.fetchall()

    f_count=0
    for row in data:
        f_count+=1
    response['recordsFiltered']=f_count


    cursor.execute("SELECT count(*) from "+table)
    totalcnt=cursor.fetchone()
    response['recordsTotal']=totalcnt[0]
    response['http_status']=200

    db.commit()
    db.close()
    return response

def validproviders(incoming):
    response = {}

    if 'status' in incoming:
        all_provider_ids = enumerate_table('provider', {'status': incoming['status']})
    else:
        all_provider_ids = enumerate_table('provider')

    if len(all_provider_ids) == 0:
        response['status'] = 'failed'
        response['message'] = 'No relevant data found!'
    else:
        response['status'] = 'success'
        response['data'] = []
    #print str(dt.now())

    for a_provider_id in all_provider_ids:
        a_provider = Provider(provider_id=a_provider_id)
        if a_provider.campaign_end !=None:
            a=dt.strptime(str(a_provider.campaign_end), "%Y-%m-%d")
            if a > dt.now():
                response['data'].append(deepcopy(a_provider.export()))
        else:
            response['data'].append(deepcopy(a_provider.export()))
    return response

def check_slug(slug_url):
    trim_slug = str(slug_url).strip()
    if trim_slug == "None" or trim_slug == "":
        response = {"http_status": 418,
                    "status":"failed",
                    "message":"please provide a value"}
        return response
    response={}
    response['http_status']=200
    response['status']='success'
    response['available'] = True
    db=wrappers.Postgres.init()
    cursor=wrappers.Postgres.get_cursor(db, 'dict')
    slug_url = slugify(slug_url)
    response['slug_url'] = slug_url
    query = "SELECT id from provider where slug_url=%(slug_url)s"
    cursor.execute(query, {'slug_url': slug_url})
    result = cursor.fetchone()
    if result is not None:
        response['available'] = False
    return response
