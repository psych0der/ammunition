import wrappers
from app.commons.Toolkit import enumerate_table
from models.Raw_content_registry import Raw_content_registry
import os
import upload_s3 as fileops
from copy import deepcopy
from flask import request
import json
from array import array
import config
import time
from datatable_adapter import generic_adapter


def fetch(raw_content_id, incoming):
    requested_raw= Raw_content_registry(raw_content_id=raw_content_id)
    response = {'exists' : False, 'status': 'failure'}

    if requested_raw._exists == True:
        response['exists'] = True
        response['status'] = 'success'
        response['data'] = requested_raw.export()
        if response['data']['remote_link']!=None:
            headers={}
            if requested_raw.media_type=='audio':
                headers={'response-content-type':'audio/mpeg3'}
            else:
                headers={'response-content-type':'video/mp4'}
            link_fragments = response['data']['remote_link'].split('/')
            response['data']['remote_link']=fileops.sign(bucket=config.RAW_CONTENT_BUCKET,path=link_fragments[-1],headers=headers)

    return response

def keep(raw_content_id, incoming, incoming_files):
    if raw_content_id == None:
        raw_content_id = '0'

    a_raw = Raw_content_registry(raw_content_id=raw_content_id)
    response = {}

    # a_raw.date_created= int(round(time.time() * 1000))
    # print str(a_raw.date_created)+"####"
    a_raw.consume(incoming)

    saved_id = a_raw.save()

    if saved_id is None:
        response['status'] = 'failed'
        response['message'] = 'There was a problem with saving this record. Please check your request!'
        response['http_status'] = 412
    else:
        response['status'] = 'success'
        response['message'] = 'Record saved!'
        response['storage_id'] = saved_id

    return response

def rain(incoming):
    response = {}
    #if 'status' in incoming:
    #all_raw_ids = enumerate_table('raw_content_registry', {'used': 'null'})
    #else:
    #all_raw_ids = enumerate_table('Raw_content_registry')
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db, 'default')
    a_query ="SELECT id FROM raw_content_registry WHERE NOT used = %(used)s"
    cursor.execute(a_query,{'used':'yes'})
    res=cursor.fetchall()
    all_raw_ids=[]
    for item in res:
        all_raw_ids.append(item[0])

    if len(all_raw_ids) == 0:
        response['status'] = 'failed'
        response['message'] = 'No relevant data found!'
    else:
        response['status'] = 'success'
        response['data'] = []

    index=0
    for a_raw_id in all_raw_ids:
        a_raw = Raw_content_registry(raw_content_id=str(a_raw_id))
        response['data'].append(deepcopy(a_raw.export()))
        if response['data'][index]['remote_link']!=None:
            headers={}
            if a_raw.media_type=='audio':
                headers={'response-content-type':'audio/mpeg3'}
            else:
                headers={'response-content-type':'video/mp4'}
            link_fragments = response['data'][index]['remote_link'].split('/')

            response['data'][index]['remote_link']=fileops.sign(bucket=config.RAW_CONTENT_BUCKET,path=link_fragments[-1],headers=headers)
        index=index+1

    return response

def datatable_rain(incoming):
    table='raw_content_registry'
    query_dict=generic_adapter(incoming,table)
    query=query_dict['complete_query']
    search_query=query_dict['search_query']
    search_value = query_dict['search_value']

    print query

    response = {}
    response['draw']=incoming['draw']
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)

    a_query='SELECT column_name FROM information_schema.columns WHERE table_name =%(table_name)s'
    cursor.execute(a_query,{'table_name':table})
    res=cursor.fetchall()

    id_position=0

    if res:
        for each in res:
            if each[0] == 'id':
                break
            else:
                id_position+=1
    print query
    print search_query
    cursor.execute(query,{'search':search_value})
    data=cursor.fetchall()
    response['data']=[]
    for row in data:
        a_raw_content= Raw_content_registry(raw_content_id=str(row[id_position]))
        response['data'].append(deepcopy(a_raw_content.export()))

    cursor.execute(search_query,{'search':search_value})
    data=cursor.fetchall()
    print "-------", query
    print "-------", search_query
    f_count=0
    for row in data:
        f_count+=1
    response['recordsFiltered']=f_count


    cursor.execute("SELECT count(*) from "+table+" where used='no'")
    totalcnt=cursor.fetchone()
    print '======>>>>>>', totalcnt[0]
    response['recordsTotal']=totalcnt[0]
    response['http_status']=200

    db.commit()
    db.close()
    return response

def unhook(raw_content_id):
    a_raw=Raw_content_registry(raw_content_id=raw_content_id)
    item_id=a_raw.delete()
    ext='.mp4'if a_raw.media_type=='video' else '.mp3'
    filename=a_raw.id+ext
    fileops.manage_delete(file_path=filename,bucket_name=config.RAW_CONTENT_BUCKET)
    response={}
    if item_id is None:
        response['status'] = 'failed'
        response['message'] = 'There was a problem with saving this record. Please check your request!'
        response['http_status'] = 412
    else:
        response['status'] = 'success'
        response['message'] = 'Record deleted!'
    return response
