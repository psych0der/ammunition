import wrappers
from app.commons.Toolkit import enumerate_table
from models.Sponsor import Sponsor
import ads_map
import os
import upload_s3 as fileops
from copy import deepcopy
from flask import request
import json
from array import array
import time
import config
from jobs import celery_tasks as tasks
from datatable_adapter import generic_adapter
from slugify import slugify
from app.commons.modules.userlogs import log_activity
from app.commons.modules.beacon import beacon_redis_del


def fetch(sponsor_id, incoming):
    requested_sponsor = Sponsor(sponsor_id=sponsor_id)
    response = {'exists' : False, 'status': 'failure'}

    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)

    if requested_sponsor._exists == True:
        response['exists'] = True
        response['status'] = 'success'
        response['data'] = requested_sponsor.export()
        response['data']['apk_link']=response['data']['apk']
        response['data']['logo_favicon'] = None
        response['data']['image_favicon'] = None
        response['data']['cover_image_favicon'] = None

        if response['data']['logo']!=None:
            response['data']['logo']=str(response['data']['logo'])+'?T='+str(int(round(time.time()*1000)))
            response['data']['logo_favicon'] = str(response['data']['logo']).replace('cloudfront.net/','cloudfront.net/small/')

        if response['data']['image']!=None:
            response['data']['image']=str(response['data']['image'])+'?T='+str(int(round(time.time()*1000)))
            response['data']['image_favicon'] = str(response['data']['image']).replace('cloudfront.net/','cloudfront.net/small/')

        if response['data']['cover_image']!=None:
            response['data']['cover_image']=str(response['data']['cover_image'])+'?T='+str(int(round(time.time()*1000)))
            response['data']['cover_image_favicon'] = str(response['data']['cover_image']).replace('cloudfront.net/','cloudfront.net/small/')

        cursor.execute("SELECT ad_id,resource_location,id from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s",{'sponsor_id':int(requested_sponsor.id.split('-')[-1]),'ad_type':'preroll'})
        preroll_data=cursor.fetchall()
        cursor.execute("SELECT ad_id,resource_location,id from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s",{'sponsor_id':int(requested_sponsor.id.split('-')[-1]),'ad_type':'splash'})
        splash_data=cursor.fetchall()
        cursor.execute("SELECT ad_id,resource_location,id from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s",{'sponsor_id':int(requested_sponsor.id.split('-')[-1]),'ad_type':'image_ad'})
        image_ad_data=cursor.fetchall()

        preroll=[]
        preroll_global={}
        for k in preroll_data:
            preroll.append({sponsor_id+'-'+str(k[0]):k[1]})
            preroll_global['pp-pre-roll-'+sponsor_id.split('-')[-1]+'-'+str(k[2])]=k[1]
        response['data']['preroll']=preroll
        response['data']['preroll_global']=preroll_global

        splash=[]
        splash_global={}
        for k in splash_data:
            splash.append({sponsor_id+'-'+str(k[0]):k[1]})
            splash_global['pp-splash-'+sponsor_id.split('-')[-1]+'-'+str(k[2])]=k[1]
        response['data']['splash']=splash
        response['data']['splash_global']=splash_global

        image_ads=[]
        image_ad_global={}
        for k in image_ad_data:
            image_ads.append({sponsor_id+'-'+str(k[0]):k[1]})
            image_ad_global['pp-image-ad-'+sponsor_id.split('-')[-1]+'-'+str(k[2])]=k[1]
        response['data']['image_ads']=image_ads
        response['data']['image_ad_global']=image_ad_global

    db.commit()
    db.close()

    return response

def keep(sponsor_id, incoming, incoming_files):
    URL_UNSAFE=[ '<', '>' ,'#' ,'%' ,'{', '}' ,'|' ,'\\' ,'^','~', '[' ,']' ,'`',' ','(',')']
    activity_type = 'edit'
    if sponsor_id == None:
        sponsor_id = '0'
        activity_type = 'new'
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    change_logo_flag=0
    change_image_flag=0
    change_apk_flag=0
    change_cover_image_flag=0
    response = {}
    a_sponsor = Sponsor(sponsor_id=sponsor_id)

    #to get the value of link before they are made None in consume()
    try:
        logo_path=a_sponsor.logo
        change_logo_flag=1
    except AttributeError:
        logo_path=None

    try:
        image_path=a_sponsor.image
        change_image_flag=1
    except AttributeError:
        image_path=None

    try:
        cover_image_path=a_sponsor.image
        change_cover_image_flag=1
    except AttributeError:
        cover_image_path=None

    try:
        apk_path=a_sponsor.apk
        change_apk_flag=1
    except AttributeError:
        apk_path=None

    a_sponsor.consume(incoming)

    if a_sponsor._exists is True:
        a_sponsor.date_modified= int(round(time.time() * 1000))

        if 'status' in incoming :
            if request.form['status']=='complete' or request.form['status']=='approved':

                if a_sponsor.logo is None or a_sponsor.name is None or a_sponsor.tagline is None:
                    response['status']='failed'
                    response['http_status']=412

                    if a_sponsor.logo is None:
                        response['message']='Please add a logo'
                        return response
                    elif a_sponsor.name is None:
                        response['message']='Please add name'
                        return response
                    elif a_sponsor.tagline is None:
                        response['message']='Please add tagline'
                        return response
    else:
        a_sponsor.date_created= int(round(time.time() * 1000))

        if 'status' in incoming :
            if request.form['status']=='complete' or request.form['status']=='approved':
                if 'logo_name' not in incoming or a_sponsor.name is None or a_sponsor.tagline is None:
                    response['status']='failed'
                    response['http_status']=412

                    if 'logo_name' not in incoming:
                        response['message']='Please add a logo'
                        return response
                    elif a_sponsor.name is None:
                        response['message']='Please add name'
                        return response
                    elif a_sponsor.tagline is None:
                        response['message']='Please add tagline'
                        return response

    if 'app_data' in incoming:
        data=json.loads(incoming['app_data'])
        if 'package_android' in data:
            a_sponsor.app_package=data['package_android']
        if 'package_ios' in data:
            a_sponsor.app_package_ios=data['package_ios']
        if 'remote_android' in data:
            a_sponsor.play_store_url=data['remote_android']
        if 'remote_ios' in data:
            a_sponsor.ios_store_url =data['remote_ios']


    if 'apk_filename' in incoming:
        if incoming['apk_filename']=='':
            response['http_status']=412
            response['status']='failed'
            response['message']='please enter a valid filename'
            return response
        if 'null' not in incoming['apk_filename']:
            if change_apk_flag:
                fileops.manage_delete(apk_path)
            apk_name=incoming['apk_filename'].split('.')[0]
            for each in URL_UNSAFE:
                apk_name=apk_name.replace(each,"-")
            if incoming['apk_filename'].split('.')[1]!='apk':
                response['http_status']=412
                response['status']='failed'
                response['message']='Not a valid apk file'
                return response
            path=fileops.save_file(request.files['apk_file'],apk_name,'/tmp')
            temp_link=fileops.manage_upload(path,request.files['apk_file'].content_type,apk_name)
            a_sponsor.apk=config.CDN_PREFIX+os.path.basename(temp_link)

        else:
            a_sponsor.apk=None
            fileops.manage_delete(apk_path)


    saved_id = a_sponsor.save()

    if saved_id is None:
        response['status'] = 'failed'
        response['message'] = 'There was a problem with saving this record. Please check your request!'
        response['http_status'] = 412
        if a_sponsor._unique_error is True:
            response['message'] = 'Error saving record. Please check if duplicate sponsor!'
        if a_sponsor._token_id!=-1:
            response['token_id']=a_sponsor._token_id
        return response
    else:
        response['status'] = 'success'
        response['message'] = 'Record saved!'
        response['storage_id'] = saved_id

        #a_same_sponsor=Sponsor(sponsor_id=saved_id)

        if 'logo_name' in request.form:         #if logo_name is in request
            if 'null' not in request.form['logo_name']:
                if change_logo_flag:
                    #print "deleting logo"
                    fileops.manage_delete(logo_path)
                    fileops.manage_delete(logo_path,config.IMAGE_RESOURCE_BUCKET)

                file_name=saved_id+"-logo"
                path=fileops.save_file(request.files['logo'],file_name,'/tmp')
                fileops.manage_upload(path,request.files['logo'].content_type,file_name)
                temp_link= fileops.manage_upload(path,request.files['logo'].content_type,file_name,config.IMAGE_RESOURCE_BUCKET)
                a_sponsor.logo=config.CDN_PREFIX+os.path.basename(temp_link)
            else:
                #print "deleting logo"
                a_sponsor.logo=None
                fileops.manage_delete(logo_path)
                fileops.manage_delete(logo_path,config.IMAGE_RESOURCE_BUCKET)



        if 'image_name' in request.form:         #if in request
            if 'null' not in request.form['image_name']:
                if change_image_flag:
                    fileops.manage_delete(image_path)
                    fileops.manage_delete(image_path,config.IMAGE_RESOURCE_BUCKET)
                file_name=saved_id+"-image"
                path=fileops.save_file(request.files['image'],file_name,'/tmp')
                fileops.manage_upload(path,request.files['image'].content_type,file_name)
                temp_link= fileops.manage_upload(path,request.files['image'].content_type,file_name,config.IMAGE_RESOURCE_BUCKET)
                a_sponsor.image=config.CDN_PREFIX+os.path.basename(temp_link)
            else:
                a_sponsor.image=None
                fileops.manage_delete(image_path)
                fileops.manage_delete(image_path,config.IMAGE_RESOURCE_BUCKET)


        if 'cover_image_name' in request.form:         #if in request
            if 'null' not in request.form['cover_image_name']:
                if change_cover_image_flag:
                    fileops.manage_delete(cover_image_path)
                    fileops.manage_delete(cover_image_path,config.IMAGE_RESOURCE_BUCKET)
                file_name=saved_id+"-cover-image"
                path=fileops.save_file(request.files['cover_image'],file_name,'/tmp')
                fileops.manage_upload(path,request.files['cover_image'].content_type,file_name)
                temp_link= fileops.manage_upload(path,request.files['cover_image'].content_type,file_name,config.IMAGE_RESOURCE_BUCKET)
                a_sponsor.cover_image=config.CDN_PREFIX+os.path.basename(temp_link)
            else:
                a_sponsor.cover_image=None
                fileops.manage_delete(cover_image_path)
                fileops.manage_delete(cover_image_path,config.IMAGE_RESOURCE_BUCKET)

        preroll=[]

        if 'preroll_names' in incoming :
            m=json.loads(request.form['preroll_names'])
            max_size=0
            for elem in m:
                size=int(elem.split('-')[1])
                if size>max_size:
                    max_size=size
            temp_file = [None] * (max_size+1)
            preroll== [None] * (max_size+1)
            for key in m:
                index=key.split('-')[1]
                cursor.execute("SELECT ad_id,resource_location,id from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s",{'sponsor_id':int(a_sponsor.sponsor_id.split('-')[-1]),'ad_type':'preroll'})
                result=cursor.fetchall()
                file_s3_path=''
                flag=0
                for entry in result:
                    if int(index) == entry[0]:
                        file_s3_path=entry[1]
                        channel_feed_ad_id=entry[2]
                        flag=1
                        break

                if flag:        #if the entry exists ..means update
                    cursor.execute("SELECT * from channel_feed where item_id=%(item_id)s",{'item_id':channel_feed_ad_id})
                    res=cursor.fetchone()
                    if res is None:
                        if m[key] == '':            #condition for updating the database
                            fileops.manage_delete(file_path=file_s3_path)    #removing from s3
                            temp_file[int(index)]=''
                        else:
                            file_name=saved_id+"-"+key.split('-')[0]+'-'+m[key].split('.')[0]
                            for each in URL_UNSAFE:
                                file_name=file_name.replace(each,"-")
                            path=fileops.save_file(request.files['preroll-'+index],file_name,config.MEDIA_TMP_LOCATION)
                            temp_link=fileops.manage_upload(path,request.files['preroll-'+index].content_type,file_name)
                            temp=config.CDN_PREFIX+os.path.basename(temp_link)
                            basename, ext = os.path.splitext(path)
                            tasks.video_converter.apply_async((path, basename+'-converted'+ext),{'target_bucket': config.S3_CONTENT_RESOURCES_BUCKET},queue=config.CELERY_QUEUE)
                            temp_file[int(index)]=temp
                            fileops.manage_delete(file_path=file_s3_path)    #removing from s3

                else:
                    #print "else mai hu"+index                              #if file is to be inserted
                    file_name=saved_id+"-"+key.split('-')[0]+'-'+m[key].split('.')[0]
                    for each in URL_UNSAFE:
                        file_name=file_name.replace(each,"-")
                    path=fileops.save_file(request.files['preroll-'+index],file_name,config.MEDIA_TMP_LOCATION)
                    temp_link=fileops.manage_upload(path,request.files['preroll-'+index].content_type,file_name)
                    temp=config.CDN_PREFIX+os.path.basename(temp_link)
                    basename, ext = os.path.splitext(path)
                    tasks.video_converter.apply_async((path, basename+'-converted'+ext),{'target_bucket': config.S3_CONTENT_RESOURCES_BUCKET},queue=config.CELERY_QUEUE)
                    #print temp

                    temp_file[int(index)]=temp #array to put in ascending order
                    #print temp_file
            ctr=0

            preroll=temp_file


        splash=[]
        if 'splash_names' in incoming :
            m=json.loads(request.form['splash_names'])
            max_size=0
            for elem in m:
                size=int(elem.split('-')[1])
                if size>max_size:
                    max_size=size
            temp_file = [None] * (max_size+1)
            splash== [None] * (max_size+1)
            for key in m:
                index=key.split('-')[1]
                cursor.execute("SELECT ad_id,resource_location,id from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s",{'sponsor_id':int(a_sponsor.sponsor_id.split('-')[-1]),'ad_type':'splash'})
                result=cursor.fetchall()
                file_s3_path=''
                flag=0
                for entry in result:
                    if int(index) == entry[0]:
                        file_s3_path=entry[1]
                        channel_feed_ad_id=entry[2]
                        flag=1
                        break

                if flag:        #if the entry exists ..means update
                    cursor.execute("SELECT * from channel_feed where item_id=%(item_id)s",{'item_id':channel_feed_ad_id})
                    res=cursor.fetchone()
                    if res is None:
                        if m[key] == '':            #condition for updating the database
                            fileops.manage_delete(file_path=file_s3_path)    #removing from s3
                            temp_file[int(index)]=''
                        else:
                            file_name=saved_id+"-"+key.split('-')[0]+'-'+m[key].split('.')[0]
                            for each in URL_UNSAFE:
                                file_name=file_name.replace(each,"-")
                            path=fileops.save_file(request.files['splash-'+index],file_name,config.MEDIA_TMP_LOCATION)
                            temp_link=fileops.manage_upload(path,request.files['splash-'+index].content_type,file_name)
                            temp=config.CDN_PREFIX+os.path.basename(temp_link)

                            basename, ext = os.path.splitext(path)
                            tasks.video_converter.apply_async((path, basename+'-converted'+ext),{'target_bucket': config.S3_CONTENT_RESOURCES_BUCKET},queue=config.CELERY_QUEUE)
                            temp_file[int(index)]=temp
                            fileops.manage_delete(file_path=file_s3_path)    #removing from s3


                else:
                    file_name=saved_id+"-"+key.split('-')[0]+'-'+m[key].split('.')[0]           #file name
                    for each in URL_UNSAFE:
                        file_name=file_name.replace(each,"-")
                    path=fileops.save_file(request.files['splash-'+index],file_name,config.MEDIA_TMP_LOCATION)
                    temp_link=fileops.manage_upload(path,request.files['splash-'+index].content_type,file_name)
                    temp=config.CDN_PREFIX+os.path.basename(temp_link)
                    basename, ext = os.path.splitext(path)
                    tasks.video_converter.apply_async((path, basename+'-converted'+ext),{'target_bucket': config.S3_CONTENT_RESOURCES_BUCKET},queue=config.CELERY_QUEUE)
                    temp_file[int(index)]=temp #array to put in ascending order
            splash=temp_file

        image_ad=[]
        if 'image_ad_names' in incoming :
            m=json.loads(request.form['image_ad_names'])
            max_size=0
            for elem in m:
                size=int(elem.split('-')[1])
                if size>max_size:
                    max_size=size
            temp_file = [None] * (max_size+1)
            image_ad== [None] * (max_size+1)
            for key in m:
                index=key.split('-')[1]
                cursor.execute("SELECT ad_id,resource_location,id from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s",{'sponsor_id':int(a_sponsor.sponsor_id.split('-')[-1]),'ad_type':'image_ad'})
                result=cursor.fetchall()
                file_s3_path=''
                flag=0
                for entry in result:
                    if int(index) == entry[0]:
                        file_s3_path=entry[1]
                        channel_feed_ad_id=entry[2]
                        flag=1
                        break

                if flag:        #if the entry exists ..means update
                    cursor.execute("SELECT * from channel_feed where item_id=%(item_id)s",{'item_id':channel_feed_ad_id})
                    res=cursor.fetchone()
                    if res is None:
                        if m[key] == '':            #condition for updating the database
                            fileops.manage_delete(file_path=file_s3_path)    #removing from s3
                            fileops.manage_delete(file_s3_path,config.IMAGE_RESOURCE_BUCKET)
                            temp_file[int(index)]=''
                        else:
                            file_name=saved_id+"-"+key.split('-')[0]+'-'+m[key].split('.')[0]
                            for each in URL_UNSAFE:
                                file_name=file_name.replace(each,"-")
                            path=fileops.save_file(request.files['image_ad-'+index],file_name,'/tmp')
                            fileops.manage_upload(path,request.files['image_ad-'+index].content_type,file_name)
                            temp_link=fileops.manage_upload(path,request.files['image_ad-'+index].content_type,file_name,config.IMAGE_RESOURCE_BUCKET)
                            temp=config.CDN_PREFIX+os.path.basename(temp_link)
                            temp_file[int(index)]=temp
                            fileops.manage_delete(file_path=file_s3_path)    #removing from s3
                            fileops.manage_delete(file_s3_path,config.IMAGE_RESOURCE_BUCKET)

                else:
                    file_name=saved_id+"-"+key.split('-')[0]+'-'+m[key].split('.')[0]           #file name
                    for each in URL_UNSAFE:
                        file_name=file_name.replace(each,"-")
                    path=fileops.save_file(request.files['image_ad-'+index],file_name,'/tmp')
                    fileops.manage_upload(path,request.files['image_ad-'+index].content_type,file_name)
                    temp_link=fileops.manage_upload(path,request.files['image_ad-'+index].content_type,file_name,config.IMAGE_RESOURCE_BUCKET)
                    temp=config.CDN_PREFIX+os.path.basename(temp_link)
                    temp_file[int(index)]=temp #array to put in ascending order

            image_ad=temp_file

        saved_id=a_sponsor.save()
        ads_map.keep(preroll,image_ad,splash,saved_id,a_sponsor.export(),a_sponsor._exists)   #mapping after successful storage

    if saved_id is None:
        response['status'] = 'failed'
        response['message'] = 'There was a problem with saving this record. Please check your request!'
        response['http_status'] = 412
        if a_sponsor._unique_error is True:
            response['message'] = 'Error saving record. Please check if duplicate sponsor!'
        if a_sponsor._token_id!=-1:
            response['token_id']=a_sponsor._token_id
    else:
        log_activity(endpoint='sponsor/keep/', activity_type=activity_type,
                    item_id=saved_id, item_type='sponsor',
                    request=request)
        beacon_redis_del(item_id=saved_id, item_type='sponsor')
        response['status'] = 'success'
        response['message'] = 'Record saved!'
        response['storage_id'] = saved_id
    db.commit()
    db.close()

    return response

def rain(incoming):
    response = {}

    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)

    if 'status' in incoming:
        all_sponsor_ids = enumerate_table('sponsors', {'status': incoming['status']})
        if incoming['status'] == 'complete':
            all_sponsor_ids +=enumerate_table('sponsors', {'status': 'approved'})
    else:
        all_sponsor_ids = enumerate_table('sponsors')

    if len(all_sponsor_ids) == 0:
        response['status'] = 'failed'
        response['message'] = 'No relevant data found!'
    else:
        response['status'] = 'success'
        response['data'] = []

    index=0
    for a_sponsor_id in all_sponsor_ids:
        a_sponsor = Sponsor(sponsor_id=a_sponsor_id)
        response['data'].append(deepcopy(a_sponsor.export()))
        response['data'][index]['apk_link']=response['data'][index]['apk']
        if response['data'][index]['logo']!=None:
            response['data'][index]['logo']=str(response['data'][index]['logo'])+'?T='+str(int(round(time.time()*1000)))

        if response['data'][index]['image']!=None:
            response['data'][index]['image']=str(response['data'][index]['image'])+'?T='+str(int(round(time.time()*1000)))

        cursor.execute("SELECT ad_id,resource_location,id from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s",{'sponsor_id':int(a_sponsor.id.split('-')[-1]),'ad_type':'preroll'})
        preroll_data=cursor.fetchall()
        cursor.execute("SELECT ad_id,resource_location,id from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s",{'sponsor_id':int(a_sponsor.id.split('-')[-1]),'ad_type':'splash'})
        splash_data=cursor.fetchall()
        cursor.execute("SELECT ad_id,resource_location,id from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s",{'sponsor_id':int(a_sponsor.id.split('-')[-1]),'ad_type':'image_ad'})
        image_ad_data=cursor.fetchall()

        preroll=[]
        preroll_global={}
        for k in preroll_data:
            preroll.append({'pp-sponsor-'+a_sponsor_id+'-'+str(k[0]):k[1]})
            preroll_global['pp-pre-roll-'+a_sponsor_id+'-'+str(k[2])]=k[1]
        response['data'][index]['preroll']=preroll
        response['data'][index]['preroll_global']=preroll_global

        splash=[]
        splash_global={}
        for k in splash_data:
            splash.append({'pp-sponsor-'+a_sponsor_id+'-'+str(k[0]):k[1]})
            splash_global['pp-splash-'+a_sponsor_id+'-'+str(k[2])]=k[1]
        response['data'][index]['splash']=splash
        response['data'][index]['splash_global']=splash_global

        image_ads=[]
        image_ads_global={}
        for k in image_ad_data:
            image_ads.append({'pp-sponsor-'+a_sponsor_id+'-'+str(k[0]):k[1]})
            image_ads_global['pp-image-ad-'+a_sponsor_id+'-'+str(k[2])]=k[1]
        response['data'][index]['image_ads']=image_ads
        response['data'][index]['image_ads_global']=image_ads_global
        index=index+1

    db.commit()
    db.close()
    return response

def datatable_rain(incoming):

    table='sponsors'
    query_dict=generic_adapter(incoming,table)
    query=query_dict['complete_query']
    search_query=query_dict['search_query']
    search_value=query_dict['search_value']

    print query

    response = {}
    response['draw']=incoming['draw']
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)

    a_query='SELECT column_name FROM information_schema.columns WHERE table_name =%(table_name)s'
    cursor.execute(a_query,{'table_name':table})
    res=cursor.fetchall()

    id_position=0

    if res:
        for each in res:
            if each[0] == 'id':
                break
            else:
                id_position+=1


    cursor.execute(query,{'search':search_value})
    data=cursor.fetchall()
    response['data']=[]
    for row in data:
        a_sponsor= Sponsor(sponsor_id=str(row[id_position]))
        response['data'].append(deepcopy(a_sponsor.export()))

    cursor.execute(search_query,{'search':search_value})
    data=cursor.fetchall()

    f_count=0
    for row in data:
        f_count+=1
    response['recordsFiltered']=f_count


    cursor.execute("SELECT count(*) from "+table)
    totalcnt=cursor.fetchone()
    response['recordsTotal']=totalcnt[0]
    response['http_status']=200

    db.commit()
    db.close()
    return response


def batch_fetch(field_csv, id_csv, ad_type):
    response={}
    response['http_status']=200
    response['status']='success'
    db=wrappers.Postgres.init()
    data = {}
    cursor=wrappers.Postgres.get_cursor(db, 'dict')
    id_list = id_csv.split(',')
    field_list = field_csv.split(',')

    if len(id_list) > 0 and len(field_list) > 0:
        field_list = set(field_list)
        field_list.add('id')
        id_list = set(id_list)
        join_id = ", ".join(map(str, id_list))

        query = "SELECT %s from sponsors where id in (%s)" % (", ".join(field_list),  join_id)
        cursor.execute(query)
        results = cursor.fetchall()

        for row in results:
            row_obj = dict(row)
            key = "pp-sponsor-" + str(row_obj['id'])
            if 'logo' in row_obj:
                row_obj['logo_favicon'] = None
                if row_obj['logo'] != None:
                    row_obj['logo_favicon'] = str(row_obj['logo']).replace('cloudfront.net/','cloudfront.net/small/')
            if 'image' in row_obj:
                row_obj['image_favicon'] = None
                if row_obj['image'] != None:
                    row_obj['image_favicon'] = str(row_obj['image']).replace('cloudfront.net/','cloudfront.net/small/')
            data[key] = row_obj

        if ad_type is not None and ad_type != "":
            ad_type = set(ad_type.split(','))

            if 'all' in ad_type:
                query = "SELECT resource_location, ad_id, ad_type, sponsor_id from ads_map where sponsor_id in (%s)" % (join_id)
            else:
                ad_type = ["'%s'" % x for x in ad_type]
                query = "SELECT resource_location, ad_id, ad_type, sponsor_id from ads_map where sponsor_id in (%s) and ad_type in (%s)" % (join_id, ", ".join(ad_type))

            cursor.execute(query)
            results = cursor.fetchall()

            for row in results:
                row_obj = dict(row)
                key = "pp-sponsor-" + str(row_obj['sponsor_id'])
                ad_type = row_obj['ad_type']
                row_obj['resource_location_favicon'] = None
                if ad_type == 'image_ad' and row_obj['resource_location'] != None:
                        row_obj['resource_location_favicon'] = str(row_obj['resource_location']).replace('cloudfront.net/','cloudfront.net/small/')

                ad_key = key + "-" + str(row_obj['ad_id'])
                if key in data:
                    if ad_type not in data[key]:
                        data[key][ad_type] = {}
                    data[key][ad_type][ad_key] = row_obj['resource_location']

    response['data'] = data
    return response


def check_slug(slug_url):
    trim_slug = str(slug_url).strip()
    if trim_slug == "None" or trim_slug == "":
        response = {"http_status": 418,
                    "status":"failed",
                    "message":"please provide a value"}
        return response
    response={}
    response['http_status']=200
    response['status']='success'
    response['available'] = True
    db=wrappers.Postgres.init()
    cursor=wrappers.Postgres.get_cursor(db, 'dict')
    slug_url = slugify(slug_url)
    response['slug_url'] = slug_url
    query = "SELECT id from sponsors where slug_url=%(slug_url)s"
    cursor.execute(query, {'slug_url': slug_url})
    result = cursor.fetchone()
    if result is not None:
        response['available'] = False
    return response


def sponsor_mapping(version_id):
    response = {'http_status': 200,
                'status': 'success'}
    if version_id not in [None, '', 'null']:
        if version_id == 'live':
            query = "SELECT version_data from version_store where live='yes' and environment='internet'"
        else:
            vid = str(version_id).split('-')[-1]
            query = "SELECT version_data from version_store where id=" + vid
        db=wrappers.Postgres.init()
        cursor=wrappers.Postgres.get_cursor(db, 'dict')
        cursor.execute(query)
        result = cursor.fetchone()
        if result is None:
            response['http_status'] = 412
            response['status'] = 'failed'
            response['message'] = "Could not fetch data!"
        else:
            version_data = result['version_data']
            channel_feed = version_data['channel-feed']
            data = {}
            sponsor_set = set()
            channel_set = set()
            for channel in channel_feed:
                channel_id = str(channel['channel_id']).split('-')[-1]
                channel_set.add(channel_id)
                data[channel_id] = {'top_bar': None,
                                    'preroll': [],
                                    'image_ad': []}
                if 'sellable_entities' in channel:
                    if 'top-bar-sponsor' in channel['sellable_entities']:
                        sponsor_id = str(channel['sellable_entities']['top-bar-sponsor']).split('-')[-1]
                        data[channel_id]['top_bar'] = sponsor_id
                        sponsor_set.add(sponsor_id)

                    if 'preroll' in channel['sellable_entities']:
                        for preroll in channel['sellable_entities']['preroll']:
                            sponsor_id = str(preroll['sponsor']).split('-')[-1]
                            sponsor_set.add(sponsor_id)
                            data[channel_id]['preroll'].append(sponsor_id)

                for content in channel['content']:
                    if content['type'] == 'image_ad':
                        sponsor_id = str(content['sponsor_id']).split('-')[-1]
                        sponsor_set.add(sponsor_id)
                        data[channel_id]['image_ad'].append(sponsor_id)

            if len(channel_set) > 0 and len(sponsor_set) > 0:
                sponsorquery = "select id, name from sponsors where id in (%s)" % (", ".join(sponsor_set))
                cursor.execute(sponsorquery)
                results = cursor.fetchall()
                sponsor_object = {}
                for sponsor in results:
                    sponsor_object[str(sponsor['id'])] = sponsor['name']

                channelquery = "select id, name from channels where id in (%s)" % (", ".join(channel_set))
                cursor.execute(channelquery)
                results = cursor.fetchall()
                channel_object = {}
                for channel in results:
                    channel_object[str(channel['id'])] = channel['name']

                channel_response = []
                for key, value in data.iteritems():
                    value['name'] = channel_object[key]
                    if value['top_bar'] is not None:
                        sponsor_name = sponsor_object[value['top_bar']]
                        value['top_bar'] = sponsor_name

                    preroll_list = []
                    for sponsor in value['preroll']:
                        sponsor_name = sponsor_object[sponsor]
                        preroll_list.append(sponsor_name)
                    value['preroll'] = preroll_list

                    image_ad_list = []
                    for sponsor in value['image_ad']:
                        sponsor_name = sponsor_object[sponsor]
                        image_ad_list.append(sponsor_name)
                    value['image_ad'] = image_ad_list
                    channel_response.append(value)

                response['data'] = {'channel': channel_response}

            else:
                response['http_status'] = 412
                response['status'] = 'failed'
                response['message'] = "Could not fetch data!"
    else:
        response['http_status'] = 412
        response['status'] = 'failed'
        response['message'] = "Please provide valid version id!"

    return response
