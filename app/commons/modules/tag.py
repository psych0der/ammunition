import wrappers
from app.commons.Toolkit import enumerate_table
from models.Tag import Tag
from models.Channel import Channel
from models.Content import Content
import os
import upload_s3 as fileops
from copy import deepcopy
from flask import request
import config
import time
from datatable_adapter import generic_adapter
from app.commons.modules.userlogs import log_activity
from app.commons.modules.beacon import beacon_redis_del

def fetch(tag_id, incoming):
    requested_tag = Tag(tag_id=tag_id)
    response = {'exists' : False, 'status': 'failure'}
    if requested_tag._exists == True:
        response['exists'] = True
        response['status'] = 'success'
        response['data'] = requested_tag.export()
        response['data']['poster_favicon'] = None
        response['data']['poster_small_favicon'] = None
        response['data']['cover_image_favicon'] = None
        if response['data']['poster'] is not None:
            response['data']['poster_favicon'] = str(response['data']['poster']).replace('cloudfront.net/','cloudfront.net/small/')
        if response['data']['poster_small'] is not None:
            response['data']['poster_small_favicon'] = str(response['data']['poster_small']).replace('cloudfront.net/','cloudfront.net/small/')
        if response['data']['cover_image'] is not None:
            response['data']['cover_image_favicon'] = str(response['data']['cover_image']).replace('cloudfront.net/','cloudfront.net/small/')

    return response

def keep(tag_id, incoming, incoming_files):
    URL_UNSAFE=[ '<', '>' ,'#' ,'%' ,'{', '}' ,'|' ,'\\' ,'^','~', '[' ,']' ,'`',' ','(',')']
    activity_type = 'edit'
    if tag_id == None:
        tag_id = '0'
        activity_type = 'new'
    change_poster_flag=0
    change_poster_small_flag=0
    change_cover_image_flag=0
    response = {}
    a_tag = Tag(tag_id=tag_id)

    try:        #to get the value of link before they are made None in consume()
        poster_path=a_tag.poster
        change_poster_flag=1
    except AttributeError:
        poster_path=None

    try:
        poster_small_path=a_tag.poster_small
        change_poster_small_flag=1
    except AttributeError:
        poster_small_path=None

    try:
        cover_image_path=a_tag.cover_image
        change_cover_image_flag=1
    except AttributeError:
        cover_image_path=None

    if a_tag._exists==True:
        a_tag.date_modified= int(round(time.time() * 1000))
    else:
        a_tag.date_created= int(round(time.time() * 1000))

    a_tag.consume(incoming)

    #sanity check for approved data
    if 'status' in incoming :
        if request.form['status']=='complete' or request.form['status']=='approved':
            if a_tag.poster is None or a_tag.poster_small is None:
                response['status']='failed'
                response['message']='poster or small poster not added'
                response['http_status']=412
                return response

    if 'poster_name' in request.form:         #if logo_name is in request
        if 'null' not in request.form['poster_name']:
            if change_poster_flag:
                fileops.manage_delete(poster_path)
                fileops.manage_delete(poster_path,config.IMAGE_RESOURCE_BUCKET)
            tag_name=request.form['poster_name'].split('.')[0]
            for each in URL_UNSAFE:
                tag_name=tag_name.replace(each,"-")
            file_name="tag-poster-"+tag_name  #get part before .png/.avi
            path=fileops.save_file(request.files['poster'],file_name,'/tmp')
            fileops.manage_upload(path,request.files['poster'].content_type,file_name)
            temp_link= fileops.manage_upload(path,request.files['poster'].content_type,file_name,config.IMAGE_RESOURCE_BUCKET)
            a_tag.poster =config.CDN_PREFIX+os.path.basename(temp_link)
        else:
            a_tag.poster=None
            fileops.manage_delete(poster_path)
            fileops.manage_delete(poster_path,config.IMAGE_RESOURCE_BUCKET)

    if 'poster_small_name' in request.form:         #if logo_name is in request
        if 'null' not in request.form['poster_small_name']:
            if change_poster_small_flag:
                fileops.manage_delete(poster_small_path)
                fileops.manage_delete(poster_small_path,config.IMAGE_RESOURCE_BUCKET)
            tag_name=request.form['poster_small_name'].split('.')[0]
            for each in URL_UNSAFE:
                tag_name=tag_name.replace(each,"-")
            file_name="tag-poster-small-"+tag_name   #get part before .png/.avi
            path=fileops.save_file(request.files['poster_small'],file_name,'/tmp')
            fileops.manage_upload(path,request.files['poster_small'].content_type,file_name)
            temp_link = fileops.manage_upload(path,request.files['poster_small'].content_type,file_name,config.IMAGE_RESOURCE_BUCKET)
            a_tag.poster_small=config.CDN_PREFIX+os.path.basename(temp_link)
        else:
            a_tag.poster_small=None
            fileops.manage_delete(poster_small_path)
            fileops.manage_delete(poster_small_path,config.IMAGE_RESOURCE_BUCKET)

    if 'cover_image_name' in request.form:         #if logo_name is in request
        if 'null' not in request.form['cover_image_name']:
            if change_cover_image_flag:
                fileops.manage_delete(cover_image_path)
                fileops.manage_delete(cover_image_path,config.IMAGE_RESOURCE_BUCKET)
            tag_name=request.form['cover_image_name'].split('.')[0]
            for each in URL_UNSAFE:
                tag_name=tag_name.replace(each,"-")
            file_name="tag-cover-image-"+tag_name   #get part before .png/.avi
            path=fileops.save_file(request.files['cover_image'],file_name,'/tmp')
            fileops.manage_upload(path,request.files['cover_image'].content_type,file_name)
            temp_link = fileops.manage_upload(path,request.files['cover_image'].content_type,file_name,config.IMAGE_RESOURCE_BUCKET)
            a_tag.cover_image=config.CDN_PREFIX+os.path.basename(temp_link)
        else:
            a_tag.cover_image=None
            fileops.manage_delete(cover_image_path)
            fileops.manage_delete(cover_image_path,config.IMAGE_RESOURCE_BUCKET)

    if a_tag.collection == 'no':
        a_tag.ready='no'

    saved_id = a_tag.save()

    if saved_id is None:
        response['status'] = 'failed'
        response['message'] = 'There was a problem with saving this record. Please check your request!'
        response['http_status'] = 412
        if a_tag._token_id!=-1:
            response['token_id']=a_tag._token_id

    else:
        log_activity(endpoint='tag/keep/', activity_type=activity_type,
                    item_id=saved_id, item_type='tag',
                    request=request)
        beacon_redis_del(item_id=saved_id, item_type='collection')
        response['status'] = 'success'
        response['message'] = 'Record saved!'
        response['storage_id']=saved_id

    return response

def rain(incoming):

    response = {}
    if 'status' in incoming:
        all_tag_ids = enumerate_table('tags', {'status': incoming['status']})
        if 'collection' in incoming:
            all_tag_ids = enumerate_table('tags', {'status':incoming['status'],'collection': incoming['collection']})

    else:
        all_tag_ids = enumerate_table('tags')

    if len(all_tag_ids) == 0:
        response['status'] = 'failed'
        response['message'] = 'No relevant data found!'
    else:
        response['status'] = 'success'
        response['data'] = []

    for a_tag_id in all_tag_ids:
        a_tag = Tag(tag_id=a_tag_id)
        response['data'].append(deepcopy(a_tag.export()))

    return response

def datatable_rain(incoming):

    table='tags'
    query_dict=generic_adapter(incoming,table)
    query=query_dict['complete_query']
    search_query=query_dict['search_query']
    search_value=query_dict['search_value']

    print query

    response = {}
    response['draw']=incoming['draw']
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)

    a_query='SELECT column_name FROM information_schema.columns WHERE table_name =%(table_name)s'
    cursor.execute(a_query,{'table_name':table})
    res=cursor.fetchall()

    id_position=0

    if res:
        for each in res:
            if each[0] == 'id':
                break
            else:
                id_position+=1


    cursor.execute(query,{'search':search_value})
    data=cursor.fetchall()
    response['data']=[]
    for row in data:
        a_tag= Tag(tag_id=str(row[id_position]))
        response['data'].append(deepcopy(a_tag.export()))

    cursor.execute(search_query,{'search':search_value})
    data=cursor.fetchall()

    f_count=0
    for row in data:
        f_count+=1
    response['recordsFiltered']=f_count


    cursor.execute("SELECT count(*) from "+table)
    totalcnt=cursor.fetchone()
    response['recordsTotal']=totalcnt[0]
    response['http_status']=200

    db.commit()
    db.close()
    return response

def data_rain(tag_id, version_id=None):
    response = {}
    db = wrappers.Postgres.init()
    tag_id = int(tag_id.split('-')[-1])
    cursor = wrappers.Postgres.get_cursor(db, 'realdict')
    cursor.execute("SELECT concat('pp-content-',c.id) AS id, c.name, tm.item_type as type FROM tags_map AS tm LEFT JOIN content AS c ON c.id = tm.item_id WHERE tm.tag_id = %(tag_id)s AND tm.item_type = 'content' AND c.status IN ('complete', 'approved') UNION SELECT concat('pp-channel-',c.id) AS id, c.name, tm.item_type as type FROM tags_map AS tm LEFT JOIN channels AS c ON c.id = tm.item_id WHERE tm.tag_id = %(tag_id)s AND tm.item_type = 'channel' AND c.status IN ('complete', 'approved')",{'tag_id': tag_id})
    res=cursor.fetchall()

    if res :
        response['status'] = 'success'
        response['data'] = res
        if version_id is not None and version_id != 'null' and version_id != '':
            temp_list = []
            data_list = []
            temp_obj = {}
            version_id = int(str(version_id).split('-')[-1])
            cursor.execute("SELECT version_data from version_store where id=%(version_id)s", {'version_id': version_id})
            collection_feed = cursor.fetchone()['version_data']['collection-feed']
            if collection_feed:
                for collection in collection_feed:
                    collection_id = int(collection['collection_id'].split('-')[-1])
                    if collection_id == tag_id:
                        for content in collection['data']:
                            temp_list.append(content['id'])

                for row in res:
                    temp_obj[row['id']] = row

                for item in temp_list:
                    if item in temp_obj:
                        data_list.append(temp_obj[item])
                        del temp_obj[item]

                response['data'] = data_list + temp_obj.values()
                response['status'] = 'success'

    else:
        response['status'] = 'failed'
        response['message'] = 'No relevant data found!'
        response['data'] = []
    return response
