import wrappers
from slugify import slugify

def keep(t_id, t_type, temp):
    __TRACKED_FIELDS = ["tag_id", "item_id", "item_type"]
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    #deleting entries for channel/content with id t_id
    cursor.execute("DELETE FROM tags_map where item_type=%(item_type)s and item_id=%(item_id)s",{'item_type':t_type,'item_id':t_id})


    for tagobj in temp:
        #condition to check for tag in tags
        #print type(tagobj)
        exist=None
        #print tagobj['name']
        cursor.execute("SELECT id from tags where name=%(name)s",{'name':tagobj['name']})
        result=cursor.fetchone()
        if result!=None:
            exist=result[0]

        #create a new tag and add tag mapping
        if tagobj['type']=='new' and exist is None:
            slug_url = slugify(str(tagobj['name']))

            try:
                cursor.execute("INSERT INTO tags (name,display_name,slug_url) VALUES (%(name)s,%(display_name)s,%(slug_url)s)RETURNING *",{"name":tagobj['name'],"display_name":tagobj['name'],"slug_url":slug_url})
                db.commit()
                tagid=cursor.fetchone()[0]
            except:
                db.commit()
                cursor.execute("SELECT id from tags where slug_url=%(slug_url)s", {"slug_url":slug_url})
                db.commit()
                tagid = cursor.fetchone()[0]
            try:
                a_query = """INSERT INTO tags_map(""" + ', '.join(__TRACKED_FIELDS) +  """)VALUES ( %(""" + ')s, %('.join(__TRACKED_FIELDS) + """)s )"""
                cursor.execute(a_query, {"tag_id":tagid , "item_id": t_id, "item_type": t_type})
                db.commit()
            except Exception, e:
                db.commit()
                print e
                print tagid, " already exists!"
        #tag already exists ..just add mapping
        elif tagobj['type']=='existing' and exist is not None:
            try:
                a_query = """INSERT INTO tags_map(""" + ', '.join(__TRACKED_FIELDS) +  """)VALUES ( %(""" + ')s, %('.join(__TRACKED_FIELDS) + """)s )"""
                cursor.execute(a_query, {"tag_id":exist , "item_id": t_id, "item_type": t_type})
                db.commit()
            except Exception, e:
                db.commit()
                print e
                print tagid, " already exists!"
    db.commit()
    db.close()
