'''
  Torrent utility methods
'''


import config
import sys
import urllib
import bencode
import hashlib
import base64

def isTorrentFile(filename):    
    return '.' in filename and \
           filename.rsplit('.', 1)[1] == 'torrent'


def magnetize(path):
    '''
    Returns magnet link for given torrent file
    '''

    if not isTorrentFile(path):
        return None

    torrent = open(path, 'r').read()

    metadata = bencode.bdecode(torrent)

    hashcontents = bencode.bencode(metadata['info'])

    digest = hashlib.sha1(hashcontents).digest()
    b32hash = base64.b32encode(digest)

    params = {'xt': 'urn:btih:%s' % b32hash,
          'dn': metadata['info']['name']}

    announcestr = ''
    for announce in metadata['announce-list']:
      announcestr += '&' + urllib.urlencode({'tr':announce[0]})

    paramstr = urllib.urlencode(params) + announcestr
    magneturi = 'magnet:?%s' % paramstr

    return magneturi
