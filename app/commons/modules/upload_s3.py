import math, os
import hashlib
import shutil
import urllib
import config
from boto.s3.connection import S3Connection, Location , Bucket, Key
from filechunkio import FileChunkIO

#os.environ['S3_USE_SIGV4']='True'
def md5sum(filename, blocksize=65536):
    hash_dig = hashlib.md5()
    with open(filename, "rb") as f:
        for block in iter(lambda: f.read(blocksize), b""):
            hash_dig.update(block)
    return hash_dig.hexdigest()

def transfer_s3(file_path, content_type='binary/octet-stream', target_bucket=config.S3_CONTENT_RESOURCES_BUCKET):
    conn = S3Connection(config.AWS_CREDS['KEY'], config.AWS_CREDS['SECRET'])
    try:
        content_bucket = conn.get_bucket(target_bucket)
    except:
        content_bucket = conn.create_bucket(target_bucket, location=Location.APSoutheast)

    file_size = os.stat(file_path).st_size
    target_key = os.path.basename(file_path)
    upload_op = content_bucket.initiate_multipart_upload(target_key)

    # Use a chunk size of 5 MiB (feel free to change this)
    chunk_size = 5242880
    chunk_count = int(math.ceil(file_size / float(chunk_size)))

    # Send the file parts, using FileChunkIO to create a file-like object
    # that points to a certain byte range within the original file. We
    # set bytes to never exceed the original file size.
    for i in range(chunk_count):
        offset = chunk_size * i
        byte_count = min(chunk_size, file_size - offset)
        with FileChunkIO(file_path, 'r', offset=offset, bytes=byte_count) as fp:
            upload_op.upload_part_from_file(fp, part_num=i + 1)

    # Finish the upload
    upload_op.complete_upload()

    uploaded_file = content_bucket.get_key(target_key)
    uploaded_file.set_remote_metadata({'Content-Type': content_type}, {}, True)
    uploaded_file.set_acl('public-read')
    resource_url = uploaded_file.generate_url(expires_in=0, query_auth=False)
    return resource_url

def manage_upload(file_on_disk,content_type, name=None, bucket_name=config.S3_CONTENT_RESOURCES_BUCKET):

    temp=file_on_disk.split('/')
    static=file_on_disk.replace(temp[-1],"")
    static = static[:-1]
    target_bucket = bucket_name
    # if content_type == 'image/png':
    #     target_bucket=config.IMAGE_RESOURCE_BUCKET
    md5_name = md5sum(file_on_disk) + os.path.splitext(file_on_disk)[1]
    if name is not None:
        md5_name = name + os.path.splitext(file_on_disk)[1]
        #print "md5_name "+str(md5_name)
    file_final = os.path.join(static, md5_name)
    print "file_final "+str(md5_name)
    shutil.move(file_on_disk, file_final)
    #return transfer_s3(file_final, file_obj.content_type, target_bucket)
    return transfer_s3(file_final, content_type, target_bucket)

def manage_delete(file_path,bucket_name=config.S3_CONTENT_RESOURCES_BUCKET):
    if file_path is not None:
        conn = S3Connection(config.AWS_CREDS['KEY'], config.AWS_CREDS['SECRET'])
        b = Bucket(conn, bucket_name)
        k = Key(b)
        #print "file_path is"+file_path
        k.key = file_path.split('/')[-1]
        b.delete_key(k)

def manage_list(bucket_name=config.S3_CONTENT_RESOURCES_BUCKET):
    conn = S3Connection(config.AWS_CREDS['KEY'], config.AWS_CREDS['SECRET'])
    bucket = conn.get_bucket(bucket_name)
    s3list=[]
    ALLOWED_TYPE = ['mp3', 'mp4','mov','mkv','webm']
    for key in bucket.list():
        name=key.name
        ext=name.split('.')[-1]
        base_url = key.generate_url(expires_in=0, query_auth=False)
        #url=  base_url.split('/')[-1]
        #print url

        headers={}
        if ext=='mp3':
            headers={'response-content-type':'audio/mpeg3'}
        else:
            headers={'response-content-type':'video/mpeg'}
        signedurl=sign(bucket=bucket_name,path=name,access_key=config.AWS_CREDS['KEY'],headers=headers,secret_key=config.AWS_CREDS['SECRET'])
        if ext in ALLOWED_TYPE:
            if ext=='mp3':
                s3list.append({'name':name,'src:':'aws','link':signedurl,'type':'audio'})
            else:
                s3list.append({'name':name,'src:':'aws','link':signedurl,'type':'video'})

    return s3list

def rename_file(orig_name,new_name,bucket_name=config.S3_CONTENT_RESOURCES_BUCKET):
    s3 = boto.connect_s3()
    bucket = s3.lookup(bucket_name)
    # Lookup the existing object in S3
    key = bucket.lookup(orig_name)

    # Copy the key back on to itself, with new metadata
    return key.copy(bucket_name, new_name,metadata=None)

def sign(bucket, path,headers, access_key=config.AWS_CREDS['KEY'], secret_key=config.AWS_CREDS['SECRET'], https=True, expiry=86400):
    c = S3Connection(access_key, secret_key)
    return c.generate_url(
       expires_in=long(expiry),
       method='GET',
       bucket=bucket,
       key=path,
       response_headers=headers,
       query_auth=True,
       force_http=(not https)
    )
    #return c.generate_url(expires_in=0,method='GET',bucket=bucket,key=path,query_auth=False)

def get_bucket_contents(bucket_name):
    if bucket_name == None:
        return {'status':'failed','message':'Kahan se nikal k doon?','http_status':499}
    if bucket_name == 'raw-content':
        real_buck = config.RAW_CONTENT_BUCKET
    elif bucket_name == 'content-repo':
        real_buck = config.CONTENT_REPO_BUCKET
    else:
        real_buck = None

    if real_buck == None:
        return {'status':'failed','message':'Aisi koi jagah nahin hai','http_status':499}


    contents = manage_list(real_buck)
    return {'status':'success', 'content_list':contents}

def copy_file(source, destination):
    '''
        source and destination contain bucket as well as file name
    '''
    source_bucket = source.split('/')[0]
    source_file = source.split('/')[1]
    destination_bucket = destination.split('/')[0]
    destination_file = destination.split('/')[1]
    conn = S3Connection(config.AWS_CREDS['KEY'], config.AWS_CREDS['SECRET'])

    destbucket = conn.get_bucket(destination_bucket)
    try:
        new_key = destbucket.copy_key(destination_file, source_bucket, source_file)
        return {'status':'success', 'new_key':new_key}
    except Exception,e:
        return {'status':'failed', 'message':str(e)}

def manage_download(file_name,bucket_name):
    response={}
    response['http_status']=500
    response['status']='failed'
    response['message']='file not found'
    conn = S3Connection(config.AWS_CREDS['KEY'], config.AWS_CREDS['SECRET'])
    bucket = conn.get_bucket(bucket_name)
    for key in bucket.list():
        name=key.name
        if name==file_name:
            key.get_contents_to_filename(config.TMP+name)
            response['http_status']=200
            response['status']='success'
            response['path']=config.TMP+name
            return response
    return response



def save_file(file_obj,file_name,save_path):
    upload_folder = save_path
    static = save_path
    file_on_disk = os.path.join(upload_folder,file_name)

    print "content_type : "+str(file_obj.content_type)
    if file_obj.content_type == 'image/png':
        file_on_disk += '.png'
    elif file_obj.content_type == 'video/mp4':
        file_on_disk += '.mp4'
    elif file_obj.content_type == 'image/jpeg':
        file_on_disk += '.jpg'
    elif file_obj.content_type == 'application/vnd.android.package-archive':
        file_on_disk += '.apk'
    else:
        file_on_disk += '.unresolved'
    print "**file on disk"+str(file_on_disk)
    file_obj.save(file_on_disk)

    return file_on_disk
