from re import escape as rescape
import wrappers
import time
import json

def log_activity(endpoint, activity_type, item_id, item_type, request, otherdata=False):
    TRACKED_FIELDS = ['user_name', 'email', 'data', 'endpoint', 'activity_type', 'timestamp', 'item_id', 'item_type', 'user_ip', 'user_agent']
    try:
        data = {}
        if otherdata is not False:
            data = otherdata
        else:
            if request.form:
                data = request.form
        user_ip = request.remote_addr
        if request.headers.getlist("X-Forwarded-For"):
           user_ip = request.headers.getlist("X-Forwarded-For")[0]
        user_agent = request.headers.get('User-Agent')
        db = wrappers.Postgres.init()
        cursor = wrappers.Postgres.get_cursor(db)
        username = None
        email = None
        timestamp = int(round(time.time() * 1000))
        response_bool = True
        if 'user_name' in data:
            username = data['user_name']
        if 'user_email' in data:
            email = data['user_email']
        if item_id is not None:
            item_id = str(item_id).split('-')[-1]
        json_data = json.dumps(data)
        query = """INSERT INTO ammo_user_activity ("""+', '.join(TRACKED_FIELDS)+""") VALUES (%("""+')s, %('.join(TRACKED_FIELDS)+""")s)"""
        query_object = {'user_name': username,
                        'email': email,
                        'data': json_data,
                        'endpoint': endpoint,
                        'activity_type': activity_type,
                        'timestamp': timestamp,
                        'item_id': item_id,
                        'item_type': item_type,
                        'user_ip': user_ip,
                        'user_agent': user_agent}
        cursor.execute(query, query_object)
        db.commit()
        db.close()
    except Exception, e:
        db.commit()
        db.close()
        print "User activity not logged ---> ", e
        response_bool = False
    finally:
        return response_bool
