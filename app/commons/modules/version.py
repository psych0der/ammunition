import wrappers
from app.commons.Toolkit import enumerate_table
from models.Channel import Channel
from models.Sponsor import Sponsor
from models.Content import Content
from models.Tag import Tag
import collection
import feed
from models.Version_store import Version_store
import os
import upload_s3 as fileops
from copy import deepcopy
from flask import request
import json
from datetime import datetime as dt
import time
from jobs import celery_tasks as tasks
import config
from datatable_adapter import generic_adapter
from app.commons.modules.userlogs import log_activity



def fetch(version_id, incoming):
    response={}
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)

    if version_id==None:
        try:
            cursor.execute("SELECT id from version_store where live=%(live)s",{'live':'yes'})
            res=cursor.fetchone()
            if res!=None:
                version_id=str(res[0])
            else:
                response['message']='No version live'
                response['http_status']=418
                response['status']='failed'
                return response
        except Exception,e:
            response['error']=str(e)
            response['message']='Some error in fetching version data!'
            response['http_status']=420
            response['status']='failed'
            return response
    a_version_stored = Version_store(version_id=version_id)
    response = {'exists': False, 'status': 'failure'}
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    if a_version_stored._exists == True:
        response['exists'] = True
        response['status'] = 'success'
        response['data'] = a_version_stored.export()

        if response['data']['parent']!=None:
            response['data']['parent']='pp-version-'+str(response['data']['parent'])

        if  a_version_stored.version_data==None:
            db.close()
            return response

        for channel_feed_obj in a_version_stored.version_data['channel-feed']:
            for content_obj in channel_feed_obj['content']:
                if content_obj['type']=='image_ad':
                    a_query="SELECT id,resource_location from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s and ad_id=%(ad_id)s"
                    cursor.execute(a_query,{'sponsor_id':int(content_obj['id'].split('-')[-2]),'ad_type':'image_ad','ad_id':int(content_obj['id'].split('-')[-1])})
                    res=cursor.fetchone()
                    if res != None:
                        content_obj['ad_id']='pp-image-ad-'+content_obj['id'].split('-')[-2]+'-'+str(res[0])

            if 'preroll' in channel_feed_obj['sellable_entities']:
                for sellable_obj in channel_feed_obj['sellable_entities']['preroll']:
                        a_query="SELECT id,resource_location from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s and ad_id=%(ad_id)s"
                        cursor.execute(a_query,{'sponsor_id':int(sellable_obj['preroll_id'].split('-')[-2]),'ad_type':'preroll','ad_id':int(sellable_obj['preroll_id'].split('-')[-1])})
                        res=cursor.fetchone()
                        if res!=None:
                            sellable_obj['ad_id']='pp-pre-roll-'+sellable_obj['preroll_id'].split('-')[-2]+'-'+str(res[0])

            if 'splash' in channel_feed_obj['sellable_entities']:
                for sellable_obj in channel_feed_obj['sellable_entities']['splash']:
                        a_query="SELECT id,resource_location from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s and ad_id=%(ad_id)s"
                        cursor.execute(a_query,{'sponsor_id':int(sellable_obj['splash_id'].split('-')[-2]),'ad_type':'splash','ad_id':int(sellable_obj['splash_id'].split('-')[-1])})
                        res=cursor.fetchone()
                        if res!=None:
                            sellable_obj['ad_id']='pp-splash-'+sellable_obj['splash_id'].split('-')[-2]+'-'+str(res[0])

        for channel_feed_obj in a_version_stored.version_data['home-feed']:
            for content_obj in channel_feed_obj:
                if content_obj['type']=='image_ad':
                    a_query="SELECT id,resource_location from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s and ad_id=%(ad_id)s"
                    cursor.execute(a_query,{'sponsor_id':int(content_obj['id'].split('-')[-2]),'ad_type':'image_ad','ad_id':int(content_obj['id'].split('-')[-1])})
                    res=cursor.fetchone()
                    if res != None:
                        content_obj['ad_id']='pp-image-ad-'+content_obj['id'].split('-')[-2]+'-'+str(res[0])
    db.close()
    return response


def keep(version_id, incoming, incoming_files):
    activity_type = 'edit'
    if version_id is None:
        version_id='0'
        activity_type = 'new'

    response = {}
    a_version=Version_store(version_id=version_id)


    a_version.date_created=int(round(time.time() * 1000))
    a_version.consume(incoming)
    if 'parent' in incoming:
        if incoming['parent']!='null' and incoming['parent']!='':
            try:
                check_version=Version_store(version_id=incoming['parent'])
                if check_version._exists==False:
                    response['http_status']=420
                    response['status']='failed'
                    response['message']='invalid parent version_id'
                    return response
            except:
                response['http_status']=420
                response['status']='failed'
                response['message']='invalid parent version_id'
                return response
            a_version.parent=int(incoming['parent'].split('-')[-1])

    saved_id=a_version.save()

    if saved_id is None:
        response['status'] = 'failed'
        response['http_status'] = 412
        if a_version._token_id!=-1:
            response['token_id']=a_version._token_id
    else:
        log_activity(endpoint='version/keep/', activity_type=activity_type,
                    item_id=saved_id, item_type='version',
                    request=request, otherdata=incoming)
        response['status'] = 'success'
        response['storage_id']=saved_id

    return response



def rain(incoming):

    response = {'data': []}
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    cursor.execute('SELECT id, environment,live,description FROM version_store')
    results = cursor.fetchall()

    version_list=[]
    for a_result in results:
        if a_result[2] == 'yes':
            response['current_version']=a_result[0]
        version_list.append({'id':a_result[0],'environment':a_result[1],'description':a_result[3]})
    response['data']=version_list


    db.close()
    return response

def datatable_rain(incoming):
    table='version_store'
    query_dict=generic_adapter(incoming,table)
    query=query_dict['complete_query']
    search_query=query_dict['search_query']
    search_value=query_dict['search_value']

    print query

    response = {}
    response['draw']=incoming['draw']
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)

    a_query='SELECT column_name FROM information_schema.columns WHERE table_name =%(table_name)s'
    cursor.execute(a_query,{'table_name':table})
    res=cursor.fetchall()

    id_position=0

    if res:
        for each in res:
            if each[0] == 'id':
                break
            else:
                id_position+=1
    cursor.execute(query,{'search':search_value})
    data=cursor.fetchall()
    response['data']=[]
    for row in data:
        a_version= Version_store(version_id=str(row[id_position]))
        response['data'].append(deepcopy(a_version.export()))

    cursor.execute(search_query,{'search':search_value})
    data=cursor.fetchall()

    # f_count=0
    # for row in data:
    #     f_count+=1
    response['recordsFiltered']=len(data)

    cursor.execute("SELECT count(*) from "+table)
    totalcnt=cursor.fetchone()
    response['recordsTotal']=totalcnt[0]
    response['http_status']=200

    db.commit()
    db.close()
    return response


def publish(version_id,test=False):

    response={}
    item_id = '0'

    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    a_version_stored = Version_store(version_id=version_id)
    #print str(a_version_stored.environment)+"$$$$$"
    if a_version_stored.environment=='internet':
        if os.path.isfile(config.MONGO_LOCKFILE) is True:
            response['http_status'] = 418
            response['status'] = 'failed!'
            response['message'] = 'Another version publish task pending, please wait!'
            return response
        outfile = open(config.MONGO_LOCKFILE, 'w')
        outfile.close()
        channel_feed_data=[]
        if a_version_stored._exists == True:
            if  a_version_stored.version_data==None:
                db.close()
                response['status']='failed'
                response['http_status']=418
                response['message']='version data is null'
                return response

            response['status'] = 'success'
            response['version_id'] = version_id

            # home_feed_saved=feed.keep(a_version_stored.version_data['home-feed'],test)
            # collection_saved=collection.keep(a_version_stored.version_data['collection-feed'],test)

            if test==False:
                channel_tags(version_id=version_id)
            # if home_feed_saved['http_status'] !=200:
            #     return home_feed_saved
            # if collection_saved['http_status']!= 200:
            #     return collection_saved


            if test==False:
                cursor.execute("UPDATE version_store SET live=%(live)s",{'live':'no'})
                db.commit()
                a_version_stored.live='yes'
                a_version_stored.publish_date = int(round(time.time()*1000))
                saved_version=a_version_stored.save()

                if saved_version is None:
                    response['status'] = 'failed'
                    response['message'] = 'There was a problem updating the live attribute'
                    response['http_status'] = 412
                    return response

                #mongo db celery task
                item_id = saved_version
                tasks.fire_up_mongo.apply_async(queue=config.CELERY_QUEUE)
                print "$$fireupmongo task started$$"
                response['status']='success'
                response['http_status']=200
                response['message']='Version publish started!'

            db.close()

    else:
        if a_version_stored._exists == True:
            if  a_version_stored.version_data==None:
                db.close()
                response['status']='failed'
                response['http_status']=418
                response['message']='version data is null'
                return response
        a_version_stored.publish_date = int(round(time.time()*1000))
        saved_version = a_version_stored.save()
        if saved_version is None:
            response['status'] = 'failed'
            response['message'] = 'There was a problem publishing box version'
            response['http_status'] = 412
            return response

        #call celerytask for card process
        #tasks.download_initiator.apply_async((str(version_id).split('-')[-1],str(config.BOX_ZIP_PATH)),queue=config.CELERY_QUEUE)
        version_id_int = version_id.split('-')[-1]
        tasks.download_initiator.apply_async((str(version_id_int),str(config.BOX_ZIP_PATH)),queue=config.CELERY_QUEUE)
        item_id = version_id
        #print "$$celery task started$$"
        response['status']='success'
        response['http_status']=200
        response['message']='Box version in progress!'

    log_activity(endpoint='version/publish/', activity_type='new',
                item_id=item_id, item_type='version',
                request=request)

    return response


def channel_tags(version_id):
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    a_version_stored = Version_store(version_id=version_id)
    for channel_map_obj in a_version_stored.version_data['channel-feed']:
        a_channel=Channel(channel_id=channel_map_obj['channel_id']) #fetching name from id using object
        #print a_channel.name
        try:
            cursor.execute("INSERT INTO tags (name,display_name) VALUES (%(name)s,%(display_name)s)RETURNING id",{"name":a_channel.name,"display_name":a_channel.name})
        except Exception ,e:
            print 'error '+ str(e)
            db.commit()
            cursor.execute("SELECT id from tags where name=%(name)s",{"name":a_channel.name})

        tagid=cursor.fetchone()[0]
        print "checking the tags ------> ", tagid
        db.commit()
        for a_content_obj in channel_map_obj['content']:
            if a_content_obj['type']=='content':
                a_query="INSERT INTO tags_map (tag_id,item_id,item_type) VALUES (%(tag_id)s,%(item_id)s,%(item_type)s)"
                #print a_query
                try:
                    cursor.execute(a_query,{'tag_id':int(tagid),'item_id':int(a_content_obj['id'].split('-')[-1]),'item_type':'content'})
                    db.commit()
                except Exception ,e:
                    print'error '+str(e)
                    db.commit()



    db.close()
    response={}
    response['status'] = 'success'
    return response


def latest_version_id():
    response={}
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    try:
        cursor.execute("SELECT id from version_store where live=%(live)s",{'live':'yes'})
        res=cursor.fetchone()
        if res is not None:
            response['version']=str(res[0])
            response['http_status']=200
            response['status']='success'
        else:
            response['message']='No version live'
            response['http_status']=418
            response['status']='failed'
        return response
    except Exception ,e:
        response['error']=str(e)
        response['message']='Could not fetch latest version data'
        response['http_status']=420
        response['status']='failed'
        return response
