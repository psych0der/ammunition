'''
  instantiates sharable resources across application, such as redis client
'''

import redis
import putio
import config
import pathlib
import logging
import logging.handlers
from logging.handlers import SMTPHandler
redis_host = config.REDIS_HOST if hasattr(config, 'REDIS_HOST') else 'localhost'
redis_port = config.REDIS_PORT if hasattr(config, 'REDIS_PORT') else 6379
redis_host_beacon = config.REDIS_HOST_BEACON
redis_port_beacon = config.REDIS_PORT_BEACON

if hasattr(config, 'REDIS_PASSWORD') :
    redis_cli = redis.StrictRedis(host=redis_host, port=redis_port, db=0, password=config.REDIS_PASSWORD)
else:
    redis_cli = redis.StrictRedis(host=redis_host, port=redis_port, db=0)

if hasattr(config, 'REDIS_PASSWORD_BEACON') :
    beacon_redis_cli = redis.StrictRedis(host=redis_host, port=redis_port, db=0, password=config.REDIS_PASSWORD_BEACON)
else:
    beacon_redis_cli = redis.StrictRedis(host=redis_host, port=redis_port, db=0)



# initializing putio helper
putio_helper = putio.AuthHelper(config.PUTIO_CLIENT_ID, config.PUTIO_CLIENT_SECRET, \
                          "http://test-ammo.pressplaytv.in/hooks/putio", type='token')
#putio_helper.open_authentication_url()
putio_client = putio.Client(config.PUTIO_OAUTH_TOKEN)
#

LOG_FILENAME = config.LOGS_DIR + 'init_requests.out'
# Set up a specific logger with our desired output level

my_logger = logging.getLogger('MyLogger')
my_logger.setLevel(logging.DEBUG)

# Add the log message handler to the logger

handler = logging.handlers.RotatingFileHandler(LOG_FILENAME, maxBytes=200000, backupCount=5)
my_logger.addHandler(handler)
