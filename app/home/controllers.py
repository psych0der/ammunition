"""
    Controller to dictate working of __URL__/ and all associated APIs and pages.
"""
from flask import Blueprint, render_template
from app.commons.CORS import crossdomain
from app.commons.Toolkit import respond, get_timestamp
import models
import json
import app.commons.modules as modules
import config
import pathlib
import os

# Setting up access to this controller under '<OUR_AWESOME_DOMAIN>/*'
home_api = Blueprint('home', __name__, url_prefix='/')
data = {}

static_mapper = os.path.join(config.BASE_DIR, 'static-mapper.json')
with open(static_mapper) as data_file:
    data = json.load(data_file)

@home_api.route('/', methods=['GET'])
@home_api.route('content/', methods=['GET'])
@home_api.route('content/<path:path>', methods=['GET'])
@home_api.route('channel/', methods=['GET'])
@home_api.route('channel/<path:path>', methods=['GET'])
@home_api.route('sponsor/', methods=['GET'])
@home_api.route('sponsor/<path:path>', methods=['GET'])
@home_api.route('version/', methods=['GET'])
@home_api.route('version/<path:path>', methods=['GET'])
@home_api.route('feed/', methods=['GET'])
@home_api.route('feed/<path:path>', methods=['GET'])
@home_api.route('tag/', methods=['GET'])
@home_api.route('tag/<path:path>', methods=['GET'])
@home_api.route('raw-content/', methods=['GET'])
@home_api.route('raw-content/<path:path>', methods=['GET'])
@home_api.route('provider/', methods=['GET'])
@home_api.route('provider/<path:path>', methods=['GET'])

# def get_scripts(path):
# 	scripts = []
# 	scripts.append(data['compressed_names'][path])
# 	return scripts

# def get_stylesheets(path):
# 	styles = []
# 	styles.append(data['compressed_names'][path])
# 	return styles

def home(path=None):
	scripts = []
 	scripts.append(data['compressed_names']["home"])
 	styles = []
 	styles.append(data['compressed_names']["home"])
    # render home page
	return render_template('home.html', debug=config.DEBUG, scripts=scripts, stylesheets=styles)
