"""
    Controller to dictate working of __URL__/hooks/ and all associated APIs and pages.
    Defines POST hooks for external services
"""
from flask import Blueprint, render_template, request, g
from app.commons.CORS import crossdomain
from app.commons.Toolkit import respond, get_timestamp
from app.commons.resources import redis_cli, putio_client
from models.Content import Content
import redis
import putio
import app.commons.modules as modules
import config
import wrappers
import pathlib
from jobs import celery_tasks as tasks
from app.commons.resources import my_logger
import datetime


# Setting up access to this controller under '<OUR_AWESOME_DOMAIN>/testing/*'
post_hooks = Blueprint('post_hooks', __name__, url_prefix='/hooks')


@post_hooks.route('/putio', methods=['POST'])
#@crossdomain(origin='*')
def putio_hook():
    """
        POST hook for putio transfer status
    """
    remote_id = g.incoming['id']
    if not remote_id:
        respond({'status': 'failed', 'message': 'fuck you bitch'})

    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db,'dict')


    select_query = 'SELECT id,name FROM raw_content_registry WHERE remote_id = %(remote_id)s'
    cursor.execute(select_query, {'remote_id': remote_id})
    raw_content_info = cursor.fetchone()
    db.commit()
    db.close()

    tasks.fetch_from_putio.apply_async((str(raw_content_info['id']), remote_id, raw_content_info['name']),queue=CELERY_QUEUE)
    # redis_cli.publish('pp-downloader', '$'.join(['PUTIO_TRANSFER', str(raw_content_info['id']), remote_id, raw_content_info['name']]))


    return respond({'message': 'Thanks ya', 'status': 'we gotch ya',  'http_status': 200})


@post_hooks.route('/brightcove', methods=['POST'])
@crossdomain(origin='*')
def brightcove_hook():
    """
        Post hook for brightcove notifications
    """
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db,'dict')
    #print g.incoming

    if g.incoming['status'] == 'FAILED' and g.incoming['entityType'] == 'DIGITAL_MASTER':
        cursor.execute('UPDATE content SET digital_master_uploaded = %(no)s WHERE remote_id = %(remote_id)s',{'no':'no', 'remote_id':g.incoming['entity']})
        print 'failed to load video'
        time=str(datetime.datetime.now())
        timestamp= time.split('.')[0]
        my_logger.error(str(timestamp)+'failed to load video')

    elif g.incoming['status'] == 'SUCCESS' and g.incoming['entityType'] == 'DIGITAL_MASTER':
        cursor.execute('UPDATE content SET digital_master_uploaded = %(yes)s WHERE remote_id = %(remote_id)s',{'yes':'yes', 'remote_id':g.incoming['entity']})


    elif g.incoming['status'] == 'SUCCESS' and g.incoming['entityType'] == 'TITLE':
        cursor.execute('UPDATE content SET media_processed = %(yes)s WHERE remote_id = %(remote_id)s',{'yes':'yes', 'remote_id':g.incoming['entity']})


    elif g.incoming['status'] == 'FAILED' and g.incoming['entityType'] == 'TITLE':
        cursor.execute('UPDATE content SET media_processed = %(no)s WHERE remote_id = %(remote_id)s',{'no':'no', 'remote_id':g.incoming['entity']})
        print 'Title : '+g.incoming['entity']+' failed !'
        time=str(datetime.datetime.now())
        timestamp= time.split('.')[0]
        my_logger.error(str(timestamp)+' Title : '+g.incoming['entity']+' failed !')
    db.commit()
    db.close()
    return respond({'message': 'Thanks ya', 'status': 'we gotch ya',  'http_status': 200})


@post_hooks.route('/brightcove/trailer', methods=['POST'])
@crossdomain(origin='*')
def brightcove_hook_trailer():
    """
        Post hook for brightcove notifications
    """
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db,'dict')
    #print g.incoming

    if g.incoming['status'] == 'FAILED' and g.incoming['entityType'] == 'DIGITAL_MASTER':
        cursor.execute('UPDATE trailers_map SET digital_master_uploaded = %(no)s WHERE remote_id = %(remote_id)s',{'no':'no', 'remote_id':g.incoming['entity']})
        print 'failed to load video'
        time=str(datetime.datetime.now())
        timestamp= time.split('.')[0]
        my_logger.error(str(timestamp)+'failed to load video')

    elif g.incoming['status'] == 'SUCCESS' and g.incoming['entityType'] == 'DIGITAL_MASTER':
        cursor.execute('UPDATE trailers_map SET digital_master_uploaded = %(yes)s WHERE remote_id = %(remote_id)s',{'yes':'yes', 'remote_id':g.incoming['entity']})


    elif g.incoming['status'] == 'SUCCESS' and g.incoming['entityType'] == 'TITLE':
        cursor.execute('UPDATE trailers_map SET media_processed = %(yes)s WHERE remote_id = %(remote_id)s',{'yes':'yes', 'remote_id':g.incoming['entity']})


    elif g.incoming['status'] == 'FAILED' and g.incoming['entityType'] == 'TITLE':
        cursor.execute('UPDATE trailers_map SET media_processed = %(no)s WHERE remote_id = %(remote_id)s',{'no':'no', 'remote_id':g.incoming['entity']})

        print 'Title : '+g.incoming['entity']+' failed !'
        time=str(datetime.datetime.now())
        timestamp= time.split('.')[0]
        my_logger.error(str(timestamp)+' Title : '+g.incoming['entity']+' failed !')

    db.commit()
    db.close()
    return respond({'message': 'Thanks ya', 'status': 'we gotch ya',  'http_status': 200})
