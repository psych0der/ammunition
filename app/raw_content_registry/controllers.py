"""
    Controller to dictate working of __URL__/raw_content_registry/ and all associated APIs.
"""
from flask import Blueprint, request, g
from app.commons.CORS import crossdomain
from app.commons.Toolkit import respond, get_timestamp
import models
import app.commons.modules as modules
from werkzeug.datastructures import CombinedMultiDict, MultiDict

# Setting up access to this controller under '<OUR_AWESOME_DOMAIN>/raw_content_registry/*'
api = Blueprint('raw_content_registry', __name__, url_prefix='/raw-content')

@api.route('/', methods=['POST', 'GET'])
#@crossdomain(origin='*')
def base():
    """
        The base method for this controller does not need to do anything.
        A list of methods is possible, but might make us vulnerable.
        So we just do something fun for the time being.
    """
    response = {
        'message': "We don't make coffee. How about an XKCD instead?",
        'alternative': 'https://c.xkcd.com/random/comic/',
        'status': "It's complicated",
        'http_status': 418
    }

    # Returning with the joke 418 status code, because why not
    return respond(response)

@api.route('/fetch/<raw_content_id>', methods=['GET'])
#@crossdomain(origin='*')
def fetch(raw_content_id):
    response = modules.raw_content_registry.fetch(raw_content_id, g.incoming)
    return respond(response)

@api.route('/keep/', methods=['POST'])
@api.route('/keep/<raw_content_id>', methods=['POST'])
#@crossdomain(origin='*')
def keep(raw_content_id=None):
    response = modules.raw_content_registry.keep(raw_content_id, g.incoming, g.incoming_files)
    return respond(response)

@api.route('/rain', methods=['GET'])
#@crossdomain(origin='*')
def rain():
    response = modules.raw_content_registry.rain(g.incoming)
    return respond(response)

@api.route('/datatable-rain', methods=['GET'])
#@crossdomain(origin='*')
def datatable_rain():
    response = modules.raw_content_registry.datatable_rain(g.incoming)
    return respond(response)

@api.route('/unhook/<raw_content_id>',methods=['POST'])
def unhook(raw_content_id):
    response = modules.raw_content_registry.unhook(raw_content_id=raw_content_id)
    return respond(response)
