"""
    Controller to dictate working of __URL__/sponsor/ and all associated APIs.
"""
from flask import Blueprint, request, g
from app.commons.CORS import crossdomain
from app.commons.Toolkit import respond, get_timestamp
import models
import app.commons.modules as modules
from werkzeug.datastructures import CombinedMultiDict, MultiDict

# Setting up access to this controller under '<OUR_AWESOME_DOMAIN>/sponsor/*'
api = Blueprint('sponsor', __name__, url_prefix='/sponsor')

@api.route('/', methods=['POST', 'GET'])
#@crossdomain(origin='*')
def base():
    """
        The base method for this controller does not need to do anything.
        A list of methods is possible, but might make us vulnerable.
        So we just do something fun for the time being.
    """
    response = {
        'message': "We don't make coffee. How about an XKCD instead?",
        'alternative': 'https://c.xkcd.com/random/comic/',
        'status': "It's complicated",
        'http_status': 418
    }

    # Returning with the joke 418 status code, because why not
    return respond(response)

@api.route('/fetch/<sponsor_id>', methods=['GET'])
#@crossdomain(origin='*')
def fetch(sponsor_id):
    response = modules.sponsor.fetch(sponsor_id, g.incoming)
    return respond(response)

@api.route('/keep/', methods=['POST'])
@api.route('/keep/<sponsor_id>', methods=['POST'])
#@crossdomain(origin='*')
def keep(sponsor_id=None):
    response = modules.sponsor.keep(sponsor_id, g.incoming, g.incoming_files)
    return respond(response)

@api.route('/rain', methods=['GET'])
#@crossdomain(origin='*')
def rain():
    response = modules.sponsor.rain(g.incoming)
    return respond(response)

@api.route('/datatable-rain', methods=['GET'])
#@crossdomain(origin='*')
def datatable_rain():
    response = modules.sponsor.datatable_rain(g.incoming)
    return respond(response)

@api.route('/batch_fetch',methods=['GET'])
def batch_fetch():
    id_csv = request.args.get('id')
    field_csv = request.args.get('field')
    ad_type = request.args.get('ad_type')
    response = modules.sponsor.batch_fetch(field_csv, id_csv, ad_type)
    return respond(response)

@api.route('/slugify',methods=['GET'])
def check_slug():
    slug_url = request.args.get('value')
    response = modules.sponsor.check_slug(slug_url)
    return respond(response)

@api.route('/map/<version_id>',methods=['GET'])
def sponsor_mapping(version_id):
    response = modules.sponsor.sponsor_mapping(version_id)
    return respond(response)
