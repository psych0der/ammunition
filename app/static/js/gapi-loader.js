"use strict";

(function gapiLoader(document) {

    var gauth = null;
    var currentUserState = {};
    var googleUser = {};

    var emitCurrentUserState = function() {
            var status = true;
            if (Object.keys(currentUserState).length == 0) {
                status = false;
            }
            $(document).trigger('guser-changed', [{
                'status': status,
                'currentUserState': currentUserState
            }]);
        }
        /**
         * Initializes the Sign-In client.
         */
    window.initClient = function() {
        console.log('calling init client');
        $("head").append('<meta name="google-signin-client_id" content="702460920190-fnggjela05at5utrfdtsgg8h5fqon3gm.apps.googleusercontent.com">');
        gapi.load('auth2', function() {
            /**
             * Retrieve the singleton for the GoogleAuth library and set up the
             * client.
             */
            gauth = gapi.auth2.init();

            // currently disabled
            //gauth.isSignedIn.listen(signinChanged);

            // Listen for changes to current user.
            gauth.currentUser.listen(userChanged);

            // Sign in the user if they are currently signed in.
            if (gauth.isSignedIn.get() == true) {
                gauth.signIn(then(onSuccess));
            }


            // Start with the current live values.
            //refreshValues();

            // Attach the click handler to the sign-in button
            gauth.attachClickHandler('signin-button', {}, onSuccess, onFailure);
        });
    };

    /**
     * Handle successful sign-ins.
     */
    var onSuccess = function(user) {
        userChanged();
    };

    /**
     * Handle sign-in failures.
     */
    var onFailure = function(error) {
        userChanged();
    };

    var signinChanged = function(val) {
        console.log(' signinchanged');
        if (val == false) {
            currentUserState = {};
        }
        emitCurrentUserState();
    };

    /**
     * Listener method for when the user changes.
     * @param {GoogleUser} user the updated user.
     */
    var userChanged = function(user) {
        googleUser = user;
        updateGoogleUser();
    };

    /**
     * Updates the properties in the Google User table using the current user.
     */
    var updateGoogleUser = function() {
        if (googleUser && gauth.isSignedIn.get()) {
            var user_profile = googleUser.getBasicProfile();
            currentUserState['user_name'] = user_profile.getName();
            currentUserState['display_image'] = user_profile.getImageUrl()
            currentUserState['email'] = user_profile.getEmail();
            currentUserState['user_id'] = user_profile.getId();
            currentUserState['auth'] = gauth;
        } else {
            currentUserState = {};
        }
        console.log(' google user changed');
        emitCurrentUserState();
    };

    /**
     * Retrieves the current user and signed in states from the GoogleAuth
     * object.
     */
    var refreshValues = function() {
        if (gauth) {
            googleUser = gauth.currentUser.get();
            updateGoogleUser();
        }
    }
    /*TODO uncomment after testing*/
    $(document).ready(function() {
        $.getScript("https://apis.google.com/js/platform.js?onload=initClient", function() {
            console.log('GAPI loaded');
        }).fail(function(jqxhr, settings, exception){
            console.log(exception);
        });
    });

})(document)
