"use strict";

/* patching String prototype */
if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position) {
        position = position || 0;
        return this.indexOf(searchString, position) === position;
    };
}

(function homer(document) {


    var BASE_URL = window.location.origin + '/';
        /* TODO REMOVE*/

         //BASE_URL = 'http://test-ammo.pressplaytv.in/';


    var current_active_user = {};
    var buffer = {};

    var version_unapproved_content = {
        'content': [],
        'channel': [],
        'collection': [],
        'sponsor': []
    };

    var feed_element_count = {
        'video': 0,
        'movie': 0,
        'channel': 0,
        'collection': 0
    };

    var previous_version_sponsor_mapping = {};

    var auth = null;

    var user_key = null;

    /* dimensions of content poster */
    var contentPosterDimensions = {
        height: 400,
        width: 720
    }

    var contentIconDimensions = {
        height: 512,
        width: 512
    }

    var contentCoverDimensions = {
        height: 250,
        width: 1050
    }

    var channelPosterDimensions = {
        height: 400,
        width: 720
    }

    var channelIconDimensions = {
        height: 512,
        width: 512
    }
    var channelCoverDimensions = {
        height: 250,
        width: 1050
    }

    var sponsorLogoDimensions = {
        width: 512,
        height: 512
    }
    var sponsorCoverDimensions = {
        height: 250,
        width: 1050
    }

    var tagPosterDimensions = {
        height: 400,
        width: 720
    }
    var tagIconDimensions = {
        height: 512,
        width: 512
    }
    var tagCoverDimensions = {
        height: 250,
        width: 1050
    }

    var sponsorImageDimesions = {
        width: 540,
        height: 960
    }

    var ImageAdDimesions = {
        height: 400,
        width: 720
    }

    var max_current_row = 0;
    var max_column = 2;
    var current_column = 0;
    var current_row = 0;

    var content_cover_data_url = null;
    var channel_cover_data_url = null;
    var sponsor_cover_data_url = null;
    var tag_cover_data_url = null;

    /* image data of content poster image */
    var content_poster_data_url = null;
    var content_poster_small_data_url = null;
    var channel_icon_small_data_url = null;

    var channel_icon_data_url = null;
    var sponsor_logo_data_url = null;
    var sponsor_image_data_url = null;
    var tag_icon_data_url = null;
    var tag_poster_data_url = null;

    var compiled_image_template = null;
    var compiled_video_template = null;

    var splashFileObject = null;
    var sponsorSplashObjects = {};
    var sponsorSplashFileObject = null;
    var sponsorPrerollFileObject = null;
    var sponsorPrerollObjects = {};

    var sponsorImageAds = {};
    var apk_file = null;
    var apk_filename = null;
    var sponsor_fetched_apk_filename = null;

    /* collection of sortable instances */
    var sortableInstances = {};
    var mainSortableInstance = null;
    var topVideoSortableInstance = null;

    var mainCollectionSortableInstance = null;
    var mainCollectionSortableInstanceArrays = {};

    var mainFeedSortableInstance = null;
    //var feedElementSortableInstanceArray = [];

    /* defining approved collections*/
    var approved_channel_list = [];
    var approved_content_list = [];
    var approved_sponsor_list = [];
    var approved_collection_list = [];
    var approved_content_image_list = [];
    var tag_name_list = [];

    /* approved entity maps */
    var approved_channel_map = {};
    var approved_content_map = {};
    var approved_sponsor_map = {};
    var approved_collection_map = {};

    var sponsor_splash_map = {};
    var sponsor_preroll_map = {};
    var sponsor_image_ad_map = {};

    /* object to hold info about selected channels*/
    var insertedChannels = {};
    var alreadyInserted = {
        feed: [],
        channel: [],
        collection: []
    };

    var elementsToFetch = {
        'channel': [],
        'sponsor': [],
        'tag': [],
        'content' : []
    };
    var cache = {};

    /* variable to keep track of last fetched version data */
    var last_fetched_version = null;
    var last_fetched_version_id = null;
    var top_videos = [];

    var available_feed_channel = {
        channels: [],
        add: function(item_id) {
            //check if channel id doesn't exist continue
            if(!(item_id in approved_channel_map))
                return;

            var element = $.grep(approved_channel_list, function(e) {
                return e.id == item_id;
            })[0];

            this.channels.push(element);
        },
        remove: function(item_id) {
            var ch = this.channels;
            for (var i in this.channels) {
                if (this.channels[i]['id'] == item_id) {
                    this.channels.splice(i, 1);
                }
            }
        }
    }

    //Top video content not maintained at this point
    var selected_content = {
        data: [],
        add: function(item) {
            this.data.push(item);
            updateTopVideoSelector(this.data);
        },
        remove: function(item) {
            this.data.splice(this.data.indexOf(item), 1);
            updateTopVideoSlider(item);
            $("#version-top-content-list option[value='" + item + "']").remove();
            $('#version-top-content-list').trigger("chosen:updated");
        }
    }


    //handle function for showing errors on any request
    function showError(data){
        if (data.status == 500  || data.status == 0) {
            toastr.error('Something weird occured','',{timeOut: 15000});
        }else {
            if('responseJSON' in data){
                if('token_id' in data['responseJSON']){
                    toastr.error(data['responseJSON']['message'] + ': ' + data['responseJSON']['token_id'] ,'',{timeOut: 15000});
                } else {
                    toastr.error(data['responseJSON']['message'],'',{timeOut: 15000});
                }
            } else {
                if('message' in data){
                    toastr.error(data['message'],'',{timeOut: 15000});
                } else {
                    toastr.error('Bad Gateway : Someone\'s playing with net ','',{timeOut: 15000});
                }
            }
        }
    }

    //prepares a combined list map of content and image_ads
    function prepareContentImageList() {

        approved_content_image_list = [];

        for (var id in approved_content_map) {
            var object = {};
            object['name'] = approved_content_map[id]['name'];
            object['type'] = 'content';
            object['id'] = id;
            approved_content_image_list.push(object);
        }

        for (var id in sponsor_image_ad_map) {
            var object = {};
            object['name'] = sponsor_image_ad_map[id]['name'];
            object['type'] = 'image_ad';
            object['id'] = id;
            object['sponsor'] = sponsor_image_ad_map[id]['sponsor'];
            approved_content_image_list.push(object);
        }
    }

    /**
     * creates a list of available tag names for typeahead
     * @param  array tag_list a list of fetched tags
     * @return  a simple array containing names of all tags
     */
     function prepareTagNameList(tag_list){
        var tag_name_list = [];
        tag_list.forEach(function(obj){
            tag_name_list.push(obj['name']);
        });

        return tag_name_list;
     }


    /**
     * Hides select containers except provided exception. If no exception provided, all select containers will be hidden
     * @param  String   exception  Exception entity name whose select container won't be hidden
     * @param  Boolean  condition  Boolean,if provided, will control visibility of exceptional entity's select container
     */

    function hideSelectContainers(exceptions, condition) {
        var entities = ['channel', 'content','image', 'sponsor', 'top-bar-sponsor', 'splash', 'preroll', 'image-ad', 'top-content', 'collection', 'maincollection', 'feed-channel', 'feed-content', 'feed-image', 'feed-row-element-count'];
        if (condition == null)
            condition = true;
        var exceptions = exceptions || [''];

        entities.forEach(function(entity) {
            if ($.inArray(entity, exceptions) < 0) {
                $('.' + entity + '-select-container').hide();
            } else {
                if (condition) {
                    $('.' + entity + '-select-container').show();
                } else {
                    $('.' + entity + '-select-container').hide();
                }
            }
        });
    }

    /**STALLED
     * Updates select box of top videos according to selected content
     * @param Array  content_list list of selected content ids
     */
    function updateTopVideoSelector(content_list) {
        var selected_content_list = [];
        content_list.forEach(function(id) {
            selected_content_list.push({
                'id': id,
                'name': approved_content_map[id]['name']
            });
        });
        $('#version-top-content-list').html(prepareOptionList(selected_content_list)).prepend("<option></option>").trigger("chosen:updated");
    }

    /**
     * Removes provided content id  from slider
     * @param  String   content_id  id of the content to be removed
     */
    function updateTopVideoSlider(content_id) {
        $('.top-content-' + content_id).remove();
        $('#top-video-carousel').find('.item').removeClass('active');
        $('#top-video-carousel').find('.item').first().addClass('active');
        $('#top-video-carousel').carousel({
            interval: false,
        });
    }

    /**
     * Context element click handler for content element
     * @param   DOM     context  target DOM element on which context menu was opened
     * @param   Object  e        Error event of context element
     */
    function channelContentContextHandler(context, e) {
        var content_id = $(context).data('id');
        var channel_id = $(context).parents(':eq(1)').attr('data-channel');
        var id = content_id.split('*')[0];
        $(context).remove();
        var index = alreadyInserted['channel'].indexOf(id);
        if (index > -1) {
            alreadyInserted['channel'].splice(index, 1);
        }
        if($.inArray(id,alreadyInserted['channel']) < 0 && $.inArray(id,alreadyInserted['feed']) < 0 && $.inArray(id,alreadyInserted['collection']) < 0){
            handleVersionPublish(id,'content','remove');
        }
        /* hiding context menu */
        $('#context-menu-channel-content').hide();
    }

    /**
     * Context element click handler for channel image ad element
     * @param   DOM     context  target DOM element on which context menu was opened
     * @param   Object  e        Error event of context element
     */
    function channelImageAdContextHandler(context, e) {
        var image_ad_id = $(context).data('id');
        var channel_id = $(context).parents(':eq(1)').attr('data-channel');
        var id = image_ad_id.split('*')[0];
        var sponsor_id = image_ad_id.split('*')[2];
        $(context).remove();
        
        if(!checkIfAnySponsorElementExist(sponsor_id)){
            handleVersionPublish(sponsor_id,'sponsor','remove');
        }
        /* hiding context menu */
        $('#context-menu-channel-image-ad').hide();
    }

    /**
     * Converts array of object to array of object ids. Required : Object id
     * @param  Object  arrayOfObject   array of Object having id attribute
     * @return Array    returns array of ids
     */
    function arrayOfId(arrayOfObject) {
        var arr = [];
        arrayOfObject.forEach(function(obj) {
            arr.push(obj.id);
        });
        return arr;
    }


    /**DEPRICATED
     * Context element click handler for any empty element
     * @param   DOM     context  target DOM element on which context menu was opened
     * @param   Object  e        Error event of context element
     */
    function emptyElementContextHandler(context,e){
        console.log('empty element context handler');
        var parent = $(context).parent();
        $('#context-menu-empty-element').hide();

        //check if the element is a channel
        if($(context).hasClass('.channel-preview')){
            var selectedChannel = $(context).data('id');
            /* removing channel info from insertedChannels*/
            delete insertedChannels[selectedChannel];

            /* removing sortable instance */
            delete sortableInstances[selectedChannel];

            /*remove channel from channel array available for feed.*/
            available_feed_channel.remove(selectedChannel);

            /* removing content drawer for partcular channel */
            var contentDrawerClass = selectedChannel + '-content-section';
            var sponsorDrawerClass = selectedChannel + '-sponsor-section';
            var topBarSponsorDrawerClass = selectedChannel + '-top-bar-sponsor-section';
            var splashDrawerClass = selectedChannel + '-splash-section';
            var prerollDrawerClass = selectedChannel + '-preroll-section';
            var imageAdDrawerClass = selectedChannel + '-image-ad-section';

            if ($('.' + contentDrawerClass).length) {
                /* removing content from contet drawer */
                $('.' + contentDrawerClass).find('.content-preview').each(function(i) {
                    var content_id = $(this).attr('data-id');
                    if (content_id.split('*')[1] == 'content')
                        pushBackChannelContent(content_id, selectedChannel);
                    else {
                        pushBackChannelImageAd(content_id, selectedChannel);
                    }
                });
                $('.' + contentDrawerClass).remove();
            }
            if ($('.' + topBarSponsorDrawerClass).length) {
                $('.' + topBarSponsorDrawerClass).find('.top-bar-sponsor-list').children().each(function() {
                    pushBackSponsor($(this).attr('data-id'), selectedChannel);
                });
            }
            if ($('.' + splashDrawerClass).length) {
                $('.' + splashDrawerClass).find('.splash-list').children().each(function() {
                    pushBackSplash($(this).attr('data-id'), selectedChannel);
                });
            }
            if ($('.' + prerollDrawerClass).length) {
                $('.' + prerollDrawerClass).find('.preroll-list').children().each(function() {
                    pushBackPreroll($(this).attr('data-id'), selectedChannel);
                });
            }
            if ($('.' + imageAdDrawerClass).length) {
                $('.' + imageAdDrawerClass).find('.image-ad-list').children().each(function() {
                    pushBackImageAd($(this).attr('data-id'), selectedChannel);
                });
            }
        } else if($(context).hasClass('sponsor-preview')){
            var channel_id = $(context).parents(':eq(1)').attr('data-channel');
            if($(context).hasClass('empty-sponsor')){
                delete insertedChannels[channel_id]['sponsor']['top-bar-sponsor'];
            } else if($(context).hasClass('empty-splash')){
                var id = $(context).data('id');
                pushBackSplash(id,channel_id,true);
            } else if($(context).hasClass('empty-preroll')){
                var id = $(context).data('id');
                pushBackPreroll(id,channel_id,true);
            } else if($(context).hasClass('empty-imagead')){
                var id = $(context).data('id');
                pushBackImageAd(id,channel_id,true);
            }
        }

        $(context).remove();
        if($(parent).hasClass('feed-row')){
            var row_no = $(parent).data('id').split('-')[2];
            checkIfFilled(row_no);
        }

    }


    /**DEPRICATED
     * Context element click handler for top content element
     * @param   DOM     context  target DOM element on which context menu was opened
     * @param   Object  e        Error event of context element
     */
    function topContentContextHandler(context, e) {
        var content_id = $(context).data('id');
        $('.top-content-' + content_id).remove();
        $('#top-video-carousel').find('.item').removeClass('active');
        $('#top-video-carousel').find('.item').first().addClass('active');
        $('#top-video-carousel').carousel({
            interval: false,
        });
        pushBackTopContent(content_id);
        /* hiding context menu */
        $('#context-menu-top-content').hide();
    }

    function checkIfAnySponsorElementExist(id){
        if($('.sponsor-' + id).length > 0){
            return true;
        }
        for(var i=0; i<previous_version_sponsor_mapping[id]['preroll'].length;i++){
            preroll = previous_version_sponsor_mapping[id]['preroll'][i];
            if($('.preroll-' + preroll).length > 0){
                return true;
            }
        }
        for(var i=0; i<previous_version_sponsor_mapping[id]['splash'].length;i++){
            splash = previous_version_sponsor_mapping[id]['splash'][i];
            if($('.splash-' + splash).length > 0){
                return true;
            }
        }
        for(var i=0; i<previous_version_sponsor_mapping[id]['image_ad'].length;i++){
            ad = previous_version_sponsor_mapping[id]['image_ad'][i];
            if($.inArray(ad,alreadyInserted['channel'])>=0 || $.inArray(ad,alreadyInserted['feed'])>=0 || $.inArray(ad,alreadyInserted['collection']>=0)>=0){
                return true;
            }    
        }

        return false;
    }

    /**
     * Context element click handler for channel element
     * @param   DOM     context  target DOM element on which context menu was opened
     * @param   Object  e        Error event of context element
     */
    function channelContextHandler(context, e) {
        var data_id = $(context).data('id');
        var id = data_id.split('*')[0];
        $('.channel-' + id).remove();
        var index = alreadyInserted['channel'].indexOf(id);
        if(index > -1){
            alreadyInserted['channel'].splice(index,1);
        }
        pushBackChannel(id);
        if($.inArray(id,alreadyInserted['channel']) < 0 && $.inArray(id,alreadyInserted['feed']) < 0 && $.inArray(id,alreadyInserted['collection']) < 0){
            handleVersionPublish(id,'channel','remove');
        }
        /* hiding context menu */
        $('#context-menu-channel').hide();
    }


    /**
     * Context element click handler for maincollection element
     * @param   DOM     context  target DOM element on which context menu was opened
     * @param   Object  e        Error event of context element
     */
    function mainCollectionContextHandler(context, e) {
        var data_id = $(context).data('id');
        var id = data_id.split('*')[0];
        $('.maincollection-' + id).remove();
        var index = alreadyInserted['collection'].indexOf(id);
        if (index > -1) {
            alreadyInserted['collection'].splice(index, 1);
        }
        $('.' + id + '-content-section').remove();
        delete mainCollectionSortableInstanceArrays[id];
        if($.inArray(id,alreadyInserted['channel']) < 0 && $.inArray(id,alreadyInserted['feed']) < 0 && $.inArray(id,alreadyInserted['collection']) < 0){
            handleVersionPublish(id,'collection','remove');
        }
        /* hiding context menu */
        $('#context-menu-maincollection').hide();
    }




    /**
     * Context element click handler for collection element
     * @param   DOM     context  target DOM element on which context menu was opened
     * @param   Object  e        Error event of context element
     */
    function collectionContextHandler(context, e) {
        var collection_id = $(context).data('id');
        var id = collection_id.split('*')[0];
        var index = alreadyInserted['feed'].indexOf(id);
        if(index > -1){
            alreadyInserted['feed'].splice(index,1);
        }
        var row_no = $('.collection-' + id).parent().data('id').split('-')[2];
        $('.collection-' + id).remove();
        updateFeedElementCount(id,'collection','remove');
        /* hiding context menu */
        $('#context-menu-collection').hide();
        if($.inArray(id,alreadyInserted['channel']) < 0 && $.inArray(id,alreadyInserted['feed']) < 0 && $.inArray(id,alreadyInserted['collection']) < 0){
            handleVersionPublish(id,'collection','remove');
        }
        checkIfFilled(row_no);
    }

    /**
     * Context element click handler for feed content element
     * @param   DOM     context  target DOM element on which context menu was opened
     * @param   Object  e        Error event of context element
     */
    function feedContentContextHandler(context, e) {
        var content_id = $(context).data('id');
        var id = content_id.split('*')[0];
        var row_no = $('.feed-content-' + id).parent().data('id').split('-')[2];
        $('.feed-content-' + id).remove();
        var video_content_type = $(context).data('feedtype');
        console.log(video_content_type);
        updateFeedElementCount(id,video_content_type,'remove');
        var index = alreadyInserted['feed'].indexOf(id);
        if(index > -1){
            alreadyInserted['feed'].splice(index,1);
        }
        if($.inArray(id,alreadyInserted['channel']) < 0 && $.inArray(id,alreadyInserted['feed']) < 0 && $.inArray(id,alreadyInserted['collection']) < 0){
            handleVersionPublish(id,'content','remove');
        }
        /* hiding context menu */
        $('#context-menu-feed-content').hide();
        checkIfFilled(row_no);
    }

    /**
     * Context element click handler for feed channel element
     * @param   DOM     context  target DOM element on which context menu was opened
     * @param   Object  e        Error event of context element
     */
    function feedChannelContextHandler(context, e) {
        var channel_id = $(context).data('id');
        var id = channel_id.split('*')[0];
        var index = alreadyInserted['feed'].indexOf(id);
        if(index > -1){
            alreadyInserted['feed'].splice(index,1);
        }
        var row_no = $('.feed-channel-' + id).parent().data('id').split('-')[2];
        $('.feed-channel-' + id).remove();
        updateFeedElementCount(id,'channel','remove');
        /* hiding context menu */
        if($.inArray(id,alreadyInserted['channel']) < 0 && $.inArray(id,alreadyInserted['feed']) < 0 && $.inArray(id,alreadyInserted['collection']) < 0){
            handleVersionPublish(id,'channel','remove');
        }
        $('#context-menu-feed-channel').hide();
        checkIfFilled(row_no);
    }

    /**
     * Context element click handler for feed content element
     * @param   DOM     context  target DOM element on which context menu was opened
     * @param   Object  e        Error event of context element
     */
    function feedImageAdContextHandler(context, e) {
        var image_ad_id = $(context).data('id');
        var id = image_ad_id.split('*')[0];
        var sponsor_id = image_ad_id.split('*')[2];
        //var ad_id = id.split('-')[3];
        var index = alreadyInserted['feed'].indexOf(id);
        if(index > -1){
            alreadyInserted['feed'].splice(index,1);
        }
        var row_no = $('.feed-image-ad-' + id).parent().data('id').split('-')[2];
        $(context).remove();
        if(!checkIfAnySponsorElementExist(sponsor_id)){
            handleVersionPublish(sponsor_id,'sponsor','remove');
        }
        /* hiding context menu */
        $('#context-menu-feed-image-ad').hide();
        checkIfFilled(row_no);
    }



    /**
     * Context element click handler for sponsor element
     * @param   DOM     context  target DOM element on which context menu was opened
     * @param   Object  e        Error event of context element
     */
    function sponsorContextHandler(context, e) {
        var data_id = $(context).data('id');
        var sponsor_id = data_id.split('*')[0];
        var channel_id = $(context).parents(':eq(1)').attr('data-channel');
        $(context).remove();
        $('.top-bar-sponsor-select-display').show();
        $('.' + channel_id + '-top-bar-sponsor-section').find('.no-top-bar-sponsor').show();
        /* update channel listing */
        delete insertedChannels[channel_id]['sponsor']['top-bar-sponsor'];
        $('.top-bar-sponsor-select-container').show();
        $('.top-bar-sponsor-display').hide();
        /* hiding context menu */
        if(!checkIfAnySponsorElementExist(sponsor_id)){
            handleVersionPublish(sponsor_id,'sponsor','remove');
        }
        $('#context-menu-sponsor').hide();
    }

    /**
     * Context element click handler for splash element
     * @param   DOM     context  target DOM element on which context menu was opened
     * @param   Object  e        Error event of context element
     */
    function splashContextHandler(context, e) {
        var data_id = $(context).data('id');
        var splash_id = data_id.split('*')[0];
        var channel_id = $(context).parents(':eq(1)').attr('data-channel');
        var sponsor_id = $(context).data('sponsor');
        console.log(sponsor_id);
        $(context).remove();
        var splashDrawerClass = channel_id + '-splash-section';
        if ($('.' + splashDrawerClass).find('.splash-list').children().length < 1) {
            $('.' + splashDrawerClass).find('.no-sponsor-splash').show();
        } else {
            $('.' + splashDrawerClass).find('.no-sponsor-splash').hide();
        }
        var remaining = $.grep(insertedChannels[channel_id]['sponsor']['splash'],function(e){
            if(e.splash_id != splash_id)
                return e;
        });
        insertedChannels[channel_id]['sponsor']['splash'] = remaining;
        if (!insertedChannels[channel_id]['sponsor']['splash'].length)
            delete insertedChannels[channel_id]['sponsor']['splash'];
        //$('.sponsor-splash-select-container').show();
        if(!checkIfAnySponsorElementExist(sponsor_id)){
            handleVersionPublish(sponsor_id,'sponsor','remove');
        }
        $('.sponsor-splash-display').hide();
        /* hiding context menu */
        $('#context-menu-splash').hide();
    }

    /**
     * Context element click handler for preroll element
     * @param   DOM     context  target DOM element on which context menu was opened
     * @param   Object  e        Error event of context element
     */
    function prerollContextHandler(context, e) {
        var data_id = $(context).data('id');
        var preroll_id = data_id.split('*')[0];
        var channel_id = $(context).parents(':eq(1)').attr('data-channel');
        var sponsor_id = $(context).data('sponsor');
        console.log(sponsor_id);
        $(context).remove();
        console.log(preroll_id);
        var prerollDrawerClass = channel_id + '-preroll-section';
        if ($('.' + prerollDrawerClass).find('.preroll-list').children().length < 1) {
            $('.' + prerollDrawerClass).find('.no-sponsor-preroll').show();
        } else {
            $('.' + prerollDrawerClass).find('.no-sponsor-preroll').hide();
        }
        var remaining = $.grep(insertedChannels[channel_id]['sponsor']['preroll'],function(e){
            if(e.preroll_id != preroll_id)
                return e;
        });
        insertedChannels[channel_id]['sponsor']['preroll'] = remaining;
        //insertedChannels[channel_id]['sponsor']['preroll'].splice(insertedChannels[channel_id]['sponsor']['preroll'].indexOf(selectedPreroll), 1);
        if (!insertedChannels[channel_id]['sponsor']['preroll'].length)
            delete insertedChannels[channel_id]['sponsor']['preroll'];
        if(!checkIfAnySponsorElementExist(sponsor_id)){
            handleVersionPublish(sponsor_id,'sponsor','remove');
        }
        $('.sponsor-preroll-display').hide();
        /* hiding context menu */
        $('#context-menu-preroll').hide();
    }

    /**
     * Context element click handler for image ad element
     * @param   DOM     context  target DOM element on which context menu was opened
     * @param   Object  e        Error event of context element
     */
    function imageAdContextHandler(context, e) {
        var image_ad_id = $(context).data('id');
        var channel_id = $(context).parents(':eq(1)').attr('data-channel');
        $('.image-ad-' + image_ad_id).remove();
        pushBackImageAd(image_ad_id, channel_id, true);
        $('.sponsor-image-ad-select-container').show();
        $('.sponsor-image-ad-display').hide();
        /* hiding context menu */
        $('#context-menu-image-ad').hide();
    }


    /**
     * Prepares object for quick lookup. Creates object with 'id' as attribute
     * @param  Array    approved_channel_list list of approved resource
     * @return Object   Object with corresponding 'id' as key for list elements
     */
    function prepareObject(approved_channel_list) {
        var obj = {};
        approved_channel_list.forEach(function(ele) {
            obj[ele.id] = ele;
        });
        return obj;
    }

    /**
     * Accumulates sellabale entity from sponsor mapping into global variable
     * @param  Object   object  approved sponsor object
     * @param  String   entity  entity which will be accumulated in object
     * @return Object           object containing entities from all sponsors. Key of this object will be id of entity being accumulated
     */
    function accumulateSellableEntity(object, entity) {
        var obj = {};
        for (var sponsor_id in object) {
            var entities = object[sponsor_id][entity];
            /*if (typeof entities != 'object')
                continue;*/
            entities.forEach(function(inner_obj) {
                var obj_key = Object.keys(inner_obj)[0];
                var key = /*sponsor_id+'-'+*/ obj_key;
                var obj_value = inner_obj[obj_key].split('/');
                obj[key] = {
                    'id': key,
                    'sponsor': sponsor_id,
                    'sponsor-name': object[sponsor_id]['name'],
                    'link': inner_obj[obj_key],
                    'name': obj_value[obj_value.length - 1]
                }
            });
        }
        return obj;
    }


    /* config for content form validation */
    var content_validator_seed = {
        'duration': {
            'id': 'content-duration',
            'custom_validator': function(val, required) {
                if (val == '') {
                    return {
                        'status': 'warning',
                        'message': ''
                    };
                }
                if (/^(\d{1,2}:\d{1,2}:\d{1,2})$/.test(val)) {
                    var time_elements = val.split(':');
                    var hours = parseInt(time_elements[0]);
                    var minutes = parseInt(time_elements[1]);
                    var seconds = parseInt(time_elements[2]);
                    if (!((hours >= 0 && hours <= 23) && (minutes >= 0 && minutes <= 60) && (seconds >= 0 && seconds <= 60))) {
                        return {
                            'status': 'error',
                            'message': 'Invalid duration format'
                        };
                    } else {
                        return {
                            'status': 'success'
                        };
                    }
                } else {
                    return {
                        'status': 'error',
                        'message': 'Invalid duration format'
                    };
                }
            }
        },
        'expiry': {
            'id': 'content-expiry',
            'custom_validator': function(val, required) {
                if (val == '') {
                    return {
                        'status': 'warning',
                        'message': ''
                    };
                }
                if (/^(\d{1,3}:\d{1,2}:\d{1,2})$/.test(val)) {
                    var time_elements = val.split(':');
                    var hours = parseInt(time_elements[0]);
                    var minutes = parseInt(time_elements[1]);
                    var seconds = parseInt(time_elements[2]);
                    if (!((hours >= 0) && (minutes >= 0 && minutes < 60) && (seconds >= 0 && seconds < 60))) {
                        return {
                            'status': 'error',
                            'message': 'Invalid expiry duration format'
                        };
                    } else {
                        return {
                            'status': 'success'
                        };
                    }
                } else {
                    return {
                        'status': 'error',
                        'message': 'Invalid expiry duration format'
                    };
                }
            }
        },
        'cost': {
            'id': 'content-cost',
            'custom_validator': function(val, required) {
                if (val == '') {
                    return {
                        'status': 'warning',
                        'message': 'The cost will be now Rs. 0.00'
                    };
                }
                if (/^(\d{1,4}\.\d{1,2})$/.test(val)) {
                    var cost_elements = val.split('.');
                    var rupees = parseInt(cost_elements[0]);
                    var chillar = parseInt(cost_elements[1]);

                    if (!((rupees >= 0) && (chillar >= 0 && chillar < 100))) {
                        return {
                            'status': 'error',
                            'message': 'Invalid cost format'
                        };
                    } else {
                        return {
                            'status': 'success'
                        };
                    }
                } else {
                    return {
                        'status': 'error',
                        'message': 'Invalid cost format'
                    };
                }
            }
        },
        'genre': {
            'id': 'content-genre',
            'regex': ''
        },
        'name': {
            'id': 'content-name',
            'regex': '',
            'required': true
        },
        'language': {
            'id': 'content-language',
            'regex': ''
        },
        'artist-name': {
            'id': 'content-star-cast',
            'regex': ''
        },
        'type': {
            'id': 'content-type',
            'custom_validator': function(val, required) {
                if (val == null) {
                    return {
                        'status': 'warning',
                        'message': ''
                    };
                } else {
                    return {
                        'status': 'success'
                    };
                }
            },
            'view_update': function(selector, state, message) {
                var new_selector = $(selector).parent().find("input[type='text']");
                formValidationViewUpdate(new_selector, state, message);
            }
        }
    }

    /* config for channel form validation */
    var channel_validator_seed = {
        'name': {
            'id': 'channel-name',
            'regex': '',
            'required': true
        },
        'display-name': {
            'id': 'channel-display-name',
            'regex': '',
            'required': true
        }
    }

    var raw_content_seed = {
        'name': {
            'id': 'raw-content-name',
            'regex': '',
            'required': true
        },
        'type': {
            'id': 'content-type',
            'custom_validator': function(val, required) {
                if (val == null) {
                    return {
                        'status': 'error',
                        'message': 'Please choose a valid type'
                    };
                } else {
                    return {
                        'status': 'success'
                    };
                }
            },
            'view_update': function(selector, state, message) {
                var new_selector = $(selector).parent().find("input[type='text']");
                formValidationViewUpdate(new_selector, state, message);
            }
        }
    }


    var sponsor_validator_seed = {
        'name': {
            'id': 'sponsor-name',
            'required': true,
            'custom_validator': function(val, required) {
                if (val.length > 30) {
                    return {
                        'status': 'error',
                        'message': 'Max length allowed : 30'
                    }
                } else if (val.length == 0) {
                    return {
                        'status': 'error',
                        'message': ''
                    }
                } else {
                    return {
                        'status': 'success'
                    }
                }
            }

        },
        'desc_small': {
            'id': 'sponsor-desc-small',
            'custom_validator': function(val, required) {
                if (val.length > 50) {
                    return {
                        'status': 'error',
                        'message': 'Max length allowed : 50'
                    }
                } else {
                    return {
                        'status': 'success'
                    }
                }
            }

        },
        'sponsor-url': {
            'id': 'sponsor-url',
            'regex': '(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})'
        },
        'sponsor-app-url-android': {
            'id': 'sponsor-app-url-android',
            'regex': '(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})'
        },
        'sponsor-package-android': {
            'id': 'sponsor-package-android',
            'regex': ''
        },
        'sponsor-app-url-ios': {
            'id': 'sponsor-app-url-ios',
            'regex': '(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})'
        },
        'sponsor-package-ios': {
            'id': 'sponsor-package-ios',
            'regex': ''
        },
        'sponsor-tagline': {
            'id': 'sponsor-tagline',
            'regex': ''
        },
        'desc_large': {
            'id': 'sponsor-desc-large',
            'regex': ''
        },
    }

    var tag_validator_seed = {
        'name': {
            'id': 'tag-name',
            'regex': '',
            'required': true
        },
        'display-name': {
            'id': 'tag-display-name',
            'regex': '',
            'required': true
        }
    }

    var provider_validator_seed = {
        'name': {
            'id': 'provider-name',
            'regex': '',
            'required': true
        },
        'display-name': {
            'id': 'provider-display-name',
            'regex': '',
            'required': true
        }
    }

    /**
     * determines if data can be staged based on warning object and essential fields
     * @param  Object   warnings   warning object recieved from validator
     * @param  Array    essentials list of essential fields
     * @return Boolean  signifies if entity can be staged
     */
    function canBeStaged(warnings, essentials) {

        for(var i=0 ; i<essentials.length;i++){
            if (essentials[i] in warnings){
                return false;
            }
        }
        return true;
    }

    /**
     * Returns greeting based on time
     * @return String time based greeting
     */
    function getGreeting() {
        var hours = new Date().getHours();
        if ((hours < 12) && (hours >= 6)) {
            return 'Good morning, ';
        } else if ((hours >= 12) && (hours < 18)) {
            return 'Good afternoon, ';
        } else if ((hours >= 18) && (hours <= 23)) {
            return 'Good evening, ';
        } else if ((hours >= 0) && (hours < 4)) {
            return 'It sure is getting late, '
        } else {
            return 'Nothing like getting up early, ';
        }
    }

    /**
     * Converts base64 data url to HTML5 File BLOB
     * @param  String  dataURI  data uri of resource to be converted to file blob
     * @return Blob    Blob object
     */
    function dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = unescape(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], {
            type: mimeString
        });
    }

    /**
     * Fetches list of approved data from server
     * @param Function callback Callback after data is fetched
     */
    function fetchApprovedData(callback) {
        /* create jquery ajax deferreds */
        var fetchApprovedChannels = $.get(BASE_URL + 'channel/rain?status=complete');
        var fetchApprovedContent = $.get(BASE_URL + 'content/rain?status=complete');
        var fetchApprovedSponsors = $.get(BASE_URL + 'sponsor/rain?status=complete');
        var fetchApprovedCollections = $.get(BASE_URL + 'tag/rain?status=complete&collection=yes');


        /* wiat till we get all the data and phone image is loaded*/
        $.when(fetchApprovedChannels, fetchApprovedContent, fetchApprovedSponsors, fetchApprovedCollections).done(function(d1, d2, d3, d4) {
            var approved_channel_list = d1[0]['data'] || [];
            var approved_content_list = d2[0]['data'] || [];
            var approved_sponsor_list = d3[0]['data'] || [];
            var approved_collection_list = d4[0]['data'] || [];

            if (approved_channel_list.length == 0 || approved_content_list.length == 0 || approved_sponsor_list.length == 0) {
                callback({
                    'error': 'Not enough data'
                });
            } else {
                /* calling callback*/
                callback({
                    'approvedChannels': approved_channel_list,
                    'approvedContent': approved_content_list,
                    'approvedSponsors': approved_sponsor_list,
                    'approvedCollections': approved_collection_list
                });
            }

        });
    }


    /**
     * Updates form elements depending on validation status
     * @param  String selector   Jquery DOM selector or form element
     * @param  String type       validation status
     * @param  String message    Error message [OPTIONAL]
     */
    function formValidationViewUpdate(selector, status, message) {
        var message = message || '';
        if (status == 'valid') {
            $(selector).removeClass('invalid warning');
            if (!$(selector).hasClass('valid')) {
                $(selector).addClass('valid');
            }
            /* clearing data html of corresponding label */
            $("label[for='" + $(selector).attr('id') + "']").attr('data-error', '');
        } else if (status == 'error') {
            $(selector).removeClass('valid warning');
            if (!$(selector).hasClass('invalid')) {
                $(selector).addClass('invalid');
            }
            $("label[for='" + $(selector).attr('id') + "']").attr('data-error', message);
        } else {
            $(selector).removeClass('invalid valid');
            if (!$(selector).hasClass('warning')) {
                $(selector).addClass('warning');
            }
            $("label[for='" + $(selector).attr('id') + "']").attr('data-error', '');
        }
    }


    /**
     * replaces contents of target with a valid image of error message
     * @param  String    selector jquery selector for image uploader modal container
     * @param  DOM       image element constructed from resource specified
     * @param  String    target element where image should be appended
     * @param Function callback  callback to execute after replace image
     */
    function replaceContent(selector, img, target, callback) {
        var content;
        /* modifying image  styling */
        img.style.width = '50%';
        img.style.height = 'inherit';
        img.className = img.className + " poster-preview-image card";

        if (!(img.src || img instanceof HTMLCanvasElement)) {
            toastr.error('Unable to preview image. Your browser does not support the URL or FileReader API.')
            return;
        } else {
            content = img
        }
        $(selector).find(target).empty().append(content);
        if (callback) {
            callback(img);
        }
    }

    /**
     * display image constructed from file resource
     * @param  String   selector   jquery selector string for image uploader modal container
     * @param  FILE     file       file object received from event handler
     * @param  String   target     Dom selector of target element
     * @param  Object   options    options for load image library
     * @param  Object   callbacks  Object containing apply and remove callbacks
     * @param  Object   dimensions Object containing dimensions
     */
    function displayImage(selector, file, target, options, callbacks, dimensions) {
        var url = false;
        var url_filename;
        if(typeof file == 'string'){
            url = true;
            var splitarray = file.split('/');
            url_filename = splitarray[splitarray.length-1];
        }
        var callbacks = callbacks || null;
        if (!loadImage(
                file,
                function(img) {
                    $(selector).find('.poster-input-section').hide();
                    $(selector).find('.poster-info').show();
                    replaceContent(selector, img, target, function(img) {
                        $(img).cropper({
                            'autoCropArea': 0,
                            'minCropBoxWidth': dimensions.width,
                            'minCropBoxHeight': dimensions.height,
                            'aspectRatio': dimensions.width / dimensions.height,
                            'cropBoxResizable': false,
                            'toggleDragModeOnDblclick': false,
                            'cropBoxMovable': false,
                            'dragMode': 'move',
                            built: function() {
                                $(img).cropper("setContainerData", {
                                    width: dimensions.width + 200,
                                    height: dimensions.height + 150
                                });
                                $(img).cropper("setCropBoxData", dimensions);
                                $(selector).find('.m-preloader-copy').remove();
                                $(selector).find('.cropper-tool').show();
                                $(selector).find('.poster-zoom-in').off().click(function() {
                                    $(img).cropper('zoom', 0.1);
                                });
                                $(selector).find('.poster-zoom-out').off().click(function() {
                                    $(img).cropper('zoom', -0.1);
                                });
                                $(selector).find('.poster-reset').off().click(function() {
                                    $(img).cropper('reset');
                                });

                                $(selector).find('.poster-crop').off().click(function() {
                                    var result = $(img).cropper('getCroppedCanvas', dimensions);
                                    if (result) {
                                        $(selector).find('.poster-preview').hide();
                                        $(selector).find('.poster-confirmation').show();
                                        $(selector).find('.cropper-tool').hide();

                                        $(selector).find('.poster-confirm-clear').off().click(function() {
                                            $(selector).find('.poster-confirm-preview').empty();
                                            $(selector).find('.poster-preview').show();
                                            $(selector).find('.cropper-tool').show();
                                            $(selector).find('.poster-confirmation').hide();
                                        });

                                        $(selector).find('.poster-confirm-done').off().click(function() {
                                            $(selector).find(target).empty().html(result);
                                            $(selector).find('.poster-preview').show();
                                            $(selector).find('.poster-confirmation').hide();
                                            var image_src = result.toDataURL();
                                            if(image_src.length > 103000){
                                                var imageFile = document.createElement('img');
                                                imageFile.src = image_src;
                                                image_src = jic.compress(imageFile,90,'jpeg').src;
                                            }else {
                                                var imageFile = document.createElement('img');
                                                imageFile.src = image_src;
                                                image_src = jic.compress(imageFile,97,'jpeg').src;
                                            }
                                            $(img).cropper('destroy');
                                            if(url){
                                                callbacks['apply'](image_src, url_filename);
                                            }
                                            else{
                                                callbacks['apply'](image_src, file.name);
                                            }
                                        });

                                        $(selector).find('.poster-confirm-preview').empty().html(result);
                                    } else {
                                        toastr.error('Something weird happened while croping.');
                                    }

                                });
                            }
                        });

                    });
                },
                options
            )) {
            $(target).children().replaceWith(
                toastr.error('Unable to preview image. Your browser does not support the URL or FileReader API.')
            );
        }
    }

    /**
     * Returns event handler for drop and file select event with injected context objects
     * @param  String   selector   jquery selector string for uploader modal container
     * @param  Object   callbacks  Object of apply and remove callbacks
     * @param  Object   dimensions Object with height and width attributes for cropper
     * @return Function Event handler for drop and file select events
     */
    function getDropChangeHandler(selector, callbacks, dimensions) {
        return function(e) {
            e.preventDefault();
            e = e.originalEvent;
            var target = e.dataTransfer || e.target,
                file = target && target.files && target.files[0],
                options = {
                    maxWidth: '80%',
                    canvas: true
                };
            if (!file) {
                toastr.error('Unable to load image, please try again');
                return;
            }

            //code to restrict image type
            /*if (!file.name.match(/\.(png|PNG)$/)) {
                toastr.error('We only accept png images at the moment. Please upload a valid png image');
                return;
            }*/
            $(selector).find('.poster-preloader-container').append($('.m-preloader').clone().addClass('m-preloader-copy').show());
            displayImage(selector, file, '.poster-preview', options, callbacks, dimensions);
        }
    }

    /**
     * returns event handler for file video input
     * @param  String    selector       jquery selecto for video uploader modal container
     * @param  Function  applyCallback  callback to fire on successful file select
     */
    function getVideoDropHandler(selector, applyCallback) {
        return function(e) {
            $(selector).find('.video-preloader-container').append($('.m-preloader').clone().addClass('m-preloader-copy').show());
            e.preventDefault();
            e = e.originalEvent;
            var target = e.dataTransfer || e.target,
                file = target && target.files && target.files[0];

            if (!file) {
                toastr.error('Unable to load Video, please try again');
                return;
            }
            if (!file.name.match(/\.(mp4)$/)) {
                toastr.error('We only accept mp4 videos at the moment. Please upload a valid video file');
                return;
            }

            /* setting up video player */

            /* checking  if video can be played */
            var video_player = $(selector).find('#video-player')[0];
            var canPlay = video_player.canPlayType(file.type);
            canPlay = (canPlay === '' ? 'no' : canPlay);
            if (canPlay == 'no') {
                toastr.error('We are unable to generate preview of this video');
                /* clearing */
                $(selector).find('#video-file-input').val('');
                $(selector).find('m-preloader-copy').remove();
                return;
            }

            var URL = window.URL || window.webkitURL;
            var fileURL = URL.createObjectURL(file);
            $(selector).find(video_player).attr('src', fileURL);
            $(selector).find(video_player).load();

            $(selector).find(video_player).off().on('loadeddata', function() {
                $(selector).find('.m-preloader-copy').remove();
                $(selector).find('.video-input-section').hide();
                $(selector).find('.video-info').show();
                $(selector).find('.video-preview').show();
                applyCallback(file);
            });
        }
    }



    /**
     * Prepares image uploader with cropper by attaching event handles and initializing required libraries
     * @param  String    selector        jquery selector string for modal container
     * @param  Function  applyCallback   callback to fire when image has been cropped
     * @param  Function  removeCallback  callback to fire when image has been removed
     * @param  Object    dimensions      Object of width and height to apply to image cropper
     */
    function prepareImageUploaderModal(selector, applyCallback, removeCallback, dimensions) {
        var apply = applyCallback;
        var remove = removeCallback;
        $(selector).find('.poster-selector').off().click(function() {
            $(selector).find('.poster-file-input').click();
        });
        $(selector).find('.poster-file-input').off().on('change', getDropChangeHandler(selector, {
            'apply': apply,
            'remove': remove
        }, dimensions));
        $(selector).off().on('dragover', function(e) {
                e.preventDefault();
                e = e.originalEvent;
                e.dataTransfer.dropEffect = 'copy';
            })
            .on('drop', getDropChangeHandler(selector, {
                'apply': apply,
                'remove': remove
            }, dimensions));

        $(selector).find('.poster-url-field').off().change(function() {
            var val = $(this).val();
            /*if (!val.match(/\.(png)$/)) {
                toastr.error('We only accept png images at the moment. Please upload a valid png image');
                return;
            }*/

            $.ajax({
                type: "POST",
                url: BASE_URL + "util/fetch_net_image",
                data: {
                    'imageurl': val
                }
            }).success(function(newurl) {
                val = newurl["message"];

                $(selector).find('.poster-preloader-container').append($('.m-preloader').clone().addClass('m-preloader-copy').show());
                displayImage(selector, val, '.poster-preview', {
                    maxWidth: '80%'
                }, {
                    'apply': apply,
                    'remove': remove
                }, dimensions);


            }).fail(function(error) {
                console.log(error);
                toastr.error(error);
                console.log("failure: " + error);
            });
        });

        /* poster undo binding */
        $(selector).find('.poster-undo').off().click(function() {
            $(selector).find('.content-poster-file-input').val('');
            $(selector).find('.poster-input-section').show();
            $(selector).find('.poster-info').hide();
            $(selector).find('.poster-url-field').val('');
            /* calling remove callbacks */
            removeCallback();
        });
    }


    /**
     * Prepares video upload modal and initializes js components for the same
     * @param  String    selector       jquery selector for video uploader modal
     * @param  Function  applyCallback  callback on successful video selection
     * @param  Function  removeCallback callback on video removal
     */
    function prepareVideoUploaderModal(selector, applyCallback, removeCallback) {
        var apply = applyCallback;
        var remove = removeCallback;
        $(selector).find('.video-selector').off().click(function() {
            $(selector).find('.video-file-input').click();
        });
        $(selector).find('.video-undo').off().click(function() {
            $(selector).find('.video-file-input').val('');
            $(selector).find('.video-input-section').show();
            $(selector).find('.video-info').hide();
            $(selector).find('#video-preview').attr('src', '');
            /* calling remove callbacks */
            removeCallback();
        });

        $(selector).find('.video-file-input').off().on('change', getVideoDropHandler(selector, applyCallback));
        $(selector).find('#splash-add-modal').off().on('dragover', function(e) {
                e.preventDefault();
                e = e.originalEvent;
                e.dataTransfer.dropEffect = 'copy';
            })
            .on('drop', getVideoDropHandler(selector, applyCallback));
    }

    /**
     * cleans logged in state. Calls sinout on auth and hides logout button
     * @param  Object auth auth object from google signin
     */
    function cleanLoggedInState(auth) {
        if (auth) {
            auth.signOut();
            auth = null;
        }

        $('.signin-button-container').show();
        $('.main-container').hide();
        $('.logout-button').hide();
    }


    /**
     * Fetches list of entities from server and passes to render function
     * @param  Function renderFunction Function to render result obtained from server
     * @param  String   type           type of entity, in lowercase
     * @param  String   tableSelector  Jquery selector of element in which data will be rendered
     */
    function catchRain(renderFunction, type, tableSelector) {
        $('.' + type + '-fetch-retry').hide();
        $('.page-content').append($('.spin').clone().addClass('spin-clone').show());

        renderFunction('', tableSelector, type);
        $('.spin-clone').remove();
    }

    /**
     * Function to populate provider table with data returned from ajax
     * @param  Object   result           data object returned from server
     * @param  String   tableSelector  jquery selector of table to be filled
     * @param  String   dataType           type of entity. used to contruct url prefix and id column name
     */
    function populateProviderTable(result, tableSelector, dataType) {
        var table = $(tableSelector).DataTable({
            //"data": result['data'],
            "processing":true,
            "serverSide": true,
            "ajax": "/provider/datatable-rain",
            "columns": [{
                "data": "name",
                "title": "Name",
                "render": function(data, type, row, meta) {
                    if (type === "display") {
                        return "<div class=\"provider-name hyperlink\" data-url=\"/" + dataType + "/" + row['id'] + "\">" + data + "</div>";
                    } else {
                        return data;
                    }
                }
            }, {
                "data": "status",
                "title": "Status",
                "render": function(data, type, row, meta) {
                    if (type === "display") {
                        if (data == "incomplete") {
                            return "<div class=\"btn btn-danger btn-outline btn-xs \">INCOMPLETE</div>";
                        } else if (data == "staged") {
                            return "<div class=\"btn btn-warning btn-outline btn-xs \">STAGED</div>";
                        } else {
                            return "<div class=\"btn btn-success btn-outline btn-xs \">APPROVED</div>";
                        }
                    } else {
                        return data;
                    }
                }
            }, {
                "data": "campaign_start",
                "title": "Campaign start date"
            }, {
                "data": "campaign_end",
                "title": "Campaign end date"
            }, {
                "data": "id",
                "title": dataType + ' id'
            }]
        });
        $('.provider-table-refresh-button').click(function(){
            table.ajax.reload();
        });
        $(tableSelector).show();
    }



    /**
     * Function to populate tag table with data returned from ajax
     * @param  Object   result           data object returned from server
     * @param  String   tableSelector  jquery selector of table to be filled
     * @param  String   dataType           type of entity. used to contruct url prefix and id column name
     */
    function populateTagTable(result, tableSelector, dataType) {
        var table = $(tableSelector).DataTable({
            //"data": result['data'],
            "serverSide":true,
            "processing":true,
            "ajax":"/tag/datatable-rain",
            "columns": [{
                "data": "name",
                "title": "Name",
                "render": function(data, type, row, meta) {
                    if (type === "display") {
                        return "<div class=\"sponsor-name hyperlink\" data-url=\"/" + dataType + "/" + row['id'] + "\">" + data + "</div>";
                    } else {
                        return data;
                    }
                }
            }, {
                "data": "status",
                "title": "Status",
                "render": function(data, type, row, meta) {
                    if (type === "display") {
                        if (data == "incomplete") {
                            return "<div class=\"btn btn-danger btn-outline btn-xs \">INCOMPLETE</div>";
                        } else if (data == "staged") {
                            return "<div class=\"btn btn-warning btn-outline btn-xs \">STAGED</div>";
                        } else {
                            return "<div class=\"btn btn-success btn-outline btn-xs \">APPROVED</div>";
                        }
                    } else {
                        return data;
                    }
                }
            }, {
                "data": "id",
                "title": dataType + ' id'
            }, {
                "data": "collection",
                "title": "Collection status",
                "render": function(data, type, row, meta) {
                    if (type === "display") {
                        if (data == "yes") {
                            return "<div class=\"btn btn-success btn-outline btn-xs \">COLLECTION</div>";
                        } else if (data == "no") {
                            return "<div class=\"btn btn-warning btn-outline btn-xs \">NONE</div>";
                        }
                    } else {
                        return data;
                    }
                }
            }]
        });
        $('.tag-table-refresh-button').click(function(){
            table.ajax.reload();
        });
        $(tableSelector).show();
    }

    /**
     * Function to populate raw content table with data returned from ajax
     * @param  Object   result           data object returned from server
     * @param  String   tableSelector  jquery selector of table to be filled
     * @param  String   dataType           type of entity. used to contruct url prefix and id column name
     */
    function populateRawContentTable(result, tableSelector, dataType) {
        var table = $(tableSelector).DataTable({
            //"data": result['data'],
            "serverSide":true,
            "processing":true,
            "ajax":"/raw-content/datatable-rain",
            "columns": [{
                "data": "name",
                "title": "Name",
                "render": function(data, type, row, meta) {
                    if (type === "display") {
                        return '<div class="sponsor-name" data-id="' + row['id'] + '">' + data + '</div>';
                    } else {
                        return data;
                    }
                }
            }, {
                "data": "status",
                "title": "Status",
                "render": function(data, type, row, meta) {
                    if (type === "display") {
                        if (data == "started") {
                            return "<div class=\"btn btn-info btn-outline btn-xs \">STARTED</div>";
                        } else if (data == "converting") {
                            return "<div class=\"btn btn-info btn-outline btn-xs \">CONVERTING</div>";
                        } else if (data == "complete") {
                            return "<div class=\"btn btn-success btn-outline btn-xs \">COMPLETE</div>";
                        } else if (data == "error") {
                            return '<div class=\"btn btn-danger btn-outline btn-xs " data-hint="' + row['remarks'] + '">ERROR</div>';
                        }
                    } else {
                        return data;
                    }
                }
            }, {
                "data": "source",
                "title": "Source",
                "render": function(data, type, row, meta) {
                    if (type === "display") {
                        if (data == "UPLOAD") {
                            return "<div class=\"btn btn-info btn-outline btn-xs \">UPLOAD</div>";
                        } else if (data == "DOWNLOAD") {
                            return "<div class=\"btn btn-success btn-outline btn-xs \">DOWNLOAD</div>";
                        } else if (data == "MAGNET") {
                            return "<div class=\"btn btn-grey btn-outline btn-xs \">MAGNET</div>";
                        } else if (data == "S3") {
                            return "<div class=\"btn btn-danger btn-outline btn-xs \">S3</div>";
                        } else if (data == "TORRENT") {
                            return "<div class=\"btn btn-warning btn-outline btn-xs \">TORRENT</div>";
                        }
                    } else {
                        return data;
                    }
                }
            }, {
                "data": "media_type",
                "title": "Media type",
                "render": function(data, type, row, meta) {
                    if (type === "display") {
                        if (data == "video") {
                            return "<div class=\"btn btn-info btn-outline btn-xs \">VIDEO</div>";
                        } else if (data == "audio") {
                            return "<div class=\"btn btn-warning btn-outline btn-xs \">AUDIO</div>";
                        } else if (data == null) {
                            return "<div class=\"btn btn-danger btn-outline btn-xs \">UNAVAILABLE</div>";
                        }
                    } else {
                        return data;
                    }
                }
            }, {
                "data": "remote_link",
                "title": "Preview",
                "render": function(data, type, row, meta) {
                    if (type === "display") {
                        if (data != null) {
                            return '<div class="btn btn-success btn-outline btn-xs raw-media-preview-trigger pointer" data-type="'+row['media_type']+'"  data-name="' + row['name'] + '" data-link="' + row['remote_link'] + '">PREVIEW</div>';
                        } else if (data == null) {
                            return '<div class="btn btn-grey btn-outline btn-xs" disabled>PREVIEW</div>';
                        }
                    } else {
                        return data;
                    }
                }
            }, {
                "data": "id",
                "title": "Raw content id"
            }]/*, {
                "data": "id",
                "title": "Remove raw content",
                "render": function(data, type, row, meta) {
                    if (type === "display") {
                        return '<div class="remove-raw-content pointer" data-hint="delete this content" data-id="' + row['id'] + '"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></div>'
                    } else {
                        return data;
                    }
                }
            }]*/
        });
        $('.raw-content-table-refresh-button').click(function(){
            table.ajax.reload();
        });
        $(tableSelector).show();
    }


    /**
     * Function to populate content table with data returned from ajax
     * @param  Object   result           data object returned from server
     * @param  String   tableSelector  jquery selector of table to be filled
     * @param  String   dataType           type of entity. used to contruct url prefix and id column name
     */
    function populateContentTable(result, tableSelector, dataType){
        var table = $(tableSelector).DataTable({
            /*"data": result['data'],*/
            "processing":true,
            "serverSide": true,
            "ajax": "/content/datatable-rain",
            "columns": [{
                "data": "name",
                "title": "Name",
                "render": function(data, type, row, meta) {
                    if (type === "display") {
                        return "<div class=\"sponsor-name hyperlink\" data-url=\"/" + dataType + "/" + row['id'] + "\">" + data + "</div>";
                    } else {
                        return data;
                    }
                }
            }, {
                "data": "status",
                "title": "Status",
                "render": function(data, type, row, meta) {
                    if (type === "display") {
                        if (data == "incomplete") {
                            return "<div class=\"btn btn-danger btn-outline btn-xs \">INCOMPLETE</div>";
                        } else if (data == "staged") {
                            return "<div class=\"btn btn-warning btn-outline btn-xs \">STAGED</div>";
                        } else if (data == "seo_pending") {
                            return "<div class=\"btn btn-info btn-outline btn-xs \">SEO PENDING</div>";
                        } else {
                            return "<div class=\"btn btn-success btn-outline btn-xs \">APPROVED</div>";
                        }
                    } else {
                        return data
                    }
                }
            }, {
                "data": "id",
                "title": dataType + ' id'
            },{
                "data": "local_link",
                "title": 'Preview',
                "render": function(data, type, row, meta){
                    if (type === "display") {
                        if (data == null) {
                            return "<div class=\"btn btn-grey btn-outline btn-xs disabled \">Preview</div>";
                        } else {
                            return "<div class=\"btn btn-success btn-outline btn-xs pointer media-previewer\" data-name=" + row['name'] + " data-link=" + data + ">PREVIEW</div>";
                        }
                    } else {
                        return data
                    }
                }
            },{
                "data": "live",
                "title": "Live on version",
                "render": function(data, type, row, meta) {
                    if (type === "display") {
                        if (data == "yes") {
                            return "<div class=\"btn btn-success btn-outline btn-xs \">ONLINE</div>";
                        } else if (data == "no") {
                            return "<div class=\"btn btn-grey btn-outline btn-xs \">OFFLINE</div>";
                        }
                    } else {
                        return data;
                    }
                }
            }]
        });
        $('.content-table-refresh-button').click(function(){
            table.ajax.reload();
        });
        $(tableSelector).show();
    }

    /**
     * Function to populate table with data returned from ajax
     * @param  Object   result           data object returned from server
     * @param  String   tableSelector  jquery selector of table to be filled
     * @param  String   dataType           type of entity. used to contruct url prefix and id column name
     */
    function populateTable(result, tableSelector, dataType) {
        var table = $(tableSelector).DataTable({
            //"data": result['data'],
            "processing": true,
            "serverSide": true,
            "ajax": "/" + dataType + "/datatable-rain",
            "columns": [{
                "data": "name",
                "title": "Name",
                "render": function(data, type, row, meta) {
                    if (type === "display") {
                        return "<div class=\"sponsor-name hyperlink\" data-url=\"/" + dataType + "/" + row['id'] + "\">" + data + "</div>";
                    } else {
                        return data;
                    }
                }
            }, {
                "data": "status",
                "title": "Status",
                "render": function(data, type, row, meta) {
                    if (type === "display") {
                        if (data == "incomplete") {
                            return "<div class=\"btn btn-danger btn-outline btn-xs \">INCOMPLETE</div>";
                        } else if (data == "staged") {
                            return "<div class=\"btn btn-warning btn-outline btn-xs \">STAGED</div>";
                        } else {
                            return "<div class=\"btn btn-success btn-outline btn-xs \">APPROVED</div>";
                        }
                    } else {
                        return data
                    }
                }
            }, {
                "data": "id",
                "title": dataType + ' id'
            }]
        });
        $('.sponsor-table-refresh-button,.channel-table-refresh-button').click(function(){
            table.ajax.reload();
        });
        $(tableSelector).show();
    }

    /**
     * Function to populate Version table table with data returned from ajax
     * @param  Object   result           data object returned from server
     * @param  String   tableSelector  jquery selector of table to be filled
     * @param  String   dataType           type of entity. used to contruct url prefix and id column name
     */
    function populateVersionTable(result, tableSelector, dataType) {
        /*preparing data */
        var table_data = result['data'];

        $.ajax({
            'url': BASE_URL + 'version/fetch/live',
            'method': 'GET'
        }).done(function(data){
            if(data['status'] == 'success'){
                $('#current-version-button').data('url','/version/' + data['version']);
            } else{
                $('#current-version-button').hide();
                toastr.error('unable to fetch current version id');
            }
        }).fail(function(data){
            $('#current-version-button').hide();
            toastr.error('unable to fetch current version id');
        });

        var table = $(tableSelector).DataTable({
            //"data": table_data,
            "processing":true,
            "serverSide": true,
            "ajax": "/version/datatable-rain",
            "columns": [{
                "data": "id",
                "title": "Version ID",
                "render": function(data, type, row, meta) {
                    if (type === "display") {
                        return "<div class=\"version-id hyperlink\" data-url=\"/" + dataType + "/" + row['id'] + "\">" + data + "</div>";
                    } else {
                        return data;
                    }
                }
            }, {
                "data": "environment",
                "title": "Version type",
                "render": function(data, type, row, meta) {
                    if (type === "display") {
                        if (data == "box") {
                            return "<div class=\"btn btn-orange btn-outline btn-xs \">BOX</div>";
                        } else if (data == "internet") {
                            return "<div class=\"btn btn-primary btn-outline btn-xs \">INTERNET</div>";
                        } else {
                            return "<div class=\"btn btn-primary btn-outline btn-xs \">NULL</div>";
                        }
                    } else {
                        return data
                    }
                }
            },{
                "data": "id",
                "title": "Push version",
                "render": function(data, type, row, meta) {
                    if (type === "display") {
                        return "<div data-id='" + data + "' class=\"btn btn-danger publish-button btn-outline btn-xs pointer \">PUBLISH VERSION</div>";
                    } else {
                        return data
                    }
                }
            },{
                "data": "description",
                "title": "Version Description"
            }]
        });

        $(tableSelector).show();
        $('.modal-trigger').off().leanModal();
        $(document).off('click', '.publish-button');
        $(document).on('click', '.publish-button', function(){
            var version_id = $(this).data('id');
            $('.version-modal-title').html('Where you want to publish version ' + version_id + ' to?')
            $('.test-publish-button').data('url','/test-publish/' + version_id);
            $('.main-publish-button').data('url','/publish/' + version_id);
            $('#version-publish-modal-click').click();
        });

        $('.version-table-refresh-button').click(function(){
            table.ajax.reload();
        });
    }

    /** DEPRICATED
     * Fetches content detail from server and passes data to render Function
     * @param  String    content_id    content id
     * @param  Function  renderFunction  view renderer for content
     */
    function fetchContentDetails(content_id, renderFunction) {
        $.ajax({
            url: BASE_URL + 'content/fetch/' + content_id,
            method: 'GET'
        }).done(function(result) {
            renderFunction({
                'status': 'success',
                'response': result
            });
        }).fail(function() {
            renderFunction({
                'status': 'fail'
            })
        });
    }

    /**
     * Fetches data identified by URL from server and passes data to render Function
     * @param  String    url    url relativ to BASE URL to fetch data
     * @param  Function  renderFunction  view renderer for content
     */
    function fetchData(url, renderFunction) {
        /*TODO remove*/
        // renderFunction({
        //     'status': "success",
        //     response: {
        //         data: {
        //             'name': 'paytm',
        //             'video-url': 'http://paytm.com',
        //             'id': 'pp-content-4',
        //             'duration': 1440,
        //             'specifics' : {
        //               'cast': ['Big one']
        //             }                    ,
        //             'genre': ['Action', 'jackson'],
        //             'status': "staged",
        //             'language': "Hindi",
        //             'poster': "https://upload.wikimedia.org/wikipedia/commons/9/91/Adium.png"
        //         }
        //     }
        // });
        //  return;
        $.ajax({
            url: BASE_URL + url,
            method: 'GET'
        }).done(function(result) {
            renderFunction({
                'status': 'success',
                'response': result
            });
        }).fail(function() {
            renderFunction({
                'status': 'fail'
            })
        });
    }

    /** DEPRICATED
     * Fetches details of particular channel and renders using function provided
     * @param  String    channel_id     id of curren channel
     * @param  Function  renderFunction  function to render result after result has been received
     */
    function fetchChannelDetails(channel_id, renderFunction) {
        /*TODO remove*/
        /*renderFunction({
            'status': "success",
            response: {
                data: {
                    'name': 'Action',
                    'display_name': 'Action',
                    'id': 'pp-channel-2',
                    'highlight': "yes",
                    'splash': "http://sample-videos.com/video/mp4/720/big_buck_bunny_720p_50mb.mp4"
                }
            }
        });*/
        return;
        $.ajax({
            url: BASE_URL + 'channel/fetch/' + channel_id,
            method: 'GET'
        }).done(function(result) {
            renderFunction({
                'status': 'success',
                'response': result
            });
        }).fail(function() {
            renderFunction({
                'status': 'fail'
            })
        });
    }

    /**
     * converts duration in hh:mm:ss to seconds
     * @param   String  duration  durtion string in format of hh:mm:ss
     * @return  Number    number of seconds
     */
    function hmsToSeconds(duration) {
        var a = duration.split(':');
        var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
        return (isNaN(seconds)) ? null : seconds;
    }

    /**
     * converts duration in seconds to hms
     * @param   Number  totalSec  number of seconds
     * @return  String  duration in hh:mm:ss
     */
    function secondsToHms(totalSec) {
        var totalSec = parseInt(totalSec);
        var hours = parseInt(totalSec / 3600) % 24;
        var minutes = parseInt(totalSec / 60) % 60;
        var seconds = totalSec % 60;
        return hours + ':' + minutes + ':' + seconds;
    }

    function secondsToHmsKeepingHRS(totalSec) {
        var totalSec = parseInt(totalSec);
        var hours = parseInt(totalSec / 3600);
        var minutes = parseInt(totalSec / 60) % 60;
        var seconds = totalSec % 60;
        return hours + ':' + minutes + ':' + seconds;
    }

    function returnInIndianCostFormat(cost){
        var cost = cost.toString();
        var rupees = cost.split('.')[0];
        var chillar = cost.split('.')[1];
        if(chillar == undefined){
            chillar = '00';
        }
        return rupees + '.' + chillar;
    }

    /**
     * Renders content read only view for particular content
     * @param  Object  result  response object received from ajax request
     */
    function renderContentView(result) {
        if (result['status'] == 'success') {
            $('.content-fetch-retry').remove();
            var data = result['response']['data'];
            var compiled_template = templates['content-display-view'];
            //var template = $('#content-display-view').html();
            //var compiled_template = Hogan.compile(template);
            var context = {};
            context['content_name'] = data['name'];
            context['content_duration'] = (data['duration'] != null) ? secondsToHms(data['duration']) : 'BLANK';
            context['content_language'] = (data['language'] != null && data['language'] != '') ? data['language'] : 'BLANK';
            context['content_type'] = ('type' in data && data['type'] != null) ? data['type'] : 'BLANK';
            context['content_genre'] = (data['genre'] != null && data['genre'] != "") ? data['genre'].join(',') : 'BLANK';
            context['content_artists'] = ('specifics' in data && data['specifics'] != null && 'cast' in data['specifics'] && data['specifics']['cast'] != null) ? data['specifics']['cast'].join(',') : 'BLANK';
            context['content_id'] = data['id'];
            context['box_only'] = data['box_only'] == "yes";
            context['is_movie'] = data['is_movie'] == "yes";
            context['featured'] = data['featured'] == "yes";
            context['premium'] = data['premium'] == "yes";
            context['allow_download'] = data['allow_download'] == "yes";
            context['preview-available'] = (data['local_link'] != null && data['local_link'] != "") ? true : false;
            context['local_link'] = (data['local_link'] != "" && data['local_link'] != null) ? data['local_link'] : null ;
            context['media-added'] = (data['raw_content_id'] != null)
            context['media-processed'] = data['digital_master_uploaded'] == 'yes' && data['media_processed'] == 'yes';
            context['status'] = data['status'];
            context['tags'] = data['tags'];
            context['seo'] = ('seo' in data && data['seo'] != null && data['seo'] != '') ? data['seo'] : 'BLANK';
            context['expiry'] = (data['expiry'] != null) ? secondsToHmsKeepingHRS(data['expiry']/1000) : 'BLANK';
            context['cost'] = (data['cost'] != null) ? returnInIndianCostFormat(data['cost']) : 'BLANK';
            context['provider'] = (data['provider_name'] != null && data['provider_name'] != "") ? data['provider_name'] : 'BLANK';
            context['description'] = (data['description'] != null && data['description'] != "") ? data['description'] : 'BLANK';
            context['short_description'] = (data['short_description'] != null && data['short_description'] != "") ? data['short_description'] : 'BLANK';
            context['video_content_type'] = (data['video_content_type'] != null && data['video_content_type'] != "") ? data['video_content_type'] : 'shortform'

            $('.page-content').append(compiled_template.render(context));

            var trailers_processed = true;

            if(context['content_type'] == 'audio'){
                if(context['local_link'] != null){
                    context['media-processed'] = true;
                }
            }

            if(context['content_type'] == 'video'){
                $('.content-video-type-wrapper').show();
            }

            if(context['premium']){
                $('.premium-requirements').show();
            }

            if(data['video_content_type'] == 'movie'){
                $('.content-video-type-wrapper').show();
            }

            var deferreds = [];
            var d1 = $.Deferred();
            var d2 = $.Deferred();
            deferreds.push(d1);
            deferreds.push(d2);


            $('#media-preview-modal').html($('#media-preview-template').html());
            $('.modal-trigger').off().leanModal();

            if (!('cover_image' in data) || (data['cover_image'] == "" || data['cover_image'] == null)) {
                $('.content-cover-thumbnail-container').hide();
                $('.cover-alt-text').show();
            } else {
                /* settings image source of cover */
                $('.cover-thumbnail').attr('src', '/static/img/cover_loader.gif').attr('data-original',data['cover_image']).lazyload({'effect':'fadeIn'}).on('error',function(){
                    toastr.error('Failed To load Image maybe a wrong link');
                    $('.cover-error-text').show();
                });
            }

            if (!('poster' in data) || (data['poster'] == "" || data['poster'] == null)) {
                $('.content-poster-thumbnail-container').hide();
                $('.poster-alt-text').show();
                d1.resolve();
            } else {
                /* settings image source of poster */
                $('.poster-thumbnail').attr('src', '/static/img/image_loader.gif').attr('data-original',data['poster']).lazyload({'effect':'fadeIn'}).on('error',function(){
                    toastr.error('Failed To load Image maybe a wrong link');
                    $('.poster-error-text').show();
                    d1.resolve();
                });
            }

            if(data['trailers'].length == 0){
                $('#trailer-container').hide();
            }

            data['trailers'].forEach(function(trailer,index){
                var trailer_template = templates['trailer-processing-field'];
                var trailer_context = {};
                var number  = index + 1;
                trailer_context['trailer_name'] = trailer['name'];
                trailer_context['trailer_type'] = trailer['media_type'];
                trailer_context['trailer_number'] = number;
                trailer_context['trailer_link'] = (trailer['local_link'] != "" && trailer['local_link'] != null) ? trailer['local_link'] : null ;
                trailer_context['trailer-added'] = (trailer['raw_content_id'] != null)
                trailer_context['trailer-processed'] = trailer['media_processed'] == 'yes';
                trailer_context['preview-available'] = (trailer['local_link'] != null && trailer['local_link'] != "") ? true : false;
                $('#trailor-container').append(trailer_template.render(trailer_context));
                if(!trailer_context['trailer-processed']){
                    trailers_processed = false;
                }
            });

            $(document).off('click', '.media-previewer');
            $(document).on('click', '.media-previewer', function() {
                var name = $(this).data('name');
                var link = $(this).data('link');
                var type = $(this).data('type');
                $('#media-preview-modal').find('.media-title').html(name);
                var link_fragments = link.split('.');
                //var ext = link_fragments[link_fragments.length - 1]
                if (type == 'audio') {
                    $('#media-preview-modal').find('.video-preview').hide();
                    $('#media-preview-modal').find('.audio-preview').show();
                    $('#media-preview-modal').find('#audio-player').attr('src', link).load();
                    $('#media-preview-modal').find('#audio-player').off().on('error', function() {
                        toastr.error('Unable to preview media. Probably some error occured ');
                        $('.modal-close').click();
                    })
                } else {
                    $('#media-preview-modal').find('.audio-preview').hide();
                    $('#media-preview-modal').find('.video-preview').show();
                    $('#media-preview-modal').find('#video-player').attr('src', link).load();
                    $('#media-preview-modal').find('#video-player').off().on('error', function() {
                        toastr.error('Unable to preview media. Probably some error occured ');
                        $('.modal-close').click();
                    });
                }
                $('.media-preview-trigger').click();
            });


            if (!('poster_small' in data) || (data['poster_small'] == "" || data['poster_small'] == null)) {
                $('.content-poster-small-thumbnail-container').hide();
                $('.poster-small-alt-text').show();
                d2.resolve();
            } else {
                /* settings image source of poster */
                $('.poster-small-thumbnail').attr('src', '/static/img/image_loader.gif').attr('data-original',data['poster_small']).lazyload({'effect':'fadeIn'}).on('error',function(){
                    toastr.error('Failed To load Image maybe a wrong link');
                    $('.icon-error-text').show();
                    d2.resolve();
                });
            }

            if(data['type'] == 'audio'){
                    $('.media-processing-field').hide();
            }

            $('.spin-clone').remove();
            $('.content-info-container').show();
            $.when.apply($,deferreds).done(function(){
            });

            if (context['status'] == 'staged' && data['digital_master_uploaded'] == 'yes' && data['media_processed'] == 'yes' && trailers_processed) {
                $('.content-form-approve').css('display', 'inline-block').on('click', function() {
                    $(this).attr('disabled', 'disabled').removeClass('pointer');
                    $('.submit-indicator').empty().html($('.m-preloader').addClass('preloader-copy').clone().show());
                    $.ajax({
                        url: BASE_URL + 'content/keep/' + context['content_id'],
                        method: 'POST',
                        data: {
                            'status': 'complete',
                            'user_name': current_active_user['name'],
                            'user_email': current_active_user['email']
                        }
                    }).done(function(data) {
                        toastr.success('Content approved successfully');
                        page('/content/' + context['content_id']);
                    }).fail(function(error) {
                        showError(error);
                        $('.submit-indicator').empty();
                        $('.content-form-approve').removeAttr('disabled', 'disabled').addClass('pointer');
                        console.log(error);
                    });
                })
            }

        } else {
            toastr.error('There war some error fetching your request');
            $('.spin-clone').remove();
            // show retry button on content error
            $('.content-fetch-retry').show()
        }
    }

    /**
     * renders static view of particular channel
     * @param  Object  result  result obtained from ajax request
     */
    function renderChannelView(result) {
        if (result['status'] == 'success') {
            $('.channel-fetch-retry').remove();
            var data = result['response']['data'];
            var compiled_template = templates['channel-display-view'];
            /*var template = $('#channel-display-view').html();
            var compiled_template = Hogan.compile(template);*/

            var context = {};
            context['channel_name'] = data['name'];
            context['channel_display_name'] = data['display_name'];
            context['channel_id'] = data['id'];
            context['highight'] = data['highlighted'] == "yes";
            context['is_global'] = data['is_global'] == "yes";
            context['show_title'] = data['show_title'] == "yes";
            context['tagline'] = ('tagline' in data && data['tagline'] != null) ? data['tagline'] : 'BLANK';
            context['splash'] = ('base_splash' in data && data['base_splash'] != null) ? data['base_splash'] : 'BLANK';
            context['description'] = ('description' in data && data['description'] != null && data['description'] != '') ? data['description'] : 'BLANK';
            context['short_description'] = ('short_description' in data && data['short_description'] != null && data['short_description'] != '') ? data['short_description'] : 'BLANK';
            context['status'] = data['status'];
            context['tags'] = data['tags'];
            context['provider'] = data['provider_name'];
            $('.page-content').append(compiled_template.render(context));
            $('.modal-trigger').off().leanModal();

            /* deferreds for applying jqueyr `when` construct  */
            var deferreds = [];
            var d1 = $.Deferred();
            var d2 = $.Deferred();
            var d3 = $.Deferred();
            deferreds.push(d1);
            deferreds.push(d2);
            deferreds.push(d3);

            if (!('cover_image' in data) || (data['cover_image'] == "" || data['cover_image'] == null)) {

                $('.channel-cover-thumbnail-container').hide();
                $('.cover-alt-text').show();
            } else {
                /* settings image source of poster */
                $('.cover-thumbnail').attr('src', '/static/img/cover_loader.gif').attr('data-original',data['cover_image']).lazyload({'effect':'fadeIn'}).on('error',function(){
                    toastr.error('Failed To load Image maybe a wrong link');
                    $('.cover-error-text').show();
                });
            }

            if (!('icon' in data) || (data['icon'] == "" || data['icon'] == null)) {

                $('.channel-icon-thumbnail-container').hide();
                $('.icon-alt-text').show();
                d1.resolve();
            } else {
                /* settings image source of poster */
                $('.icon-thumbnail').attr('src', '/static/img/image_loader.gif').attr('data-original',data['icon']).lazyload({'effect':'fadeIn'}).on('error',function(){
                    toastr.error('Failed To load Image maybe a wrong link');
                    $('.icon-error-text').show();
                    d1.resolve();
                });
            }

            if (!('icon_small' in data) || (data['icon_small'] == "" || data['icon_small'] == null)) {

                $('.channel-icon-small-thumbnail-container').hide();
                $('.icon-small-alt-text').show();
                d2.resolve();
            } else {
                /* settings image source of poster */
                $('.icon-small-thumbnail').attr('src', '/static/img/image_loader.gif').attr('data-original',data['icon_small']).lazyload({'effect':'fadeIn'}).on('error',function(){
                    toastr.error('Failed To load Image maybe a wrong link');
                    $('.icon-small-error-text').show();
                    d2.resolve();
                });
            }

            if (context['splash'] == 'BLANK') {
                d3.resolve();
                $('.splash-view-control').hide();
                $('.splash-alt-text').show();
            } else {
                $('#video-player').attr('src', context['splash']);
                $('#video-player').load();
                $('#video-player').off().on('loadeddata', function() {
                    $('.video-preview').show();
                    d3.resolve();
                }).on('error',function(){
                    toastr.error('Failed To load video');
                    d3.resolve();
                });
            }

            if (context['status'] == 'staged') {
                $('.channel-form-approve').css('display', 'inline-block').on('click', function() {
                    $(this).attr('disabled', 'disabled').removeClass('pointer');
                    $('.submit-indicator').empty().html($('.m-preloader').addClass('preloader-copy').clone().show());
                    $.ajax({
                        url: BASE_URL + 'channel/keep/' + context['channel_id'],
                        method: 'POST',
                        data: {
                            'status': 'complete',
                            'user_name': current_active_user['name'],
                            'user_email': current_active_user['email']
                        }
                    }).done(function(data) {
                        toastr.success('Channel approved successfully');
                        page('/channel/' + context['channel_id']);
                    }).fail(function(error) {
                        $('.submit-indicator').empty();
                        $('.channel-form-approve').removeAttr('disabled', 'disabled').addClass('pointer');
                        console.log(error);
                        if (error.status == 500  || error.status == 0) {
                            toastr.error('Something weird occured','',{timeOut: 15000});
                        }else{
                            if('responseJSON' in error){
                                if('token_id' in error['responseJSON']){
                                    toastr.error(error['responseJSON']['message'] + ': ' + error['responseJSON']['token_id'] ,'',{timeOut: 15000});
                                } else {
                                    toastr.error(error['responseJSON']['message'],'',{timeOut: 15000});
                                }
                            } else {
                                toastr.error('Bad Gateway : Someone\'s playing with net ','',{timeOut: 15000});
                            }
                        }
                    });
                })
            }

            $('.spin-clone').remove();
            $('.channel-info-container').show();
            $.when.apply($,deferreds).done(function(result1, result2,result3) {
            });
        } else {
            toastr.error('There war some error fetching your request');
            $('.spin-clone').remove();
            $('.channel-fetch-retry').show();

        }
    }


    /**
     * renders static view of particular tag
     * @param  Object  result  result obtained from ajax request
     */
    function renderProviderView(result) {
        if (result['status'] == 'success') {

            $('.provider-fetch-retry').remove();
            var data = result['response']['data'];
            var compiled_template = templates['provider-display-view'];
            /*var template = $('#provider-display-view').html();
            var compiled_template = Hogan.compile(template);*/
            console.log(data);

            var context = {};
            context['provider_name'] = data['name'];
            context['provider_display_name'] = data['name'];
            context['provider_id'] = data['id'];
            context['active'] = data['active'] == "yes";
            context['status'] = result['response']['data']['status']; //data['status'];
            context['campaign_start'] = data['campaign_start'];
            context['campaign_end'] = data['campaign_end'];
            context['twitter_link'] = (data['twitter_link'] != null && data['twitter_link'] != '') ? data['twitter_link'] : 'BLANK';
            context['fb_link'] = (data['fb_link'] != null && data['fb_link'] != '') ? data['fb_link'] : 'BLANK';
            context['slug_url'] = (data['slug_url'] != null && data['slug_url'] != '') ? data['slug_url'] : 'BLANK';
            $('.page-content').append(compiled_template.render(context));
            $('.modal-trigger').off().leanModal();


            if (context['status'] == 'staged') {

                $('.provider-form-approve').show().css('display', 'inline-block').on('click', function() {
                    $(this).attr('disabled', 'disabled').removeClass('pointer');
                    $('.submit-indicator').empty().html($('.m-preloader').addClass('preloader-copy').clone().show());
                    $.ajax({
                        url: BASE_URL + 'provider/keep/' + context['provider_id'],
                        method: 'POST',
                        data: {
                            'status': 'complete',
                            'user_name': current_active_user['name'],
                            'user_email': current_active_user['email']
                        }
                    }).done(function(data) {
                        toastr.success('Provider approved successfully');
                        page('/provider/' + context['provider_id']);
                    }).fail(function(error) {
                        $('.submit-indicator').empty();
                        $('.provider-form-approve').removeAttr('disabled', 'disabled').addClass('pointer');
                        console.log(error);
                        if (error.status == 500 || error.status == 0) {
                            toastr.error('Something weird occured','',{timeOut: 15000});
                        }else{
                            if('responseJSON' in error){
                                if('token_id' in error['responseJSON']){
                                    toastr.error(error['responseJSON']['message'] + ': ' + error['responseJSON']['token_id'] ,'',{timeOut: 15000});
                                } else {
                                    toastr.error(error['responseJSON']['message'],'',{timeOut: 15000});
                                }
                            } else {
                                toastr.error('Bad Gateway : Someone\'s playing with net ','',{timeOut: 15000});
                            }
                        }
                    });
                })
            }

            $('.spin-clone').remove();
            $('.provider-info-container').show();

        } else {
            toastr.error('There war some error fetching your request');
            $('.spin-clone').remove();
            $('.tag-fetch-retry').show();

        }
    }




    /**
     * renders static view of particular tag
     * @param  Object  result  result obtained from ajax request
     */
    function renderTagView(result) {
        if (result['status'] == 'success') {
            $('.tag-fetch-retry').remove();
            var data = result['response']['data'];
            var compiled_template = templates['tag-display-view'];
            /*var template = $('#tag-display-view').html();
            var compiled_template = Hogan.compile(template);*/

            var context = {};
            context['tag_name'] = data['name'];
            context['tag_display_name'] = data['display_name'];
            context['tag_id'] = data['id'];
            context['tagline'] = ('tagline' in data && data['tagline'] != null) ? data['tagline'] : 'BLANK';
            context['seo'] = ('seo' in data && data['seo'] != null) ? data['seo'] : 'BLANK';
            context['tag_description'] = ('description' in data && data['description'] != null) ? data['description'] : 'BLANK';
            context['tag_short_description'] = ('short_description' in data && data['short_description'] != null) ? data['short_description'] : 'BLANK';
            context['collection'] = data['collection'] == "yes";
            context['show_title'] = data['show_title'] == "yes";
            context['poster_small'] = ('poster_small' in data && data['poster_small'] != null) ? data['poster_small'] : 'BLANK';
            context['poster'] = ('poster' in data && data['poster'] != null) ? data['poster'] : 'BLANK';
            context['status'] = result['response']['data']['status']; //data['status'];
            $('.page-content').append(compiled_template.render(context));
            $('.modal-trigger').off().leanModal();


            /* deferreds for applying jqueyr `when` construct  */
            var d1 = $.Deferred();
            var d2 = $.Deferred();


            if (!('poster_small' in data) || (data['poster_small'] == "" || data['poster_small'] == null)) {

                $('.tag-icon-thumbnail-container').hide();
                $('.icon-alt-text').show();
                d1.resolve();
            } else {
                /* settings image source of poster */
                $('.icon-thumbnail').attr('src', '/static/img/image_loader.gif').attr('data-original',data['poster_small']).lazyload({'effect':'fadeIn'}).on('error',function(){
                    toastr.error('Failed To load Image maybe a wrong link');
                    $('.icon-error-text').show();
                    d1.resolve();
                });
            }

            if (!('cover_image' in data) || (data['cover_image'] == "" || data['cover_image'] == null)) {

                $('.tag-cover-thumbnail-container').hide();
                $('.cover-alt-text').show();
            } else {
                /* settings image source of poster */
                $('.cover-thumbnail').attr('src', '/static/img/cover_loader.gif').attr('data-original',data['cover_image']).lazyload({'effect':'fadeIn'}).on('error',function(){
                    toastr.error('Failed To load Image maybe a wrong link');
                    $('.cover-error-text').show();
                });
            }

            if (!('poster' in data) || (data['poster'] == "" || data['poster'] == null)) {

                $('.tag-poster-thumbnail-container').hide();
                $('.poster-alt-text').show();
                d2.resolve();
            } else {
                /* settings image source of poster */
                $('.poster-thumbnail').attr('src', '/static/img/image_loader.gif').attr('data-original',data['poster']).lazyload({'effect':'fadeIn'}).on('error',function(){
                    toastr.error('Failed To load Image maybe a wrong link');
                    $('.poster-error-text').show();
                    d2.resolve();
                });
            }
            if (context['status'] == 'staged') {

                $('.tag-form-approve').show().css('display', 'inline-block').on('click', function() {
                    $(this).attr('disabled', 'disabled').removeClass('pointer');
                    $('.submit-indicator').empty().html($('.m-preloader').addClass('preloader-copy').clone().show());
                    $.ajax({
                        url: BASE_URL + 'tag/keep/' + context['tag_id'],
                        method: 'POST',
                        data: {
                            'status': 'complete',
                            'user_name': current_active_user['name'],
                            'user_email': current_active_user['email']
                        }
                    }).done(function(data) {
                        toastr.success('Tag approved successfully');
                        page('/tag/' + context['tag_id']);
                    }).fail(function(error) {
                        $('.submit-indicator').empty();
                        $('.tag-form-approve').removeAttr('disabled', 'disabled').addClass('pointer');
                        console.log(error);
                        if (error.status == 500 || error.status == 0) {
                            toastr.error('Something weird occured','',{timeOut: 15000});
                        }else{
                            if('responseJSON' in error){
                                if('token_id' in error['responseJSON']){
                                    toastr.error(error['responseJSON']['message'] + ': ' + error['responseJSON']['token_id'] ,'',{timeOut: 15000});
                                } else {
                                    toastr.error(error['responseJSON']['message'],'',{timeOut: 15000});
                                }
                            } else {
                                toastr.error('Bad Gateway : Someone\'s playing with net ','',{timeOut: 15000});
                            }
                        }
                    });
                })
            }

            $('.spin-clone').remove();
            $('.tag-info-container').show();
            $.when(d1, d2).done(function(result1, result2) {
            });
        } else {
            toastr.error('There war some error fetching your request');
            $('.spin-clone').remove();
            $('.tag-fetch-retry').show();

        }
    }

    /**
     * Renders view for sponsor
     * @param  Object  result  result object obtained from server
     */
    function renderSponsorView(result) {
        if (result['status'] == 'success') {
            $('.sponsor-fetch-retry').remove();
            var data = result['response']['data'];
            var compiled_template = templates['sponsor-display-view'];
            /*var template = $('#sponsor-display-view').html();
            var compiled_template = Hogan.compile(template);*/

            var context = {};
            context['sponsor_name'] = data['name'];
            context['sponsor_url'] = (data['url'] != null && data['url'] != '') ? data['url'] : 'BLANK';
            context['sponsor_id'] = data['id'];
            context['apps_download'] = ('apps_download' in data['app_data']) ? data['app_data']['apps_download'] == "yes" : false;
            context['sponsor_status'] = data['active'] == "yes";
            context['splash'] = (data['splash'] != null) ? data['splash'] : 'BLANK';
            context['preroll'] = (data['preroll'] != null) ? data['preroll'] : 'BLANK';
            context['image_ads'] = (data['image_ads'] != null) ? data['image_ads'] : 'BLANK';
            context['sponsor_app_url_android'] = ('remote_android' in data['app_data'] && data['app_data']['remote_android'] != null && data['app_data']['remote_android'] != '') ? data['app_data']['remote_android'] : 'BLANK';
            context['sponsor_app_identifier_android'] = ('package_android' in data['app_data'] && data['app_data']['package_android'] != null && data['app_data']['package_android'] != '') ? data['app_data']['package_android'] : 'BLANK';
            context['sponsor_app_url_ios'] = ('remote_ios' in data['app_data'] && data['app_data']['remote_ios'] != null && data['app_data']['remote_ios'] != '') ? data['app_data']['remote_ios'] : 'BLANK';
            context['sponsor_app_identifier_ios'] = ('package_ios' in data['app_data'] && data['app_data']['package_ios'] != null && data['app_data']['package_ios'] != '') ? data['app_data']['package_ios'] : 'BLANK';
            context['sponsor_app_tagline'] = (data['tagline'] != null && data['tagline'] != '') ? data['tagline'] : 'BLANK';
            context['sponsor_desc_large'] = (data['description'] != null && data['description'] != '') ? data['description'] : 'BLANK';
            context['sponsor_desc_small'] = (data['description_small'] != null && data['description_small'] != '') ? data['description_small'] : 'BLANK';
            context['twitter_link'] = (data['twitter_link'] != null && data['twitter_link'] != '') ? data['twitter_link'] : 'BLANK';
            context['fb_link'] = (data['fb_link'] != null && data['fb_link'] != '') ? data['fb_link'] : 'BLANK';
            context['slug_url'] = (data['slug_url'] != null && data['slug_url'] != '') ? data['slug_url'] : 'BLANK';
            context['apk_available'] = ('apk_link' in data && data['apk_link'] != null && data['apk_link'] != '');
            context['status'] = data['status'];
            context['is_global'] = data['is_global'] == "yes";
            $('.page-content').append(compiled_template.render(context));
            $('.modal-trigger').off().leanModal();

            var deferreds = [];
            var d1 = $.Deferred();
            var d2 = $.Deferred();
            deferreds.push(d1);
            deferreds.push(d2);


            if ((data['cover_image'] == "" || data['cover_image'] == null)) {

                $('.sponsor-cover-thumbnail-container').hide();
                $('.cover-alt-text').show();
            } else {
                $('.cover-thumbnail').attr('src', '/static/img/cover_loader.gif').attr('data-original',data['cover_image']).lazyload({'effect': 'fadeIn'}).on('error',function(){
                    toastr.error('Failed To load Image maybe a wrong link');
                    $('.cover-error-text').show();
                });
            }

            if ((data['logo'] == "" || data['logo'] == null)) {

                $('.sponsor-logo-thumbnail-container').hide();
                $('.logo-alt-text').show();
                d1.resolve();
            } else {
                /* settings image source of poster */
                //$('.logo-thumbnail').attr('src', '/static/img/image_loader.gif');
                $('.logo-thumbnail').attr('src', '/static/img/image_loader.gif').attr('data-original',data['logo']).lazyload({'effect': 'fadeIn'}).on('error',function(){
                    toastr.error('Failed To load Image maybe a wrong link');
                    $('.logo-error-text').show();
                    d1.resolve();
                });
            }

            if ((data['image'] == "" || data['image'] == null)) {
                $('.sponsor-image-thumbnail-container').hide();
                $('.image-desc').hide();
                $('.image-alt-text').show();
                d2.resolve();
            } else {
                /* settings image source of poster */
                $('.image-thumbnail').attr('src','/static/img/image_loader.gif').attr('data-original', data['image']).lazyload({'effect': 'fadeIn'}).on('error',function(){
                    toastr.error('Failed To load Image maybe a wrong link');
                    $('.image-error-text').show();
                    d2.resolve();
                });
            }

            if (context['splash'] == 'BLANK') {
                var d3 = $.Deferred();
                $('.splash-alt-text').show();
                d3.resolve();
                deferreds.push(d3);
            } else {
                /* iterating through each splash  and creating display modal */
                context['splash'].forEach(function(splash) {
                    var key = Object.keys(splash)[0];
                    var num = key.split('-')[3];
                    var index = parseInt(num);
                    var splash_modal = $('#splash-display-modal-test').clone().attr('id', 'splash-display-modal-' + index);
                    splash_modal.find('#video-player').attr('src', splash[key]);
                    var d = $.Deferred();
                    splash_modal.find('#video-player').load();
                    splash_modal.insertAfter('#splash-display-modal-test');
                    deferreds.push(d);
                    (function(d) {
                        splash_modal.find('#video-player').off().on('loadeddata', function() {
                            $('#splash-view-control-' + index).html('View Splash');
                            splash_modal.find('.video-preview').show();
                            d.resolve();
                        }).on('error',function(){
                            toastr.error('Failed To load splash maybe a wrong link');
                            d.resolve();
                        });
                    })(d);
                    $('.splash-view-control-template').clone().removeClass('splash-view-control-template').attr('href', '#splash-display-modal-' + index).attr('id', 'splash-view-control-' + index).show().appendTo('.splash-button-container');

                });
            }

            if (context['preroll'] == 'BLANK') {
                var d4 = $.Deferred();
                $('.preroll-alt-text').show();
                d4.resolve();
                deferreds.push(d4);
            } else {
                /* iterating through each preroll  and creating display modal */
                context['preroll'].forEach(function(preroll) {
                    var key = Object.keys(preroll)[0];
                    var num = key.split('-')[3];
                    var index = parseInt(num);
                    var preroll_modal = $('#preroll-display-modal-test').clone().attr('id', 'preroll-display-modal-' + index);
                    preroll_modal.find('#video-player').attr('src', preroll[key]);
                    var d = $.Deferred();
                    preroll_modal.find('#video-player').load();
                    preroll_modal.insertAfter('#preroll-display-modal-test');
                    deferreds.push(d);
                    (function(d) {
                        preroll_modal.find('#video-player').off().on('loadeddata', function() {
                            $('#preroll-view-control-' + index).html('View preroll');
                            preroll_modal.find('.video-preview').show();
                            d.resolve();
                        }).on('error',function(){
                            toastr.error('Failed To load preroll maybe a wrong link');
                            d.resolve();
                        });
                    })(d);
                    $('.preroll-view-control-template').clone().removeClass('preroll-view-control-template').attr('href', '#preroll-display-modal-' + index).attr('id', 'preroll-view-control-' + index).show().appendTo('.preroll-button-container');
                });

            }

            if (context['image_ads'] == 'BLANK') {
                var d5 = $.Deferred();
                $('.image-ad-alt-text').show();
                d5.resolve();
                deferreds.push(d5);
            } else {
                /* iterating through each image ads and creating display modal */
                context['image_ads'].forEach(function(image_ad) {
                    var key = Object.keys(image_ad)[0];
                    var num = key.split('-')[3];
                    var index = parseInt(num);
                    var image_ad_container = $('.image-ad-container-template').clone().attr('class', 'col s6 image-ad-container').show().insertAfter('.image-ad-container-template');
                    var d = $.Deferred();
                    deferreds.push(d);
                    /* injecting d */
                    (function(d) {
                        image_ad_container.find('img').attr('src', '/static/img/image_loader.gif').attr('data-original', image_ad[key]).lazyload({'effect': 'fadeIn'}).on('error',function(){
                            toastr.error('Failed To load Image ads maybe a wrong link');
                            $('.image-ad-error-text').show();
                            d.resolve();
                        });
                    })(d)
                    image_ad_container.find('.image-ad-name').html(image_ad['name']);
                });

            }

            $('.modal-trigger').off().leanModal();

            if (context['status'] == 'staged') {
                $('.sponsor-form-approve').css('display', 'inline-block').on('click', function() {
                    $(this).attr('disabled', 'disabled').removeClass('pointer');
                    $('.submit-indicator').empty().html($('.m-preloader').addClass('preloader-copy').clone().show());
                    $.ajax({
                        url: BASE_URL + 'sponsor/keep/' + context['sponsor_id'],
                        method: 'POST',
                        data: {
                            'status': 'complete',
                            'user_name': current_active_user['name'],
                            'user_email': current_active_user['email']
                        }
                    }).done(function(data) {
                        toastr.success('Sponsor approved successfully');
                        page('/sponsor/' + context['sponsor_id']);
                    }).fail(function(error) {
                        $('.submit-indicator').empty();
                        $('.sponsor-form-approve').removeAttr('disabled', 'disabled').addClass('pointer');
                        console.log(error);
                        if (error.status == 500 || error.status == 0) {
                            toastr.error('Something weird occured','',{timeOut: 15000});
                        }else {
                            if('responseJSON' in error){
                                if('token_id' in error['responseJSON']){
                                    toastr.error(error['responseJSON']['message'] + ': ' + error['responseJSON']['token_id'] ,'',{timeOut: 15000});
                                } else {
                                    toastr.error(error['responseJSON']['message'],'',{timeOut: 15000});
                                }
                            } else {
                                toastr.error('Bad Gateway : Someone\'s is playing with net ','',{timeOut: 15000});
                            }
                        }
                    });
                })
            }
            $('.spin-clone').remove();
            $('.sponsor-info-container').show();
            $.when.apply($, deferreds).done(function() {
            });

        } else {
            toastr.error('There war some error fetching your request');
            $('.spin-clone').remove();
            $('.sponsor-fetch-retry').show();
        }

    }

    /**
     * Prepares content edit form and binds event handlers
     * @param  Object  result  Object description recieved from ajax
     */
    function prepareContentEditForm(result) {
        if (result['status'] == 'success') {
            var alreadyInsertedRawContent = [];
            var contentCoverFileName = null;
            var contentPosterFileName = null;
            var contentPosterSmallFileName = null;
            var data = result['response']['data'];
            var compiled_template = templates['content-edit-form'];
            var selected_provider = (data['provider_id'] == null) ? 'pp-provider-1' : data['provider_id'];
            /*var template = $('#content-edit-form').html();
            var compiled_template = Hogan.compile(template);*/
            $('.content-fetch-retry').remove();
            tag_name_list = [];

            content_poster_data_url = null;
            content_cover_data_url = null;

            var raw_media_id = null;
            var raw_media_url = null;

            var context = {};
            context['name'] = data['name'];
            context['duration'] = ('duration' in data && data['duration'] != null) ? secondsToHms(data['duration']) : '';
            context['language'] = ('language' in data && data['language'] != null) ? data['language'] : '';
            context['description'] = ('description' in data && data['description'] != null) ? data['description'] : '';
            context['short_description'] = ('short_description' in data && data['short_description'] != null) ? data['short_description'] : '';
            context['genre'] = (data['genre'] != null) ? data['genre'].join(',') : '';
            context['cast'] = ('specifics' in data && data['specifics'] != null && 'cast' in data['specifics'] && data['specifics']['cast'] != null) ? data['specifics']['cast'].join(',') : '';
            context['id'] = data['id'];
            context['status'] = data['status'];
            context['tags'] = data['tags'];
            context['provider'] = data['provider_name'];
            context['expiry'] = (('expiry' in data) && data['expiry'] != null && data['expiry'] != '') ? secondsToHmsKeepingHRS(data['expiry']/1000) : '';
            context['cost'] = (('cost' in data) && data['cost'] != null && data['cost'] != '') ? returnInIndianCostFormat(data['cost']) : '';
            context['seo'] = ('seo' in data && data['seo'] != null && data['seo'] != '') ? data['seo'] : '';
            var original_poster_url = data['poster'];
            var original_poster_small_url = data['poster_small'];
            var original_cover_url = data['cover_image'];
            content_poster_data_url = data['poster'];
            content_cover_data_url = data['cover_image'];
            content_poster_small_data_url = data['poster_small'];
            var old_local_link = data['local_link'];
            raw_media_url = old_local_link;
            $('.page-content').append(compiled_template.render(context));
            var type = data['type'];
            var box_only = data['box_only'] == "yes";
            var featured = data['featured'] == "yes";
            var premium = data['premium'] == "yes";
            var is_movie = data['is_movie'] == "yes";
            var allow_download = data['allow_download'] == "yes";
            var trailer_items = {};
            var trailers_stored = [];
            $('#content-type').val(type);
            if (is_movie) {
                $('#content-is-movie').attr('checked', 'checked');
            }
            if (box_only) {
                $('#content-box-only').attr('checked', 'checked');
            }
            if (featured) {
                $('#content-feature').attr('checked', 'checked');
            }
            if (premium) {
                $('#content-premium').attr('checked', 'checked');
                $('.premium-requirements').show();
            }
            if (allow_download) {
                $('#content-allow-download').attr('checked', 'checked');
            }


            $('#content-video-type').val(data['video_content_type']);
            if(data['type'] == 'video'){
                $('.content-video-type-wrapper').show();
            }

            $('#content-type').on('change',function(){
                if($(this).val() == 'video' ){
                    $('.content-video-type-wrapper').show('slow');
                } else {
                    $('.content-video-type-wrapper').hide('hide');
                }
            });

            //updates slug for typed name
            $('#content-name').on('input',function(){
                var name = $(this).val();
                var slug = slugify(name);
                if(name != ''){
                    $('.slug-available').show();
                    $('.generated-slug').text(slug);
                } else {
                    $('.slug-available').hide();
                }
            });

            $('#content-premium').on('change',function(){
                if($('#content-premium').is(':checked')){
                    $('.premium-requirements').show('slow');
                } else {
                    $('.premium-requirements').hide('slow');
                }
            });

            //concatinates fetched tags to tag_list to keep track of existing tags
            tag_name_list = tag_name_list.concat(context['tags']);

            /* add modal */
            /* rendering image upload template */
            $('#cover-add-modal').html(compiled_image_template.render({
                title: 'Edit cover'
            }));
            $('#poster-add-modal').html(compiled_image_template.render({
                title: 'Edit poster'
            }));

            $('#poster-small-add-modal').html(compiled_image_template.render({
                title: 'Edit poster'
            }));
            $('input.length-field').characterCounter();
            $('.modal-trigger').off().leanModal();


            $('#media-preview-modal').html($('#media-preview-template').html());
            $('#file-explorer-modal').html($('#file-explorer').html());

            $('#file-explorer-modal').find('.wait-text').html('Fetching raw content list');
            //console.log($('.spin').clone().addClass('spin-clone').show());
            $('#file-explorer-modal').find('.spinner-section').append($('.spin').clone().addClass('spin-clone').show());

            $('.modal-trigger').off().leanModal();

            /* initialize js components */
            $('#content-type').material_select();
            $('#content-video-type').material_select();

            data['trailers'].forEach(function(trailer,i){

                var new_trailer_button = $('.content-trailer-item.base-template').clone();
                $(new_trailer_button).insertBefore('.trailer-add-button');
                $(new_trailer_button).attr('id','content-trailer-item-' + i);
                $(new_trailer_button).removeClass('base-template');
                $(new_trailer_button).removeClass('p-hidden');
                $(new_trailer_button).find('.trailer-toolbar').show();
                $(new_trailer_button).find('.trailer-add-trigger').hide();
                var name = trailer['name'];
                var trailer_id = trailer['raw_content_id'];
                var trailer_link = trailer['local_link'];
                var media_type = trailer['media_type'];
                $(new_trailer_button).find('.trailer-name').html(name);
                $(new_trailer_button).find('.trailer-preview').attr('data-link', trailer_link);
                $(new_trailer_button).find('.trailer-preview').attr('data-type', media_type);
                trailers_stored.push(trailer['raw_content_id']);
                trailer_items['trailer-' + i] = trailer_id;
            });

            prepareImageUploaderModal('#cover-add-modal', function(poster_src, filename) {
                contentCoverFileName = filename;
                $('.cover-thumbnail').attr('src', poster_src);
                content_cover_data_url = poster_src;
                $('.content-cover-thumbnail-container').show();
                $('.content-cover-control').html('Edit cover');
            }, function() {
                contentCoverFileName = null;
                content_cover_data_url = null;
                $('.cover-thumbnail').attr('src', '');
                $('.content-cover-thumbnail-container').hide();
                $('.content-cover-control').html('Add cover');
            }, contentCoverDimensions);

            prepareImageUploaderModal('#poster-add-modal', function(poster_src, filename) {
                contentPosterFileName = filename;
                $('.poster-thumbnail').attr('src', poster_src);
                content_poster_data_url = poster_src;
                $('.content-poster-thumbnail-container').show();
                $('.content-poster-control').html('Edit poster');
            }, function() {
                contentPosterFileName = null;
                content_poster_data_url = null;
                $('.poster-thumbnail').attr('src', '');
                $('.content-poster-thumbnail-container').hide();
                $('.content-poster-control').html('Add poster');
            }, contentPosterDimensions);

            prepareImageUploaderModal('#poster-small-add-modal', function(poster_src, filename) {
                contentPosterSmallFileName = filename;
                $('.poster-small-thumbnail').attr('src', poster_src);
                content_poster_small_data_url = poster_src;
                $('.content-poster-small-thumbnail-container').show();
                $('.content-poster-small-control').html('Edit Icon');
            }, function() {
                contentPosterSmallFileName = null;
                content_poster_small_data_url = null;
                $('.poster-small-thumbnail').attr('src', '');
                $('.content-poster-small-thumbnail-container').hide();
                $('.content-poster-small-control').html('Add icon');
            }, contentIconDimensions);

            $('.modal-close').click(function(){
                $('.active-trailer').removeClass('active-trailer');
            })

            $(document).off('click', '.file-add-trigger , .trailer-add-trigger');
            $(document).on('click', '.file-add-trigger , .trailer-add-trigger',function() {
                var content_type = $('#content-type').val();
                if(content_type == null){
                    toastr.error('Please select content type first');
                    return;
                }

                var type = 'file';

                if($(this).hasClass('trailer-add-trigger')){
                    var parent = $(this).parent();
                    $(parent).addClass('active-trailer');
                    type = 'trailer';
                }
                console.log(type);

                $('.file-explorer-wait-section').show();
                $('.main-explorer-container').hide();
                $('.file-explorer-preview').hide().find('.audio-preview').hide();
                $('.file-explorer-preview').find('.video-preview').hide();
                $('.s3-explorer').click();
                $.ajax({
                    url: BASE_URL + 'util/get-raw-contents/'+content_type,
                    method: 'GET',
                }).done(function(result) {
                    if (!result['file-list'].length) {
                        $('.modal-close').click();
                        toastr.error('No content available in S3 bucket');
                        return;
                    }
                    prepareRawFileExplorer(result['file-list'],type,alreadyInsertedRawContent);
                }).fail(function(data) {
                    $('.modal-close').click();
                    console.log(data);
                    toastr.error('Unable to fetch data from S3 bucket. Please try again later');
                })
            });

            $(document).off('click', '.file-play');
            $(document).on('click', '.file-play', function() {
                var type = $(this).attr('data-type');
                var link = $(this).attr('data-link');
                if (type == 'audio') {
                    $('.file-explorer-preview').find('.audio-preview').show();
                    var audio_player = $('.file-explorer-preview').find('audio');
                    $(audio_player).show().attr('src', link).load();
                    $('.file-explorer-preview').show();
                } else {
                    $('.file-explorer-preview').find('.video-preview').show();
                    var video_player = $('.file-explorer-preview').find('video');
                    $(video_player).show().attr('src', link).load();
                    $('.file-explorer-preview').show();
                }
                $('#file-explorer-modal').scrollTop(0);
            });
            $(document).off('click', '.file-preview-close');
            $(document).on('click', '.file-preview-close', function() {
                $('.file-explorer-preview').find('audio').attr('src', '');
                $('.file-explorer-preview').find('video').attr('src', '');
                $('.file-explorer-preview').hide();
            });

            $(document).off('click', '.trailer-add');
            $(document).on('click', '.trailer-add', function() {
                var name = $(this).attr('data-name');
                var trailer_id = $(this).attr('data-id');
                var trailer_link = $(this).attr('data-link');
                var media_type = $(this).attr('data-type');
                $('.active-trailer').find('.trailer-name').html(name);
                $('.active-trailer').find('.trailer-preview').attr('data-link', trailer_link);
                $('.active-trailer').find('.trailer-preview').attr('data-type', media_type);
                var index = $('.active-trailer').attr('id').split('-')[3];
                trailer_items['trailer-' + index] = trailer_id;
                trailers_stored[index] = trailer_id;
                $('.active-trailer').find('.trailer-add-trigger').hide();
                $('.active-trailer').find('.trailer-toolbar').show();
                $('.modal-close').click();
                $('.active-trailer').removeClass('active-trailer');
                if($.inArray(trailer_id,alreadyInsertedRawContent) < 0){
                    alreadyInsertedRawContent.push(trailer_id);
                }
            });

            $(document).off('click', '.trailer-remove');
            $(document).on('click', '.trailer-remove', function() {
                var container = $(this).parent().parent();
                var id = $(container).attr('id');
                var index = id.split('-')[3];
                var raw_id = trailer_items['trailer-' + index];
                delete trailer_items['trailer-' + index];
                trailers_stored[index] = '';
                $(container).find('.trailer-add-trigger').show();
                $(container).find('.trailer-toolbar').hide();
                var arrayi = alreadyInsertedRawContent.indexOf(raw_id);
                if(arrayi > -1){
                    alreadyInsertedRawContent.splice(arrayi,1);
                }
            });

            $(document).off('click', '.file-add');
            $(document).on('click', '.file-add', function() {
                var name = $(this).attr('data-name');
                raw_media_id = $(this).attr('data-id');
                raw_media_url = $(this).attr('data-link');
                var media_type = $(this).attr('data-type');
                $('.media-name').html(name);
                $('.media-preview').attr('data-link', raw_media_url);
                $('.media-preview').attr('data-type', media_type);

                $('.file-add-trigger').hide();
                $('.media-toolbar').show();
                $('.modal-close').click();
                if($.inArray(raw_media_id,alreadyInsertedRawContent) < 0){
                    alreadyInsertedRawContent.push(raw_media_id);
                }
            });

            $(document).off('click', '.media-remove');
            $(document).on('click', '.media-remove', function() {
                raw_media_url = null;
                raw_media_id = null;
                $('.file-add-trigger').show();
                $('.media-toolbar').hide();
                var arrayi = alreadyInsertedRawContent.indexOf(raw_media_id);
                if(arrayi > -1){
                    alreadyInsertedRawContent.splice(arrayi,1);
                }
            });

            $(document).off('click', '.media-preview , .trailer-preview');
            $(document).on('click', '.media-preview , .trailer-preview', function() {
                var name = $(this).data('name');
                var link = $(this).data('link');
                var type = $(this).data('type');
                $('#media-preview-modal').find('.media-title').html(name);
                var link_fragments = link.split('.');
                var ext = link_fragments[link_fragments.length - 1]
                if (type == 'audio') {
                    $('#media-preview-modal').find('.video-preview').hide();
                    $('#media-preview-modal').find('.audio-preview').show();
                    $('#media-preview-modal').find('#audio-player').attr('src', link).load();
                    $('#media-preview-modal').find('#audio-player').off().on('error', function() {
                        toastr.error('Unable to preview media. Probably some error occured ');
                        $('.modal-close').click();
                    })
                } else {
                    $('#media-preview-modal').find('.audio-preview').hide();
                    $('#media-preview-modal').find('.video-preview').show();
                    $('#media-preview-modal').find('#video-player').attr('src', link).load();
                    $('#media-preview-modal').find('#video-player').off().on('error', function() {
                        toastr.error('Unable to preview media. Probably some error occured ');
                        $('.modal-close').click();
                    });
                }
                $('.media-preview-trigger').click();
            });

            $('.trailer-add-button').click(function(){
                for(var i=0;i<trailers_stored.length;i++ ){
                    if(trailers_stored[i] == ''){
                        toastr.error('use already avialable element');
                        return;
                    }
                }
                var new_trailer_button = $('.content-trailer-item.base-template').clone();
                $(new_trailer_button).insertBefore('.trailer-add-button');
                $(new_trailer_button).removeClass('base-template');
                $(new_trailer_button).removeClass('p-hidden');
                $(new_trailer_button).find('.trailer-toolbar').hide();
                $(new_trailer_button).find('.trailer-add-trigger').show();
                var trailer_index = trailers_stored.length;
                $(new_trailer_button).attr('id','content-trailer-item-' + trailer_index);
                trailers_stored.push('');
            });

            if(raw_media_url != '' && raw_media_url != null){
                raw_media_url = data['local_link'];
                raw_media_id = data['raw_content_id'];
                $('.media-preview').attr('data-link', raw_media_url);
                $('.media-preview').attr('data-type', data['type']);
                $('.file-add-trigger').hide();
                $('.media-toolbar').show();

            }
            else{
                $('.file-add-trigger').show();
                $('.media-toolbar').hide();
            }


            /* add form validation */
            /* binding validator class */

            var content_form_validator = new Validator(content_validator_seed, '.content-form-update', function(errors, warnings, cleanCallback) {
                if (Object.keys(errors).length > 0) {
                    toastr.error('Please correct the errors in form');
                    cleanCallback();
                } else {
                    /* creating form data to post data to server */
                    var fd = new FormData();
                    fd.append('name', $('#content-name').val().trim());
                    fd.append('slug_url',slugify($('#content-name').val()));
                    var content_genre = $('#content-genre').val().trim();
                    if (content_genre != null) {
                        fd.append('genre', JSON.stringify(content_genre.split(',')));
                    } else {
                        fd.append('genre', null);
                    }

                    var language = $('#content-language').val().trim();
                    if (language != null && language != "") {
                        fd.append('language', language);
                    } else {
                        fd.append('language', '');
                    }

                    fd.append('trailers',JSON.stringify(trailer_items));

                    var duration = hmsToSeconds($('#content-duration').val());
                    if (duration != null && duration != "") {
                        fd.append('duration', duration);
                    } else {
                        fd.append('duration', '');
                    }

                    var type = $('#content-type').val();
                    if (type != null && type != "") {
                        fd.append('type', type);
                    } else {
                        fd.append('type', '');
                    }

                    var short_description = $('#content-short-description').val().trim();
                    if (short_description != null && short_description != "") {
                        fd.append('short_description', short_description);
                    } else {
                        fd.append('short_description', '');
                    }

                    var description = $('#content-description').val().trim();
                    if (description != null && description != "") {
                        fd.append('description', description);
                    } else {
                        fd.append('description', '');
                    }

                    var seo = $('#content-seo').val().trim();
                    if (seo != null && seo != "") {
                        fd.append('seo', seo);
                    } else {
                        fd.append('seo', '');
                    }


                    if($('#edit-content-tag-list').val() == ""){
                        fd.append('tags','[]');
                    }
                    else{
                        var tag_string = $('#edit-content-tag-list').val().split(',');
                        var tag_array = [];
                        tag_string.forEach(function(name){
                            if($.inArray(name,tag_name_list)<0){
                                var trimed = name.trim();
                                tag_array.push({
                                    type: "new",
                                    name: trimed
                                });
                            }
                            else {
                                tag_array.push({
                                    type: "existing",
                                    name: name
                                });
                            }
                        });
                        fd.append('tags',JSON.stringify(tag_array));
                    }

                    fd.append('provider_id',selected_provider);

                    if(old_local_link != raw_media_url){
                            fd.append('raw_content_id', raw_media_id);
                    }

                    var expiry = $('#content-expiry').val();
                    var cost = $('#content-cost').val();
                    if($('#content-premium').is(':checked')){
                        fd.append('premium',"yes");
                        fd.append('expiry',hmsToSeconds(expiry) * 1000);
                        fd.append('cost',cost);
                    } else {
                        fd.append('premium',"no");
                    }

                    var starcast = $('#content-star-cast').val().trim();
                    if (starcast != null && starcast != "") {
                        var specifics = {};
                        specifics['cast'] = starcast.split(',');
                        fd.append('specifics', JSON.stringify(specifics));
                    } else {
                        var specifics = {};
                        specifics['cast'] = null;
                        fd.append('specifics', JSON.stringify(specifics));
                    }
                    fd.append('box_only', $('#content-box-only').is(':checked') ? "yes" : "no");
                    fd.append('featured', $('#content-feature').is(':checked') ? "yes" : "no");
                    fd.append('allow_download', $('#content-allow-download').is(':checked') ? "yes" : "no");
                    var video_content_type = $('#content-video-type').val();
                    fd.append('video_content_type', video_content_type);

                    if (original_cover_url != content_cover_data_url) {
                        if (content_cover_data_url != null) {
                            var blob = dataURItoBlob(content_cover_data_url);
                            fd.append('cover_image', blob);
                            fd.append('cover_image_name', contentCoverFileName);
                        } else {
                            fd.append('cover_image', null);
                            fd.append('cover_image_name', null);
                        }
                    }

                    if (original_poster_url != content_poster_data_url) {
                        if (content_poster_data_url != null) {
                            var blob = dataURItoBlob(content_poster_data_url);
                            fd.append('poster', blob);
                            fd.append('poster_name', contentPosterFileName);
                        } else {
                            fd.append('poster', null);
                            fd.append('poster_name', null);
                        }
                    }

                    if (original_poster_small_url != content_poster_small_data_url) {
                        if (content_poster_small_data_url != null) {
                            var blob = dataURItoBlob(content_poster_small_data_url);
                            fd.append('poster_small', blob);
                            fd.append('poster_small_name', contentPosterSmallFileName);
                        } else {
                            fd.append('poster_small', null);
                            fd.append('poster_small_name', null);
                        }
                    }

                    var status = "incomplete";
                    var essentials = ['name', 'duration' , 'type'];
                    /* If warnings object is empty */
                    if (canBeStaged(warnings, essentials) && content_poster_data_url != null && content_poster_small_data_url != null && type != null && raw_media_id != null && tag_string != null) {
                        if(video_content_type == 'shortform'){
                            if(seo == null || seo == ""){
                                status = 'seo_pending';
                            } else if (context['status'] == 'complete') {
                                status = 'complete';
                            } else {
                                status = "staged";
                            }
                        } else {
                            if(description == null || description == '' || short_description == null || short_description == '' ){
                                status = 'incomplete';
                            } else if(seo == null || seo == ""){
                                status = 'seo_pending';
                            } else if (context['status'] == 'complete') {
                                status = 'complete';
                            } else {
                                status = "staged";
                            }
                        }
                    }


                    fd.append("status", status);

                    fd.append('user_name', current_active_user['name']);
                    fd.append('user_email', current_active_user['email']);
                    /* posting data to server */
                    $('.content-form-update').attr('disabled', 'disabled').removeClass('pointer');
                    $('.submit-indicator').empty().html($('.m-preloader').addClass('preloader-copy').clone().show());


                    $.ajax({
                        url: BASE_URL + 'content/keep/' + context['id'],
                        method: 'POST',
                        data: fd,
                        processData: false,
                        contentType: false
                    }).done(function(result) {
                        toastr.success('Content updated successully');
                        toastr.info('Redirecting to content display page');
                        setTimeout(
                            function() {
                                page('/content/' + result['storage_id']);
                            }, 2000);

                    }).fail(function(data) {
                        console.log(data);

                    }).always(function() {
                        $('.content-form-update').removeAttr('disabled').addClass('pointer');
                        $('.submit-indicator').empty();
                        cleanCallback();
                    });

                }
            }, formValidationViewUpdate);

            content_form_validator.fortify();

            var deferreds = [];
            /*var content_tag_list = $.get(BASE_URL + 'tag/rain?status=complete');
            deferreds.push(content_tag_list);*/
            var d2 = $.Deferred();
            deferreds.push(d2)
            var d3 = $.Deferred();
            deferreds.push(d3)


            if (data['cover_image'] != null) {
                /* settings image source of poster */
                $('.cover-thumbnail').attr('src','/static/img/cover_loader.gif').attr('data-original',data['cover_image']).lazyload({'effect':'fadeIn'}).on('load', function() {
                    $('#cover-add-modal').find('.poster-preview').empty().html($('.cover-thumbnail').clone().css('opacity','1'));
                    $('#cover-add-modal').find('.poster-input-section').hide();
                    $('#cover-add-modal').find('.poster-info').show();
                    $('#cover-add-modal').find('.cropper-tool').hide();
                    $('#cover-add-modal').find('.poster-preview').show();
                    $('#cover-add-modal').find('.poster-confirmation').hide();
                    $('.content-cover-thumbnail-container').show();
                });
            }

            if (data['poster'] != null) {
                /* settings image source of poster */
                $('.poster-thumbnail').attr('src','/static/img/image_loader.gif').attr('data-original',data['poster']).lazyload({'effect':'fadeIn'}).on('load', function() {
                    $('#poster-add-modal').find('.poster-preview').empty().html($('.poster-thumbnail').clone().css('opacity','1'));
                    $('#poster-add-modal').find('.poster-input-section').hide();
                    $('#poster-add-modal').find('.poster-info').show();
                    $('#poster-add-modal').find('.cropper-tool').hide();
                    $('#poster-add-modal').find('.poster-preview').show();
                    $('#poster-add-modal').find('.poster-confirmation').hide();
                    $('.content-poster-thumbnail-container').show();
                    d2.resolve();
                });
            } else {
                d2.resolve();
            }


            if (data['poster_small'] != null) {
                /* settings image source of poster */
                $('.poster-small-thumbnail').attr('src', '/static/img/image_loader.gif').attr('data-original',data['poster_small']).lazyload({'effect':'fadeIn'}).on('load', function() {
                    $('#poster-small-add-modal').find('.poster-preview').empty().html($('.poster-small-thumbnail').clone().css('opacity','1'));
                    $('#poster-small-add-modal').find('.poster-input-section').hide();
                    $('#poster-small-add-modal').find('.poster-info').show();
                    $('#poster-small-add-modal').find('.cropper-tool').hide();
                    $('#poster-small-add-modal').find('.poster-preview').show();
                    $('#poster-small-add-modal').find('.poster-confirmation').hide();
                    $('.content-poster-small-thumbnail-container').show();
                    d3.resolve();
                });
            } else {
                d3.resolve();
            }

            $('#edit-content-tag-list').val(data['tags']);

            $('#edit-content-tag-list').tagsinput({
                typeahead: {
                    source: function (query,process) {
                        $.get(BASE_URL + 'bloodhound/tag',{query: query},function(data){
                            tag_name_list = tag_name_list.concat(data.data);
                            return process(data.data);
                        });
                    },
                    minLength: 3
                }
            });
            $('.bootstrap-tagsinput input').attr('placeholder','Choose tags');

            $('#content-provider').typeahead({
                source: function(query,process){
                    return $.get(BASE_URL + 'bloodhound/provider',{query: query},function(data){
                        return process(data.data);
                    });
                },
                minLength:3,
                afterSelect: function(item){
                    selected_provider = item.id;
                }
            });
            $('.spin-clone').remove();
            $('.content-edit-container').show();
            $.when.apply($,deferreds).done(function(d2,d3){
            });

        } else {
            toastr.error('There war some error fetching your request');
            $('.content-fetch-retry').show();
            $('.spin-clone').hide();
        }
    };

    /**
     * prepares channel edit form using result object passed
     * @param  Object   result  Object recived after requesting data from server
     */
    function prepareChannelEditForm(result) {
        if (result['status'] == 'success') {

            var channelCoverFileName = null;
            var channelPosterFileName = null;
            var channelPosterSmallFileName = null;
            var data = result['response']['data'];
            var compiled_template = templates['channel-edit-form'];
            var selected_provider = (data['provider_id'] == null) ? 'pp-provider-1' : data['provider_id'];
            /*var template = $('#channel-edit-form').html();
            var compiled_template = Hogan.compile(template);*/
            $('.channel-fetch-retry').remove();
            $('input.length-field').characterCounter();
            tag_name_list = [];
            var context = {};
            context['name'] = data['name'];
            context['display_name'] = data['display_name'];
            context['id'] = data['id'];
            context['status'] = data['status'];
            context['tags'] = data['tags'];
            context['description'] = ('description' in data && data['description'] != null && data['description'] != '') ? data['description'] : '';
            context['short_description'] = ('short_description' in data && data['short_description'] != null && data['short_description'] != '') ? data['short_description'] : '';
            context['provider'] = data['provider_name'];
            context['tagline'] = ('tagline' in data && data['tagline'] != null) ? data['tagline'] : '';

            var original_channel_cover = data['cover_image'];
            channel_cover_data_url = data['cover_image'];

            var original_channel_icon = data['icon'];
            channel_icon_data_url = data['icon'];

            var original_channel_icon_small = data['icon_small'];
            channel_icon_small_data_url = data['icon_small'];

            var original_channel_splash = data['base_splash'];
            splashFileObject = data['base_splash'];

            $('.page-content').append(compiled_template.render(context));

            var highlight = data['highlighted'] == "yes";
            if (highlight) {
                $('#channel-highlight').attr('checked', 'checked');
            }
            var is_global = data['is_global'] == "yes";
            if (is_global) {
                $('#channel-is-global').attr('checked', 'checked');
            }
            var show_title = data['show_title'] == "yes";
            if (show_title) {
                $('#channel-show-title').attr('checked', 'checked');
            }

            /* rendering image upload template */
            $('#cover-add-modal').html(compiled_image_template.render({
                title: 'Add Cover'
            }));
            $('#poster-add-modal').html(compiled_image_template.render({
                title: 'Add poster'
            }));
            $('#poster-small-add-modal').html(compiled_image_template.render({
                title: 'Add Icon'
            }));
            $('#splash-add-modal').html(compiled_video_template.render({
                title: 'Add Splash'
            }));
            /* rendering image upload template */

            //concatinates fetched tags to tag_list to keep track of existing tags
            tag_name_list = tag_name_list.concat(context['tags']);

            /* initialize js components */

            prepareImageUploaderModal('#cover-add-modal', function(poster_src, filename) {
                channelCoverFileName = filename;
                $('.cover-thumbnail').attr('src', poster_src);
                channel_cover_data_url = poster_src;
                $('.channel-cover-thumbnail-container').show();
                $('.channel-cover-control').html('Edit Cover');
            }, function() {
                channelcoverFileName = null;
                channel_cover_data_url = null;
                $('.cover-thumbnail').attr('src', '');
                $('.channel-cover-thumbnail-container').hide();
                $('.channel-cover-control').html('Add cover');
            }, channelCoverDimensions);

            prepareImageUploaderModal('#poster-add-modal', function(poster_src, filename) {
                channelPosterFileName = filename;
                $('.icon-thumbnail').attr('src', poster_src);
                channel_icon_data_url = poster_src;
                $('.channel-icon-thumbnail-container').show();
                $('.channel-icon-control').html('Edit icon');
            }, function() {
                channelPosterFileName = null;
                channel_icon_data_url = null;
                $('.icon-thumbnail').attr('src', '');
                $('.channel-icon-thumbnail-container').hide();
                $('.channel-icon-control').html('Add icon');
            }, channelPosterDimensions);

            prepareImageUploaderModal('#poster-small-add-modal', function(poster_src, filename) {
                channelPosterSmallFileName = filename;
                $('.icon-small-thumbnail').attr('src', poster_src);
                channel_icon_small_data_url = poster_src;
                $('.channel-icon-small-thumbnail-container').show();
                $('.channel-icon-small-control').html('Edit Small icon');
            }, function() {
                channelPosterSmallFileName = null;
                channel_icon_small_data_url = null;
                $('.icon-small-thumbnail').attr('src', '');
                $('.channel-icon-small-thumbnail-container').hide();
                $('.channel-icon-small-control').html('Add Small icon');
            }, channelIconDimensions);

            prepareVideoUploaderModal('#splash-add-modal', function(file) {
                splashFileObject = file;
                $('.channel-splash-name').html(file.name).show();
                $('.new-channel-splash').html('Edit splash');
            }, function() {
                splashFileObject = null;
                $('.channel-splash-name').html('').hide();
                $('.new-channel-splash').html('Upload splash');
            });

            $('.modal-trigger').off().leanModal();

            $('#channel-name').on('input',function(){
                var name = $(this).val();
                var slug = slugify(name);
                if(name != ''){
                    $('.slug-available').show();
                    $('.generated-slug').text(slug);
                } else {
                    $('.slug-available').hide();
                }
            });

            /* binding validator class */

            var channel_form_validator = new Validator(channel_validator_seed, '.channel-form-update', function(errors, warnings, cleanCallback) {
                if (Object.keys(errors).length > 0) {
                    console.log(errors);
                    toastr.error('Please correct the errors in form');
                    cleanCallback;
                } else {
                    /* creating form data to post data to server */
                    var fd = new FormData();
                    fd.append('name', $('#channel-name').val().trim());
                    fd.append('slug_url', slugify($('#channel-name').val()));
                    fd.append('display_name', $('#channel-display-name').val().trim());
                    var description = $('#channel-description').val().trim();
                    fd.append('description', description);
                    var short_description = $('#channel-short-description').val().trim();
                    fd.append('short_description', short_description);
                    var tagline = $('#channel-tagline').val().trim();
                    fd.append('tagline', tagline);

                    fd.append('highlighted', $('#channel-highlight').is(':checked') ? "yes" : "no");
                    fd.append('is_global', $('#channel-is-global').is(':checked') ? "yes" : "no");
                    fd.append('show_title', $('#channel-show-title').is(':checked') ? "yes" : "no");


                    if($('#edit-channel-tag-list').val() == ""){
                        fd.append('tags','[]');
                    }
                    else{
                        var tag_string = $('#edit-channel-tag-list').val().split(',');
                        var tag_array = [];
                        tag_string.forEach(function(name){
                            if($.inArray(name,tag_name_list)<0){
                                var trimed = name.trim();
                                tag_array.push({
                                    type: "new",
                                    name: trimed
                                });
                            }
                            else {
                                tag_array.push({
                                    type: "existing",
                                    name: name
                                });
                            }
                        });
                        fd.append('tags',JSON.stringify(tag_array));
                    }
                    fd.append('provider_id',selected_provider);

                    if (original_channel_cover != channel_cover_data_url) {
                        if (channel_cover_data_url != "" && channel_cover_data_url != null) {
                            var blob = dataURItoBlob(channel_cover_data_url);
                            fd.append('cover_image', blob);
                            fd.append('cover_image_name', channelCoverFileName);
                        } else {
                            fd.append('cover_image', null);
                            fd.append('cover_image_name', null);
                        }
                    }

                    if (original_channel_icon != channel_icon_data_url) {
                        if (channel_icon_data_url != "" && channel_icon_data_url != null) {
                            var blob = dataURItoBlob(channel_icon_data_url);
                            fd.append('icon', blob);
                            fd.append('icon_name', channelPosterFileName);
                        } else {
                            fd.append('icon', null);
                            fd.append('icon_name', null);
                        }
                    }

                    if (original_channel_icon_small != channel_icon_small_data_url) {
                        if (channel_icon_small_data_url != "" && channel_icon_small_data_url != null) {
                            var blob = dataURItoBlob(channel_icon_small_data_url);
                            fd.append('icon_small', blob);
                            fd.append('icon_small_name', channelPosterSmallFileName);
                        } else {
                            fd.append('icon_small', null);
                            fd.append('icon_small_name', null);
                        }
                    }

                    if (original_channel_splash != splashFileObject) {
                        if (splashFileObject != null && splashFileObject != "") {
                            fd.append('base_splash', splashFileObject);
                            fd.append('base_splash_name', splashFileObject.name);
                        } else {
                            fd.append('base_splash', null);
                            fd.append('base_splash_name', null);
                        }
                    }


                    var status = "incomplete";
                    var essentials = ['name', 'display-name'];
                    /* If warnings object is empty */
                    if (canBeStaged(warnings, essentials) && channel_icon_data_url != null && channel_icon_small_data_url != null && tag_string != null && description!=null && description!='' && short_description!=null && short_description!='') {
                        if (context['status'] == 'complete') {
                            status = 'complete';
                        } else {
                            status = "staged";
                        }
                    }


                    fd.append("status", status);
                    fd.append('user_name', current_active_user['name']);
                    fd.append('user_email', current_active_user['email']);
                    /* posting data to server */
                    $('.channel-form-update').attr('disabled', 'disabled').removeClass('pointer');
                    $('.submit-indicator').append($('.m-preloader').addClass('preloader-copy').clone().show());

                    // TODO remove
                    // var xhr = new XMLHttpRequest;
                    // xhr.open('POST', '/', true);
                    // xhr.send(fd);
                    // return;
                    $.ajax({
                        url: BASE_URL + 'channel/keep/' + context['id'],
                        method: 'POST',
                        data: fd,
                        processData: false,
                        contentType: false
                    }).done(function(result) {
                        toastr.success('Channel edited successully');
                        toastr.info('Redirecting to channel display page');
                        setTimeout(
                            function() {
                                page('/channel/' + result['storage_id']);
                            }, 2000);
                    }).fail(function(data) {
                        console.log(data);
                        showError(data);
                    }).always(function() {
                        $('.channel-form-submit').removeAttr('disabled', 'disabled').addClass('pointer');
                        $('.submit-indicator').empty();
                        cleanCallback();
                    });

                }
            }, formValidationViewUpdate)

            channel_form_validator.fortify();

            var d1 = $.Deferred();
            var d2 = $.Deferred();
            var d3 = $.Deferred();
            var deferreds = [];
            deferreds.push(d1);
            deferreds.push(d2);
            deferreds.push(d3);

            if (data['cover_image'] != null) {
                /* settings image source of poster */
                $('.cover-thumbnail').attr('src', '/static/img/cover_loader.gif').attr('data-original',data['cover_image']).lazyload({'effect':'fadeIn'}).on('load', function() {
                    $('#cover-add-modal').find('.poster-preview').empty().html($('.cover-thumbnail').clone().css('opacity','1'));
                    $('#cover-add-modal').find('.poster-input-section').hide();
                    $('#cover-add-modal').find('.poster-info').show();
                    $('#cover-add-modal').find('.cropper-tool').hide();
                    $('#cover-add-modal').find('.poster-preview').show();
                    $('#cover-add-modal').find('.poster-confirmation').hide();
                    $('.channel-cover-thumbnail-container').show();
                    $('.channel-cover-control').html('Edit cover');
                });
            }

            if (data['icon'] != null) {
                /* settings image source of poster */
                $('.icon-thumbnail').attr('src', '/static/img/image_loader.gif').attr('data-original',data['icon']).lazyload({'effect':'fadeIn'}).on('load', function() {
                    $('#poster-add-modal').find('.poster-preview').empty().html($('.icon-thumbnail').clone().css('opacity','1'));
                    $('#poster-add-modal').find('.poster-input-section').hide();
                    $('#poster-add-modal').find('.poster-info').show();
                    $('#poster-add-modal').find('.cropper-tool').hide();
                    $('#poster-add-modal').find('.poster-preview').show();
                    $('#poster-add-modal').find('.poster-confirmation').hide();
                    $('.channel-icon-thumbnail-container').show();
                    $('.channel-icon-control').html('Edit icon');
                    d1.resolve();
                    console.log('icon loaded');
                });
            } else {
                d1.resolve();
                console.log('not available');

            }

            if (data['icon_small'] != null) {
                /* settings image source of poster */
                $('.icon-small-thumbnail').attr('src', '/static/img/image_loader.gif').attr('data-original',data['icon_small']).lazyload({'effect':'fadeIn'}).on('load', function() {
                    $('#poster-small-add-modal').find('.poster-preview').empty().html($('.icon-small-thumbnail').clone().css('opacity','1'));
                    $('#poster-small-add-modal').find('.poster-input-section').hide();
                    $('#poster-small-add-modal').find('.poster-info').show();
                    $('#poster-small-add-modal').find('.cropper-tool').hide();
                    $('#poster-small-add-modal').find('.poster-preview').show();
                    $('#poster-small-add-modal').find('.poster-confirmation').hide();
                    $('.channel-icon-small-thumbnail-container').show();
                    $('.channel-icon-small-control').html('Edit icon');
                    d2.resolve();
                    console.log('icon loaded');
                });
            } else {
                d2.resolve();
                console.log('not available');

            }

            if (data['base_splash'] != null) {
                $('.video-input-section').hide();
                $('.video-info').show();
                $('.channel-splash').html('Edit splash');
                $('#video-player').attr('src', data['base_splash']);
                $('#video-player').load();
                console.log('video player');
                $('#video-player').off().on('loadeddata', function() {
                    console.log('video loaded');
                    $('.video-preview').show();
                    d3.resolve();
                });
            } else {
                $('.video-input-section').show();
                $('.video-info').hide();
                d3.resolve();
            }

            $('#edit-channel-tag-list').val(data['tags']);
            $('#edit-channel-tag-list').tagsinput({
                typeahead: {
                    source: function (query,process) {
                        $.get(BASE_URL + 'bloodhound/tag',{query: query},function(data){
                            tag_name_list = tag_name_list.concat(data.data);
                            return process(data.data);
                        });
                    },
                    minLength: 3
                }
            });

            $('#channel-provider').typeahead({
                source: function(query,process){
                    return $.get(BASE_URL + 'bloodhound/provider',{query: query},function(data){
                        return process(data.data);
                    });
                },
                minLength:3,
                afterSelect: function(item){
                    selected_provider = item.id;
                }
            });

            $('.bootstrap-tagsinput input').attr('placeholder','Choose tags');
            $('.spin-clone').remove();
            $('.channel-form').show();
            $.when(d1,d2).done(function(d1, d2) {

            });


        } else {
            toastr.error('There war some error fetching your request');
            $('.channel-fetch-retry').show();
            $('.spin-clone').hide();
        }
    }

    //Depricated
    /*function to change format of '/' seperated date format tp materialize pick a date format*/
    /*function change_date_format(date){
        console.log(new Date(date));
        var date = new Date(date).getDate();
        var month = new Date(date).getMonth();
        var year = new Date(date).getFullYear();

        return date + ' ' + month + ',' + year;
    }*/

    /**
     * prepares provider edit form using result object passed
     * @param  Object   result  Object recived after requesting data from server
     */
    function prepareProviderEditForm(result) {
        if (result['status'] == 'success') {

            var data = result['response']['data'];
            var compiled_template = templates['provider-edit-form'];
            /*var template = $('#provider-edit-form').html();
            var compiled_template = Hogan.compile(template);*/
            $('.provider-fetch-retry').remove();

            var context = {};
            context['name'] = data['name'];
            context['display_name'] = data['name'];
            context['id'] = data['id'];
            context['status'] = data['status'];
            context['twitter_link'] = (data['twitter_link'] != null && data['twitter_link'] != '') ? data['twitter_link'] : '';
            context['fb_link'] = (data['fb_link'] != null && data['fb_link'] != '') ? data['fb_link'] : '';
            context['slug_url'] = (data['slug_url'] != null && data['slug_url'] != '') ? data['slug_url'] : '';

            if(data['campaign_start'] != null)
                context['campaign_start'] = data['campaign_start'].split('-').join('/');

            if(data['campaign_end'] != null)
                context['campaign_end'] = data['campaign_end'].split('-').join('/');

            $('.page-content').append(compiled_template.render(context));

            var active = data['active'] == "yes";
            if (active) {
                $('#provider-active').attr('checked', 'checked');
            }

            $('#provider-campaign-start').pickadate({
                format: 'dd-mm-yyyy',
                selectMonths: true, // Creates a dropdown to control month
                selectYears: 15 // Creates a dropdown of 15 years to control year
            });

            $('#provider-campaign-start').val((data['campaign_start']));

            $('#provider-campaign-end').pickadate({
                format: 'dd-mm-yyyy',
                selectMonths: true, // Creates a dropdown to control month
                selectYears: 15 // Creates a dropdown of 15 years to control year
            });
            $('#provider-campaign-end').val((data['campaign_end']));

            //updates slug for typed name
            $('#provider-slug-url').on('input',function(){
                var url = $(this).val();
                var slug = slugify(url);
                if(url != ''){
                    $.ajax({
                        'url': BASE_URL + 'provider/slugify?value=' + slug,
                        'method': 'GET'
                    }).done(function(data){
                        if(data.available){
                            $('.slug-available').show();
                            $('.slug-unavailable').hide();
                            $('.generated-slug').html(slug);
                        } else {
                            $('.slug-unavailable').show();
                            $('.slug-available').hide();
                        }

                    });
                } else {
                    $('.slug-available').hide();
                    $('.slug-unavailable').hide();
                }
            });

            var new_provider_form_validator = new Validator(provider_validator_seed, '.provider-form-update', function(errors, warnings, cleanCallback) {

                if (Object.keys(errors).length > 0) {
                    console.log(errors);
                    toastr.error('Please correct the errors in form');
                    cleanCallback();
                } else {
                    var startparts = $('#provider-campaign-start').val().split('-');
                    var endparts = $('#provider-campaign-end').val().split('-');
                    if(new Date(startparts[2],startparts[1]-1,startparts[0]) >= new Date(endparts[2],endparts[1]-1,endparts[0])){
                        toastr.error('please enter a valid date,you know what a valid date is?');
                        $('.new-provider-form-submit').removeAttr('disabled', 'disabled').addClass('pointer');
                        $('.submit-indicator').empty();
                        cleanCallback();
                        return;
                    }
                    var fd = new FormData();
                    var name = $('#provider-name').val().trim();
                    fd.append('name', name);
                    fd.append('display_name', $('#provider-display-name').val().trim());
                    fd.append('active', $('#provider-active').is(':checked') ? "yes" : "no");

                    if($('#provider-campaign-start').val() != null && $('#provider-campaign-start').val() != '')
                        fd.append('campaign_start',$('#provider-campaign-start').val());
                    else
                        fd.append('campaign_start', null);

                    if($('#provider-campaign-end').val() != null && $('#provider-campaign-end').val() != '')
                        fd.append('campaign_end', $('#provider-campaign-end').val());
                    else
                        fd.append('campaign_end', null);

                    var provider_twitter_link = $('#provider-twitter-link').val().trim();
                    if (provider_twitter_link != null && provider_twitter_link != '') {
                        fd.append('twitter_link',provider_twitter_link);
                    } else {
                        fd.append('twitter_link','');
                    }

                    var provider_fb_link = $('#provider-fb-link').val().trim();
                    if (provider_fb_link != null && provider_fb_link != '') {
                        fd.append('fb_link',provider_fb_link);
                    } else {
                        fd.append('fb_link','');
                    }

                    var provider_slug_url = $('#provider-slug-url').val().trim();
                    if (provider_slug_url != null && provider_slug_url != '') {
                        fd.append('slug_url',slugify(provider_slug_url));
                    } else {
                        fd.append('slug_url',slugify(name));
                    }

                    var status = "incomplete";
                    var essentials = ['name', 'display-name'];
                    /* If warnings object is empty */
                    if (canBeStaged(warnings, essentials) && $('#provider-campaign-start').val() != null && $('#provider-campaign-start').val() != '' ) {
                        if (context['status'] == 'complete') {
                            status = 'complete';
                        } else {
                            status = "staged";
                        }
                    }

                    fd.append("status", status);
                    fd.append('user_name', current_active_user['name']);
                    fd.append('user_email', current_active_user['email']);
                    $('.tag-form-update').attr('disabled', 'disabled').removeClass('pointer');
                    $('.submit-indicator').append($('.m-preloader').addClass('preloader-copy').clone().show());

                    $.ajax({
                        url: BASE_URL + 'provider/keep/' + context['id'],
                        method: 'POST',
                        data: fd,
                        processData: false,
                        contentType: false
                    }).done(function(result) {
                        toastr.success('Provider updated successully');
                        toastr.info('Redirecting to Provider display page');
                        setTimeout(
                            function() {
                                page('/provider/' + result['storage_id']);
                            }, 2000);
                    }).fail(function(data) {
                        console.log(data);
                        showError(data);
                    }).always(function() {
                        $('.provider-form-submit').removeAttr('disabled', 'disabled').addClass('pointer');
                        $('.submit-indicator').empty();
                        cleanCallback();
                    });

                }

            }, formValidationViewUpdate)

            new_provider_form_validator.fortify();

            $('.spin-clone').remove();
        } else {
            toastr.error('There war some error fetching your request');
            $('.provider-fetch-retry').show();
            $('.spin-clone').hide();
        }
    }


    /**
     * prepares tag edit form using result object passed
     * @param  Object   result  Object recived after requesting data from server
     */
    function prepareTagEditForm(result) {
        if (result['status'] == 'success') {
            var tagIconFileName = null;
            var tagPosterFileName = null;
            var tagCoverFileName = null;
            var data = result['response']['data'];
            var compiled_template = templates['tag-edit-form'];
            /*var template = $('#tag-edit-form').html();
            var compiled_template = Hogan.compile(template);*/
            $('.tag-fetch-retry').remove();

            var context = {};
            context['name'] = data['name'];
            context['display_name'] = data['display_name'];
            context['description'] = data['description'];
            context['short_description'] = data['short_description'];
            context['tagline'] = data['tagline'];
            context['seo'] = data['seo'];
            context['id'] = data['id'];
            context['status'] = data['status'];

            var original_tag_cover = data['cover_image'];
            tag_cover_data_url = data['cover_image'];

            var original_tag_icon = data['poster_small'];
            tag_icon_data_url = data['poster_small'];

            var original_tag_poster = data['poster'];
            tag_poster_data_url = data['poster'];

            $('.page-content').append(compiled_template.render(context));
            $('input.length-field').characterCounter();

            var show_title = data['show_title'] == "yes";
            if (show_title) {
                $('#tag-show-title').attr('checked', 'checked');
            }

            var collection = data['collection'] == "yes";
            if (collection) {
                $('#tag-collection').attr('checked', 'checked');
            }

            /* rendering image upload template */
            $('#cover-add-modal').html(compiled_image_template.render({
                title: 'Add Cover'
            }));
            $('#icon-add-modal').html(compiled_image_template.render({
                title: 'Add Icon'
            }));
            $('#poster-add-modal').html(compiled_image_template.render({
                title: 'Add Poster'
            }));
            /* rendering image upload template */


            $('.modal-trigger').off().leanModal();
            /* initialize js components */
            //updates slug for typed name
            $('#tag-name').on('input',function(){
                var name = $(this).val();
                var slug = slugify(name);
                if(name != ''){
                    $('.slug-available').show();
                    $('.generated-slug').text(slug);
                } else {
                    $('.slug-available').hide();
                }
            });

            prepareImageUploaderModal('#cover-add-modal', function(cover_src, filename) {
                tagCoverFileName = filename;
                $('.cover-thumbnail').attr('src', cover_src);
                tag_cover_data_url = cover_src;
                $('.tag-cover-thumbnail-container').show();
                $('.tag-cover-control').html('Edit cover');
            }, function() {
                tagCoverFileName = null;
                tag_cover_data_url = null;
                $('.cover-thumbnail').attr('src', '');
                $('.tag-cover-thumbnail-container').hide();
                $('.tag-cover-control').html('Add cover');
            }, tagCoverDimensions);

            prepareImageUploaderModal('#icon-add-modal', function(icon_src, filename) {
                tagIconFileName = filename;
                $('.icon-thumbnail').attr('src', icon_src);
                tag_icon_data_url = icon_src;
                $('.tag-icon-thumbnail-container').show();
                $('.tag-icon-control').html('Edit icon');
            }, function() {
                tagIconFileName = null;
                tag_icon_data_url = null;
                $('.icon-thumbnail').attr('src', '');
                $('.tag-icon-thumbnail-container').hide();
                $('.tag-icon-control').html('Add icon');
            }, tagIconDimensions);

            //madal for poster
            prepareImageUploaderModal('#poster-add-modal', function(poster_src, filename) {
                tagPosterFileName = filename;
                $('.poster-thumbnail').attr('src', poster_src);
                tag_poster_data_url = poster_src;
                $('.tag-poster-thumbnail-container').show();
                $('.tag-poster-control').html('Edit poster');
            }, function() {
                tagPosterFileName = null;
                tag_poster_data_url = null;
                $('.poster-thumbnail').attr('src', '');
                $('.tag-poster-thumbnail-container').hide();
                $('.tag-poster-control').html('Add poster');
            }, tagPosterDimensions);


            var new_tag_form_validator = new Validator(tag_validator_seed, '.tag-form-update', function(errors, warnings, cleanCallback) {

                if (Object.keys(errors).length > 0) {
                    console.log(errors);
                    toastr.error('Please correct the errors in form');
                    cleanCallback();
                } else {
                    var fd = new FormData();
                    fd.append('name', $('#tag-name').val().trim());
                    fd.append('slug_url', slugify($('#tag-name').val()));
                    fd.append('display_name', $('#tag-display-name').val().trim());
                    fd.append('collection', $('#tag-collection').is(':checked') ? "yes" : "no");
                    fd.append('show_title', $('#tag-show-title').is(':checked') ? "yes" : "no");

                    var description = $('#tag-description').val().trim();
                    if(description != null && description != ''){
                        fd.append('description', description);
                    } else {
                        fd.append('description', '');
                    }

                    var short_description = $('#tag-short-description').val().trim();
                    if(short_description != null && short_description != ''){
                        fd.append('short_description', short_description);
                    } else {
                        fd.append('short_description', '');
                    }

                    var tagline = $('#tag-tagline').val().trim();
                    if(tagline != null && tagline != ''){
                        fd.append('tagline', tagline);
                    } else {
                        fd.append('tagline', '');
                    }

                    var seo = $('#tag-seo').val().trim();
                    if(seo != null && seo != ''){
                        fd.append('seo', seo);
                    } else {
                        fd.append('seo', '');
                    }

                    if (original_tag_icon != tag_icon_data_url) {
                        if (tag_icon_data_url != "" && tag_icon_data_url != null) {
                            var blobIcon = dataURItoBlob(tag_icon_data_url);
                            fd.append('poster_small', blobIcon);
                            fd.append('poster_small_name', tagIconFileName);
                        } else {
                            fd.append('poster_small', null);
                            fd.append('poster_small_name', null);
                        }
                    }
                    if (original_tag_poster != tag_poster_data_url) {
                        if (tag_poster_data_url != "" && tag_poster_data_url != null) {
                            var blobPoster = dataURItoBlob(tag_poster_data_url);
                            fd.append('poster', blobPoster);
                            fd.append('poster_name', tagPosterFileName);
                        } else {
                            fd.append('poster', null);
                            fd.append('poster_name', null);
                        }
                    }
                    if (original_tag_cover != tag_cover_data_url) {
                        if (tag_cover_data_url != "" && tag_cover_data_url != null) {
                            var blobPoster = dataURItoBlob(tag_cover_data_url);
                            fd.append('cover_image', blobPoster);
                            fd.append('cover_image_name', tagCoverFileName);
                        } else {
                            fd.append('cover_image', null);
                            fd.append('cover_image_name', null);
                        }
                    }

                    var status = "incomplete";
                    var essentials = ['name', 'display-name'];
                    /* If warnings object is empty */
                    if (canBeStaged(warnings, essentials) && tag_poster_data_url != null && tag_poster_data_url != "" && description!=null && description!='' && short_description!=null && short_description!='' && seo!=null && seo!='') {
                        if(data['status'] == 'complete'){
                            status = "complete";
                        } else{
                            status = "staged";
                        }
                    }

                    fd.append("status", status);
                    fd.append('user_name', current_active_user['name']);
                    fd.append('user_email', current_active_user['email']);
                    $('.tag-form-update').attr('disabled', 'disabled').removeClass('pointer');
                    $('.submit-indicator').append($('.m-preloader').addClass('preloader-copy').clone().show());

                    $.ajax({
                        url: BASE_URL + 'tag/keep/' + context['id'],
                        method: 'POST',
                        data: fd,
                        processData: false,
                        contentType: false
                    }).done(function(result) {
                        toastr.success('Tag updated successully');
                        toastr.info('Redirecting to Tag display page');
                        setTimeout(
                            function() {
                                page('/tag/' + result['storage_id']);
                            }, 2000);
                    }).fail(function(data) {
                        console.log(data);
                        showError(data);
                    }).always(function() {
                        $('.tag-form-submit').removeAttr('disabled', 'disabled').addClass('pointer');
                        $('.submit-indicator').empty();
                        cleanCallback();
                    });

                }

            }, formValidationViewUpdate)

            new_tag_form_validator.fortify();


            var d1 = $.Deferred();
            var d2 = $.Deferred();

            if (data['cover_image'] != null) {
                /* settings image source of poster */
                $('.cover-thumbnail').attr('src', '/static/img/cover_loader.gif').attr('data-original',data['cover_image']).lazyload({'effect':'fadeIn'}).on('load', function() {
                    $('#cover-add-modal').find('.poster-preview').empty().html($('.cover-thumbnail').clone().css('opacity','1'));
                    $('#cover-add-modal').find('.poster-input-section').hide();
                    $('#cover-add-modal').find('.poster-info').show();
                    $('#cover-add-modal').find('.cropper-tool').hide();
                    $('#cover-add-modal').find('.poster-preview').show();
                    $('#cover-add-modal').find('.poster-confirmation').hide();
                    $('.tag-cover-thumbnail-container').show();
                    $('.tag-cover-control').html('Edit cover');
                });
            }

            if (data['poster_small'] != null) {
                /* settings image source of poster */
                $('.icon-thumbnail').attr('src', '/static/img/image_loader.gif').attr('data-original',data['poster_small']).lazyload({'effect':'fadeIn'}).on('load', function() {
                    $('#icon-add-modal').find('.poster-preview').empty().html($('.icon-thumbnail').clone().css('opacity','1'));
                    $('#icon-add-modal').find('.poster-input-section').hide();
                    $('#icon-add-modal').find('.poster-info').show();
                    $('#icon-add-modal').find('.cropper-tool').hide();
                    $('#icon-add-modal').find('.poster-preview').show();
                    $('#icon-add-modal').find('.poster-confirmation').hide();
                    $('.tag-icon-thumbnail-container').show();
                    $('.tag-icon-control').html('Edit icon');
                    d1.resolve();
                });
            } else {
                d1.resolve();
                console.log('Icon not available');

            }

            if (data['poster'] != null) {
                /* settings image source of poster */
                $('.poster-thumbnail').attr('src',  '/static/img/image_loader.gif').attr('data-original',data['poster']).lazyload({'effect':'fadeIn'}).on('load', function() {
                    $('#poster-add-modal').find('.poster-preview').empty().html($('.poster-thumbnail').clone().css('opacity','1'));
                    $('#poster-add-modal').find('.poster-input-section').hide();
                    $('#poster-add-modal').find('.poster-info').show();
                    $('#poster-add-modal').find('.cropper-tool').hide();
                    $('#poster-add-modal').find('.poster-preview').show();
                    $('#poster-add-modal').find('.poster-confirmation').hide();
                    $('.tag-poster-thumbnail-container').show();
                    $('.tag-poster-control').html('Edit poster');
                    d2.resolve();
                });
            } else {
                d2.resolve();
                console.log('Poster not available');

            }
            $('.spin-clone').remove();
            $('.tag-form').show();
            $.when(d1, d2).done(function(result1, result2) {
            });

        } else {
            toastr.error('There war some error fetching your request');
            $('.tag-fetch-retry').show();
            $('.spin-clone').hide();
        }
    }



    /**
     * Prepares edit form for particular sponsor identified by sponsor id
     * @param  Object result   result object obtained form server response
     */
    function prepareSponsorEditForm(result) {
        if (result['status'] == 'success') {
            apk_file = null;
            apk_filename = null;
            sponsor_fetched_apk_filename = null;
            var sponsorCoverFileName = null;
            var sponsorLogoFileName = null;
            var sponsorImageFileName = null;
            var data = result['response']['data'];
            var compiled_template = templates['sponsor-edit-form'];
            /*var template = $('#sponsor-edit-form').html();
            var compiled_template = Hogan.compile(template);*/
            $('.sponsor-fetch-retry').remove();

            var context = {};
            context['sponsor_name'] = data['name'];
            context['sponsor_url'] = (data['url'] != null && data['url'] != '') ? data['url'] : '';
            context['sponsor_id'] = data['id'];
            context['apps_download'] = ('apps_download' in data['app_data'] && data['app_data']['apps_download'] != null) ? data['app_data']['apps_download'] == "yes" : false;
            context['sponsor_app_identifier_android'] = ('package_android' in data['app_data']) ? data['app_data']['package_android'] : '';
            context['sponsor_app_identifier_ios'] = ('package_ios' in data['app_data']) ? data['app_data']['package_ios'] : '';
            context['splash'] = data['splash'];
            context['preroll'] =  data['preroll'] ;
            context['image_ads'] = data['image_ads'] ;
            context['sponsor_app_url_android'] = ('remote_android' in data['app_data'] && data['app_data']['remote_android'] != null && data['app_data']['remote_android'] != '') ? data['app_data']['remote_android'] : '';
            context['sponsor_app_url_ios'] = ('remote_ios' in data['app_data'] && data['app_data']['remote_ios'] != null && data['app_data']['remote_ios'] != '') ? data['app_data']['remote_ios'] : '';
            context['sponsor_app_tagline'] = (data['tagline'] != null && data['tagline'] != '') ? data['tagline'] : '';
            context['sponsor_desc_large'] = (data['description'] != null && data['app_data']['description'] != '') ? data['description'] : '';
            context['sponsor_desc_small'] = (data['description_small'] != null && data['description_small'] != '') ? data['description_small'] : '';
            context['twitter_link'] = (data['twitter_link'] != null && data['twitter_link'] != '') ? data['twitter_link'] : '';
            context['fb_link'] = (data['fb_link'] != null && data['fb_link'] != '') ? data['fb_link'] : '';
            context['slug_url'] = (data['slug_url'] != null && data['slug_url'] != '') ? data['slug_url'] : '';
            context['status'] = data['status'];

            var splashObjects = {};
            var prerollObjects = {};
            var imageAdObjects = {};

            context['splash'].forEach(function(splash_object) {
                var key = Object.keys(splash_object)[0];
                var num = key.split('-')[3];
                splashObjects[num] = splash_object[key];
            });

            context['preroll'].forEach(function(preroll_object) {
                var key = Object.keys(preroll_object)[0];
                var num = key.split('-')[3];
                prerollObjects[key] = preroll_object[key];
            });

            context['image_ads'].forEach(function(image_ad_object) {
                var key = Object.keys(image_ad_object)[0];
                var num = key.split('-')[3];
                imageAdObjects[num] = image_ad_object[key];
            });


            var original_sponsor_cover = data['cover_image'];
            sponsor_cover_data_url = data['cover_image'];

            var original_sponsor_logo = data['logo'];
            sponsor_logo_data_url = data['logo'];

            var original_sponsor_splash = data['splash'];
            sponsorSplashObjects = {}

            var original_sponsor_preroll = data['preroll'];
            sponsorPrerollObjects = {};

            var original_sponsor_image = data['image'];
            sponsor_image_data_url = data['image'];

            var original_sponsor_image_ads = context['image_ads'];
            sponsorImageAds = {};

            /* deferreds for applying jquery `when` construct  */
            var deferreds = [];

            var active = data['active'] == "yes";
            var is_global = data['is_global'] == "yes";

            $('.page-content').append(compiled_template.render(context));
            $('.modal-trigger').off().leanModal();

            if(is_global){
                $('#sponsor-is-global').attr('checked', 'checked');
            }
            if (active) {
                $('#sponsor-status').attr('checked', 'checked');
            }

            if (context['apps_download']) {
                $('#sponsor-apps-download').attr('checked', 'checked');
            }

            /* rendering image upload template */
            $('#logo-add-modal').html(compiled_image_template.render({
                title: 'Add logo'
            }));
            $('#cover-add-modal').html(compiled_image_template.render({
                title: 'Add cover'
            }));
            $('#sponsor-image-add-modal').html(compiled_image_template.render({
                title: 'Add app image'
            }));

            //initial rendering of apk upload button
            if('apk_link' in data && data['apk_link'] != null && data['apk_link'] != ''){
                var link = data['apk_link'];
                sponsor_fetched_apk_filename = data['apk_link'].split('/').pop();
                $('.edit-sponsor-apk-package-name').html(sponsor_fetched_apk_filename);
                $('.edit-sponsor-apk-package').html('Remove apk package');
                apk_filename = sponsor_fetched_apk_filename;
            }

            //function for every click on apk upload button click
            $('.edit-sponsor-apk-package').click(function(){
                console.log('edit sponsor apk clicked');
                if(apk_filename == null){
                    $('.edit-sponsor-apk-package-input').click();
                    $('.edit-sponsor-apk-package-input').on('change',function(){
                        apk_file = $('.edit-sponsor-apk-package-input')[0].files[0];
                        apk_filename = $('.edit-sponsor-apk-package-input').val().split('\\').pop();

                        if(apk_file != null && apk_file != ''){
                            console.log('file selected');
                            $('.edit-sponsor-apk-package-name').html(apk_filename);
                            $('.edit-sponsor-apk-package').html('Remove apk package');
                        }
                    });
                }else {
                    apk_filename = null;
                    apk_file = null;
                    $('.edit-sponsor-apk-package-input').val('');
                    $('.edit-sponsor-apk-package-name').empty();
                    $('.edit-sponsor-apk-package').html('Upload apk package');
                }
            });

            $('#sponsor-slug-url').on('input',function(){
                var url = $(this).val();
                var slug = slugify(url);
                if(url != ''){
                    $.ajax({
                        'url': BASE_URL + 'sponsor/slugify?value=' + slug,
                        'method': 'GET'
                    }).done(function(data){
                        if(data.available){
                            $('.slug-available').show();
                            $('.slug-unavailable').hide();
                            $('.generated-slug').html(slug);
                        } else {
                            $('.slug-unavailable').show();
                            $('.slug-available').hide();
                        }

                    });
                } else {
                    $('.slug-available').hide();
                    $('.slug-unavailable').hide();
                }
            });

            prepareImageUploaderModal('#cover-add-modal', function(poster_src, filename) {
                sponsorCoverFileName = filename;
                $('.cover-thumbnail').attr('src', poster_src);
                sponsor_cover_data_url = poster_src;
                $('.new-sponsor-cover-thumbnail-container').show();
                $('.sponsor-cover-control').html('Edit cover');
            }, function() {
                sponsorCoverFileName = null;
                sponsor_cover_data_url = null;
                $('.cover-thumbnail').attr('src', '');
                $('.new-sponsor-cover-thumbnail-container').hide();
                $('.sponsor-cover-control').html('Add cover');
            }, sponsorCoverDimensions);

            prepareImageUploaderModal('#logo-add-modal', function(poster_src, filename) {
                sponsorLogoFileName = filename;
                $('.logo-thumbnail').attr('src', poster_src);
                sponsor_logo_data_url = poster_src;
                $('.new-sponsor-logo-thumbnail-container').show();
                $('.sponsor-logo-control').html('Edit logo');
            }, function() {
                sponsorLogoFileName = null;
                sponsor_logo_data_url = null;
                $('.logo-thumbnail').attr('src', '');
                $('.new-sponsor-logo-thumbnail-container').hide();
                $('.sponsor-logo-control').html('Add logo');
            }, sponsorLogoDimensions);

            prepareImageUploaderModal('#sponsor-image-add-modal', function(poster_src, filename) {
                sponsorImageFileName = filename;
                $('.sponsor-image-thumbnail').attr('src', poster_src);
                sponsor_image_data_url = poster_src;
                $('.new-sponsor-image-thumbnail-container').show();
                $('.sponsor-image-control').html('Edit image');
            }, function() {
                sponsorImageFileName = null;
                sponsor_image_data_url = null;
                $('.sponsor-image-thumbnail').attr('src', '');
                $('.new-sponsor-image-thumbnail-container').hide();
                $('.sponsor-image-control').html('Add image');
            }, sponsorImageDimesions);

            /* iterating over splashes and prerolls */

            var splash_indexes = [];
            context['splash'].forEach(function(splash) {
                var key = Object.keys(splash)[0];
                var key1 = key.split('-')[3];
                var index = parseInt(key1);
                splash_indexes.push(index);

                /* adding new splash element */
                $('#splash-add-modal-template').clone().attr('id', 'splash-add-modal-' + (index)).insertAfter('#splash-add-modal');
                $('#splash-add-modal-' + (index)).html(compiled_video_template.render({
                    title: 'Edit Splash'
                }));
                sponsorSplashObjects['splash-' + index] = splash[key];
                console.log(sponsorSplashObjects['splash-' + index]);
                /* attaching event handler */
                (function(splash_no) {
                    prepareVideoUploaderModal('#splash-add-modal-' + (splash_no), function(file) {
                        sponsorSplashObjects['splash-' + (splash_no)] = file;
                        $('.sponsor-splash-name-' + (splash_no)).html(file.name).show();
                        $('.new-sponsor-splash-' + (splash_no)).html('Edit splash');
                    }, function() {
                        sponsorSplashObjects['splash-' + (splash_no)] = null;
                        $('.sponsor-splash-name-' + (splash_no)).html('').hide();
                        $('.new-sponsor-splash-' + (splash_no)).html('Upload splash');
                    });
                })(index);

                var new_splash_button = $('.sponsor-splash-item.base-template').clone().removeClass('base-template');
                new_splash_button.find('.new-sponsor-splash').attr('href', '#splash-add-modal-' + (index)).removeClass('new-sponsor-splash')
                    .addClass('new-sponsor-splash-' + (index)).html('Edit splash');
                new_splash_button.find('.sponsor-splash-name').html('').removeClass('sponsor-splash-name').addClass('sponsor-splash-name-' + (index))
                new_splash_button.show().insertBefore('.splash-add-button');

                /* loading video */
                var video_container = $('#splash-add-modal-' + (index));
                video_container.find('.video-input-section').hide();
                video_container.find('.video-info').show();
                video_container.find('.new-sponsor-splash').html('Edit splash');
                video_container.find('#video-player').attr('src', splash[key]);
                video_container.find('#video-player').load();
                var d = $.Deferred();
                deferreds.push(d);

                (function(d) {
                    video_container.find('#video-player').off().on('loadeddata', function() {
                        video_container.find('.video-preview').show();
                        d.resolve();
                    });
                })(d);
            });

            var preroll_indexes = [];
            /* creating video modals for prerolls*/
            context['preroll'].forEach(function(preroll) {
                var key = Object.keys(preroll)[0];
                var key1 = key.split('-')[3];
                var index = parseInt(key1);
                preroll_indexes.push(index);
                /* adding new preroll element */
                $('#preroll-add-modal-template').clone().attr('id', 'preroll-add-modal-' + (index)).insertAfter('#preroll-add-modal');
                $('#preroll-add-modal-' + (index)).html(compiled_video_template.render({
                    title: 'Edit preroll'
                }));

                sponsorPrerollObjects['preroll-' + index] = preroll[key];

                /* attaching event handler */
                (function(preroll_no) {
                    prepareVideoUploaderModal('#preroll-add-modal-' + (preroll_no), function(file) {
                        sponsorPrerollObjects['preroll-' + (preroll_no)] = file;
                        $('.sponsor-preroll-name-' + (preroll_no)).html(file.name).show();
                        $('.new-sponsor-preroll-' + (preroll_no)).html('Edit preroll');
                    }, function() {
                        sponsorPrerollObjects['preroll-' + (preroll_no)] = null;
                        $('.sponsor-preroll-name-' + (preroll_no)).html('').hide();
                        $('.new-sponsor-preroll-' + (preroll_no)).html('Upload preroll');
                    });
                })(index);

                var new_preroll_button = $('.sponsor-preroll-item.base-template').clone().removeClass('base-template');
                new_preroll_button.find('.new-sponsor-preroll').attr('href', '#preroll-add-modal-' + (index)).removeClass('new-sponsor-preroll')
                    .addClass('new-sponsor-preroll-' + (index)).html('Edit preroll');
                new_preroll_button.find('.sponsor-preroll-name').html('').removeClass('sponsor-preroll-name').addClass('sponsor-preroll-name-' + (index))
                new_preroll_button.show().insertBefore('.preroll-add-button');

                /* adding preroll videos */
                var video_container = $('#preroll-add-modal-' + (index));
                video_container.find('.video-input-section').hide();
                video_container.find('.video-info').show();
                video_container.find('.new-sponsor-splash').html('Edit preroll');
                video_container.find('#video-player').attr('src', preroll[key]);
                video_container.find('#video-player').load();
                var d = $.Deferred();
                deferreds.push(d);

                (function(d) {
                    video_container.find('#video-player').off().on('loadeddata', function() {
                        video_container.find('.video-preview').show();
                        d.resolve();
                    });
                })(d);
            });

            var image_ad_indexes = [];
            /* creating video modals for prerolls*/
            context['image_ads'].forEach(function(image_ad) {
                var key = Object.keys(image_ad)[0];
                var key1 = key.split('-')[3];
                var index = parseInt(key1);
                image_ad_indexes.push(index);
                /* adding new image-ad element */
                $('#image-ad-add-modal-template').clone().attr('id', 'image-ad-add-modal-' + (index)).insertAfter('#image-ad-add-modal');
                $('#image-ad-add-modal-' + (index)).html(compiled_image_template.render({
                    title: 'Edit image-ad'
                }));

                sponsorImageAds['image-ad-' + index] = image_ad[key];

                /* attaching event handler */
                (function(image_ad_no) {
                    prepareImageUploaderModal('#image-ad-add-modal-' + image_ad_no, function(image_ad_src, filename) {
                        sponsorImageAds['image-ad-' + image_ad_no] = [image_ad_src, filename]
                        $('.sponsor-image-ad-name-' + image_ad_no).html(filename).show();
                        $('.new-sponsor-image-ad-' + image_ad_no).html('Edit image ad');
                    }, function() {
                        sponsorImageAds['image-ad-' + image_ad_no] = null;
                        $('.sponsor-image-ad-name-' + image_ad_no).hide();
                        $('.new-sponsor-image-ad' + image_ad_no).html('Upload image-ad');
                    }, ImageAdDimesions);
                })(index);

                var new_image_ad_button = $('.sponsor-image-ad-item.base-template').clone().removeClass('base-template').show();
                new_image_ad_button.find('.new-sponsor-image-ad').attr('href', '#image-ad-add-modal-' + (index)).removeClass('new-sponsor-image-ad')
                    .addClass('new-sponsor-image-ad-' + (index)).html('Edit image ad');
                new_image_ad_button.find('.sponsor-image-ad-name').html('').removeClass('sponsor-image-ad-name').addClass('sponsor-image-ad-name-' + (index))
                new_image_ad_button.show().insertBefore('.image-ad-add-button');

                var d = $.Deferred();
                deferreds.push(d);

                /* adding preroll videos */
                var image_ad_container = $('#image-ad-add-modal-' + (index));
                image_ad_container.find('.poster-input-section').hide();
                image_ad_container.find('.poster-info').show();
                image_ad_container.find('.cropper-tool').hide();
                image_ad_container.find('.poster-preview').show();
                image_ad_container.find('.poster-confirmation').hide();
                var image_ad_ele = $('<img>').attr('src', image_ad[key])
                image_ad_container.find('.poster-preview').empty().html(image_ad_ele);

                (function(d) {
                    image_ad_ele.on('load', function() {
                        d.resolve();
                    })
                })(d);
            });

            /* binding event handlers for splash add buttons */
            $('.splash-add-button').click(function() {
                console.log('adding a new splash');
                for (var sp in sponsorSplashObjects) {
                    if (sponsorSplashObjects[sp] == null) {
                        toastr.error('use alredy added element');
                        return;
                    }
                }
                var splashes;
                if (splash_indexes.length == 0) {
                    splashes = 0;
                } else {
                    splashes = (Math.max.apply(Math,splash_indexes)) + 1;
                }
                splash_indexes.push(splashes);
                sponsorSplashObjects['splash-' + splashes] = null;
                /* adding new splash element */
                $('#splash-add-modal-template').clone().attr('id', 'splash-add-modal-' + (splashes)).insertAfter('#splash-add-modal');
                $('#splash-add-modal-' + (splashes)).html(compiled_video_template.render({
                    title: 'Add Splash'
                }));

                /* attaching event handler */
                (function(splash_no) {
                    prepareVideoUploaderModal('#splash-add-modal-' + (splash_no), function(file) {
                        sponsorSplashObjects['splash-' + (splash_no)] = file;
                        $('.sponsor-splash-name-' + (splash_no)).html(file.name).show();
                        $('.new-sponsor-splash-' + (splash_no)).html('Edit splash');
                    }, function() {
                        sponsorSplashObjects['splash-' + (splash_no)] = null;
                        $('.sponsor-splash-name-' + (splash_no)).html('').hide();
                        $('.new-sponsor-splash-' + (splash_no)).html('Upload splash');
                    });
                })(splashes);

                var new_splash_button = $('.sponsor-splash-item.base-template').clone().show().removeClass('base-template');
                new_splash_button.find('.new-sponsor-splash').attr('href', '#splash-add-modal-' + (splashes)).removeClass('new-sponsor-splash')
                    .addClass('new-sponsor-splash-' + (splashes)).html('Upload splash');
                new_splash_button.find('.sponsor-splash-name').html('').removeClass('sponsor-splash-name').addClass('sponsor-splash-name-' + (splashes))
                new_splash_button.insertBefore('.splash-add-button');
                $('.modal-trigger').off().leanModal();
                $(new_splash_button).find('.modal-trigger').click();
            });

            $('.preroll-add-button').click(function() {
                for (var sp in sponsorPrerollObjects) {
                    if (sponsorPrerollObjects[sp] == null) {
                        toastr.error('use alredy added element');
                        return;
                    }
                }

                var prerolls;
                if (preroll_indexes.length == 0) {
                    prerolls = 0;
                } else {
                    prerolls = (Math.max.apply(Math,preroll_indexes)) + 1;
                }
                preroll_indexes.push(prerolls);

                sponsorPrerollObjects['preroll-' + prerolls] = null;
                /* adding new splash element */
                $('#preroll-add-modal-template').clone().attr('id', 'preroll-add-modal-' + (prerolls)).insertAfter('#preroll-add-modal');
                $('#preroll-add-modal-' + (prerolls)).html(compiled_video_template.render({
                    title: 'Add Preroll'
                }));

                /* attaching event handler */
                (function(preroll_no) {
                    prepareVideoUploaderModal('#preroll-add-modal-' + (preroll_no), function(file) {
                        sponsorPrerollObjects['preroll-' + (preroll_no)] = file;
                        $('.sponsor-preroll-name-' + (preroll_no)).html(file.name).show();
                        $('.new-sponsor-preroll-' + (preroll_no)).html('Edit preroll');
                    }, function() {
                        sponsorPrerollObjects['preroll-' + (preroll_no)] = null;
                        $('.sponsor-preroll-name-' + (preroll_no)).html('').hide();
                        $('.new-sponsor-preroll-' + (preroll_no)).html('Upload preroll');
                    });
                })(prerolls)

                var new_preroll_button = $('.sponsor-preroll-item.base-template').clone().show().removeClass('base-template');
                new_preroll_button.find('.new-sponsor-preroll').attr('href', '#preroll-add-modal-' + (prerolls)).removeClass('new-sponsor-preroll')
                    .addClass('new-sponsor-preroll-' + (prerolls)).html('Upload preroll');
                new_preroll_button.find('.sponsor-preroll-name').html('').removeClass('sponsor-preroll-name').addClass('sponsor-preroll-name-' + (prerolls))
                new_preroll_button.insertBefore('.preroll-add-button');
                $('.modal-trigger').off().leanModal();
                $(new_preroll_button).find('.modal-trigger').click();
            });

            /* event handler for  image ads*/
            $('.image-ad-add-button').click(function() {
                for (var ia in sponsorImageAds) {
                    if (sponsorImageAds[ia] == null) {
                        toastr.error('use alredy added element');
                        return;
                    }
                }

                var imageAds;
                if (image_ad_indexes.length == 0) {
                    imageAds = 0;
                } else {
                    imageAds = (Math.max.apply(Math,image_ad_indexes)) + 1;
                }
                image_ad_indexes.push(imageAds);
                /* adding new splash element */
                $('#image-ad-add-modal-template').clone().attr('id', 'image-ad-add-modal-' + (imageAds)).insertAfter('#image-ad-add-modal');
                $('#image-ad-add-modal-' + (imageAds)).html(compiled_image_template.render({
                    title: 'Add Image Ad'
                }));

                sponsorImageAds['image-ad-' + imageAds] = null;

                /* attaching event handler */
                (function(image_ad_no) {
                    prepareImageUploaderModal('#image-ad-add-modal-' + image_ad_no, function(image_ad_src, filename) {
                        sponsorImageAds['image-ad-' + image_ad_no] = [image_ad_src, filename]
                        $('.sponsor-image-ad-name-' + image_ad_no).html(filename).show();
                        $('.new-sponsor-image-ad-' + image_ad_no).html('Edit image ad');
                    }, function() {
                        sponsorImageAds['image-ad-' + image_ad_no] = null;
                        $('.sponsor-image-ad-name-' + image_ad_no).hide();
                        $('.new-sponsor-image-ad' + image_ad_no).html('Upload image-ad');
                    }, ImageAdDimesions);
                })(imageAds)

                var new_image_ad_button = $('.sponsor-image-ad-item.base-template').clone().removeClass('base-template').show();
                new_image_ad_button.find('.new-sponsor-image-ad').attr('href', '#image-ad-add-modal-' + (imageAds)).removeClass('new-sponsor-image-ad')
                    .addClass('new-sponsor-image-ad-' + (imageAds)).html('Upload image ad');
                new_image_ad_button.find('.sponsor-image-ad-name').html('').removeClass('sponsor-image-ad-name').addClass('sponsor-image-ad-name-' + (imageAds))
                new_image_ad_button.insertBefore('.image-ad-add-button');
                $('.modal-trigger').off().leanModal();
                $(new_image_ad_button).find('.modal-trigger').click();
            });

            /* rendering image upload template */
            $('.modal-trigger').off().leanModal();

            /* binding validator class */
            var sponsor_form_validator = new Validator(sponsor_validator_seed, '.sponsor-form-update', function(errors, warnings, cleanCallback) {
                if (Object.keys(errors).length > 0) {
                    console.log(errors);
                    toastr.error('Please correct the errors in form');
                    cleanCallback();
                } else {
                    /* creating form data to post data to server */
                    var fd = new FormData();
                    var name =  $('#sponsor-name').val().trim();
                    fd.append('name',name);

                    var sponsor_url = $('#sponsor-url').val().trim();
                    if (sponsor_url != null && sponsor_url != '') {
                        fd.append('url', sponsor_url)
                    } else {
                        fd.append('url', '')
                    }

                    fd.append('active', $('#sponsor-status').is(':checked') ? "yes" : "no")
                    var apps = {};
                    apps['apps_download'] = $('#sponsor-apps-download').is(':checked') ? "yes" : "no";

                    var sponsor_app_tagline = $('#sponsor-tagline').val().trim();
                    if (sponsor_app_tagline != null && sponsor_app_tagline != '') {
                        fd.append('tagline', sponsor_app_tagline);
                    } else {
                        fd.append('tagline', '');
                    }

                    var sponsor_app_url_android = $('#sponsor-app-url-android').val().trim();
                    if (sponsor_app_url_android != null && sponsor_app_url_android != '') {
                        apps['remote_android'] = sponsor_app_url_android;
                    } else {
                        apps['remote_android'] = null;
                    }

                    var sponsor_app_package_android = $('#sponsor-package-android').val().trim();
                    if (sponsor_app_package_android != null && sponsor_app_package_android != '') {
                        apps['package_android'] = sponsor_app_package_android;
                    } else {
                        apps['package_android'] = '';
                    }

                    var sponsor_app_url_ios = $('#sponsor-app-url-ios').val().trim();
                    if (sponsor_app_url_ios != null && sponsor_app_url_ios != '') {
                        apps['remote_ios'] = sponsor_app_url_ios;
                    } else {
                        apps['remote_ios'] = null;
                    }

                    var sponsor_app_package_ios = $('#sponsor-package-ios').val().trim();
                    if (sponsor_app_package_ios != null && sponsor_app_package_ios != '') {
                        apps['package_ios'] = sponsor_app_package_ios;
                    } else {
                        apps['package_ios'] = '';
                    }

                    var sponsor_desc_large = $('#sponsor-desc-large').val().trim();
                    if (sponsor_desc_large != null && sponsor_desc_large != '') {
                        fd.append('description', sponsor_desc_large);
                    } else {
                        fd.append('description', '');
                    }

                    var sponsor_desc_small = $('#sponsor-desc-small').val().trim();
                    if (sponsor_desc_small != null && sponsor_desc_small != '') {
                        fd.append('description_small', sponsor_desc_small);
                    } else {
                        fd.append('description_small', '');
                    }

                    var sponsor_twitter_link = $('#sponsor-twitter-link').val().trim();
                    if (sponsor_twitter_link != null && sponsor_twitter_link != '') {
                        fd.append('twitter_link',sponsor_twitter_link);
                    } else {
                        fd.append('twitter_link','');
                    }

                    var sponsor_fb_link = $('#sponsor-fb-link').val().trim();
                    if (sponsor_fb_link != null && sponsor_fb_link != '') {
                        fd.append('fb_link',sponsor_fb_link);
                    } else {
                        fd.append('fb_link','');
                    }

                    var sponsor_slug_url = $('#sponsor-slug-url').val().trim();
                    if (sponsor_slug_url != null && sponsor_slug_url != '') {
                        fd.append('slug_url',slugify(sponsor_slug_url));
                    } else {
                        fd.append('slug_url',slugify(name));
                    }
                    fd.append('is_global', $('#sponsor-is-global').is(':checked') ? "yes" : "no")
                    fd.append('app_data', JSON.stringify(apps));

                    if (original_sponsor_cover != sponsor_cover_data_url) {
                        if (sponsor_cover_data_url != "" && sponsor_cover_data_url != null) {
                            var blob = dataURItoBlob(sponsor_cover_data_url);
                            fd.append('cover_image', blob);
                            fd.append('cover_image_name', sponsorCoverFileName);
                        } else {
                            fd.append('cover_image', null);
                            fd.append('cover_image_name', null);
                        }
                    }

                    if (original_sponsor_logo != sponsor_logo_data_url) {
                        if (sponsor_logo_data_url != "" && sponsor_logo_data_url != null) {
                            var blob = dataURItoBlob(sponsor_logo_data_url);
                            fd.append('logo', blob);
                            fd.append('logo_name', sponsorLogoFileName);
                        } else {
                            fd.append('logo', null);
                            fd.append('logo_name', null);
                        }
                    }

                    if (original_sponsor_image != sponsor_image_data_url) {
                        if (sponsor_image_data_url != "" && sponsor_image_data_url != null) {
                            var blob = dataURItoBlob(sponsor_image_data_url);
                            fd.append('image', blob);
                            fd.append('image_name', sponsorImageFileName);
                        } else {
                            fd.append('image', null);
                            fd.append('image_name', null);
                        }
                    }

                    /* adding splashes and  prerolls */
                    var splash_names = {}
                    for (var sp in sponsorSplashObjects) {
                        var n = parseInt(sp.split('-')[1]);
                        if (splashObjects[n] != sponsorSplashObjects[sp]) {
                            fd.append('splash-' + n, sponsorSplashObjects[sp]);
                            if (sponsorSplashObjects[sp] == null) {
                                splash_names['splash-' + n] = "";
                            } else {
                                splash_names['splash-' + n] = sponsorSplashObjects[sp].name;
                            }

                        }
                    }
                    var preroll_names = {}
                    for (var pr in sponsorPrerollObjects) {
                        var n = parseInt(pr.split('-')[1]);
                        if (prerollObjects[data['id'] +'-'+n] != sponsorPrerollObjects[pr]) {
                            fd.append('preroll-' + n, sponsorPrerollObjects[pr]);
                            if (sponsorPrerollObjects[pr] == null) {
                                preroll_names['preroll-' + n] = "";
                            } else {
                                preroll_names['preroll-' + n] = sponsorPrerollObjects[pr].name;
                            }
                        }
                    }

                    var image_ad_names = {};
                    for (var ia in sponsorImageAds) {
                        var n = parseInt(ia.split('-')[2]);
                        if (imageAdObjects[n] != sponsorImageAds[ia]) {
                            if (sponsorImageAds[ia] == null) {
                                fd.append('image_ad-' + n, null);
                                image_ad_names['image_ad-' + n] = "";
                            } else {
                                var blob = dataURItoBlob(sponsorImageAds[ia][0]);
                                fd.append('image_ad-' + n, blob);
                                image_ad_names['image_ad-' + n] = sponsorImageAds[ia][1];
                            }
                        }
                    }

                    /* addimg respective file names */
                    if (Object.keys(image_ad_names).length)
                        fd.append('image_ad_names', JSON.stringify(image_ad_names));
                    if (Object.keys(splash_names).length)
                        fd.append('splash_names', JSON.stringify(splash_names));
                    if (Object.keys(preroll_names).length)
                        fd.append('preroll_names', JSON.stringify(preroll_names));

                    /*for (var pr in sponsorPrerollObjects) {
                        var n = parseInt(pr.split('-')[1]);
                        if (context['preroll'][n] != sponsorPrerollObjects[pr])
                            fd.append('preroll-' + n, sponsorPrerollObjects[pr]);
                    }*/

                    if(sponsor_fetched_apk_filename != apk_filename){
                        fd.append('apk_filename',apk_filename);
                        fd.append('apk_file',apk_file);
                    }


                    var status = "incomplete";
                    var essentials = ['name', 'sponsor-tagline'];
                    /* If warnings object is empty */
                    if (canBeStaged(warnings, essentials) && sponsor_logo_data_url != null && $('#sponsor-status').is(':checked')) {
                        if (context['status'] == 'complete') {
                            status = 'complete';
                        } else {
                            status = "staged";
                        }
                    }

                    fd.append("status", status);
                    fd.append('user_name', current_active_user['name']);
                    fd.append('user_email', current_active_user['email']);
                    /* posting data to server */
                    $('.sponsor-form-update').attr('disabled', 'disabled').removeClass('pointer');
                    $('.submit-indicator').append($('.m-preloader').addClass('preloader-copy').clone().show());

                    //TODO remove
                    /*var xhr = new XMLHttpRequest;
                    xhr.open('POST', '/', true);
                    xhr.send(fd);
                    return;*/

                    $.ajax({
                        url: BASE_URL + 'sponsor/keep/' + context['sponsor_id'],
                        method: 'POST',
                        data: fd,
                        processData: false,
                        contentType: false
                    }).done(function(result) {
                        toastr.success('Sponsor edited successully');
                        toastr.info('Redirecting to sponsor display page');
                        setTimeout(
                            function() {
                                page('/sponsor/' + result['storage_id']);
                            }, 2000);
                    }).fail(function(data) {
                        console.log(data);
                        showError(data);
                    }).always(function() {
                        $('.sponsor-form-update').removeAttr('disabled', 'disabled').addClass('pointer');
                        $('.submit-indicator').empty();
                        cleanCallback();
                    });

                }
            }, formValidationViewUpdate)

            sponsor_form_validator.fortify();


            var d1 = $.Deferred();
            var d2 = $.Deferred();

            deferreds.push(d1);
            deferreds.push(d2);

            if (data['cover_image'] != null && data['cover_image'] != '') {
                /* settings image source of poster */
                //$('.logo-thumbnail').attr('src', '/static/img/image_loader.gif');
                $('.cover-thumbnail').attr('src', '/static/img/cover_loader.gif').attr('data-original',data['cover_image']).lazyload({'effect':'fadeIn'}).on('load', function() {
                    $('#cover-add-modal').find('.poster-preview').empty().html($('.cover-thumbnail').clone().css('opacity','1'));
                    $('#cover-add-modal').find('.poster-input-section').hide();
                    $('#cover-add-modal').find('.poster-info').show();
                    $('#cover-add-modal').find('.cropper-tool').hide();
                    $('#cover-add-modal').find('.poster-preview').show();
                    $('#cover-add-modal').find('.poster-confirmation').hide();
                    $('.new-sponsor-cover-thumbnail-container').show();
                    $('.sponsor-cover-control').html('Edit Cover');
                });
            }


            if (data['logo'] != null && data['logo'] != '') {
                /* settings image source of poster */
                //$('.logo-thumbnail').attr('src', '/static/img/image_loader.gif');
                $('.logo-thumbnail').attr('src', '/static/img/image_loader.gif').attr('data-original',data['logo']).lazyload({'effect':'fadeIn'}).on('load', function() {
                    $('#logo-add-modal').find('.poster-preview').empty().html($('.logo-thumbnail').clone().css('opacity','1'));
                    $('#logo-add-modal').find('.poster-input-section').hide();
                    $('#logo-add-modal').find('.poster-info').show();
                    $('#logo-add-modal').find('.cropper-tool').hide();
                    $('#logo-add-modal').find('.poster-preview').show();
                    $('#logo-add-modal').find('.poster-confirmation').hide();
                    $('.new-sponsor-logo-thumbnail-container').show();
                    $('.sponsor-logo-control').html('Edit logo');
                    d1.resolve();
                });
            } else {
                d1.resolve();
            }

            if (data['image'] != null && data['image'] != '') {
                /* settings image source of poster */
                $('.sponsor-image-thumbnail').attr('src', '/static/img/image_loader.gif').attr('data-original',data['image']).lazyload({'effect':'fadeIn'}).on('load', function() {
                    $('#sponsor-image-add-modal').find('.poster-preview').empty().html($('.sponsor-image-thumbnail').clone().css('opacity','1'));
                    $('#sponsor-image-add-modal').find('.poster-input-section').hide();
                    $('#sponsor-image-add-modal').find('.poster-info').show();
                    $('#sponsor-image-add-modal').find('.cropper-tool').hide();
                    $('#sponsor-image-add-modal').find('.poster-preview').show();
                    $('#sponsor-image-add-modal').find('.poster-confirmation').hide();
                    $('.new-sponsor-image-thumbnail-container').show();
                    $('.sponsor-image-control').html('Edit image');
                    d2.resolve();
                });
            } else {
                d2.resolve();
            }
            $('.spin-clone').remove();
            $('.sponsor-form').show();
            $.when.apply($, deferreds).done(function() {
            });

        } else {
            toastr.error('There war some error fetching your request');
            $('.sponsor-fetch-retry').show();
            $('.spin-clone').hide();
        }
    }

    /**
     * Initialized js components for creating new version
     * @parm Boolean  readOnly  Boolean to signify creation of main sortable instance
     */
    function initializeVersionMakerComponents(readonly) {
        var readonly = readonly || false;
        previous_version_sponsor_mapping = {};
        feed_element_count = {
            'video': 0,
            'movie': 0,
            'channel': 0,
            'collection': 0
        };
        version_unapproved_content = {
            'content': [],
            'channel': [],
            'collection': [],
            'sponsor': []
        };
        current_column = 0;
        current_row = 0;
        mainFeedSortableInstance = null;
        sortableInstances = {};
        mainSortableInstance = null;
        topVideoSortableInstance = null;
        mainCollectionSortableInstance = null;
        alreadyInserted['channel'] = [];
        alreadyInserted['collection'] = [];
        alreadyInserted['feed'] = [];

        //Top video content not maintained at this point
        //$('#version-top-content-list').html(prepareOptionList(selected_content['data'])).prepend("<option></option>");

        $('.refresh-feed-count').click(function(){
            refreshFeedElementCount();
        });

        //typeahead for main collection list input
        $('#version-maincollection-list').typeahead({
            source: function(query,process){
                return $.get(BASE_URL + 'bloodhound/collection',{query: query},function(data){
                    return process(data.data);
                });
            },
            minLength:3,
            afterSelect: function(item){
                insertSelectedMainCollection(item,'edit',null);
                $('#version-maincollection-list').val('');
            },
        });


        //channels element initialization
        $('#version-channel-list').typeahead({
            source: function(query,process){
                return $.get(BASE_URL + 'bloodhound/channel',{query: query},function(data){
                    return process(data.data);
                });
            },
            minLength:3,
            afterSelect: function(item){
                insertSelectedChannel(item,'edit');
                $('#version-channel-list').val('');
            },
        });

        $('#version-content-list').typeahead({
            source: function(query,process){
                return $.get(BASE_URL + 'bloodhound/content',{query: query},function(data){
                    return process(data.data);
                });
            },
            minLength:3,
            afterSelect: function(item){
                insertSelectedChannelContent(item);
                $('#version-content-list').val('');
            },
        });

        $('#version-image-list').typeahead({
            source: function(query,process){
                return $.get(BASE_URL + 'bloodhound/image_ad',{query: query},function(data){
                    return process(data.data);
                });
            },
            minLength:3,
            afterSelect: function(item){
                insertSelectedChannelImage(item);
                $('#version-image-list').val('');
            },
        });


        //feed elements initialization
        $('#version-feed-channel-list').typeahead({
            source: function(query,process){
                return $.get(BASE_URL + 'bloodhound/channel',{query: query},function(data){
                    return process(data.data);
                });
            },
            minLength:3,
            afterSelect: function(item){
                insertSelectedFeedChannel(item,'edit');
                $('#version-feed-channel-list').val('');
            },
        });
        $('#version-feed-content-list').typeahead({
            source: function(query,process){
                return $.get(BASE_URL + 'bloodhound/content',{query: query},function(data){
                    return process(data.data);
                });
            },
            minLength:3,
            afterSelect: function(item){
                insertSelectedFeedContent(item,'edit');
                $('#version-feed-content-list').val('');
            },
        });
        $('#version-feed-image-list').typeahead({
            source: function(query,process){
                return $.get(BASE_URL + 'bloodhound/image_ad',{query: query},function(data){
                    return process(data.data);
                });
            },
            minLength:3,
            afterSelect: function(item){
                insertSelectedFeedImage(item,'edit');
                $('#version-feed-image-list').val('');
            },
        });
        $('#version-collection-list').typeahead({
            source: function(query,process){
                return $.get(BASE_URL + 'bloodhound/collection',{query: query},function(data){
                    return process(data.data);
                });
            },
            minLength:3,
            afterSelect: function(item){
                insertSelectedCollection(item,'edit');
                $('#version-collection-list').val('');
            },
        });

        //sponsor element initialization
        $('#version-top-bar-sponsor-list').typeahead({
            source: function(query,process){
                return $.get(BASE_URL + 'bloodhound/sponsor',{query: query},function(data){
                    return process(data.data);
                });
            },
            minLength:3,
            afterSelect: function(item){
                insertSelectedSponsor(item);
                $('#version-top-bar-sponsor-list').val('');
            },
        });

        $('#version-preroll-list').typeahead({
            source: function(query,process){
                return $.get(BASE_URL + 'bloodhound/preroll',{query: query},function(data){
                    return process(data.data);
                });
            },
            minLength:3,
            afterSelect: function(item){
                insertSelectedPreroll(item,'edit');
                $('#version-preroll-list').val('');
            },
        });

        $('#version-splash-list').typeahead({
            source: function(query,process){
                return $.get(BASE_URL + 'bloodhound/splash',{query: query},function(data){
                    return process(data.data);
                });
            },
            minLength:3,
            afterSelect: function(item){
                insertSelectedSplash(item,'edit');
                $('#version-splash-list').val('');
            },
        });

        $('.version-type').find('select').material_select();
        $('.version-type').find('label').show();
        $('.version-type').show();

        $('.version-unapproved-element-status-check').click(function(){
            checkVersionState();
        });
        $('#unapproved-element-clear').click(function(){
            clearAllUnapprovedElements();
        });


        if (readonly) {
            $('.channel-button-section-header').off().click(function() {
                $('#channel-preview-container').show().animate({
                    "top": "0px"
                });
            });

            $('.top-video-button-section-header').off().click(function() {
                $('#top-videos-preview-container').show().animate({
                    "top": "0px"
                });
            });

            $('.feed-button-section-header').off().click(function() {
                $('#feed-preview-container').show().animate({
                    "top": "0px"
                });
            });

            $('.maincollection-button-section-header').off().click(function() {
                $('#maincollection-preview-container').show().animate({
                    "top": "0px"
                });
            });

        } else {
            $('.channel-button-section-header').off().click(function() {
                $('#channel-preview-container').show().animate({
                    "top": "0px"
                }, function() {
                    hideSelectContainers(['channel']);
                });
            });
            $('.top-video-button-section-header').off().click(function() {
                $('#top-videos-preview-container').show().animate({
                    "top": "0px"
                }, function() {
                    hideSelectContainers(['top-content']);
                });
            });
            $('.feed-button-section-header').off().click(function() {
                $('#feed-preview-container').show().animate({
                    "top": "0px"
                }, function() {
                    hideSelectContainers(['feed-content','feed-image', 'feed-channel', 'collection','feed-row-element-count']);
                });
                $('.feed-row-element-count').click(function(){
                    console.log($(this).val());
                });
                $('.feed-element-count').show();
            });

            $('.maincollection-button-section-header').off().click(function() {
                $('#maincollection-preview-container').show().animate({
                    "top": "0px"
                }, function() {
                    hideSelectContainers(['maincollection']);
                });
            });
        }


        if (readonly) {
            $('.channel-menu-down').off().click(function() {
                $('#channel-preview-container').animate({
                    "top": "412px"
                }).hide();
            });

            $('.top-videos-menu-down').off().click(function() {
                $('#top-videos-preview-container').animate({
                    "top": "412px"
                }).hide();
            });
            $('.feed-menu-down').off().click(function() {
                $('#feed-preview-container').animate({
                    "top": "412px"
                }).hide();
            });
            $('.maincollection-menu-down').off().click(function() {
                $('#maincollection-preview-container').animate({
                    "top": "412px"
                }).hide();
            });
        } else {
            $('.channel-menu-down').off().click(function() {
                $('#channel-preview-container').animate({
                    "top": "412px"
                }, function() {
                    hideSelectContainers();
                }).hide();
            });
            $('.top-videos-menu-down').off().click(function() {
                $('#top-videos-preview-container').animate({
                    "top": "412px"
                }, function() {
                    hideSelectContainers();
                }).hide();
            });
            $('.feed-menu-down').off().click(function() {
                $('#feed-preview-container').animate({
                    "top": "412px"
                }, function() {
                    hideSelectContainers();
                }).hide();
                $('.feed-element-count').hide();
            });
            $('.maincollection-menu-down').off().click(function() {
                $('#maincollection-preview-container').animate({
                    "top": "412px"
                }, function() {
                    hideSelectContainers();
                }).hide();
            });
        }

        var row = "<div id='feed-row-" + current_row + "'data-id='feed-row-" + current_row + "' class='feed-row highlighted-row' ></div>";
        $('.feed-list').append(row);
    }

    /**
     * Prepares option string for select box from array
     * @param  array  data  array of data items with id attribute
     * @return String   select option string
     */
    function prepareOptionList(data) {
        var optionList = '';
        data.forEach(function(data) {
            optionList += '<option value="' + data['id'] + '">' + data['name'] + '</option>';
        });
        return optionList;
    }

    /**
     * Prepares option string for select box from array of content_image_list
     * @param  array  data  array of data items with id attribute
     * @return String   select option string
     */
    function prepareContentImageOptionList(data) {
        var optionList = '';
        data.forEach(function(data) {
            if (data['type'] == 'image_ad')
                optionList += '<option value="' + data['id'] + '*' + data['type'] + '*' + data['sponsor'] + '">image-ad- ' + data['name'] + '</option>';

            else
                optionList += '<option value="' + data['id'] + '*' + data['type'] + '">' + data['name'] + '</option>';
        });
        return optionList;
    }

    /**
     * Prepares select option list for sponsor sellable entity.
     * @param  Object   data  object containing sponsor sellable entity having name and id as attributes
     * @return String   select option string
     */
    function prepareEntityOptionList(data) {
        var optionList = '';
        for (var eid in data) {
            optionList += '<option value="' + eid + '">' + data[eid]['name'] + '--' + data[eid]['sponsor-name'] + '</option>';
        }
        return optionList;
    }

    /**
     * Returns list of elements in select box
     * @param String selectBox  id for select box
     * @return Array  array of elements in select box
     */
    function getCurrentValues(selectBox) {
        var arr = [];
        $("#" + selectBox + " option").each(function() {
            /* adding non empty values */
            if ($(this).val() != '')
                arr.push({
                    'id': $(this).val(),
                    'name': $(this).html()
                });
        });
        return arr;
    }

    /**
     * Reinserts channel to chosen list
     * @param  String  selectedChannel id of channel to be pushed back
     */
    function pushBackChannel(selectedChannel) {
        /* removing channel info from insertedChannels*/
        delete insertedChannels[selectedChannel];

        /* removing sortable instance */
        delete sortableInstances[selectedChannel];

        /* removing content drawer for partcular channel */
        var contentDrawerClass = selectedChannel + '-content-section';
        var sponsorDrawerClass = selectedChannel + '-sponsor-section';
        var topBarSponsorDrawerClass = selectedChannel + '-top-bar-sponsor-section';
        var splashDrawerClass = selectedChannel + '-splash-section';
        var prerollDrawerClass = selectedChannel + '-preroll-section';
        var imageAdDrawerClass = selectedChannel + '-image-ad-section';

        if ($('.' + contentDrawerClass).length) {
            /* removing content from contet drawer */
            $('.' + contentDrawerClass).find('.content-preview').each(function(i) {
                var content_id = $(this).attr('data-id');
                var id = content_id.split('*')[0];
                var index = alreadyInserted['channel'].indexOf(id);
                if (index > -1) {
                    alreadyInserted['channel'].splice(index, 1);
                }
            });
        }
        $('.' + contentDrawerClass).remove();
        $('.' + topBarSponsorDrawerClass).remove();
        $('.' + splashDrawerClass).remove();
        $('.' + prerollDrawerClass).remove();
        $('.' + imageAdDrawerClass).remove();
        /*if ($('.' + topBarSponsorDrawerClass).length) {
            $('.' + topBarSponsorDrawerClass).find('.top-bar-sponsor-list').children().each(function() {
                pushBackSponsor($(this).attr('data-id'), selectedChannel);
            })
        }
        if ($('.' + splashDrawerClass).length) {
            $('.' + splashDrawerClass).find('.splash-list').children().each(function() {
                pushBackSplash($(this).attr('data-id'), selectedChannel);
            })
        }
        if ($('.' + prerollDrawerClass).length) {
            $('.' + prerollDrawerClass).find('.preroll-list').children().each(function() {
                pushBackPreroll($(this).attr('data-id'), selectedChannel);
            })
        }
        if ($('.' + imageAdDrawerClass).length) {
            $('.' + imageAdDrawerClass).find('.image-ad-list').children().each(function() {
                pushBackImageAd($(this).attr('data-id'), selectedChannel);
            })
        }*/
    }

    /**
     * Reinserts top content to select box
     * @param  String  selectedTopContent  content id being pushed back
     */
    function pushBackTopContent(selectedTopContent) {
        var all_elements = getCurrentValues('version-top-content-list');
        var element = approved_content_map[selectedTopContent];
        all_elements.push({
            'id': selectedTopContent,
            'name': element['name']
        });
        $('#version-top-content-list').html(prepareOptionList(all_elements)).prepend("<option></option>").trigger("chosen:updated");
    }


    /**
     * Reinserts image ad  to sponsor select box and updates channel listing
     * @param  String  selectedImageAd  image ad id being pushed back
     * @param  String  channel_id      id of target channel
     */
    function pushBackImageAd(selectedImageAd, channel_id, updateChannel) {
        var updateChannel = updateChannel || false;
        var all_elements = getCurrentValues('version-image-ad-list');
        var element = sponsor_image_ad_map[selectedImageAd];
        all_elements.push({
            'id': selectedImageAd,
            'name': element['name'] + '--' + element['sponsor-name']
        });
        $('#version-image-ad-list').html(prepareOptionList(all_elements)).prepend("<option></option>").trigger("chosen:updated");


        var imageAdDrawerClass = channel_id + '-image-ad-section';

        /* update channel listing */
        if (updateChannel) {
            $('.image-ad-select-container').show();
            if ($('.' + imageAdDrawerClass).find('.image-ad-list').children().length < 1) {
                $(imageAdDrawerClass).find('.no-sponsor-image-ad').show();
            } else {
                $(imageAdDrawerClass).find('.no-sponsor-image-ad').hide();
            }
            insertedChannels[channel_id]['sponsor']['image_ads'].splice(insertedChannels[channel_id]['sponsor']['image_ads'].indexOf(selectedImageAd), 1);
            if (!insertedChannels[channel_id]['sponsor']['image_ads'].length)
                delete insertedChannels[channel_id]['sponsor']['image_ads']
        }
    }


    /**
     * Prepares content drawer section for simulator maincollections
     * @param   String  collection_id  id of selected channel
     * @param  String  environment  operating environment [read, write]
     * @return  DOM     content drawer DOM element
     */
    function prepareCollectionContentDrawer(object,environment){
        var collection_id = object.id;
        var name = object.name;
        var collectionContentDrawerClass = collection_id + '-content-section';
        var content_drawer = $('.maincollection-content-preview-container.prototype').clone().removeClass('prototype').addClass(collectionContentDrawerClass);

        content_drawer.find('.maincollection-name-header').html(name);

        $(content_drawer).attr('data-collection', collection_id);
        $(content_drawer).find('.menu-down').click(function() {

            if (environment == 'write') {
                hideSelectContainers(['maincollection']);
            }

            $(content_drawer).animate({
                "top": "412px"
            }).hide();
        });

        return content_drawer;
    }

    /**
     * Prepares content drawer section for simulator
     * @param   String  channel_id  id of selected channel
     * @param  String  environment  operating environment [read, write]
     * @return  DOM     content drawer DOM element
     */
    function prepareContentDrawer(object, environment) {
        var channel_id = object.id;
        var name = object.name;
        var contentDrawerClass = channel_id + '-content-section';
        var content_drawer = $('.content-preview-container.prototype').clone().removeClass('prototype').addClass(contentDrawerClass + ' active-channel');

        content_drawer.find('.channel-name-header').html(name);
        $(content_drawer).attr('data-channel', channel_id);
        $(content_drawer).find('.menu-down').click((function(cdc, environment) {
            return function(e) {
                $('.' + cdc).removeClass('active-channel');
                if (environment == 'write') {
                    hideSelectContainers(['channel']);
                }

                $('.' + cdc).animate({
                    "top": "412px"
                }).hide();
            }
        })(contentDrawerClass, environment));

        var sponsorDrawerClass = channel_id + '-sponsor-section';

        $(content_drawer).find('.sponsor-toggle').click((function(sdc, environment) {
            return function(e) {
                $('.' + sdc).addClass('active-sponsor-field');
                $('.' + sdc).show().animate({
                    "left": "0px"
                });
                if (environment == 'write') {
                    hideSelectContainers();
                }
            }
        })(sponsorDrawerClass, environment));

        return content_drawer;

    }

    /**
     * Prepares sponsor drawer section for simulator
     * @param   String  channel_id  id of selected channel
     * @param  String  environment  operating environment [read, write]
     * @return  DOM     sponsor drawer DOM element
     */
    function prepareSponsorDrawer(object, environment) {
        var channel_id = object.id;
        var name = object.name;
        var sponsorDrawerClass = channel_id + '-sponsor-section';
        var sponsor_drawer = $('.sponsor-preview-container.prototype').clone().removeClass('prototype').addClass(sponsorDrawerClass);

        sponsor_drawer.find('.sponsor-name-header').html(name + ' sponsor section');
        $(sponsor_drawer).attr('data-channel', channel_id);
        $(sponsor_drawer).find('.menu-right').click((function(sdc, environment) {
            return function(e) {
                $('.' + sdc).removeClass('active-sponsor-field');
                if (environment == 'write') {
                    hideSelectContainers(['content','image']);
                }

                $('.' + sdc).animate({
                    "left": "-300px"
                }).hide();
            }
        })(sponsorDrawerClass, environment));

        var topBarSponsorDrawerClass = channel_id + '-top-bar-sponsor-section';
        var splashDrawerClass = channel_id + '-splash-section';
        var prerollDrawerClass = channel_id + '-preroll-section';
        var imageAdDrawerClass = channel_id + '-image-ad-section';

        $(sponsor_drawer).find('.top-bar-sponsor-select-display').click(function() {
            $('.' + topBarSponsorDrawerClass).addClass('active-top-bar-sponsor-field');
            $('.' + topBarSponsorDrawerClass).show().animate({
                "left": "0px"
            });
            if (environment == 'write') {
                hideSelectContainers(['top-bar-sponsor'], $('.' + topBarSponsorDrawerClass).find('.top-bar-sponsor-list').children().length < 1);
            }
        });

        $(sponsor_drawer).find('.sponsor-splash-select-display').click(function() {
            $('.' + splashDrawerClass).addClass('active-splash-field');
            $('.' + splashDrawerClass).show().animate({
                "top": "0px"
            });
            if (environment == 'write') {
                hideSelectContainers(['splash']);
            }
        });

        $(sponsor_drawer).find('.sponsor-preroll-select-display').click(function() {
            $('.' + prerollDrawerClass).addClass('active-preroll-field');
            $('.' + prerollDrawerClass).show().animate({
                "top": "0px"
            });
            if (environment == 'write') {
                hideSelectContainers(['preroll']);
            }
        });

        $(sponsor_drawer).find('.sponsor-image-ad-select-display').click(function() {
            $('.' + imageAdDrawerClass).addClass('active-image-ad-field');
            $('.' + imageAdDrawerClass).show().animate({
                "top": "0px"
            });
            if (environment == 'write') {
                hideSelectContainers(['image-ad']);
            }
        });


        return sponsor_drawer;

    }

    /**
     * Prepares top bar sponsor drawer section for simulator
     * @param   String  channel_id  id of selected channel
     * @param  String  environment  operating environment [read, write]
     * @return  DOM     top bar sponsor drawer DOM element
     */
    function prepareTopBarSponsorDrawer(object, environment) {
        var channel_id = object.id;
        var name = object.name;
        var sponsorDrawerClass = channel_id + '-top-bar-sponsor-section';
        var sponsor_drawer = $('.top-bar-sponsor-preview-container.prototype').clone().removeClass('prototype').addClass(sponsorDrawerClass);
        sponsor_drawer.find('.top-bar-sponsor-name-header').html('Top Sponsor for ' + name);
        sponsor_drawer.find('.no-top-bar-sponsor').show();
        $(sponsor_drawer).attr('data-channel', channel_id);
        $(sponsor_drawer).find('.menu-right').click((function(sdc, environment) {
            return function(e) {
                $('.' + sdc).removeClass('active-top-bar-sponsor-field');
                if (environment == 'write') {
                    hideSelectContainers();
                }
                $('.' + sdc).animate({
                    "left": "-300px"
                }).hide();
            }
        })(sponsorDrawerClass, environment));

        return sponsor_drawer;
    }

    /**
     * Prepares splash drawer section for simulator
     * @param   String  channel_id  id of selected channel
     * @param  String  environment  operating environment [read, write]
     * @return  DOM     splash drawer DOM element
     */
    function prepareSplashDrawer(object, environment) {
        var channel_id = object.id;
        var name = object.name;
        var sponsorDrawerClass = channel_id + '-splash-section';
        var splash_drawer = $('.splash-preview-container.prototype').clone().removeClass('prototype').addClass(sponsorDrawerClass);
        splash_drawer.find('.splash-name-header').html('Splash for ' + name);
        splash_drawer.find('.no-sponsor-splash').show();
        $(splash_drawer).attr('data-channel', channel_id);
        $(splash_drawer).find('.menu-down').click((function(sdc, environment) {
            return function(e) {
                $('.' + sdc).removeClass('active-splash-field');
                if (environment == 'write') {
                    hideSelectContainers();
                }
                $('.' + sdc).animate({
                    "top": "412px"
                }).hide();
            }
        })(sponsorDrawerClass, environment));

        return splash_drawer;
    }

    /**
     * Prepares prerolldrawer section for simulator
     * @param   String  channel_id  id of selected channel
     * @param  String  environment  operating environment [read, write]
     * @return  DOM     preroll drawer DOM element
     */
    function preparePrerollDrawer(object, environment) {
        var channel_id = object.id;
        var name = object.name;
        var sponsorDrawerClass = channel_id + '-preroll-section';
        var preroll_drawer = $('.preroll-preview-container.prototype').clone().removeClass('prototype').addClass(sponsorDrawerClass);
        preroll_drawer.find('.preroll-name-header').html('Preroll for ' + name);
        preroll_drawer.find('.no-sponsor-preroll').show();
        $(preroll_drawer).attr('data-channel', channel_id);
        $(preroll_drawer).find('.menu-down').click((function(sdc, environment) {
            return function(e) {
                $('.' + sdc).removeClass('active-preroll-field');
                if (environment == 'write') {
                    hideSelectContainers();
                }
                $('.' + sdc).animate({
                    "top": "412px"
                }).hide();
            }
        })(sponsorDrawerClass, environment));

        return preroll_drawer;
    }

    /**
     * Prepares image ad drawer section for simulator
     * @param   String  channel_id  id of selected channel
     * @param  String  environment  operating environment [read, write]
     * @return  DOM     splash drawer DOM element
     */
    function prepareImageAdDrawer(object, environment) {
        var channel_id = object.id;
        var name = object.name;
        var sponsorDrawerClass = channel_id + '-image-ad-section';
        var image_ad_drawer = $('.image-ad-preview-container.prototype').clone().removeClass('prototype').addClass(sponsorDrawerClass);
        image_ad_drawer.find('.image-ad-name-header').html('image ads for ' + name);
        image_ad_drawer.find('.no-sponsor-image-ad').show();
        $(image_ad_drawer).attr('data-channel', channel_id);
        $(image_ad_drawer).find('.menu-down').click((function(sdc, environment) {
            return function(e) {
                $('.' + sdc).removeClass('active-image-ad-field');
                if (environment == 'write') {
                    hideSelectContainers();
                }
                $('.' + sdc).animate({
                    "top": "412px"
                }).hide();
            }
        })(sponsorDrawerClass, environment));

        return image_ad_drawer;
    }



    /**
     * Prepares drawers for main collection DOM element on insertion
     * @param  String  collection_id  collection id of the requested main collection
     * @param  String  environment  operating environment [read, write]
     */
    function mainCollectionInsertHandler(object, environment,version_id,fetcheddata){
        //console.log('fetcheddata in inserthandler ---->' + fetcheddata);
        var old = true && fetcheddata;
        //console.log('old in inserthandler ---->' + old);
        var collection_id = object.id;
        var name = object.name;
        var collectionContentDrawerClass = collection_id + '-content-section';

        var collection_content_drawer = prepareCollectionContentDrawer(object,environment);
        /*if(collection_content_drawer == undefined){
            return;
        }*/
        $('.phone-overlay').append($(collection_content_drawer));

        $('.maincollection-' + collection_id).click(function(){
            $('.' + collection_id + '-content-section').show().animate({
                "top": "0px"
            });
            hideSelectContainers();
        });

        var get_collection_content_list;

        if(environment == 'read'){
            //console.log('mode in inserthandler ---->' + environment);
            if(fetcheddata.length == 0){
                toastr.error(name + ' collection is empty there are no content/channels mapped to it','',{timeOut: 25000});
            }
            fetcheddata.forEach(function(content){
                var element = createMainCollectionContentElement(content,old);
                if(element != undefined){
                    var list = $(collection_content_drawer).find('.content-list');
                    $(list).append(element)
                }
            });
        } else {
            //console.log('mode in inserthandler ---->' + environment);
            $.ajaxq('version_related_data',{
                'url': BASE_URL + 'tag/datarain/' + collection_id + '?version_id=' + version_id,
                'method': 'GET',
                'success': function(d1){
                    var data = d1['data'];
                    if(data.length == 0){
                        toastr.error(name + ' collection is empty there are no content/channels mapped to it','',{timeOut: 25000});
                    }
                    data.forEach(function(content){
                        if($.inArray(content['id'],alreadyInserted['channel']) > -1 || $.inArray(content['id'],alreadyInserted['feed']) > -1 ){
                            var element = createMainCollectionContentElement(content,old);
                            if(element != undefined){
                                var list = $(collection_content_drawer).find('.content-list');
                                $(list).append(element)
                            }
                        }
                    });
                    mainCollectionSortableInstanceArrays[collection_id] = Sortable.create($(collection_content_drawer).find('.content-list')[0],{
                        group: "sorting",
                        sort: true
                    });
                }
            });
        }
    }

    /**
     * Prepares click handler for channel DOM element on simulator
     * @param  String  channel_id  channel id of the requested channel
     * @param  String  environment  operating environment [read, write]
     * @return Function    event handler for click function
     */
    function getChannelClickHandler(object, environment) {
        var channel_id = object.id;
        var name = object.name;
        return function(e) {
            /* checking if div exists for particular channel */
            var contentDrawerClass = channel_id + '-content-section';
            if (!$('.' + contentDrawerClass).length) {
                var content_drawer = prepareContentDrawer(object, environment);
                var sponsor_drawer = prepareSponsorDrawer(object, environment);
                var top_bar_sponsor_drawer = prepareTopBarSponsorDrawer(object, environment);
                var splash_drawer = prepareSplashDrawer(object, environment);
                var preroll_drawer = preparePrerollDrawer(object, environment);
                var image_ad_drawer = prepareImageAdDrawer(object, environment);

                $('.phone-overlay').append($(content_drawer));
                $('.phone-overlay').append($(sponsor_drawer));
                $('.phone-overlay').append($(top_bar_sponsor_drawer));
                $('.phone-overlay').append($(splash_drawer));
                $('.phone-overlay').append($(preroll_drawer));
                $('.phone-overlay').append($(image_ad_drawer));


                if (environment == 'write') {
                    /* creating sortable area for content drawer */
                    sortableInstances[channel_id] = Sortable.create($(content_drawer).find('.content-list')[0], {
                        group: "sorting",
                        sort: true
                    });
                    hideSelectContainers(['content','image']);
                }

                $(content_drawer).show().animate({
                    "top": "0px"
                });

            } else {
                $('.' + contentDrawerClass).addClass('active-channel').show().animate({
                    "top": "0px"
                });

                if (environment == 'write') {
                    hideSelectContainers(['content','image']);
                }
            }
        }

    }


    //create unapproved list element
    function createUnapprovedListElement(id,type){
        if(type == 'content'){
            return $('<div class=" unapproved-element-' + id + ' btn btn-outline btn-success unapproved-element" >' + id + '</div>')
        } else if(type == 'channel'){
            return $('<div class=" unapproved-element-' + id + ' btn btn-outline btn-warning unapproved-element" >' + id + '</div>')
        } else if(type == 'collection'){
            return $('<div class=" unapproved-element-' + id + ' btn btn-outline btn-info unapproved-element" >' + id + '</div>')
        } else if(type == 'sponsor'){
            return $('<div class=" unapproved-element-' + id + ' btn btn-outline btn-danger unapproved-element" >' + id + '</div>')
        }
    }


    //function to clear all unapproved content
    function clearAllUnapprovedElements(){
        var elements_to_clear_from_inserted_list = {
            'channel':[],
            'feed': [],
            'collection': []
        }
        var elements_to_clear_from_ua_list = {
            'content': [],
            'channel': [],
            'collection': [],
            'sponsor': []
        };
        version_unapproved_content['content'].forEach(function(content){
            $('.channel-content-' + content).remove();
            elements_to_clear_from_inserted_list['channel'].push(content);
            if($('.feed-content-' + content).length <= 0){
                elements_to_clear_from_ua_list['content'].push(content);
            }else {
                toastr.error('there are few elements in feed that cannot be removed');
                $('.feed-content-' + content).find('img').attr('src','/static/img/grey.jpg');
            }
            $('.maincollection-content-' + content).remove();
        });

        version_unapproved_content['channel'].forEach(function(channel){
            $('.channel-' + channel).remove();
            if(channel in insertedChannels){
                delete insertedChannels[channel];
                delete sortableInstances[channel];
            }
            $('.' + channel + '-content-section').remove();
            $('.' + channel + '-sponsor-section').remove();
            $('.' + channel + '-splash-section').remove();
            $('.' + channel + '-preroll-section').remove();
            $('.' + channel + '-image-ad-section').remove();
            $('.' + channel + '-top-bar-sponsor-section').remove();
            elements_to_clear_from_inserted_list['channel'].push(channel);
            if($('.feed-channel-' + channel).length <= 0){
                elements_to_clear_from_ua_list['channel'].push(channel);
            }else {
                toastr.error('there are few elements in feed that cannot be removed');
                $('.feed-channel-' + channel).find('img').attr('src','/static/img/grey.jpg');
            }
            $('.maincollection-content-' + channel).remove();
        });

        version_unapproved_content['collection'].forEach(function(collection){
            if($('.collection-' + collection).length <= 0){
                elements_to_clear_from_ua_list['collection'].push(collection);
            }else {
                toastr.error('there are few elements in feed that cannot be removed');
                $('.collection-' + collection).find('img').attr('src','/static/img/grey.jpg');
            }
            $('.maincollection-' + collection).remove();
            delete mainCollectionSortableInstanceArrays[collection];
            $('.' + collection + '-content-section').remove();
            elements_to_clear_from_inserted_list['collection'].push(collection);
        });
        console.log(version_unapproved_content['sponsor']);
        version_unapproved_content['sponsor'].forEach(function(sponsor){
            for(var channel_id in insertedChannels){
                console.log('tbs ' + sponsor);
                if(insertedChannels[channel_id]['sponsor']['top-bar-sponsor'] == sponsor){
                    $('.sponsor-' + sponsor).remove();
                    delete insertedChannels[channel_id]['sponsor']['top-bar-sponsor'];
                    delete insertedChannels[channel_id]['sponsor']['top-bar-sponsor-name'];
                }
            }
            previous_version_sponsor_mapping[sponsor]['preroll'].forEach(function(id){
                $('.preroll-' + id).remove();
                for(var channel_id in insertedChannels){
                    if('sponsor' in insertedChannels[channel_id] && 'preroll' in insertedChannels[channel_id]['sponsor']){
                        var remaining = $.grep(insertedChannels[channel_id]['sponsor']['preroll'],function(e){
                            if(e.preroll_id != id)
                                return e;
                        });
                        insertedChannels[channel_id]['sponsor']['preroll'] = remaining;
                    }
                }
            });

            previous_version_sponsor_mapping[sponsor]['splash'].forEach(function(id){
                $('.splash-' + id).remove();
                for(var channel_id in insertedChannels){
                    if('sponsor' in insertedChannels[channel_id] && 'splash' in insertedChannels[channel_id]['sponsor']){
                        var remaining = $.grep(insertedChannels[channel_id]['sponsor']['splash'],function(e){
                            if(e.splash_id != id)
                                return e;
                        });
                        insertedChannels[channel_id]['sponsor']['splash'] = remaining;
                    }
                }
            });

            previous_version_sponsor_mapping[sponsor]['image_ad'].forEach(function(id){
                $('.channel-image-ad-' + id).remove();
                elements_to_clear_from_inserted_list['channel'].push(id);
                if($('.feed-image-ad-' + id).length <= 0){
                    elements_to_clear_from_ua_list['sponsor'].push(sponsor);
                }else {
                    toastr.error('there are few elements in feed that cannot be removed');
                    $('.feed-image-ad-' + id).find('img').attr('src','/static/img/grey.jpg');
                }
            });

            if(previous_version_sponsor_mapping[sponsor]['image_ad'].length == 0){
                elements_to_clear_from_ua_list['sponsor'].push(sponsor);
                console.log('no image_ad');
            }
        });

        //remove elements from alreadyInserted
        elements_to_clear_from_inserted_list['channel'].forEach(function(id){
            var index = alreadyInserted['channel'].indexOf(id);
            if(index > -1){
                alreadyInserted['channel'].splice(index,1);
            }
        });
        elements_to_clear_from_inserted_list['feed'].forEach(function(id){
            var index = alreadyInserted['feed'].indexOf(id);
            if(index > -1){
                alreadyInserted['feed'].splice(index,1);
            }
        });
        elements_to_clear_from_inserted_list['collection'].forEach(function(id){
            var index = alreadyInserted['collection'].indexOf(id);
            if(index > -1){
                alreadyInserted['collection'].splice(index,1);
            }
        });

        elements_to_clear_from_ua_list['content'].forEach(function(content){
            handleVersionPublish(content,'content','remove');
        });
        elements_to_clear_from_ua_list['channel'].forEach(function(channel){
            handleVersionPublish(channel,'channel','remove');
        });
        elements_to_clear_from_ua_list['collection'].forEach(function(collection){
            handleVersionPublish(collection,'collection','remove');
        });
        elements_to_clear_from_ua_list['sponsor'].forEach(function(sponsor){
            handleVersionPublish(sponsor,'sponsor','remove');
        });
    }


    //function to check current version state in terms of unapproved content
    function checkVersionState(){
        (function fetch_ua_collection(){
            var length = version_unapproved_content['collection'].length;
            if(!length) return;
            var list = version_unapproved_content['collection'];
            $.ajax({
                'url': createBatchFetchRequest('tag',list,0,length-1),
                'method': 'GET',
                'success':function(data){
                    var batch_fetched = data.data;
                    list.forEach(function(tag){
                        handleVersionPublish(tag,'collection','remove');
                        if(!(tag in batch_fetched)){
                            handleVersionPublish(tag,'collection','add');
                            toastr.error(tag + ' cant be fetched please check its validity','',{timeOut: 20000});
                            console.log(tag);
                            return;
                        }
                        var element = batch_fetched[tag];
                        var name = element.name;
                        var poster = element.poster;
                        var poster_small = element.poster_small;
                        var status = element.status;
                        if(status != 'complete'){
                            handleVersionPublish(tag,'collection','add');
                            console.log(tag);
                            return;
                        }
                        $('.maincollection-' + tag).find('img').attr("data-original",poster).lazyload({effect: "fadeIn"});
                        $('.maincollection-' + tag).find('.maincollection-preview-name').html(name);
                        $('.maincollection-' + tag).attr('data-id', tag + '*' + name);
                        $('.' + tag + '-content-section').find('.maincollection-name-header').html(name);
                        $('.collection-' + tag).each(function(i){
                            $(this).attr('data-id',tag + '*collection*' + name);
                            if($(this).hasClass('feed-element-half')){
                                $(this).find('img').attr("data-original",poster_small).lazyload({effect: "fadeIn"});
                                $(this).find('.collection-preview-name').html(name);
                            }
                            else{
                                $(this).find('img').attr("data-original",poster).lazyload({effect: "fadeIn"});
                                $(this).find('.collection-preview-name').html(name);
                            }
                        });
                    });
                }
            });
        })();
        (function fetch_ua_channel(){
            var length = version_unapproved_content['channel'].length;
            if(!length) return;
            var list = version_unapproved_content['channel'];
            $.ajax({
                'url':createBatchFetchRequest('channel',list,0,length-1),
                'method': 'GET',
                'success': function(data){
                    var batch_fetched = data.data;
                    list.forEach(function(channel){
                        handleVersionPublish(channel,'channel','remove');
                        if(!(channel in batch_fetched)){
                            handleVersionPublish(channel,'channel','add');
                            toastr.error(channel + ' cant be fetched please check its validity','',{timeOut: 20000});
                            console.log(channel);
                            return;
                        }
                        var element = batch_fetched[channel];
                        var name = element.name;
                        var poster = element.icon;
                        var poster_small = element.icon_small;
                        var status = element.status;
                        if(status != 'complete'){
                            handleVersionPublish(channel,'channel','add');
                            console.log(channel);
                            return;
                        }
                        $('.channel-' + channel).find('img').attr("data-original",poster_small).lazyload({effect: "fadeIn"});
                        $('.channel-' + channel).find('.channel-preview-name').html(name);
                        $('.channel-' + channel).attr('data-id', channel + '*' + name);
                        $('.' + channel + '-content-section').find('.channel-name-header').html(name + ' content section');
                        $('.' + channel + '-sponsor-section').find('.sponsor-name-header').html(name + ' sponsor section');
                        $('.' + channel + '-preroll-section').find('.preroll-name-header').html(name + ' preroll section');
                        $('.' + channel + '-splash-section').find('.splash-name-header').html(name + ' splash section');
                        $('.maincollection-content-' + channel).find('img').attr("data-original",poster).lazyload({effect: "fadeIn"});
                        $('.maincollection-content-' + channel).find('.content-preview-name').html(name);
                        $('.maincollection-content-' + channel).attr('data-id',channel + '*channel*' + name);
                        $('.feed-channel-' + channel).each(function(){
                            $(this).attr('data-id',channel + '*channel*' + name);
                            if($(this).hasClass('feed-element-half')){
                                $(this).find('img').attr("data-original",poster_small).lazyload({effect: "fadeIn"});
                                $(this).find('.feed-channel-preview-name').html(name);
                            }
                            else{
                                $(this).find('img').attr("data-original",poster).lazyload({effect: "fadeIn"});
                                $(this).find('.feed-channel-preview-name').html(name);
                            }
                        });
                    });
                }
            });
        })();
        (function fetch_ua_content(){
            var length = version_unapproved_content['content'].length;
            if(!length) return;
            var list = version_unapproved_content['content'];
            $.ajax({
                'url':createBatchFetchRequest('content',list,0,length-1),
                'method': 'GET',
                'success':function(data){
                    var batch_fetched = data.data;
                    list.forEach(function(content){
                        handleVersionPublish(content,'content','remove');
                        if(!(content in batch_fetched)){
                            handleVersionPublish(content,'content','add');
                            toastr.error(content + ' cant be fetched please check its validity','',{timeOut: 20000});
                            console.log(content);
                            return;
                        }
                        var element = batch_fetched[content];
                        var name = element.name;
                        var poster = element.poster;
                        var poster_small = element.poster_small;
                        var status = element.status;
                        if(status != 'complete'){
                            handleVersionPublish(content,'content','add');
                            console.log(content);
                            return;
                        }
                        $('.channel-content-' + content).find('img').attr("data-original",poster).lazyload({effect: "fadeIn"});
                        $('.channel-content-' + content).find('.content-preview-name').html(name);
                        $('.channel-content-' + content).attr('data-id',content + '*content*' + name);
                        $('.maincollection-content-' + content).find('img').attr("data-original",poster).lazyload({effect: "fadeIn"});
                        $('.maincollection-content-' + content).find('.content-preview-name').html(name);
                        $('.maincollection-content-' + content).attr('data-id',content + '*content*' + name);
                        $('.feed-content-' + content).each(function(i){
                            $(this).attr('data-id',content + '*content*' + name);
                            if($(this).hasClass('feed-element-half')){
                                $(this).find('img').attr("data-original",poster_small).lazyload({effect: "fadeIn"});
                                $(this).find('.feed-content-preview-name').html(name);
                            }
                            else{
                                $(this).find('img').attr("data-original",poster).lazyload({effect: "fadeIn"});
                                $(this).find('.feed-content-preview-name').html(name);
                            }
                        });
                    });
                }
            });
        })();
        (function fetch_ua_sponsor(){
            var length = version_unapproved_content['sponsor'].length;
            if(!length) return;
            var list = version_unapproved_content['sponsor'];
            $.ajax({
                'url':createBatchFetchRequest('sponsor',list,0,length-1),
                'method': 'GET',
                'success':function(data){
                    var batch_fetched = data.data
                    list.forEach(function(sponsor){
                        handleVersionPublish(sponsor,'sponsor','remove');
                        if(!(sponsor in batch_fetched)){
                            handleVersionPublish(sponsor,'sponsor','add');
                            toastr.error(sponsor + ' cant be fetched please check its validity','',{timeOut: 20000});
                            console.log(sponsor);
                            return;
                        }
                        var element = batch_fetched[sponsor];
                        var name = element.name;
                        var logo = element.logo;
                        var status = element.status;
                        if(status != 'complete'){
                            handleVersionPublish(sponsor,'sponsor','add');
                            console.log(sponsor);
                            return;
                        }
                        $('.sponsor-' + sponsor).find('img').attr('src',logo);
                        $('.sponsor-' + sponsor).find('.sponsor-preview-name').html(name);
                        $('.sponsor-' + sponsor).attr('data-id',sponsor + '*' + name);
                        for(var key in element.image_ad){
                            var link = element['image_ad'][key];
                            $('.channel-image-ad-' + key).find('img').attr("data-original",link).lazyload({effect: "fadeIn"});
                            $('.channel-image-ad-' + key).find('.image-ad-preview-name').html(name);
                            $('.channel-image-ad-' + key).attr('data-id',key + '*image_ad*' + sponsor + '*' + name);
                            $('.feed-image-ad-' + key).find('img').attr("data-original",link).lazyload({effect: "fadeIn"});
                            $('.feed-image-ad-' + key).find('.image-ad-preview-name').html(name);
                            $('.feed-image-ad-' + key).attr('data-id',key + '*image_ad*' + sponsor + '*' + name);
                        }
                        for(var key in element.preroll){
                            //var link = element['preroll'][key];
                            $('.preroll-' + key).find('img').attr('src',logo);
                            $('.preroll-' + key).find('.sponsor-preview-name').html(name);
                            $('.preroll-' + key).attr('data-id',key + '*' + name);
                        }
                        for(var key in element.splash){
                            $('.splash-' + key).find('img').attr('src',logo);
                            $('.splash-' + key).find('.sponsor-preview-name').html(name);
                            $('.splash-' + key).attr('data-id',key + '*' + name);
                        }
                    });
                }
            });
        })();
    }

    function handlePreviousVersionSponsorMapping(id,sponsor_id,type){
        if(!(sponsor_id in previous_version_sponsor_mapping)){
            previous_version_sponsor_mapping[sponsor_id] = {
                'preroll': [],
                'splash':[],
                'image_ad': []
            };
        }
        if(type == 'tbs'){
            return;
        }
        if($.inArray(id,previous_version_sponsor_mapping[sponsor_id][type]) < 0){
            previous_version_sponsor_mapping[sponsor_id][type].push(id);
        }
    }

    function refreshFeedElementCount(){
        feed_element_count = {
            'video': 0,
            'movie': 0,
            'channel': 0,
            'collection': 0
        };
        $('.feed-row').each(function(row){
            $(this).children().each(function(child){
                var type = $(this).data('id').split('*')[1];
                //console.log(type);
                if(type == 'image_ad'){
                    return;
                }
                var feed_type = $(this).data('feedtype');
                var id = $(this).data('id').split('*')[0];
                //console.log(feed_type);
                //console.log(id);
                updateFeedElementCount(id,feed_type,'add');
            });
        });
        toastr.success('Counter Re freshed');
    }

    /**
     * keeps a check on different types on elements in feed amd maintains count
     * @param  String  id of corresponsding element
     * @param  String  type type of element
     * @param  String  add/remove
     */
     function updateFeedElementCount(id,type,activity){
        var required_count = {
            'movie': 10,
            'video':15,
            'channel': 15,
            'collection': 5
        }

        if(activity == 'getstatus'){
            for(var item in required_count){
                if(feed_element_count[item] < required_count[item]){
                    return false;
                }
            }
            return true;
        }

        //console.log(feed_element_count[type]);
        if(activity == 'add'){
            feed_element_count[type]++;
        } else {
            feed_element_count[type]--;
        }
        
        if(feed_element_count[type] < 0){
            //console.log('zero ho gaye ' + type);
            feed_element_count[type] = 0;
            //console.log(feed_element_count[type]);
        }

        if(required_count[type] <= feed_element_count[type]){
            $('#feed-' + type + '-count').css('color','#000')
        } else {
            $('#feed-' + type + '-count').css('color','#F00')
        }
        $('#feed-' + type + '-count').html(feed_element_count[type] + '/' + required_count[type]);
     }

    /**
     * checks if all elemnts are approved or not and handles version toolbox
     * @param  String  id of corresponsding element
     * @param  String  type type of element
     * @param  String  add/remove
     */
    function handleVersionPublish(id,type,activity,intent){
        var intent = intent || 'edit';
        if(activity == 'add'){
            if($.inArray(id,version_unapproved_content[type]) < 0){
                version_unapproved_content[type].push(id);
                var element = createUnapprovedListElement(id,type);
                $('.unapproved-' + type + '-list').append(element);
            }
            if(intent == 'edit'){
                $('.version-tool-box').hide();
                $('.version-unapproved-element-tool-box').show();
            }
        } else if(activity == 'remove') {
            $('.unapproved-element-' + id ).remove();
            var index = version_unapproved_content[type].indexOf(id);
            if(index > -1){
                version_unapproved_content[type].splice(index,1);
            }
        }
        if(intent == 'edit'){
            if(!(version_unapproved_content['channel'].length) && !(version_unapproved_content['collection'].length) && !(version_unapproved_content['content'].length) && !(version_unapproved_content['sponsor'].length)){
                $('.version-tool-box').show();
                $('.version-unapproved-element-tool-box').hide();

            }
        }
        //console.log(version_unapproved_content);
    }



    /**
     * Creates feed Image ad element using jquery in detached state (FULL)
     * @param  String  image_ad_id image ad id to be created
     * @param  String  environment  operating environment [read, write] write means editable mode
     * @return DOM     jquery DOM feed image_ad element
     */
    function createFeedImageAdElementFull(object, old) {
        var image_ad_id = object.id;
        var name = object.name;
        var sponsor_id = object.sponsor_id;
        var add_no = image_ad_id;
        var image_ad_element;
        var link;
        if(old){
            image_ad_element = $('<div class="feed-image-ad-preview feed-element-full inline-block feed-image-ad-' + image_ad_id + '" data-id="' + image_ad_id + '*image_ad*' + sponsor_id + '*' + name + '">\
                            <div class="image-ad-symbol">IMAGE AD</div>\
                        <img data-original="" src="/static/img/placeholder.jpg" class="feed-image-ad-preview-icon lazy">\
                        <div class="feed-image-ad-preview-name ellipsis">' + name + '</div></div>');
            return image_ad_element;
        }
        if(sponsor_id in cache){
            var element = cache[sponsor_id];
            element.image_ads.forEach(function(obj){
                if(add_no in obj){
                    link = obj[add_no];
                }
            });
            image_ad_element = $('<div class="feed-image-ad-preview feed-element-full inline-block feed-image-ad-' + image_ad_id + '" data-id="' + image_ad_id + '*image_ad*' + sponsor_id + '*' + name + '">\
                            <div class="image-ad-symbol">IMAGE AD</div>\
                        <img data-original="' + link + '" src="/static/img/placeholder.jpg" class="feed-image-ad-preview-icon lazy">\
                        <div class="feed-image-ad-preview-name ellipsis">' + name + '</div></div>');

        } else {
            image_ad_element = $('<div class="feed-image-ad-preview feed-element-full inline-block feed-image-ad-' + image_ad_id + '" data-id="' + image_ad_id + '*image_ad*' + sponsor_id + '*' + name + '">\
                            <div class="image-ad-symbol">IMAGE AD</div>\
                        <img data-original="" src="/static/img/placeholder.jpg" class="feed-image-ad-preview-icon lazy">\
                        <div class="feed-image-ad-preview-name ellipsis">' + name + '</div></div>');

            $.ajax({
                    url: BASE_URL + 'sponsor/fetch/' + sponsor_id ,
                    method: "GET",
                    success: function(data){
                        if(data.data.status != 'complete'){
                            handleVersionPublish(sponsor_id,'sponsor','add');
                        } else {
                            data.data.image_ads.forEach(function(obj){
                                if(add_no in obj){
                                    link = obj[add_no];
                                }
                            });
                            $('.feed-image-ad-' + add_no).find('img').attr("data-original",link).lazyload({effect: "fadeIn"});
                            cache[sponsor_id] = data.data;
                        }
                    },
                    error: function(data){
                        handleVersionPublish(sponsor_id,'sponsor','add');
                        toastr.error('Cant fetch info regarding image_ad ' + name + ' please check net or it can be unapproved','',{timeOut: 30000});
                    }
                }
            );
        }
        return image_ad_element;

    }
    /**
     * Creates feed Image ad element using jquery in detached state (HALF)
     * @param  String  image_ad_id image ad id to be created
     * @param  String  environment  operating environment [read, write] write means editable mode
     * @return DOM     jquery DOM feed image_ad element
     */
    function createFeedImageAdElementHalf(object, old) {
        console.log(object);
        var image_ad_id = object.id;
        var name = object.name;
        var sponsor_id = object.sponsor_id;
        var add_no = image_ad_id;
        var image_ad_element;
        var link;
        if(old){
            image_ad_element = $('<div class="feed-image-ad-preview feed-element-half inline-block feed-image-ad-' + image_ad_id + '" data-id="' + image_ad_id + '*image_ad*' + sponsor_id + '*' + name + '">\
                            <div class="image-ad-symbol">IMAGE AD</div>\
                        <img data-original="" src="/static/img/placeholder.jpg" class="feed-image-ad-preview-icon lazy">\
                        <div class="feed-image-ad-preview-name ellipsis">' + name + '</div></div>');
            return image_ad_element;
        }
        if(sponsor_id in cache){
            var element = cache[sponsor_id];
            element.image_ads.forEach(function(obj){
                if(add_no in obj){
                    link = obj[add_no];
                }
            });
            image_ad_element = $('<div class="feed-image-ad-preview feed-element-half inline-block feed-image-ad-' + image_ad_id + '" data-id="' + image_ad_id + '*image_ad*' + sponsor_id + '*' + name + '">\
                            <div class="image-ad-symbol">IMAGE AD</div>\
                        <img data-original="' + link + '" src="/static/img/placeholder.jpg" class="feed-image-ad-preview-icon lazy">\
                        <div class="feed-image-ad-preview-name ellipsis">' + name + '</div></div>');

        } else {
            image_ad_element = $('<div class="feed-image-ad-preview feed-element-half inline-block feed-image-ad-' + image_ad_id + '" data-id="' + image_ad_id + '*image_ad*' + sponsor_id + '*' + name + '">\
                            <div class="image-ad-symbol">IMAGE AD</div>\
                        <img data-original="" src="/static/img/placeholder.jpg" class="feed-image-ad-preview-icon lazy">\
                        <div class="feed-image-ad-preview-name ellipsis">' + name + '</div></div>');
            $.ajax({
                    url: BASE_URL + 'sponsor/fetch/' + sponsor_id ,
                    method: "GET",
                    success: function(data){
                        if(data.data.status != 'complete'){
                            handleVersionPublish(sponsor_id,'sponsor','add');
                        } else {
                            data.data.image_ads.forEach(function(obj){
                                if(add_no in obj){
                                    link = obj[add_no];
                                }
                            });
                            $('.feed-image-ad-' + add_no).find('img').attr("data-original",link).lazyload({effect: "fadeIn"});
                            cache[sponsor_id] = data.data;
                        }
                    },
                    error: function(data){
                        handleVersionPublish(sponsor_id,'sponsor','add');
                        toastr.error('Cant fetch info regarding image_ad ' + name + ' please check net or it can be unapproved','',{timeOut: 30000});
                    }
                }
            );

        }
        return image_ad_element;

    }



    /**
     * Creates feed content element using jquery in detached state (FULL)
     * @param  String  content_id content id to be created
     * @param  String  environment  operating environment [read, write] write means editable mode
     * @return DOM     jquery DOM feed content element
     */
    function createFeedChannelElementFull(object, old) {
        var channel_id = object.id;
        var name = object.name;
        var feed_channel_element;
        if(old){
            feed_channel_element = $('<div class="feed-channel-preview  feed-element-full inline-block feed-channel-' + channel_id + '" data-id="' + channel_id + '*channel*' + name + '">\
                            <img data-original="" src="/static/img/placeholder.jpg" class="feed-channel-preview-icon lazy">\
                            <div class="feed-channel-preview-name ellipsis">' + name + '</div></div>');
            return feed_channel_element;
        }
        if(channel_id in cache){
            var element = cache[channel_id];
            feed_channel_element = $('<div class="feed-channel-preview feed-element-full inline-block feed-channel-' + channel_id + '" data-id="' + channel_id + '*channel*' + name + '">\
                            <img data-original="' + element['icon'] + '" src="/static/img/placeholder.jpg" class="feed-channel-preview-icon lazy">\
                            <div class="feed-channel-preview-name ellipsis">' + element['name'] + '</div></div>');
        } else {
            feed_channel_element = $('<div class="feed-channel-preview feed-element-full inline-block feed-channel-' + channel_id + '" data-id="' + channel_id + '*channel*' + name + '">\
                            <img data-original="" src="/static/img/placeholder.jpg" class="feed-channel-preview-icon lazy">\
                            <div class="feed-channel-preview-name ellipsis">' + name + '</div></div>');
            $.ajax({
                    url: BASE_URL + 'channel/fetch/' + channel_id ,
                    method: "GET",
                    success: function(data){
                        if(data.data.status != 'complete'){
                            handleVersionPublish(channel_id,'channel','add');
                        } else {
                            $('.feed-channel-' + channel_id).find('img').attr("data-original",data.data.icon).lazyload({effect: "fadeIn"});
                            cache[channel_id] = data.data;
                        }
                    },
                    error: function(data){
                        handleVersionPublish(channel_id,'channel','add');
                        toastr.error('Cant fetch info regarding collection ' + name + ' please check net or it can be unapproved','',{timeOut: 30000});
                    }
                }
            );
        }

        return feed_channel_element;
    }
    /**
     * Creates feed content element using jquery in detached state (HALF)
     * @param  String  content_id content id to be created
     * @param  String  environment  operating environment [read, write] write means editable mode
     * @return DOM     jquery DOM feed content element
     */
    function createFeedChannelElementHalf(object, old) {
        var channel_id = object.id;
        var name = object.name;
        var feed_channel_element;
        if(old){
            feed_channel_element = $('<div class="feed-channel-preview  feed-element-half inline-block feed-channel-' + channel_id + '" data-id="' + channel_id + '*channel*' + name + '">\
                            <img data-original="" src="/static/img/placeholder.jpg" class="feed-channel-preview-icon lazy">\
                            <div class="feed-channel-preview-name ellipsis">' + name + '</div></div>');
            return feed_channel_element;
        }
        if(channel_id in cache){
            var element = cache[channel_id];
            feed_channel_element = $('<div class="feed-channel-preview feed-element-half inline-block feed-channel-' + channel_id + '" data-id="' + channel_id + '*channel*' + name + '">\
                            <img data-original="' + element['icon_small'] + '" src="/static/img/placeholder.jpg" class="feed-channel-preview-icon lazy">\
                            <div class="feed-channel-preview-name ellipsis">' + element['name'] + '</div></div>');
        } else {
            feed_channel_element = $('<div class="feed-channel-preview feed-element-half inline-block feed-channel-' + channel_id + '" data-id="' + channel_id + '*channel*' + name + '">\
                            <img data-original="" src="/static/img/placeholder.jpg" class="feed-channel-preview-icon lazy">\
                            <div class="feed-channel-preview-name ellipsis">' + name + '</div></div>');
            $.ajax({
                    url: BASE_URL + 'channel/fetch/' + channel_id ,
                    method: "GET",
                    success: function(data){
                        if(data.data.status != 'complete'){
                            handleVersionPublish(channel_id,'channel','add');
                        } else {
                            $('.feed-channel-' + channel_id).find('img').attr("data-original",data.data.icon_small).lazyload({effect: "fadeIn"});
                            cache[channel_id] = data.data;
                        }
                    },
                    error: function(data){
                        handleVersionPublish(channel_id,'channel','add');
                        toastr.error('Cant fetch info regarding collection ' + name + ' please check net or it can be unapproved','',{timeOut: 30000});
                    }
                }
            );

        }

        return feed_channel_element;
    }



    /**
     * Creates feed content element using jquery in detached state (FULL)
     * @param  String  content_id content id to be created
     * @param  String  environment  operating environment [read, write] write means editable mode
     * @return DOM     jquery DOM feed content element
     */
    function createFeedContentElementFull(object, old) {
        var content_id = object.id;
        var name = object.name;
        var feed_content_element;
        if(old){
            feed_content_element = $('<div class="feed-content-preview feed-element-full inline-block feed-content-' + content_id + '" data-id="' + content_id + '*content*' + name + '">\
                        <img data-original="" src="/static/img/placeholder.jpg" class="feed-content-preview-icon lazy">\
                        <div class="feed-content-preview-name ellipsis">' + name + '</div></div>');
            return feed_content_element;
        }
        if(content_id in cache){
            var element = cache[content_id];
            feed_content_element = $('<div class="feed-content-preview feed-element-full inline-block feed-content-' + content_id + '" data-id="' + content_id + '*content*' + name + '">\
                        <img data-original="' + element['poster'] + '" src="/static/img/placeholder.jpg" class="feed-content-preview-icon lazy">\
                        <div class="feed-content-preview-name ellipsis">' + element['name'] + '</div></div>');
        } else {
            feed_content_element = $('<div class="feed-content-preview feed-element-full inline-block feed-content-' + content_id + '" data-id="' + content_id + '*content*' + name + '">\
                        <img data-original="" src="/static/img/placeholder.jpg" class="feed-content-preview-icon lazy">\
                        <div class="feed-content-preview-name ellipsis">' + name + '</div></div>');

        }
        return feed_content_element;
    }
    /**
     * Creates feed content element using jquery in detached state (HALF)
     * @param  String  content_id content id to be created
     * @param  String  environment  operating environment [read, write] write means editable mode
     * @return DOM     jquery DOM feed content element
     */
    function createFeedContentElementHalf(object, old) {
        var content_id = object.id;
        var name = object.name;
        var feed_content_element;
        if(old){
            feed_content_element = $('<div class="feed-content-preview  feed-element-half inline-block feed-content-' + content_id + '" data-id="' + content_id + '*content*' + name + '">\
                        <img data-original="" src="/static/img/placeholder.jpg" class="feed-content-preview-icon lazy">\
                        <div class="feed-content-preview-name ellipsis">' + name + '</div></div>');
            return feed_content_element;
        }
        if(content_id in cache){
            var element = cache[content_id];
            feed_content_element = $('<div class="feed-content-preview feed-element-half inline-block feed-content-' + content_id + '" data-id="' + content_id + '*content*' + name + '">\
                        <img data-original="' + element['poster'] + '" src="/static/img/placeholder.jpg" class="feed-content-preview-icon lazy">\
                        <div class="feed-content-preview-name ellipsis">' + element['name'] + '</div></div>');
        } else {
            feed_content_element = $('<div class="feed-content-preview feed-element-half inline-block feed-content-' + content_id + '" data-id="' + content_id + '*content*' + name + '">\
                        <img data-original="" src="/static/img/placeholder.jpg" class="feed-content-preview-icon lazy">\
                        <div class="feed-content-preview-name ellipsis">' + name + '</div></div>');

        }
        return feed_content_element;
    }



    /**
     * Creates collection element using jquery in detached state (HALF)
     * @param  String  collection_id collection id to be created
     * @param  String  environment  operating environment [read, write] write means editable mode
     * @return DOM     jquery DOM collection element
     */
    function createCollectionElementHalf(object, old) {
        var collection_id = object.id;
        var name = object.name;
        var collection_element;
        if(old){
            collection_element = $('<div class="collection-preview  feed-element-half inline-block collection-' + collection_id + '" data-id="' + collection_id + '*collection*' + name + '">\
                        <img data-original="" src="/static/img/placeholder.jpg" class="collection-preview-icon lazy">\
                        <div class="collection-preview-name ellipsis">' + name + '</div></div>');
            return collection_element;
        }
        if(collection_id in cache){
            var element = cache[collection_id];
            collection_element = $('<div class="collection-preview feed-element-half inline-block collection-' + collection_id + '" data-id="' + collection_id + '*collection*' + name + '">\
                        <img data-original="' + element['poster_small'] + '" src="/static/img/placeholder.jpg" class="collection-preview-icon lazy">\
                        <div class="collection-preview-name ellipsis">' + element['name'] + '</div></div>');

        } else {
            collection_element = $('<div class="collection-preview feed-element-half inline-block collection-' + collection_id + '" data-id="' + collection_id + '*collection*' + name + '">\
                        <img data-original="" src="/static/img/placeholder.jpg" class="collection-preview-icon lazy">\
                        <div class="collection-preview-name ellipsis">' + name + '</div></div>');
            $.ajax({
                    url: BASE_URL + 'tag/fetch/' + collection_id ,
                    method: "GET",
                    success: function(data){
                        if(data.data.status != 'complete'){
                            handleVersionPublish(collection_id,'collection','add');
                        } else {
                            $('.collection-' + collection_id).find('img').attr("data-original",data.data.poster_small).lazyload({effect: "fadeIn"});
                            cache[collection_id] = data.data;
                        }
                    },
                    error: function(data){
                        handleVersionPublish(collection_id,'collection','add');
                        toastr.error('Cant fetch info regarding ' + name + ' please check net or it can be unapproved','',{timeOut: 30000});
                    }
                }
            );

        }
        return collection_element;
    }
    /**
     * Creates collection element using jquery in detached state (FULL)
     * @param  String  collection_id collection id to be created
     * @param  String  environment  operating environment [read, write] write means editable mode
     * @return DOM     jquery DOM collection element
     */
    function createCollectionElementFull(object, old) {
        var collection_id = object.id;
        var name = object.name;
        var collection_element;
        if(old){
            collection_element = $('<div class="collection-preview  feed-element-full inline-block collection-' + collection_id + '" data-id="' + collection_id + '*collection*' + name + '">\
                        <img data-original="" src="/static/img/placeholder.jpg" class="collection-preview-icon lazy">\
                        <div class="collection-preview-name ellipsis">' + name + '</div></div>');
            return collection_element;
        }
        if(collection_id in cache){
            var element = cache[collection_id];
            collection_element = $('<div class="collection-preview feed-element-full inline-block collection-' + collection_id + '" data-id="' + collection_id + '*collection*' + name + '">\
                        <img data-original="' + element['poster'] + '" src="/static/img/placeholder.jpg" class="collection-preview-icon lazy">\
                        <div class="collection-preview-name ellipsis">' + element['name'] + '</div></div>');

        } else {
            collection_element = $('<div class="collection-preview feed-element-full inline-block collection-' + collection_id + '" data-id="' + collection_id + '*collection*' + name + '">\
                        <img data-original="" src="/static/img/placeholder.jpg" class="collection-preview-icon lazy">\
                        <div class="collection-preview-name ellipsis">' + name + '</div></div>');
            $.ajax({
                    url: BASE_URL + 'tag/fetch/' + collection_id ,
                    method: "GET",
                    success: function(data){
                        if(data.data.status != 'complete'){
                            handleVersionPublish(collection_id,'collection','add');
                        } else {
                            $('.collection-' + collection_id).find('img').attr("data-original",data.data.poster).lazyload({effect: "fadeIn"});
                            cache[collection_id] = data.data;
                        }
                    },
                    error: function(data){
                        handleVersionPublish(collection_id,'collection','add');
                        toastr.error('Cant fetch info regarding ' + name + ' please check net or it can be unapproved','',{timeOut: 30000});
                    }
                }
            );

        }
        return collection_element;
    }



    /**
     * Creates channel element using jquery in detached state
     * @param  String  channel_id channel id to be created
     * @param  String  environment  operating environment [read, write] write means editable mode
     * @return DOM     jquery DOM channel element
     */
    function createChannelElement(object,environment,old) {
        var old = old || false;
        var channel_id = object.id;
        var name = object.name;
        var channel_element;
        if(old){
            channel_element = $('<div class="channel-preview  inline-block channel-' + channel_id + '" data-id="' + channel_id + '*' + name + '">\
                            <img data-original="" src="/static/img/placeholder.jpg" class="channel-preview-icon lazy">\
                            <div class="channel-preview-name ellipsis">' + name + '</div></div>');
            $(channel_element).click(getChannelClickHandler(object, environment));
            return channel_element;
        }
        if(channel_id in cache){
            var element = cache[channel_id];
            channel_element = $('<div class="channel-preview inline-block channel-' + channel_id + '" data-id="' + channel_id + '*' + name + '">\
                            <img data-original="' + element['icon'] + '" src="/static/img/placeholder.jpg" class="channel-preview-icon lazy">\
                            <div class="channel-preview-name ellipsis">' + element['name'] + '</div></div>');
        } else {
            channel_element = $('<div class="channel-preview inline-block channel-' + channel_id + '" data-id="' + channel_id + '*' + name + '">\
                            <img data-original="" src="/static/img/placeholder.jpg" class="channel-preview-icon lazy">\
                            <div class="channel-preview-name ellipsis">' + name + '</div></div>');
            $.ajax({
                    url: BASE_URL + 'channel/fetch/' + channel_id ,
                    method: "GET",
                    success: function(data){
                        if(data.data.status == 'complete'){
                            $('.channel-' + channel_id).find('img').attr("data-original",data.data.icon_small).lazyload({effect: "fadeIn"});
                            cache[channel_id] = data.data;
                        }else {
                            handleVersionPublish(channel_id,'channel','add');
                            toastr.error('Cant fetch info regarding Channel ' + name + ' please check net or it can be unapproved','',{timeOut:15000});
                        }
                    },
                    error: function(data){
                        handleVersionPublish(channel_id,'channel','add');
                        toastr.error('Cant fetch info regarding Channel ' + name + ' please check net or it can be unapproved','',{timeOut:15000});
                    }
                }
            );
        }

        $(channel_element).click(getChannelClickHandler(object, environment));

        return channel_element;

    }


    /**
     * Created DOM element for selected main collection
     * @param  String  collection_id   collection id of selected element
     * @param  String  environment  operating environement [read, write] write is for editable mode
     * @return DOM     DOM element of selected collection
     */
    function createMainCollectionElement(object,old) {
        var collection_id = object.id;
        var name = object.name;
        var collection_element;
        //console.log('collection_element type:' + old);
        if(old){
            //console.log('elemnt old');
            collection_element = $('<div class="maincollection-preview concrete inline-block maincollection-' + collection_id + '" data-id="' + collection_id + '*' + name + '">\
                        <img data-original="" src="/static/img/placeholder.jpg" class="maincollection-preview-poster lazy"><div class="maincollection-image-overlay"></div>\
                        <div class="maincollection-preview-name ellipsis">' + name + '</div></div>');
            if($.inArray(collection_id,elementsToFetch['tag']) < 0){
                elementsToFetch['tag'].push(collection_id);
            }
            return collection_element;
        }
        if(collection_id in cache){
            //console.log('elemnt in cache');
            var element = cache[collection_id];
            collection_element = $('<div class="maincollection-preview concrete inline-block maincollection-' + collection_id + '" data-id="' + collection_id + '*' + name + '">\
                        <img data-original="' + element['poster'] + '" src="/static/img/placeholder.jpg" class="maincollection-preview-poster lazy"><div class="maincollection-image-overlay"></div>\
                        <div class="maincollection-preview-name ellipsis">' + element['name'] + '</div></div>');

        } else {
            //console.log('elemnt not in cache');
            collection_element = $('<div class="maincollection-preview concrete inline-block maincollection-' + collection_id + '" data-id="' + collection_id + '*' + name + '">\
                        <img data-original="" src="/static/img/placeholder.jpg" class="maincollection-preview-poster lazy"><div class="maincollection-image-overlay"></div>\
                        <div class="maincollection-preview-name ellipsis">' + name + '</div></div>');
            $.ajax({
                    url: BASE_URL + 'tag/fetch/' + collection_id ,
                    method: "GET",
                    success: function(data){
                        if(data.data.status != 'complete'){
                            handleVersionPublish(collection_id,'collection','add');
                        } else {
                            cache[collection_id] = data.data;
                            $('.maincollection-' + collection_id).find('img').attr("data-original",data.data.poster).lazyload({effect: "fadeIn"});
                        }
                    },
                    error: function(data){
                        handleVersionPublish(collection_id,'collection','add');
                        toastr.error('Cant fetch info regarding collection ' + name + ' please check net or it can be unapproved','',{timeOut: 30000});
                    }
                }
            );
        }
        return collection_element;
    }


    function createMainCollectionContentElement(content,old){
        //console.log('collection content element old state' + old);
        var content_id = content['id'];
        var type = content['type'];
        var name = ('name' in content) ? content['name'] : 'Taged Item';
        var content_element;
        if(type == 'content'){
            if(old){
                //console.log('old collection_content_element');
                content_element = $('<div class="content-preview concrete inline-block maincollection-content-' + content_id + '" data-id="' + content_id + '*content*' + name + '">\
                                <img data-original="" src="/static/img/placeholder.jpg" class="content-preview-poster lazy"><div class="content-image-overlay"></div>\
                                <div class="content-preview-name ellipsis">' + name + '</div></div>');
                if($.inArray(content_id,elementsToFetch['content']) < 0){
                    elementsToFetch['content'].push(content_id);
                }
                return content_element;
            }
            if(content_id in cache){
                //console.log('in cache collection_content_element');
                var element = cache[content_id];
                content_element = $('<div class="content-preview concrete inline-block maincollection-content-' + content_id + '" data-id="' + content_id + '*content*' + name + '">\
                                <img data-original="' + element['poster'] + '" src="/static/img/placeholder.jpg" class="content-preview-poster lazy"><div class="content-image-overlay"></div>\
                                <div class="content-preview-name ellipsis">' + element['name'] + '</div></div>');
            } else {
                //console.log('not in cache collection_content_element');
                content_element = $('<div class="content-preview concrete inline-block maincollection-content-' + content_id + '" data-id="' + content_id + '*content*' + name + '">\
                                <img data-original="" src="/static/img/placeholder.jpg" class="content-preview-poster lazy"><div class="content-image-overlay"></div>\
                                <div class="content-preview-name ellipsis">' + name + '</div></div>');
                $.ajax({
                        url: BASE_URL + 'content/fetch/' + content_id ,
                        method: "GET",
                        success: function(data){
                            if(data.data.status != 'complete'){
                                handleVersionPublish(content_id,'content','add');
                            } else {
                                $('.maincollection-content-' + content_id).find('img').attr("data-original",data.data.poster).lazyload({effect: "fadeIn"});
                                $('.maincollection-content-' + content_id).find('.content-preview-name').html(data.data.name);
                                cache[content_id] = data.data;
                            }

                        },
                        error: function(data){
                            handleVersionPublish(content_id,'content','add');
                            toastr.error('Cant fetch info regarding collection content ' + name + ' please check net or it can be unapproved','',{timeOut:30000});
                        }
                    }
                );

            }
        } else if(type == 'channel'){
            if(old){
                content_element = $('<div class="content-preview concrete inline-block maincollection-content-' + content_id + '" data-id="' + content_id + '*channel*' + name + '">\
                                <img data-original="" src="/static/img/placeholder.jpg" class="content-preview-poster lazy"><div class="content-image-overlay"></div>\
                                <div class="content-preview-name ellipsis">' + name + '</div></div>');
                if($.inArray(content_id,elementsToFetch['content']) < 0){
                    elementsToFetch['channel'].push(content_id);
                }
                return content_element;
            }
            if(content_id in cache){
                var element = cache[content_id];
                content_element = $('<div class="content-preview concrete inline-block maincollection-content-' + content_id + '" data-id="' + content_id + '*channel*' + name + '">\
                                <img data-original="' + element['icon'] + '" src="/static/img/placeholder.jpg" class="content-preview-poster lazy"><div class="content-image-overlay"></div>\
                                <div class="content-preview-name ellipsis">' + element['name'] + '</div></div>');
            } else {
                content_element = $('<div class="content-preview concrete inline-block maincollection-content-' + content_id + '" data-id="' + content_id + '*channel*' + name + '">\
                                <img data-original="" src="/static/img/placeholder.jpg" class="content-preview-poster lazy"><div class="content-image-overlay"></div>\
                                <div class="content-preview-name ellipsis">fetching name...</div></div>');
                $.ajax({
                        url: BASE_URL + 'channel/fetch/' + content_id ,
                        method: "GET",
                        success: function(data){
                            if(data.data.status != 'complete'){
                                handleVersionPublish(content_id,'channel','add');
                            } else {
                                $('.maincollection-content-' + content_id).find('img').attr("data-original",data.data.icon).lazyload({effect: "fadeIn"});
                                $('.maincollection-content-' + content_id).find('.content-preview-name').html(data.data.name);
                                cache[content_id] = data.data;
                            }
                        },
                        error: function(data){
                            handleVersionPublish(content_id,'channel','add');
                            toastr.error('Cant fetch info regarding collection ' + name + ' please check net or it can be unapproved','',{timeOut: 30000});
                        }
                    }
                );

            }
        }

        return content_element;
    }

    /**
     * Created DOM element for selected content
     * @param  String  content_id   content id of selected element
     * @param  String  channel_id  operating environement [read, write] write is for editable mode
     * @param  String  environment  operating environement [read, write] write is for editable mode
     * @return DOM     DOM element of selected content
     */
    function createChannelContentElement(object, channel_id, old) {
        var content_id = object.id;
        var name = object.name;
        var content_element;
        if(old){
            content_element = $('<div class="content-preview concrete  inline-block channel-content-' + content_id + '" data-id="' + content_id + '*content*' + name + '">\
                        <img data-original="" src="/static/img/placeholder.jpg" class="content-preview-poster lazy"><div class="content-image-overlay"></div>\
                        <div class="content-preview-name ellipsis">' + name + '</div></div>');
            return content_element;
        }
        if(content_id in cache){
            var element = cache[content_id];
            if(element == undefined){
                return undefined;
            }
            content_element = $('<div class="content-preview concrete inline-block channel-content-' + content_id + '" data-id="' + content_id + '*content*' + name + '">\
                        <img data-original="' + element['poster'] + '" src="/static/img/placeholder.jpg" class="content-preview-poster lazy"><div class="content-image-overlay"></div>\
                        <div class="content-preview-name ellipsis">' + element['name'] + '</div></div>');
        } else {
            content_element = $('<div class="content-preview concrete inline-block channel-content-' + content_id + '" data-id="' + content_id + '*content*' + name + '">\
                        <img data-original="" src="/static/img/placeholder.jpg" class="content-preview-poster lazy"><div class="content-image-overlay"></div>\
                        <div class="content-preview-name ellipsis">' + name + '</div></div>');
            $.ajax({
                    url: BASE_URL + 'content/fetch/' + content_id ,
                    method: "GET",
                    success: function(data){
                        if(data.data.status == 'complete'){
                            $('.channel-content-' + content_id).find('img').attr("data-original",data.data.poster).lazyload({effect: "fadeIn"});
                            cache[content_id] = data.data;
                        } else {
                            handleVersionPublish(content_id,'content','add');
                            toastr.error('Cant fetch info regarding content ' + name + ' please check net or it can be unapproved','',{timeOut: 30000});
                        }
                    },
                    error: function(data){
                        handleVersionPublish(content_id,'content','add');
                        toastr.error('Cant fetch info regarding content ' + name + ' please check net or it can be unapproved','',{timeOut: 30000});
                    }
                }
            );

        }

        return content_element;
    }

    /**
     * Created DOM element for selected image ad for channel drawer
     * @param  String  image_ad_id   image ad id of selected element
     * @param  String  channel_id  operating environement [read, write] write is for editable mode
     * @param  String  environment  operating environement [read, write] write is for editable mode
     * @return DOM     DOM element of selected image-ad
     */
    function createChannelImageAdElement(object, channel_id, old) {
        var image_ad_id = object.id;
        var name = object.name;
        var sponsor_id = object.sponsor_id;
        var add_no = image_ad_id;
        var link;
        var image_ad_element;
        if(old){
            image_ad_element = $('<div class="content-preview concrete  inline-block channel-image-ad-' + image_ad_id + '" data-id="' + image_ad_id + '*image_ad*' + sponsor_id + '*' + name + '">\
                            <div class="image-ad-symbol"> IMAGE AD</div>\
                        <img  data-original="" src="/static/img/placeholder.jpg" class="image-ad-preview-poster lazy"><div class="image-ad-image-overlay"></div>\
                        <div class="image-ad-preview-name ellipsis">' + name + '</div></div>');
            return image_ad_element;
        }
        if(sponsor_id in cache){
            var element = cache[sponsor_id];
            element.image_ads.forEach(function(obj){
                if(add_no in obj){
                    link = obj[add_no];
                }
            });
            image_ad_element = $('<div class="content-preview concrete inline-block channel-image-ad-' + image_ad_id + '" data-id="' + image_ad_id + '*image_ad*' + sponsor_id + '*' + name + '">\
                            <div class="image-ad-symbol"> IMAGE AD</div>\
                        <img  data-original="' + link + '" src="/static/img/placeholder.jpg" class="image-ad-preview-poster lazy"><div class="image-ad-image-overlay"></div>\
                        <div class="image-ad-preview-name ellipsis">' + element['name'] + '</div></div>');
        } else {
            image_ad_element = $('<div class="content-preview concrete inline-block channel-image-ad-' + image_ad_id + '" data-id="' + image_ad_id + '*image_ad*' + sponsor_id + '*' + name + '">\
                            <div class="image-ad-symbol"> IMAGE AD</div>\
                        <img  data-original="" src="/static/img/placeholder.jpg" class="image-ad-preview-poster lazy"><div class="image-ad-image-overlay"></div>\
                        <div class="image-ad-preview-name ellipsis">' + name + '</div></div>');
            $.ajax({
                    url: BASE_URL + 'sponsor/fetch/' + sponsor_id ,
                    method: "GET",
                    success: function(data){
                        if(data.data.status == 'complete'){
                            data.data.image_ads.forEach(function(obj){
                                if(add_no in obj){
                                    link = obj[add_no];
                                }
                            });
                            $('.channel-image-ad-' + image_ad_id).find('img').attr("data-original",link).lazyload({effect: "fadeIn"});
                            cache[sponsor_id] = data.data;
                        } else {
                            handleVersionPublish(sponsor_id,'sponsor','add');
                            toastr.error('Cant fetch info regarding collection ' + name + ' please check net or it can be unapproved','',{timeOut: 30000});
                        }
                    },
                    error: function(data){
                        handleVersionPublish(sponsor_id,'sponsor','add');
                        toastr.error('Cant fetch info regarding collection ' + name + ' please check net or it can be unapproved','',{timeOut: 30000});
                    }
                }
            );

        }

        return image_ad_element;


    }


    /**
     * create DOM element for sponsor
     * @param  String   channel_id  id of channel to which sponsor will be attached
     * @param  String   sponsor_id  id of sponsor to be attached
     * @param  Steing   environment  operating environment [read, write] write means editable
     * @return DOM      DOM element of selected sponsor
     */
    function createSponsorElement(object, channel_id,old) {
        var sponsor_id = object.id;
        var name = object.name;
        var sponsor_element;
        if(old){
            sponsor_element = $('<div class="sponsor-preview concrete inline-block sponsor-' + sponsor_id + '" data-id="' + sponsor_id + '*' + name + '">\
                        <img src="" class="sponsor-preview-logo">\
                        <div class="sponsor-preview-name ellipsis">' + name + '</div></div>');
            return sponsor_element;
        }
        if(sponsor_id in cache){
            var element = cache[sponsor_id];
            sponsor_element = $('<div class="sponsor-preview concrete inline-block sponsor-' + sponsor_id + '" data-id="' + sponsor_id + '*' + name + '">\
                        <img src="' + element['logo'] + '" class="sponsor-preview-logo">\
                        <div class="sponsor-preview-name ellipsis">' + element['name'] + '</div></div>');

        } else {
            sponsor_element = $('<div class="sponsor-preview concrete inline-block sponsor-' + sponsor_id + '" data-id="' + sponsor_id + '*' + name + '">\
                        <img src="" class="sponsor-preview-logo">\
                        <div class="sponsor-preview-name ellipsis">' + name + '</div></div>');

            $.ajax({
                    url: BASE_URL + 'sponsor/fetch/' + sponsor_id ,
                    method: "GET",
                    success: function(data){
                        if(data.data.status == 'complete'){
                            cache[sponsor_id] = data.data;
                            $('.sponsor-' + sponsor_id).find('img').attr('src',data.data.logo);
                        }else {
                            handleVersionPublish(sponsor_id,'sponsor','add');
                        }
                    },
                    error: function(data){
                        handleVersionPublish(sponsor_id,'sponsor','add');
                        toastr.error('Cant fetch info regarding ' + name + ' please check net or it can be unapproved','',{timeOut: 30000});
                    }
                }
            );
        }


        return sponsor_element;

    }

    /**
     * create DOM element for splash
     * @param  String   channel_id  id of channel to which sponsor will be attached
     * @param  String   splash_id  id of splash to be attached
     * @param  Steing   environment  operating environment [read, write] write means editable
     * @return DOM      DOM element of selected sponsor
     */
    function createSplashElement(object,channel_id, environment,old) {
        var splash_id = object.id;
        var sponsor_id = object.sponsor_id;
        var name = object.name;
        var splash_element
        if(old){
            splash_element = $('<div class="sponsor-preview concrete inline-block splash-' + splash_id + '" data-sponsor="' + sponsor_id + '" style="margin-top:5px" data-id="' + splash_id + '*' + name + '">\
                        <img src="" class="sponsor-preview-logo">\
                        <div class="sponsor-preview-name ellipsis">' + name + '</div></div>');
            return splash_element;
        }
        if(sponsor_id in cache){
            var element = cache[sponsor_id];
            splash_element = $('<div class="sponsor-preview concrete inline-block splash-' + splash_id + '" data-sponsor="' + sponsor_id + '" style="margin-top:5px" data-id="' + splash_id + '*' + name + '">\
                        <img src="' + element['logo'] + '" class="sponsor-preview-logo">\
                        <div class="sponsor-preview-name ellipsis">' + element['name'] + '</div></div>');
        } else {
            splash_element = $('<div class="sponsor-preview concrete inline-block splash-' + splash_id + '" data-sponsor="' + sponsor_id + '" style="margin-top:5px" data-id="' + splash_id + '*' + name + '">\
                        <img src="" class="sponsor-preview-logo">\
                        <div class="sponsor-preview-name ellipsis">' + name + '</div></div>');
            $.ajax({
                    url: BASE_URL + 'sponsor/fetch/' + sponsor_id ,
                    method: "GET",
                    success: function(data){
                        if(data.data.status == 'complete'){
                            cache[sponsor_id] = data.data;
                            $('.splash-' + splash_id).find('img').attr('src',data.data.logo);
                        } else {
                            handleVersionPublish(sponsor_id,'sponsor','add');
                        }
                    },
                    error: function(data){
                        handleVersionPublish(sponsor_id,'sponsor','add');
                        toastr.error('Cant fetch info regarding ' + name + ' please check net or it can be unapproved','',{timeOut: 30000});
                    }
                }
            );
        }

        return splash_element;

    }

    /**
     * create DOM element for preroll
     * @param  String   channel_id  id of channel to which sponsor will be attached
     * @param  String   preroll_id  id of splash to be attached
     * @param  Steing   environment  operating environment [read, write] write means editable
     * @return DOM      DOM element of selected sponsor
     */
    function createPrerollElement(object,channel_id, old) {
        var preroll_id = object.id;
        var sponsor_id = object.sponsor_id;
        var name = object.name;
        var preroll_element
        if(old){
            preroll_element = $('<div class="sponsor-preview concrete inline-block preroll-' + preroll_id + '" data-sponsor="' + sponsor_id + '" style="margin-top:5px" data-id="' + preroll_id + '*' + name + '">\
                        <img src="" class="sponsor-preview-logo">\
                        <div class="sponsor-preview-name ellipsis">' + name + '</div></div>');
            return preroll_element;
        }
        if(sponsor_id in cache){
            var element = cache[sponsor_id];
            preroll_element = $('<div class="sponsor-preview concrete inline-block preroll-' + preroll_id + '" data-sponsor="' + sponsor_id + '" style="margin-top:5px" data-id="' + preroll_id + '*' + name + '">\
                        <img src="' + element['logo'] + '" class="sponsor-preview-logo">\
                        <div class="sponsor-preview-name ellipsis">' + element['name'] + '</div></div>');
        } else {
            preroll_element = $('<div class="sponsor-preview concrete inline-block preroll-' + preroll_id + '" data-sponsor="' + sponsor_id + '" style="margin-top:5px" data-id="' + preroll_id + '*' + name + '">\
                        <img src="" class="sponsor-preview-logo">\
                        <div class="sponsor-preview-name ellipsis">' + name + '</div></div>');
            $.ajax({
                    url: BASE_URL + 'sponsor/fetch/' + sponsor_id ,
                    method: "GET",
                    success: function(data){
                        if(data.data.status == 'complete'){
                            cache[sponsor_id] = data.data;
                            $('.preroll-' + preroll_id).find('img').attr('src',data.data.logo);
                        } else {
                            handleVersionPublish(sponsor_id,'sponsor','add');
                        }
                    },
                    error: function(data){
                        handleVersionPublish(sponsor_id,'sponsor','add');
                        toastr.error('Cant fetch info regarding preroll' + name + ' please check net or it can be unapproved','',{timeOut: 30000});
                    }
                }
            );
        }

        return preroll_element;

    }


    /**
     * checks if particular row is completely filled .
     * @param  int  row number of current working row
     * @return boolean true if row is filled
     */
    function checkIfFilled(row_no) {
        var no_of_children = $('#feed-row-' + row_no).children().length;

        if (no_of_children == 0) {
            $('#feed-row-' + row_no).remove();

            current_row = row_no;
            if (current_row != max_current_row) {
                current_row = max_current_row;
                $('.highlighted-row').removeClass('highlighted-row');

                $('#feed-row-' + current_row).addClass('highlighted-row');
            } else {
                current_row++;
                max_current_row = current_row;
                var row = "<div id='feed-row-" + current_row + "' data-id='feed-row-" + current_row + "'class='feed-row' ></div>";
                $('.feed-list').append(row);
            }


            var noc_new = $('#feed-row-' + current_row).children().length;
            if (noc_new > 0) {
                $('#feed-row-element-count-two').prop('checked', true);
                $('#feed-row-element-count-one').prop('disabled', true);
            } else {
                $('#feed-row-element-count-one').prop('disabled', false);
            }

            $('.highlighted-row').removeClass('highlighted-row');

            $('#feed-row-' + current_row).addClass('highlighted-row');
        } else {
            if (no_of_children == 1) {
                //$('#version-collection-list').prop('disabled', true).trigger("chosen:updated");
                var child = $('#feed-row-' + row_no).children()[0];
                var type = $(child).data('id').split('*')[1];
                if ($(child).hasClass('feed-element-full')) {
                    current_row++;
                    max_current_row = current_row;
                    var row = "<div id='feed-row-" + current_row + "' data-id='feed-row-" + current_row + "'class='feed-row' ></div>";
                    $('.feed-list').append(row);
                    $('#feed-row-element-count-one').prop('disabled', false);
                    $('.highlighted-row').removeClass('highlighted-row');

                    $('#feed-row-' + current_row).addClass('highlighted-row');
                } else {
                    current_row = row_no;
                    $('.highlighted-row').removeClass('highlighted-row');

                    $('#feed-row-' + current_row).addClass('highlighted-row');
                    $('#feed-row-element-count-two').prop('checked', true);
                    $('#feed-row-element-count-one').prop('disabled', true);
                }
            } else if (no_of_children == 2) {
                if (current_row != max_current_row) {
                    current_row = max_current_row;
                    $('.highlighted-row').removeClass('highlighted-row');
                    $('#feed-row-' + current_row).addClass('highlighted-row');
                    $('#feed-row-element-count-one').prop('disabled', false);
                } else {
                    current_row++;
                    max_current_row = current_row;
                    var row = "<div id='feed-row-" + current_row + "' data-id='feed-row-" + current_row + "'class='feed-row' ></div>";
                    $('.feed-list').append(row);
                    $('#feed-row-element-count-one').prop('disabled', false);
                    $('.highlighted-row').removeClass('highlighted-row');

                    $('#feed-row-' + current_row).addClass('highlighted-row');
                }

            }


        }
    }

    function addElementInFeed(element, type) {
        $('#feed-row-' + current_row).append(element);
        checkIfFilled(current_row);
    }


    /**
     * Inserts image ad or content elements to channel.
     * @param  string  concatination of id type and sponsor_id(if image_ad)
     */
    function insertSelectedChannelContent(object, channel_id) {
        var content_id = object.id;
        var name = object.name;
        if($.inArray(content_id,alreadyInserted['channel']) > -1){
            toastr.error('Already inserted in version!!');
            return;
        }
        var channel_id = channel_id || null;
        if (channel_id == null)
            var current_content_drawer = $('.phone-overlay').find('.active-channel');
        else
            var current_content_drawer = $('.' + channel_id + '-content-section');

        /* assigning channel if from active channel */
        channel_id = $(current_content_drawer).attr('data-channel');

        var selected_element;

        selected_element = createChannelContentElement(object, channel_id);
        $(current_content_drawer).find('.content-list').prepend(selected_element);
        $('.channel-content-' + content_id).contextmenu({
            target: '#context-menu-channel-content',
            onItem: channelContentContextHandler
        });
        alreadyInserted['channel'].push(content_id);

    }

    function insertSelectedChannelImage(object, channel_id) {
        var image_ad_id = object.id;
        var name = object.name;
        var sponsor_id = object.sponsor_id;
        var channel_id = channel_id || null;
        if (channel_id == null)
            var current_content_drawer = $('.phone-overlay').find('.active-channel');
        else
            var current_content_drawer = $('.' + channel_id + '-content-section');

        /* assigning channel if from active channel */
        channel_id = $(current_content_drawer).attr('data-channel');

        var selected_element;

        selected_element = createChannelImageAdElement(object, channel_id, 'write');
        $(current_content_drawer).find('.content-list').prepend(selected_element);
        handlePreviousVersionSponsorMapping(image_ad_id,sponsor_id,'image_ad');
        $('.channel-image-ad-' + image_ad_id).contextmenu({
            target: '#context-menu-channel-content',
            onItem: channelImageAdContextHandler
        });

    }

    /**
     * Inserts image ad elements to feed.
     * @param  string  concatination of id type and sponsor_id(if image_ad)
     */
    function insertSelectedFeedImage(object) {
        var id  = object.id;
        var name = object.name;
        var sponsor_id = object.sponsor_id;
        if($.inArray(id,alreadyInserted['feed']) > -1){
            toastr.error('Already in version!!, do it one more time and then i\'ll tell');
            return;
        }
        var selected_element;
        if($("input[name=feed-row-element-count]:checked").val() == 'two'){
            selected_element = createFeedImageAdElementHalf(object);
        }else{
            selected_element = createFeedImageAdElementFull(object);
        }
        addElementInFeed(selected_element, 'image_ad');
        alreadyInserted['feed'].push(id);
        handlePreviousVersionSponsorMapping(id,sponsor_id,'image_ad');

        $('.feed-image-ad-' + id).contextmenu({
            target: '#context-menu-feed-image-ad',
            onItem: feedImageAdContextHandler
        });


    }

    /**
     * Inserts content elements to feed.
     * @param  string  concatination of id type and sponsor_id(if image_ad)
     */
    function insertSelectedFeedContent(object) {
        var content_id  = object.id;
        var name = object.name;
        if($.inArray(content_id,alreadyInserted['feed']) > -1){
            toastr.error('Already in version!!, do it one more time and then i\'ll tell');
            return;
        }
        alreadyInserted['feed'].push(content_id);
        var selected_element;
        if($("input[name=feed-row-element-count]:checked").val() == 'two'){
            selected_element = createFeedContentElementHalf(object);
        }else{
            selected_element = createFeedContentElementFull(object);
        }
        addElementInFeed(selected_element, 'content');
        $.ajax({
                url: BASE_URL + 'content/fetch/' + content_id ,
                method: "GET",
                success: function(data){
                    if(data.data.status != 'complete'){
                        handleVersionPublish(content_id,'content','add');
                    } else {
                        $('.feed-content-' + content_id).find('img').attr("data-original",data.data.poster_small).lazyload({effect: "fadeIn"});
                        cache[content_id] = data.data;
                    }
                    if(data.data.video_content_type == 'movie'){
                        $('.feed-content-' + content_id).attr('data-feedtype','movie');
                        updateFeedElementCount(content_id,'movie','add');
                    } else {
                        $('.feed-content-' + content_id).attr('data-feedtype','video');
                        updateFeedElementCount(content_id,'video','add');
                    }
                },
                error: function(data){
                    handleVersionPublish(content_id,'content','add');
                    toastr.error('Cant fetch info regarding ' + name + ' please check net or it can be unapproved','',{timeOut: 30000});
                }
            }
        );
        $('.feed-content-' + content_id).contextmenu({
            target: '#context-menu-feed-content',
            onItem: feedContentContextHandler
        });


    }

    /**
     * inserts selected content to phone's feed view and removes from select box
     * @param  String   collection id of selected collection
     */

    function insertSelectedFeedChannel(object) {
        var channel_id = object.id;
        var name = object.name;
        if($.inArray(channel_id,alreadyInserted['feed']) > -1){
            toastr.error('Already in version!! how many times i need to tell you that');
            return;
        }
        var selected_element;
        if($("input[name=feed-row-element-count]:checked").val() == 'two'){
            selected_element = createFeedChannelElementHalf(object);
        }else{
            selected_element = createFeedChannelElementFull(object);
        }

        addElementInFeed(selected_element, 'channel');
        alreadyInserted['feed'].push(channel_id);

        $('.feed-channel-' + channel_id).attr("data-feedtype",'channel');
        updateFeedElementCount(channel_id,'channel','add');
        $('.feed-channel-' + channel_id).contextmenu({
            target: '#context-menu-feed-channel',
            onItem: feedChannelContextHandler
        });
    }



    function insertSelectedMainCollection(object,intent,version_id,fetched) {
        //console.log('fetched----->' + fetched);
        var old = true && fetched;
        if($.inArray(object.id,alreadyInserted['collection']) > -1){
            toastr.error('Already exists in version');
            return;
        }
        //console.log(old);
        alreadyInserted['collection'].push(object.id);
        var fetch = fetch || false;
        var selected_element = createMainCollectionElement(object,old);

        if(old){
          //  console.log('append');
            $('.maincollection-list').append(selected_element);
        }else {
            //console.log('prepend');
            $('.maincollection-list').prepend(selected_element);
        }

        if(intent == "edit"){
            //console.log('handler attached');
            $('.maincollection-' + object.id).not('.empty_element').contextmenu({
                target: '#context-menu-maincollection',
                onItem: mainCollectionContextHandler
            });
        }

        if(intent == 'edit'){
            //console.log('edit mode');
            mainCollectionInsertHandler(object,'write',version_id,old);
        } else {
            //console.log('display mode');
            mainCollectionInsertHandler(object,'read',version_id,fetched);
        }

    }


    /**
     * inserts selected collection to phone's feed view and removes from select box
     * @param  String   collection id of selected collection
     */
    function insertSelectedCollection(object) {
        var collection_id = object.id;
        var name = object.name;
        var selected_element;
        if($.inArray(collection_id,alreadyInserted['feed']) > -1){
            toastr.error('Already exists in version');
            return;
        }
        if($("input[name=feed-row-element-count]:checked").val() == 'two'){
                selected_element = createCollectionElementHalf(object);
        }else{
                selected_element = createCollectionElementFull(object);
        }
        alreadyInserted['feed'].push(collection_id);
        addElementInFeed(selected_element, 'collection');

        $('.collection-' + collection_id).attr('data-feedtype','collection');
        updateFeedElementCount(collection_id,'collection','add');

        $('.collection-' + collection_id).contextmenu({
            target: '#context-menu-collection',
            onItem: collectionContextHandler
        });
    }


    /**
     * inserts selected channel to phone view and removes from select box
     * @param  String   channel_id id of selected channel
     */
    function insertSelectedChannel(object) {
        var channel_id = object.id;
        var name = object.name;
        if($.inArray(channel_id,alreadyInserted['channel']) > -1){
            toastr.error('Already in version');
            return;
        }
        $('.channel-list').prepend(createChannelElement(object, 'write'));
        alreadyInserted['channel'].push(channel_id);

        $('.channel-' + channel_id).contextmenu({
            target: '#context-menu-channel',
            onItem: channelContextHandler
        });

        /* running unveil */


        insertedChannels[channel_id] = {};
        insertedChannels[channel_id]['content'] = [];
        insertedChannels[channel_id]['sponsor'] = {};
    }


    /**
     * inserts selected top content to top video list and removes from select box
     * @param  String   content_id   id of selected content
     */
    function insertSelectedTopContent(content_id) {
        var all_elements = getCurrentValues('version-top-content-list');
        /* construcing  html element */
        $('.top-videos-list').append(createTopContentElement(content_id, 'write'));

        /* running unveil */

        /* attaching context handler */
        $('.top-content-' + content_id).contextmenu({
            target: '#context-menu-top-content',
            onItem: topContentContextHandler
        });

        /* callling image unveil */

        var new_data = $.grep(all_elements, function(e) {
            if (e.id != content_id)
                return e;
        });
        /* constructing chosen box with removed channel entry*/
        $('#version-top-content-list').html(prepareOptionList(new_data)).prepend("<option></option>").trigger("chosen:updated");
    }

    /**
     * inserts selected sponsor to channel list and removes from select box
     * @param  String selectedSponsor  sponsor id
     */
    function insertSelectedSponsor(object) {
        var channel_id = channel_id || null;
        var sponsor_id = object.id;
        var name = object.name;

        if (channel_id == null)
            var current_sponsor_drawer = $('.phone-overlay').find('.active-top-bar-sponsor-field');
        else
            var current_sponsor_drawer = $('.' + channel_id + '-top-bar-sponsor-section');

        var channel_id = $(current_sponsor_drawer).attr('data-channel');
        /* construcing  html element */
        $(current_sponsor_drawer).find('.top-bar-sponsor-list').append(createSponsorElement(object,channel_id)).show();
        $(current_sponsor_drawer).find('.no-top-bar-sponsor').hide();
        $('.sponsor-' + sponsor_id).contextmenu({
            target: '#context-menu-sponsor',
            onItem: sponsorContextHandler
        });
        handlePreviousVersionSponsorMapping(sponsor_id,sponsor_id,'tbs');
        insertedChannels[channel_id]['sponsor']['top-bar-sponsor'] = sponsor_id;
        insertedChannels[channel_id]['sponsor']['top-bar-sponsor-name'] = name;
        /* constructing chosen box with removed channel entry*/

        $('.top-bar-sponsor-select-container').hide();
    }

    /**
     * inserts selected splash to channel list and removes from select box
     * @param  String selectedSponsor  sponsor id
     */
    function insertSelectedSplash(object) {
        var splash_id = object.id;
        var sponsor_id = object.sponsor_id;
        var name = object.name;
        var channel_id = channel_id || null;
        if (channel_id == null) {
            var current_sponsor_drawer = $('.phone-overlay').find('.active-splash-field');
            channel_id = $(current_sponsor_drawer).attr('data-channel');
        } else
            var current_sponsor_drawer = $('.' + channel_id + '-splash-section');

        /* construcing  html element */
        $(current_sponsor_drawer).find('.splash-list').show().append(createSplashElement(object,channel_id));
        $(current_sponsor_drawer).find('.no-sponsor-splash').hide();

        $('.splash-' + splash_id).contextmenu({
            target: '#context-menu-splash',
            onItem: splashContextHandler
        });

        /* creating entry for newly inserted channel */
        if (!('splash' in insertedChannels[channel_id]['sponsor'])) {
            insertedChannels[channel_id]['sponsor']['splash'] = [];
        }
        handlePreviousVersionSponsorMapping(splash_id,sponsor_id,'splash');
        insertedChannels[channel_id]['sponsor']['splash'].push({
            'splash_id': splash_id,
            'sponsor': sponsor_id,
            'name': name
        });
    }


    /**
     * inserts selected splash to channel list and removes from select box
     * @param  String selectedSponsor  sponsor id
     */
    function insertSelectedPreroll(object) {
        var preroll_id = object.id;
        var sponsor_id = object.sponsor_id;
        var name = object.name;
        var channel_id = channel_id || null;

        if (channel_id == null) {
            var current_sponsor_drawer = $('.phone-overlay').find('.active-preroll-field');
            channel_id = $(current_sponsor_drawer).attr('data-channel');
        } else
            var current_sponsor_drawer = $('.' + channel_id + '-preroll-section');
        /* construcing  html element */
        $(current_sponsor_drawer).find('.preroll-list').show().append(createPrerollElement(object,channel_id));
        $(current_sponsor_drawer).find('.no-sponsor-preroll').hide();

        $('.preroll-' + preroll_id).contextmenu({
            target: '#context-menu-preroll',
            onItem: prerollContextHandler
        });

        /* creating entry for newly inserted channel */
        if (!('preroll' in insertedChannels[channel_id]['sponsor'])) {
            insertedChannels[channel_id]['sponsor']['preroll'] = [];
        }
        handlePreviousVersionSponsorMapping(preroll_id,sponsor_id,'preroll');
        insertedChannels[channel_id]['sponsor']['preroll'].push({
            'preroll_id': preroll_id,
            'sponsor': sponsor_id,
            'name': name
        });
    }

    /**DEPRICATED
     * inserts selected image ad to channel list and removes from select box
     * @param  String image_ad_id  image ad  id
     */
    function insertSelectedImageAd(image_ad_id, channel_id) {
        var channel_id = channel_id || null;
        var all_elements = getCurrentValues('version-image-ad-list');
        /* fetching selected element from data source*/

        var element = sponsor_image_ad_map[image_ad_id]
        var sponsor = approved_sponsor_map[element['sponsor']];

        if (channel_id == null) {
            var current_sponsor_drawer = $('.phone-overlay').find('.active-image-ad-field');
            channel_id = $(current_sponsor_drawer).attr('data-channel');
        } else
            var current_sponsor_drawer = $('.' + channel_id + '-image-ad-section');

        /* construcing  html element */
        $(current_sponsor_drawer).find('.image-ad-list').show().append(createImageAdElement(channel_id, image_ad_id, 'write'));
        $(current_sponsor_drawer).find('.no-sponsor-image-ad').hide();

        $('.image-ad-' + image_ad_id).contextmenu({
            target: '#context-menu-image-ad',
            onItem: imageAdContextHandler
        });

        var new_data = $.grep(all_elements, function(e) {
            if (e.id != image_ad_id)
                return e;
        });

        /* creating entry for newly inserted channel */
        if (!('image_ads' in insertedChannels[channel_id]['sponsor'])) {
            insertedChannels[channel_id]['sponsor']['image_ads'] = [];
        }
        insertedChannels[channel_id]['sponsor']['image_ads'].push({
            'image_id': image_ad_id,
            'sponsor': element['sponsor']
        });
        /* constructing chosen box with removed channel entry*/
        $('#version-image-ad-list').html(prepareOptionList(new_data)).prepend("<option></option>").trigger("chosen:updated");

        //$('.preroll-select-container').hide();
    }


    /**
     * Makes phone simulator editable
     * @param  Object   version_data version data fetched from server
     * @param  String    version_mode  vertion target type [internet, box]
     * @return {[type]}              [description]
     */
    function makeSimulatorEditable(version_data, version_mode) {
        var channel_feed = version_data['channel-feed'];
        var home_feed = version_data['home-feed'];
        var collection_feed = version_data['collection-feed'];

        //Top video material - currently not tracked
        //var top_videos = 'top_videos' in version_data ? version_data['top-videos'] : [];

        /* adding loader */
        $('.version-tool-box').find('.submit-indicator').append($('.m-preloader').clone().addClass('preloader-copy').show());

        mainSortableInstance = Sortable.create($('.channel-list')[0], {
            group: "sorting",
            sort: true
        });

        mainFeedSortableInstance = Sortable.create($('.feed-list')[0], {
            group: "sorting",
            sort: true
        });

        mainCollectionSortableInstance = Sortable.create($('.maincollection-list')[0], {
            group: "sorting",
            sort: true
        });

        //currently not keeping track of top videos
        /*topVideoSortableInstance = Sortable.create($('.top-videos-list')[0], {
            group: "sorting",
            sort: true
        });*/

        /* hiding content and sponsor section */
        $('.content-preview-container.active-channel').find('.menu-down').click();
        $('.sponsor-preview-container.active-sponsor-field').find('.menu-right').click();
        $('.splash-preview-container.active-splash-field').find('.menu-down').click();
        $('.preroll-preview-container.active-preroll-field').find('.menu-down').click();
        $('.top-bar-sponsor-preview-container.active-top-bar-sponsor-field').find('.menu-right').click();
        $('.image-ad-preview-container.active-image-ad-field').find('.menu-down').click();

        /* clearing sortable instances */
        sortableInstances = [];
        /* creating sortable instanes of each content section */

        channel_feed.forEach(function(channel) {
            var id = channel['channel_id'];

            sortableInstances[id] = Sortable.create($('.' + id + '-content-section').find('.content-list')[0], {
                group: "sorting",
                sort: true
            });

            var splashDrawerClass = id + '-splash-section';
            var prerollDrawerClass = id + '-preroll-section';
            var imageAdDrawerClass = id + '-image-ad-section';


            $('.channel-' + id).click(function() {
                hideSelectContainers(['content','image']);
            });


            (function(channel_id) {
                $('.channel-' + channel_id).not('.empty_element').contextmenu({
                    target: '#context-menu-channel',
                    onItem: channelContextHandler
                });
            })(id)

            channel['content'].forEach(function(content_object) {
                if (content_object.type == 'content') {
                    $('.channel-content-' + content_object.id).not('.empty_element').contextmenu({
                        target: '#context-menu-channel-content',
                        onItem: channelContentContextHandler
                    });
                } else {
                    $('.channel-image-ad-' + content_object.id).not('.empty_element').contextmenu({
                        target: '#context-menu-channel-image-ad',
                        onItem: channelImageAdContextHandler
                    });
                }
            });

            var sponsor_drawer = $('.' + id + '-sponsor-section');
            sponsor_drawer.find('.no-sponsor-splash').hide();
            sponsor_drawer.find('.no-sponsor-preroll').hide();

            if (channel['sellable_entities'] != null && typeof channel['sellable_entities'] === 'object') {

                if ('splash' in channel['sellable_entities']) {
                    channel['sellable_entities']['splash'].forEach(function(splash) {
                        $('.splash-' + splash['splash_id']).not('.empty_element').contextmenu({
                            target: '#context-menu-splash',
                            onItem: splashContextHandler
                        });
                    });
                }

                if ('preroll' in channel['sellable_entities']) {
                    channel['sellable_entities']['preroll'].forEach(function(preroll) {
                        $('.preroll-' + preroll['preroll_id']).not('.empty_element').contextmenu({
                            target: '#context-menu-preroll',
                            onItem: prerollContextHandler
                        });
                    });
                }

                /*if ('image_ads' in channel_data['data']['sponsor']) {
                    channel_data['data']['sponsor']['image_ads'].forEach(function(image_ad) {
                        $('.image-ad-' + image_ad['image_ad_id']).contextmenu({
                            target: '#context-menu-image-ad',
                            onItem: imageAdContextHandler
                        });
                    });
                }*/

                if ('top-bar-sponsor' in channel['sellable_entities']) {
                    $('.sponsor-' + channel['sellable_entities']['top-bar-sponsor']).not('.empty_element').contextmenu({
                        target: '#context-menu-sponsor',
                        onItem: sponsorContextHandler
                    });
                }
            }

            $('.image-ad-selector-container').hide();
            hideSelectContainers();

            $('.channel-button-section-header').off().click(function() {
                $('#channel-preview-container').show().animate({
                    "top": "0px"
                }, function() {
                    hideSelectContainers(['channel']);
                });
            });
            $('.top-video-section-header').off().click(function() {
                $('#top-videos-preview-container').show().animate({
                    "top": "0px"
                }, function() {
                    hideSelectContainers(['top-content']);
                });
            });
            $('.feed-button-section-header').off().click(function() {
                $('#feed-preview-container').show().animate({
                    "top": "0px"
                }, function() {
                    hideSelectContainers(['feed-content','feed-image', 'feed-channel', 'collection','feed-row-element-count']);
                });
                $('.feed-element-count').show();
            });
            $('.maincollection-button-section-header').off().click(function() {
                $('#maincollection-preview-container').show().animate({
                    "top": "0px"
                }, function(){
                    hideSelectContainers(['maincollection']);
                });
            });



            $('.channel-menu-down').off().click(function() {
                $('#channel-preview-container').animate({
                    "top": "412px"
                }, function() {
                    hideSelectContainers();
                }).hide();
            });
            $('.top-videos-menu-down').off().click(function() {
                $('#top-videos-preview-container').animate({
                    "top": "412px"
                }, function() {
                    hideSelectContainers();
                }).hide();
            });
            $('.feed-menu-down').off().click(function() {
                $('#feed-preview-container').animate({
                    "top": "412px"
                }, function() {
                    hideSelectContainers();
                }).hide();
                $('.feed-element-count').hide();
            });
            $('.maincollection-menu-down').off().click(function() {
                $('#maincollection-preview-container').animate({
                    "top": "412px"
                }, function() {
                    hideSelectContainers();
                }).hide();
            });

            $('.channel-select-container').hide();


            /* initialize select */
            $('.version-type').find('select').val(version_mode).material_select();
            $('.version-type').find('label').show();
            $('.version-type').show();


            $('.channel-menu-down').off().click(function() {
                $('#channel-preview-container').animate({
                    "top": "412px"
                }, function() {
                    hideSelectContainers();
                }).hide();
            });

            $('.top-video-button-section-header').off().click(function() {
                $('#channel-preview-container').show().animate({
                    "top": "0px"
                }, function() {
                    hideSelectContainers(['channel']);
                });
            });

            checkIfFilled(current_row);

        });



        home_feed.forEach(function(row, i) {
            row.forEach(function(element) {
                var feed_element;
                if (element.type === 'content') {
                    $('.feed-content-' + element.id).not('.empty_element').contextmenu({
                        target: '#context-menu-feed-content',
                        onItem: feedContentContextHandler
                    });
                } else if (element.type === 'channel') {
                    $('.feed-channel-' + element.id).not('.empty_element').contextmenu({
                        target: '#context-menu-feed-channel',
                        onItem: feedChannelContextHandler
                    });
                } else if (element.type === 'image_ad') {
                    $('.feed-image-ad-' + element.id).not('.empty_element').contextmenu({
                        target: '#context-menu-feed-channel',
                        onItem: feedImageAdContextHandler
                    });
                } else {
                    $('.collection-' + element.id).not('.empty_element').contextmenu({
                        target: '#context-menu-collection',
                        onItem: collectionContextHandler
                    });
                }
                addElementInFeed(feed_element, element.type);
            });
        });

        $('.empty_element').contextmenu({
            target: '#context-menu-empty-element',
            onItem: emptyElementContextHandler
        });

        /* hiding edit button */
        $('.version-edit-button').hide();

        /* removing preloader */
        $('.version-tool-box').find('.submit-indicator').empty().hide();

        /*(function prerollchecker(){
            console.log(insertedChannels['pp-channel-76']['sponsor']['preroll']);
            setTimeout(prerollchecker,10000);
        })();*/
    }

    /**
     * create slug for each string
     * @param string content-name field
     * return string slug for passed string
     */
    function slugify(text)
    {
      return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
    }

    /**
     * fills modal for currently clicked content with preview
     * @param string content_id  id of clicked content
     */
     function displayContentInfoModal(content_id,type){
        if(type == 'image_ad' || type == 'channel'){
            toastr.info('did you just try to play a channel or image ad? hmmm');
            return;
        }
        var content_type;
        var link;
        if(content_id in cache){
            var element = cache[content_id];
            content_type = element['type'];
            link = element['local_link'];
            $('#version-content-info-modal').find('.content-id-preloader').html(element['id']);
            $('#version-content-info-modal').find('.media-title').html('Name: ' +  element['name']);
            $('#version-content-info-modal').find('.media-duration').html('Duration: ' + secondsToHms(element['duration']));
            $('#version-content-info-modal').find('.media-tags').html('Tags: ' + element['tags']);

            if (type == "audio") {
                $('#version-content-info-modal').find('.video-preview').hide();
                $('#version-content-info-modal').find('.audio-preview').show();
                $('#version-content-info-modal').find('#audio-player').attr('src', link).load();
                $('#version-content-info-modal').find('#audio-player').off().on('error', function() {
                    toastr.error('Unable to preview media. Probably some error occured ');
                    $('.modal-close').click();
                });
            } else {
                $('#version-content-info-modal').find('.audio-preview').hide();
                $('#version-content-info-modal').find('.video-preview').show();
                $('#version-content-info-modal').find('#video-player').attr('src', link).load();
                $('#version-content-info-modal').find('#video-player').off().on('error', function() {
                    toastr.error('Unable to preview media. Probably some error occured ');
                    $('.modal-close').click();
                });
            }
            $('.preview-version-content').click();
        } else {
            $.ajax({
                'url': BASE_URL + 'content/fetch/' +content_id,
                'method': 'GET'
            }).done(function(data){
                var element = data.data;
                content_type = element['type'];
                link = element['local_link'];
                $('#version-content-info-modal').find('.content-id-preloader').html(element['id']);
                $('#version-content-info-modal').find('.media-title').html('Name: ' +  element['name']);
                $('#version-content-info-modal').find('.media-duration').html('Duration: ' + secondsToHms(element['duration']));
                $('#version-content-info-modal').find('.media-tags').html('Tags: ' + element['tags']);

                if (type == "audio") {
                    $('#version-content-info-modal').find('.video-preview').hide();
                    $('#version-content-info-modal').find('.audio-preview').show();
                    $('#version-content-info-modal').find('#audio-player').attr('src', link).load();
                    $('#version-content-info-modal').find('#audio-player').off().on('error', function() {
                        toastr.error('Unable to preview media. Probably some error occured ');
                        $('.modal-close').click();
                    });
                } else {
                    $('#version-content-info-modal').find('.audio-preview').hide();
                    $('#version-content-info-modal').find('.video-preview').show();
                    $('#version-content-info-modal').find('#video-player').attr('src', link).load();
                    $('#version-content-info-modal').find('#video-player').off().on('error', function() {
                        toastr.error('Unable to preview media. Probably some error occured ');
                        $('.modal-close').click();
                    });
                }
                $('.preview-version-content').click();
            });
        }
     }


    /**
     * Renders previous content version data
     * @param  Object version_data  previous version data fetched from server
     * @param  String version_mode  mode of content version [internet, box]
     * @param  String  intent   intention for rendering previos version [display, edit]
     */
    function renderPreviousContentVersion(version_data, version_mode, intent,version_id) {

        elementsToFetch.channel = [];
        elementsToFetch.tag = [];
        elementsToFetch.content = [];
        elementsToFetch.sponsor = [];

        var intent = intent || 'display';
        var channel_feed = version_data['channel-feed'];
        var home_feed = version_data['home-feed'];
        var collection_feed = version_data['collection-feed'];
        /* TODO remove *
        //channel_feed = version_data;
        /*var top_videos = 'top_videos' in version_data ? version_data['top-videos'] : [];*/

        /* initialize select boxes */
        if(intent == 'display')
            initializeVersionMakerComponents(true);
        else
            initializeVersionMakerComponents();

        $('.channel-select-container').hide();
        /* populate inserted channels */

        /* container for DOM elements  */
        var channel_elements = [];
        var content_elements = [];
        var sponsor_elements = [];
        var splash_elements = [];
        var preroll_elements = [];
        var image_ad_elements = [];
        var maincollection_elements = [];
        var top_bar_sponsor_elements = [];
        var maincollection_content_elements=[];

        var content_drawers = [];
        var sponsor_drawers = [];
        var splash_drawers = [];
        var preroll_drawers = [];
        var image_ad_drawers = [];
        var maincollection_content_drawers = [];
        var top_bar_sponsor_drawers = [];

        var maincollection_list = [];
        var content_list = [];
        var channel_list = [];
        var sponsor_list = [];
        var splash_list = [];
        var preroll_list = [];
        var top_bar_sponsor_list = [];
        var image_ad_list = [];
        available_feed_channel.channels = [];
        /*alreadyInserted['collection'] = [];
        alreadyInserted['channel'] = [];
        alreadyInserted['feed'] = [];*/

        $('.version-type').find('select').val(version_mode).material_select();
        $('.version-type').find('label').show();
        $('.version-type').show();

        if(intent == 'display'){
            $('#version_description').attr('disabled','disabled');
        }

        collection_feed.forEach(function(collection) {
            if($.inArray(collection['collection_id'],elementsToFetch['tags']) < 0){
                elementsToFetch['tag'].push(collection['collection_id']);
            }
            var id = collection['collection_id'];
            var name = ('collection_name' in collection) ? collection['collection_name'] : 'collection';
            var collectionObject = {
                'id' : id,
                'name' : name
            };
            insertSelectedMainCollection(collectionObject,intent,version_id,collection['data']);
        });

        channel_feed.forEach(function(channel) {
            var id = channel['channel_id'];
            var name = ('channel_name' in channel ) ? channel['channel_name'] : 'channel';
            if($.inArray(id,elementsToFetch['channel']) < 0){
                elementsToFetch['channel'].push(id);
            }
            alreadyInserted['channel'].push(id);
            var channelObject = {
                'id': id,
                'name': name
            }
            insertedChannels[id] = {};
            insertedChannels[id]['content'] = channel['content'];
            insertedChannels[id]['sponsor'] = channel['sellable_entities'];

            if (insertedChannels[id]['sponsor'] === null || typeof insertedChannels[id]['sponsor'] !== 'object') {
                insertedChannels[id]['sponsor'] = {};
            }

            content_elements = [];
            sponsor_elements = [];

            var channel_element = createChannelElement(channelObject, 'read',true);
            if (intent == 'display') {
                var content_drawer = prepareContentDrawer(channelObject, 'read');
                var sponsor_drawer = prepareSponsorDrawer(channelObject, 'read');
                var splash_drawer = prepareSplashDrawer(channelObject, 'read');
                var preroll_drawer = preparePrerollDrawer(channelObject, 'read');
                var top_bar_sponsor_drawer = prepareTopBarSponsorDrawer(channelObject, 'read');
                var image_ad_drawer = prepareImageAdDrawer(channelObject, 'read');
            } else if (intent == 'edit') {
                var content_drawer = prepareContentDrawer(channelObject, 'write');
                var sponsor_drawer = prepareSponsorDrawer(channelObject, 'write');
                var splash_drawer = prepareSplashDrawer(channelObject, 'write');
                var preroll_drawer = preparePrerollDrawer(channelObject, 'write');
                var top_bar_sponsor_drawer = prepareTopBarSponsorDrawer(channelObject, 'write');
                var image_ad_drawer = prepareImageAdDrawer(channelObject, 'write');
            }

            channel['content'].forEach(function(content) {
                alreadyInserted['channel'].push(content.id);
                if (content.type == 'content'){
                    if($.inArray(content.id,elementsToFetch['content']) < 0){
                        elementsToFetch['content'].push(content.id);
                    }
                    var contentObject = {};
                    contentObject['id'] = content['id'];
                    contentObject['name'] = ('name' in content) ? content['name'] : 'content';
                    content_elements.push(createChannelContentElement(contentObject, id, 'read',true));
                } else {
                    if($.inArray(content.sponsor_id,elementsToFetch['sponsor']) < 0){
                        elementsToFetch['sponsor'].push(content.sponsor_id);
                    }
                    var contentObject = {};
                    contentObject['id'] = content.id;
                    contentObject['name'] = ('name' in content) ? content['name'] : 'Image-ad';
                    contentObject['sponsor_id'] = content.sponsor_id;
                    content_elements.push(createChannelImageAdElement(contentObject, id, 'read',true));
                    handlePreviousVersionSponsorMapping(content.id,content.sponsor_id,'image_ad');
                }

                //Top video content not maintained at this point
                //selected_content.add(content_object['id']+'*'+content_object['type']);
            });

            content_drawer.find('.content-list').append(content_elements);

            /* adding sponsor */
            if (channel['sellable_entities'] != null && typeof channel['sellable_entities'] === 'object') {
                /* appending sponsor to sponsor drawer */
                if ('splash' in channel['sellable_entities']) {
                    channel['sellable_entities']['splash'].forEach(function(splash) {
                        if($.inArray(splash.sponsor,elementsToFetch['sponsor']) < 0){
                            elementsToFetch['sponsor'].push(splash.sponsor);
                        }
                        var splash_name = ('name' in splash) ? splash['name'] : 'Splash';
                        var splashObject = {
                            'sponsor_id': splash['sponsor'],
                            'id': splash['splash_id'],
                            'name': splash_name
                        };
                        splash_drawer.find('.splash-list').append(createSplashElement(splashObject,id, true));
                        splash_list.push(splash['splash_id']);
                        handlePreviousVersionSponsorMapping(splash.splash_id,splash.sponsor,'splash');
                    });
                    splash_drawer.find('.no-sponsor-splash').hide();
                } else {
                    splash_drawer.find('.no-sponsor-splash').show();
                }
                if ('preroll' in channel['sellable_entities']) {
                    channel['sellable_entities']['preroll'].forEach(function(preroll) {
                        if($.inArray(preroll.sponsor,elementsToFetch['sponsor']) < 0){
                            elementsToFetch['sponsor'].push(preroll.sponsor);
                        }
                        var preroll_name = ('name' in preroll) ? preroll['name'] : 'Preroll';
                        var prerollObject = {
                            'sponsor_id': preroll['sponsor'],
                            'id': preroll['preroll_id'],
                            'name': preroll_name
                        };
                        preroll_drawer.find('.preroll-list').append(createPrerollElement(prerollObject, id, true));
                        preroll_list.push(preroll['preroll_id']);
                        handlePreviousVersionSponsorMapping(preroll.preroll_id,preroll.sponsor,'preroll');
                    });
                    sponsor_drawer.find('.no-sponsor-preroll').hide();
                } else {
                    sponsor_drawer.find('.no-sponsor-preroll').show();
                }
                if ('top-bar-sponsor' in channel['sellable_entities']) {
                    var id = channel['sellable_entities']['top-bar-sponsor'];
                    var name = ('top-bar-sponsor-name' in channel['sellable_entities']) ? channel['sellable_entities']['top-bar-sponsor-name'] : 'top-bar-sponsor';
                    var tbsObject = {
                        'id' : id,
                        'name' : name
                    }
                    if($.inArray(id,elementsToFetch['sponsor']) < 0){
                        elementsToFetch['sponsor'].push(id);
                    }
                    top_bar_sponsor_drawer.find('.top-bar-sponsor-list').append(createSponsorElement(tbsObject,id,true));
                    top_bar_sponsor_list.push(channel['sellable_entities']['top-bar-sponsor']);
                    top_bar_sponsor_drawer.find('.no-top-bar-sponsor').hide();
                    handlePreviousVersionSponsorMapping(id,id,'tbs');
                } else {
                    top_bar_sponsor_drawer.find('.no-top-bar-sponsor').show();
                }
            } else {
                splash_drawer.find('.no-sponsor-preroll').show();
                preroll_drawer.find('.no-sponsor-splash').show();
                top_bar_sponsor_drawer.find('.no-top-bar-sponsor').show();
            }

            channel_elements.push(channel_element);
            content_drawers.push(content_drawer);
            sponsor_drawers.push(sponsor_drawer);
            splash_drawers.push(splash_drawer);
            preroll_drawers.push(preroll_drawer);
            image_ad_drawers.push(image_ad_drawer);
            top_bar_sponsor_drawers.push(top_bar_sponsor_drawer);
        });


        /* appending elements to DOM */
        $('.channel-list').append(channel_elements);
        $('.phone-overlay').append([].concat(content_drawers, sponsor_drawers, splash_drawers, preroll_drawers, image_ad_drawers, top_bar_sponsor_drawers,maincollection_content_drawers));

        /* hiding  select containers */
        hideSelectContainers();

        /* displaying version edit button*/
        $('.version-edit-button').show();

        var feed_drawer;
        current_row = 0;
        max_current_row = 0;

        if(intent == 'edit'){
            var mode = 'write';
        }

        home_feed.forEach(function(row,i){
            current_row = i;
            max_current_row = current_row;
            var feed_element;
            var length = row.length;
            row.forEach(function(element) {
                var id = element.id;
                var name = ('name' in element) ? element['name'] : 'Fetching name';
                if(length == 1){
                    if (element.type === 'content') {
                        if($.inArray(element.id,elementsToFetch['content']) < 0){
                            elementsToFetch['content'].push(element.id);
                        }
                        var feedObject = {
                            'id': id,
                            'name': name
                        }
                        var all_elements = getCurrentValues('version-feed-content-image-list');
                        feed_element = createFeedContentElementFull(feedObject,true);

                        alreadyInserted['feed'].push(element.id);

                    } else if (element.type === 'channel') {
                        if($.inArray(element.id,elementsToFetch['channel']) < 0){
                            elementsToFetch['channel'].push(element.id);
                        }
                        var feedObject = {
                            'id': id,
                            'name': name
                        }
                        var all_elements = getCurrentValues('version-feed-channel-list');

                        feed_element = createFeedChannelElementFull(feedObject,true);

                        alreadyInserted['feed'].push(element.id);
                    }
                    else if(element.type === 'image_ad'){
                        if($.inArray(element.sponsor_id,elementsToFetch['sponsor']) < 0){
                            elementsToFetch['sponsor'].push(element.sponsor_id);
                        }
                        var feedObject = {
                            'id': id,
                            'name': name,
                            'sponsor_id': element.sponsor_id
                        }
                        var all_elements = getCurrentValues('version-feed-content-image-list');
                        feed_element = createFeedImageAdElementFull(feedObject,true);

                        alreadyInserted['feed'].push(element.id);
                    } else {
                        if($.inArray(element.id,elementsToFetch['tag']) < 0){
                            elementsToFetch['tag'].push(element.id);
                        }
                        var feedObject = {
                            'id': id,
                            'name': name
                        }
                        feed_element = createCollectionElementFull(feedObject,true);
                        var all_elements = getCurrentValues('version-collection-list');

                        alreadyInserted['feed'].push(element.id);
                    }
                    addElementInFeed(feed_element, element.type);
                }else{
                    if (element.type === 'content') {
                        if($.inArray(element.id,elementsToFetch['content']) < 0){
                            elementsToFetch['content'].push(element.id);
                        }
                        var feedObject = {
                            'id': id,
                            'name': name
                        }
                        var all_elements = getCurrentValues('version-feed-content-image-list');
                        feed_element = createFeedContentElementHalf(feedObject,true);

                        alreadyInserted['feed'].push(element.id);

                    } else if (element.type === 'channel') {
                        if($.inArray(element.id,elementsToFetch['channel']) < 0){
                            elementsToFetch['channel'].push(element.id);
                        }
                        var feedObject = {
                            'id': id,
                            'name': name
                        }
                        feed_element = createFeedChannelElementHalf(feedObject,true);

                        alreadyInserted['feed'].push(element.id);
                    }
                    else if(element.type === 'image_ad'){
                        if($.inArray(element.sponsor_id,elementsToFetch['sponsor']) < 0){
                            elementsToFetch['sponsor'].push(element.sponsor_id);
                        }
                        var feedObject = {
                            'id': id,
                            'name': name,
                            'sponsor_id': element.sponsor_id
                        }
                        feed_element = createFeedImageAdElementHalf(feedObject,true);
                        alreadyInserted['feed'].push(element.id);
                    } else {
                        if($.inArray(element.id,elementsToFetch['tag']) < 0){
                            elementsToFetch['tag'].push(element.id);
                        }
                        var feedObject = {
                            'id': id,
                            'name': name
                        }
                        feed_element = createCollectionElementHalf(feedObject,true);
                        var all_elements = getCurrentValues('version-collection-list');
                        alreadyInserted['feed'].push(element.id);
                    }
                    addElementInFeed(feed_element, element.type);
                }
            });
        });

        if ($('#feed-row-' + current_row).children().length == 0) {
            $('#feed-row-' + current_row).remove();
            current_row--;
            max_current_row = current_row;
        } else if($('#feed-row-' + current_row).children().length == 1) {
            $('.highlighted-row').removeClass('highlighted-row');
        }

        if(intent == 'display'){
            $('.content-image-overlay').click(function(){
                var parent = $(this).parent()[0];
                var id = $(parent).data('id').split('*')[0];
                var type = $(parent).data('id').split('*')[1];
                displayContentInfoModal(id,type);
            });

            $('.feed-content-preview-icon').click(function(){
                var parent = $(this).parent()[0];
                var id = $(parent).data('id').split('*')[0];
                var type = $(parent).data('id').split('*')[1];
                displayContentInfoModal(id,type);
            });
        }

        $('.wait-section').hide();
        $('.version-creator').show();
        fetchVersionRelatedData(intent);
    }

    function createBatchFetchRequest(type,type_list,start,end){
        var request = BASE_URL + type + '/batch_fetch';
        var ids = 'id='
        var feilds = 'field=';
        var idarray = [];

        if(type == 'content'){
            feilds += 'name,poster,poster_small,status,video_content_type';    
        }else if(type == 'tag'){
            feilds += 'name,poster,poster_small,status';
        } else if(type == 'channel'){
            feilds += 'name,icon,icon_small,status';
        } else if(type == 'sponsor'){
            feilds += 'name,logo,status&ad_type=all';
        }

        for(var i=start;i<=end;i++){
            var id = type_list[i];
            //console.log(id);
            var key = id.split('-')[2];
            idarray.push(key);
        };
        ids += idarray.join(',');
        return request + '?' + ids + '&' + feilds;
    }


    function fetchVersionRelatedData(intent){
        var tag_length = elementsToFetch['tag'].length;
        for(var i=0; i<tag_length;i+=100){
            (function fetchtags(){
                var start = i;
                var end = (i + 99 < tag_length ) ? (i + 99) : (tag_length - 1);
                $.ajaxq('version_related_data',{
                    'url': createBatchFetchRequest('tag',elementsToFetch['tag'],start,end),
                    'method': 'GET',
                    'success':function(data){
                        var batch_fetched = data.data;
                        for(var i = start; i<=end;i++){
                            var tag = elementsToFetch['tag'][i];
                            if(!(tag in batch_fetched)){
                                handleVersionPublish(tag,'collection','add',intent);
                                console.log(tag);
                                continue;
                            }
                            var element = batch_fetched[tag];
                            var name = element.name;
                            var poster = element.poster;
                            var poster_small = element.poster_small;
                            var status = element.status;
                            if(status != 'complete'){
                                handleVersionPublish(tag,'collection','add',intent);
                                console.log(tag);
                            }
                            $('.maincollection-' + tag).find('img').attr("data-original",poster).lazyload({effect: "fadeIn"});
                            $('.maincollection-' + tag).find('.maincollection-preview-name').html(name);
                            $('.maincollection-' + tag).attr('data-id', tag + '*' + name);
                            $('.' + tag + '-content-section').find('.maincollection-name-header').html(name);
                            $('.collection-' + tag).each(function(i){
                                $(this).attr('data-id',tag + '*collection*' + name);
                                $(this).attr('data-feedtype','collection');
                                updateFeedElementCount(tag,'collection','add');
                                if($(this).hasClass('feed-element-half')){
                                    $(this).find('img').attr("data-original",poster_small).lazyload({effect: "fadeIn"});
                                    $(this).find('.collection-preview-name').html(name);
                                }
                                else{
                                    $(this).find('img').attr("data-original",poster).lazyload({effect: "fadeIn"});
                                    $(this).find('.collection-preview-name').html(name);
                                }
                            });
                        };
                    }
                });
            })();
        }
        var channel_length = elementsToFetch['channel'].length;
        for(var i=0; i<channel_length;i+=100){
            (function fetchchannels(){
                var start = i;
                var end = (i + 99 < channel_length ) ? (i + 99) : (channel_length - 1);
                $.ajaxq('version_related_data',{
                    'url':createBatchFetchRequest('channel',elementsToFetch['channel'],start,end),
                    'method': 'GET',
                    'success': function(data){
                        var batch_fetched = data.data;
                        for(var i = start; i<=end;i++){
                            var channel = elementsToFetch['channel'][i];
                            if(!(channel in batch_fetched)){
                                handleVersionPublish(channel,'channel','add',intent);
                                console.log(channel);
                                continue;
                            }
                            var element = batch_fetched[channel];
                            var name = element.name;
                            var poster = element.icon;
                            var poster_small = element.icon_small;
                            var status = element.status;
                            if(status != 'complete'){
                                handleVersionPublish(channel,'channel','add',intent);
                                console.log(channel);
                            }
                            $('.channel-' + channel).find('img').attr("data-original",poster_small).lazyload({effect: "fadeIn"});
                            $('.channel-' + channel).find('.channel-preview-name').html(name);
                            $('.channel-' + channel).attr('data-id', channel + '*' + name);
                            $('.' + channel + '-content-section').find('.channel-name-header').html(name + ' content section');
                            $('.' + channel + '-sponsor-section').find('.sponsor-name-header').html(name + ' sponsor section');
                            $('.' + channel + '-preroll-section').find('.preroll-name-header').html(name + ' preroll section');
                            $('.' + channel + '-splash-section').find('.splash-name-header').html(name + ' splash section');
                            $('.maincollection-content-' + channel).find('img').attr("data-original",poster).lazyload({effect: "fadeIn"});
                            $('.maincollection-content-' + channel).find('.content-preview-name').html(name);
                            $('.maincollection-content-' + channel).attr('data-id',channel + '*channel*' + name);
                            $('.feed-channel-' + channel).each(function(){
                                $(this).attr('data-id',channel + '*channel*' + name);
                                $(this).attr('data-feedtype','channel');
                                updateFeedElementCount(channel,'channel','add');
                                if($(this).hasClass('feed-element-half')){
                                    $(this).find('img').attr("data-original",poster_small).lazyload({effect: "fadeIn"});
                                    $(this).find('.feed-channel-preview-name').html(name);
                                }
                                else{
                                    $(this).find('img').attr("data-original",poster).lazyload({effect: "fadeIn"});
                                    $(this).find('.feed-channel-preview-name').html(name);
                                }
                            });
                        };
                    }
                });
            })();
        }
        var content_length = elementsToFetch['content'].length
        for(var i=0; i<content_length;i+=100){
            (function fetchcontent(){
                var start = i;
                var end = (i + 99 < content_length ) ? (i + 99) : (content_length - 1);
                $.ajaxq('version_related_data',{
                    'url':createBatchFetchRequest('content',elementsToFetch['content'],start,end),
                    'method': 'GET',
                    'success':function(data){
                        var batch_fetched = data.data;
                        for(var i = start; i<=end;i++){
                            var content = elementsToFetch['content'][i];
                            if(!(content in batch_fetched)){
                                handleVersionPublish(content,'content','add',intent);
                                console.log(content);
                                continue;
                            }
                            var element = batch_fetched[content];
                            var name = element.name;
                            var poster = element.poster;
                            var poster_small = element.poster_small;
                            var status = element.status;
                            var video_type = (element.video_content_type == 'movie') ? 'movie' : 'video';
                            if(status != 'complete'){
                                handleVersionPublish(content,'content','add',intent);
                                console.log(content);
                            }
                            $('.channel-content-' + content).find('img').attr("data-original",poster).lazyload({effect: "fadeIn"});
                            $('.channel-content-' + content).find('.content-preview-name').html(name);
                            $('.channel-content-' + content).attr('data-id',content + '*content*' + name);
                            $('.maincollection-content-' + content).find('img').attr("data-original",poster).lazyload({effect: "fadeIn"});
                            $('.maincollection-content-' + content).find('.content-preview-name').html(name);
                            $('.maincollection-content-' + content).attr('data-id',content + '*content*' + name);
                            $('.feed-content-' + content).each(function(i){
                                $(this).attr('data-id',content + '*content*' + name);
                                $(this).attr('data-feedtype',video_type);
                                updateFeedElementCount(content,video_type,'add');
                                if($(this).hasClass('feed-element-half')){
                                    $(this).find('img').attr("data-original",poster_small).lazyload({effect: "fadeIn"});
                                    $(this).find('.feed-content-preview-name').html(name);
                                }
                                else{
                                    $(this).find('img').attr("data-original",poster).lazyload({effect: "fadeIn"});
                                    $(this).find('.feed-content-preview-name').html(name);
                                }
                            });
                        };
                    }
                });
            })();
        }
        var sponsor_length = elementsToFetch['sponsor'].length;
        for(var i=0; i<sponsor_length;i+=100){
            (function fetchSponsor(){
                var start = i;
                var end = (i + 99 < sponsor_length ) ? (i + 99) : (sponsor_length - 1);
                $.ajaxq('version_related_data',{
                    'url':createBatchFetchRequest('sponsor',elementsToFetch['sponsor'],start,end),
                    'method': 'GET',
                    'success':function(data){
                        var batch_fetched = data.data
                        for(var i = start; i<=end;i++){
                            var sponsor = elementsToFetch['sponsor'][i];
                            if(!(sponsor in batch_fetched)){
                                handleVersionPublish(sponsor,'sponsor','add',intent);
                                console.log(sponsor);
                                continue;
                            }
                            var element = batch_fetched[sponsor];
                            var name = element.name;
                            var logo = element.logo;
                            var status = element.status;
                            if(status != 'complete'){
                                handleVersionPublish(sponsor,'sponsor','add',intent);
                                console.log(sponsor);
                            }
                            $('.sponsor-' + sponsor).find('img').attr('src',logo);
                            $('.sponsor-' + sponsor).find('.sponsor-preview-name').html(name);
                            $('.sponsor-' + sponsor).attr('data-id',sponsor + '*' + name);
                            for(var key in element.image_ad){
                                var link = element['image_ad'][key];
                                $('.channel-image-ad-' + key).find('img').attr("data-original",link).lazyload({effect: "fadeIn"});
                                $('.channel-image-ad-' + key).find('.image-ad-preview-name').html(name);
                                $('.channel-image-ad-' + key).attr('data-id',key + '*image_ad*' + sponsor + '*' + name);
                                $('.feed-image-ad-' + key).find('img').attr("data-original",link).lazyload({effect: "fadeIn"});
                                $('.feed-image-ad-' + key).find('.image-ad-preview-name').html(name);
                                $('.feed-image-ad-' + key).attr('data-id',key + '*image_ad*' + sponsor + '*' + name);
                            }
                            for(var key in element.preroll){
                                //var link = element['preroll'][key];
                                $('.preroll-' + key).find('img').attr('src',logo);
                                $('.preroll-' + key).find('.sponsor-preview-name').html(name);
                                $('.preroll-' + key).attr('data-id',key + '*' + name);
                            }
                            for(var key in element.splash){
                                $('.splash-' + key).find('img').attr('src',logo);
                                $('.splash-' + key).find('.sponsor-preview-name').html(name);
                                $('.splash-' + key).attr('data-id',key + '*' + name);
                            }
                        };
                    }
                });
            })();
        }
    }


    /**
     * Prepares and returns version data from sortable instances
     * @return Object  Object with channel,content and sponsor mappings
     */
    function getSimulatorData() {
        var channel_order = mainSortableInstance.toArray();
        //var top_videos = topVideoSortableInstance.toArray();
        var sortable_row_array = mainFeedSortableInstance.toArray();
        var collection_order = mainCollectionSortableInstance.toArray();
        var feedInstanceObjects = [];


        var collection_data = [];

        collection_order.forEach(function(collection) {
            var id = collection.split('*')[0];
            var name = collection.split('*')[1];
            var collection_object = {};
            var content_order = []
            collection_object['collection_id'] = id;
            collection_object['collection_name'] = name;
            var content_elements = mainCollectionSortableInstanceArrays[id].toArray();
            content_elements.forEach(function(id_type_name){
                var obj = {};
                obj['id'] = id_type_name.split('*')[0];
                obj['type'] = id_type_name.split('*')[1];
                obj['name'] = id_type_name.split('*')[2];
                content_order.push(obj);
            });
            collection_object['data'] = content_order;
            collection_data.push(collection_object);
        })


        sortable_row_array.forEach(function(id) {

            var row_array = [];
            $('#' + id).children().each(function(index, child) {
                var element_object = {};
                var data = $(this).data('id');
                var id = data.split('*')[0];
                var type = data.split('*')[1];
                element_object['id'] = id;
                element_object['type'] = type;
                if(type == 'image_ad'){
                    element_object['sponsor_id'] = data.split('*')[2];
                    element_object['name'] = data.split('*')[3];
                } else {
                    element_object['name'] = data.split('*')[2];
                }
                row_array.push(element_object);
            });

            if (row_array.length)
                feedInstanceObjects.push(row_array);
        });



        /* updating ordering in content sections */
        for (var channel_id in insertedChannels) {
            if (channel_id in sortableInstances) {
                insertedChannels[channel_id]['content'] = sortableInstances[channel_id].toArray();
            }
        }

        if (!channel_order.length && !collection_order.length && !sortable_row_array.length) {
            toastr.error('You need to add atleast 1 channel , Feed and Collection to update version');
            return {
                'error': 'No channel added'
            };
        }


        for (var ch_id in insertedChannels) {
            var contents = arrayToObjectList(insertedChannels[ch_id]['content']);
            insertedChannels[ch_id]['content'] = contents;
        }

        var ch_data = [];
        var insertedChannels_copy = JSON.parse(JSON.stringify(insertedChannels));
        channel_order.forEach(function(cid_name) {
            var cid = cid_name.split('*')[0];
            var name = cid_name.split('*')[1];
            var temp_obj = insertedChannels_copy[cid];
            temp_obj['sellable_entities'] = JSON.parse(JSON.stringify(temp_obj['sponsor']));
            delete temp_obj['sponsor'];
            temp_obj['channel_id'] = cid;
            temp_obj['channel_name'] = name;
            ch_data.push(temp_obj);
        });
        insertedChannels_copy = {};

        //top videos will not be sent in this version
        return {
            'channel-feed': ch_data,
            'home-feed': feedInstanceObjects,
            'collection-feed': collection_data
        };
    }


    function arrayToObjectList(content_array){
        var object_array = [];

        content_array.forEach(function(item) {
            var i = item.toString();
            var obj = {};
            obj['id'] = i.split('*')[0];
            obj['type'] = i.split('*')[1];
            if( obj['type'] == 'image_ad'){
                obj['sponsor_id'] = i.split('*')[2];
                obj['name'] = i.split('*')[3];
            } else {
                obj['name'] = i.split('*')[2];
            }
            object_array.push(obj);
        });

        return object_array;
    }

    function sponsorAddVideoObjectHandler(sponsorVideoObjects, objectType) {
        for (var ob in sponsorVideoObjects) {
            if (sponsorVideoObjects[ob] == null) {
                toastr.error('use alredy added element');
                return;
            }
        }

        var objects = Object.keys(sponsorVideoObjects).length;

        $('#' + objectType + '-add-modal-template').clone().attr('id', objectType + '-add-modal-' + (objects)).insertAfter('#' + objectType + '-add-modal');
        $('#' + objectType + '-add-modal-' + (objects)).html(compiled_video_template.render({
            title: 'Add ' + objectType
        }));

        sponsorVideoObjects[objectType + '-' + objects] = null;

        (function(object_no) {
            prepareVideoUploaderModal('#' + objectType + '-add-modal-' + object_no, function(file) {
                sponsorVideoObjects[objectType + '-' + object_no] = file;
                $('.sponsor-' + objectType + '-name-' + object_no).html(file.name).show();
                $('.new-sponsor-' + objectType + '-' + object_no).html('Edit ' + objectType);
            }, function() {
                sponsorVideoObjects[objectType + '-' + object_no] = null;
                $('.sponsor-' + objectType + '-name-' + object_no).html('').hide();
                $('.new-sponsor-' + objectType + '-' + object_no).html('Upload ' + objectType);
            });
        })(objects);

        var new_object_button = $('.sponsor-' + objectType + '-item.base-template').clone().removeClass('base-template');
        new_object_button.find('.new-sponsor-' + objectType).attr('href', '#' + objectType + '-add-modal-' + (objects)).removeClass('new-sponsor-' + objectType)
            .addClass('new-sponsor-' + objectType + '-' + (objects)).html('Upload ' + objectType);
        new_object_button.find('.sponsor-' + objectType + '-name').html('').removeClass('sponsor-' + objectType + '-name').addClass('sponsor-' + objectType + '-name-' + (objects))
        new_object_button.insertBefore('.' + objectType + '-add-button');
        $('.modal-trigger').off().leanModal();
        $(new_object_button).find('.modal-trigger').click();
    }

    function prepareFileExplorer(file_list, bucket) {
        var dom_contents = [];
        $('.file-explorer-content').empty()
        file_list.forEach(function(file) {
            var file_object = $('.file-object').clone().removeClass('prototype hidden');
            var file_name = file['name'].split('.')[0];
            $(file_object).find('.filename').html(file_name).attr('data-hint', file_name);
            $(file_object).find('.file-play').attr('data-link', file['link']).attr('data-type', file['type']).attr('data-hint', 'preview file');
            $(file_object).find('.file-add').attr('data-name', file['name']).attr('data-hint', 'chooose file');
            if (file['type'] == 'video') {
                $(file_object).find('.file-icon').addClass('glyphicon-film');
            } else {
                $(file_object).find('.file-icon').addClass('glyphicon-music');
            }
            dom_contents.push(file_object);
        });
        $('.file-explorer-wait-section').hide();
        $('.main-explorer-container').show();
        console.log(dom_contents);
        $('.file-explorer-content').append(dom_contents);
    }

    function prepareRawFileExplorer(file_list,type,alreadyInserted) {
        var dom_contents = [];
        $('.file-explorer-content').empty()
        file_list.forEach(function(file) {
            if($.inArray(file['id'],alreadyInserted) > -1){
                return;
            }
            var file_object = $('.' + type +'-object').clone().removeClass('prototype hidden');
            var file_name = file['name'];
            $(file_object).find('.filename').html(file_name).attr('data-hint', file_name);
            $(file_object).find('.file-play').attr('data-link', file['link']).attr('data-name', file['name']).attr('data-hint', 'preview file');
            $(file_object).find('.' + type + '-add').attr('data-name', file['name']).attr('data-id', file['id']).attr('data-type', file['type']).attr('data-link', file['link']).attr('data-hint', 'chooose file');
            if (file['type'] == 'video') {
                $(file_object).find('.file-icon').addClass('glyphicon-film');
            } else {
                $(file_object).find('.file-icon').addClass('glyphicon-music');
            }
            dom_contents.push(file_object);
        });
        $('.file-explorer-wait-section').hide();
        $('.main-explorer-container').show();
        $('.file-explorer-content').append(dom_contents);
    }


    $(document).ready(function() {

        /* initializing modals */

        /* display nanda modal */
        /*$('.nanda-modal-trigger').click();*/
        //offline config
        Offline.options = {
            checkOnLoad: false,
            interceptRequests: true,
            reconnect: {
                initialDelay: 3
            },
            requests: true,
            game: false,
            checks: {xhr: {url: '/version/network/live'}}
        };


        //hack to clear offline request on network up
        Offline.on('up', function() {
          Offline.requests.clear();
        })
        /* Setting toastr config */
        toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "5500",
                "hideDuration": "4000",
                "timeOut": 2000,
                "extendedTimeOut": 1000,
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            /* precompiling image upload modal */
        /*var modal_template = $('#image-uploader-template').html();
        compiled_image_template = Hogan.compile(modal_template);*/
        compiled_image_template = templates['image-uploader-template'];

        /*var video_template = $('#video-uploader-template').html();
        compiled_video_template = Hogan.compile(video_template);*/
        compiled_video_template = templates['video-uploader-template'];

        function renderHome() {
            var template = $('#home-page-content');
            /* not compiling since its raw html */
            $('.page-content').html(template.html());
        }

        page('/publish/:id',function(ctx,next){
            $('.version-publish-modal').find('.p-modal-content').hide();
            $('.version-publish-wait-section').html($('.spin').clone().addClass('spin-clone')).show();
            var version_id = ctx.params.id;
            var id = version_id.split('-')[2];
            $.ajax({
                url: BASE_URL + 'version/publish/pp-version-' + id,
                method: 'POST',
                data: {
                    'user_name': current_active_user['name'],
                    'user_email' : current_active_user['email']
                }
            }).done(function(data){
                $('.version-publish-wait-section').empty().hide();
                $('.lean-overlay').remove();
                toastr.success("Version successully published on Production");
                toastr.info(data.message);
                page('/version/' + version_id);
            }).fail(function(data){
                $('.version-publish-modal').find('.p-modal-content').show();
                $('.version-publish-wait-section').empty().hide();
                console.log(data);
                showError(data);
            });
        });

        page('/test-publish/:id',function(ctx,next){
            $('.version-publish-modal').find('.p-modal-content').hide();
            $('.version-publish-wait-section').html($('.spin').clone().addClass('spin-clone')).show();
            var version_id = ctx.params.id;
            var id = version_id.split('-')[2];
            $.ajax({
                url: BASE_URL + 'version/test-publish/pp-version-' + id,
                method: 'POST',
                data: {
                    'user_name': current_active_user['name'],
                    'user_email' : current_active_user['email']
                }
            }).done(function(data){
                $('.version-publish-wait-section').empty().hide();
                $('.lean-overlay').remove();
                toastr.success("Version successully published on Test");
                toastr.info(data.message);
                page('/version/' + version_id);
            }).fail(function(data){
                $('.version-publish-modal').find('.p-modal-content').show();
                $('.version-publish-wait-section').empty().hide();
                console.log(data);
                showError(data);
            });
        });

        page('/content/new', function(ctx, next) {
            var alreadyInsertedRawContent = [];
            var contentPosterFileName = null;
            var contentCoverFileName = null;
            tag_name_list = [];
            var contentPosterSmallFileName =null;
            var trailer_items = {};
            var trailers_stored = [];
            var raw_media_id = null;
            var raw_media_url = null;
            var selected_provider = 'pp-provider-1';
            content_cover_data_url = null;
            content_poster_data_url = null;
            content_poster_small_data_url = null;

            /*var template = $('#new-content-form').html();
            var compiled_template = Hogan.compile(template);*/
            var compiled_template = templates['new-content-form'];
            $('.page-content').html(compiled_template.render());
            $('#media-preview-modal').html($('#media-preview-template').html());
            $('#file-explorer-modal').html($('#file-explorer').html());
            $('#file-explorer-modal').find('.wait-text').html('Fetching raw content list');


            $('#file-explorer-modal').find('.spinner-section').append($('.spin').clone().addClass('spin-clone').show());

            /* rendering image upload template */
            $('#cover-add-modal').html(compiled_image_template.render({
                title: 'Add Cover'
            }));
            $('#poster-add-modal').html(compiled_image_template.render({
                title: 'Add poster'
            }));
            $('#poster-small-add-modal').html(compiled_image_template.render({
                title: 'Add Icon'
            }));
            $('input.length-field').characterCounter();
            $('.modal-trigger').off().leanModal();
            /* initialize js components */
            $('#content-type').material_select();
            $('#content-video-type').material_select();

            $('#content-type').on('change',function(){
                if($(this).val() == 'video' ){
                    $('.content-video-type-wrapper').show('slow');
                    $('.trailer-container').show('slow')
                } else {
                    $('.content-video-type-wrapper').hide('slow');
                }
            });

            $('.page-content').find('.wait-section').empty().append($('.spin').clone().addClass('spin-clone').show());
            $('.page-content').find('.wait-section').show();
            //$('.page-content').append($('.retry-prototype').clone().addClass('content-fetch-retry'));

            $('#content-premium').on('change',function(){
                if($('#content-premium').is(':checked')){
                    $('.premium-requirements').show('slow');
                } else {
                    $('.premium-requirements').hide('slow');
                }
            });

            //updates slug for typed name
            $('#content-name').on('input',function(){
                var name = $(this).val();
                var slug = slugify(name);
                if(name != ''){
                    $('.slug-available').show();
                    $('.generated-slug').text(slug);
                } else {
                    $('.slug-available').hide();
                }
            });
            prepareImageUploaderModal('#cover-add-modal', function(poster_src, filename) {
                contentCoverFileName = filename;
                $('.cover-thumbnail').attr('src', poster_src);
                content_cover_data_url = poster_src;
                $('.new-content-cover-thumbnail-container').show();
                $('.content-cover-control').html('Edit cover');
            }, function() {
                contentCoverFileName = null;
                content_cover_data_url = null;
                $('.cover-thumbnail').attr('src', '');
                $('.new-content-cover-thumbnail-container').hide();
                $('.content-cover-control').html('Add cover');
            }, contentCoverDimensions);

            prepareImageUploaderModal('#poster-add-modal', function(poster_src, filename) {
                contentPosterFileName = filename;
                $('.poster-thumbnail').attr('src', poster_src);
                content_poster_data_url = poster_src;
                $('.new-content-poster-thumbnail-container').show();
                $('.content-poster-control').html('Edit poster');
            }, function() {
                contentPosterFileName = null;
                content_poster_data_url = null;
                $('.poster-thumbnail').attr('src', '');
                $('.new-content-poster-thumbnail-container').hide();
                $('.content-poster-control').html('Add poster');
            }, contentPosterDimensions);

            prepareImageUploaderModal('#poster-small-add-modal', function(poster_src, filename) {
                contentPosterSmallFileName = filename;
                $('.poster-small-thumbnail').attr('src', poster_src);
                content_poster_small_data_url = poster_src;
                $('.new-content-poster-small-thumbnail-container').show();
                $('.content-poster-small-control').html('Edit icon');
            }, function() {
                contentPosterSmallFileName = null;
                content_poster_small_data_url = null;
                $('.poster-small-thumbnail').attr('src', '');
                $('.new-content-poster-small-thumbnail-container').hide();
                $('.content-poster-small-control').html('Add icon');
            }, contentIconDimensions);


            $('.modal-close').click(function(){
                $('.active-trailer').removeClass('active-trailer');
            })

            trailers_stored.push('');

            $(document).off('click', '.file-add-trigger , .trailer-add-trigger');
            $(document).on('click','.file-add-trigger , .trailer-add-trigger',function() {
                var content_type = $('#content-type').val();
                if(content_type == null){
                    toastr.error('Please select content type first');
                    return;
                }
                var type = 'file'
                if($(this).hasClass('trailer-add-trigger')){
                    var parent = $(this).parent();
                    $(parent).addClass('active-trailer');
                    type = 'trailer';
                }
                console.log(type);
                $('.file-explorer-wait-section').show();
                $('.main-explorer-container').hide();
                $('.file-explorer-preview').hide().find('.audio-preview').hide();
                $('.file-explorer-preview').find('.video-preview').hide();
                $('.s3-explorer').click();
                //console.log(BASE_URL + 'util/get-raw-contents/'+content_type);
                $.ajax({
                    url: BASE_URL + 'util/get-raw-contents/'+content_type,
                    method: 'GET',
                }).done(function(result) {
                    if (!result['file-list'].length) {
                        $('.modal-close').click();
                        toastr.error('No content available in S3 bucket');
                        return;
                    }
                    prepareRawFileExplorer(result['file-list'],type,alreadyInsertedRawContent);
                }).fail(function(data) {
                    $('.modal-close').click();
                    toastr.error('Unable to fetch data from S3 bucket. Please try again later');
                })
            });

            $(document).off('click', '.file-play');
            $(document).on('click', '.file-play', function() {
                var type = $(this).attr('data-type');
                var link = $(this).attr('data-link');
                console.log('playing file');
                if (type == 'audio') {
                    $('.file-explorer-preview').find('.audio-preview').show();
                    var audio_player = $('.file-explorer-preview').find('audio');
                    $(audio_player).show().attr('src', link).load();
                    $('.file-explorer-preview').show();
                } else {
                    $('.file-explorer-preview').find('.video-preview').show();
                    var video_player = $('.file-explorer-preview').find('video');
                    $(video_player).show().attr('src', link).load();
                    $('.file-explorer-preview').show();
                }
                $('#file-explorer-modal').scrollTop(0);
            });
            $(document).off('click', '.file-preview-close');
            $(document).on('click', '.file-preview-close', function() {
                $('.file-explorer-preview').find('audio').attr('src', '');
                $('.file-explorer-preview').find('video').attr('src', '');
                $('.file-explorer-preview').hide();
            });

            $(document).off('click', '.file-add');
            $(document).on('click', '.file-add', function() {
                var name = $(this).attr('data-name');
                raw_media_id = $(this).attr('data-id');
                raw_media_url = $(this).attr('data-link');
                var media_type = $(this).attr('data-type');
                $('.media-name').html(name);
                $('.media-preview').attr('data-link', raw_media_url);
                $('.media-preview').attr('data-type', media_type);

                $('.file-add-trigger').hide();
                $('.media-toolbar').show();
                $('.modal-close').click();
                if($.inArray(raw_media_id,alreadyInsertedRawContent) < 0){
                    alreadyInsertedRawContent.push(raw_media_id);
                }
            });

            $(document).off('click', '.trailer-add');
            $(document).on('click', '.trailer-add', function() {
                var name = $(this).attr('data-name');
                var trailer_id = $(this).attr('data-id');
                var trailer_link = $(this).attr('data-link');
                var media_type = $(this).attr('data-type');
                $('.active-trailer').find('.trailer-name').html(name);
                $('.active-trailer').find('.trailer-preview').attr('data-link', trailer_link);
                $('.active-trailer').find('.trailer-preview').attr('data-type', media_type);
                //console.log(trailer_id);
                var index = $('.active-trailer').attr('id').split('-')[3];
                //console.log(index);
                trailer_items['trailer-' + index] = trailer_id;
                //console.log(trailer_items);
                trailers_stored[index] = trailer_id;
                //console.log(trailers_stored);
                $('.active-trailer').find('.trailer-add-trigger').hide();
                $('.active-trailer').find('.trailer-toolbar').show();
                $('.modal-close').click();
                $('.active-trailer').removeClass('active-trailer');
                if($.inArray(trailer_id,alreadyInsertedRawContent) < 0){
                    alreadyInsertedRawContent.push(trailer_id);
                }
            });

            $(document).off('click', '.trailer-remove');
            $(document).on('click', '.trailer-remove', function() {
                var container = $(this).parent().parent();
                //console.log(container);
                var id = $(container).attr('id');
                //console.log(id);
                var index = id.split('-')[3];
                //console.log('remove-index' + index);
                var raw_id = trailer_items['trailer-' + index];
                delete trailer_items['trailer-' + index];
                trailers_stored[index] = '';
                $(container).find('.trailer-add-trigger').show();
                $(container).find('.trailer-toolbar').hide();
                var arrayi = alreadyInsertedRawContent.indexOf(raw_id);
                if(arrayi > -1){
                    alreadyInsertedRawContent.splice(arrayi,1);
                }
            });

            $(document).off('click', '.media-remove');
            $(document).on('click', '.media-remove', function() {
                raw_media_url = null;
                raw_media_id = null;
                $('.file-add-trigger').show();
                $('.media-toolbar').hide();
                var arrayi = alreadyInsertedRawContent.indexOf(raw_media_id);
                if(arrayi > -1){
                    alreadyInsertedRawContent.splice(arrayi,1);
                }
            });

            $(document).off('click', '.media-preview , .trailer-preview');
            $(document).on('click', '.media-preview , .trailer-preview', function() {
                var name = $(this).data('name');
                var link = $(this).data('link');
                var type = $(this).data('type');
                $('#media-preview-modal').find('.media-title').html(name);
                var link_fragments = link.split('.');
                var ext = link_fragments[link_fragments.length - 1]
                if (type== 'audio') {
                    $('#media-preview-modal').find('.video-preview').hide();
                    $('#media-preview-modal').find('.audio-preview').show();
                    $('#media-preview-modal').find('#audio-player').attr('src', link).load();
                    $('#media-preview-modal').find('#audio-player').off().on('error', function() {
                        toastr.error('Unable to preview media. Probably some error occured ');
                        $('.modal-close').click();
                    })
                } else {
                    $('#media-preview-modal').find('.audio-preview').hide();
                    $('#media-preview-modal').find('.video-preview').show();
                    $('#media-preview-modal').find('#video-player').attr('src', link).load();
                    $('#media-preview-modal').find('#video-player').off().on('error', function() {
                        toastr.error('Unable to preview media. Probably some error occured ');
                        $('.modal-close').click();
                    });
                }
                $('.media-preview-trigger').click();
            });

            $('.trailer-add-button').click(function(){
                for(var i=0;i<trailers_stored.length;i++ ){
                    if(trailers_stored[i] == ''){
                        toastr.error('use already avialable element');
                        return;
                    }
                }
                var new_trailer_button = $('.content-trailer-item.base-template').clone();
                $(new_trailer_button).insertBefore('.trailer-add-button');
                $(new_trailer_button).removeClass('base-template')
                $(new_trailer_button).find('.trailer-toolbar').hide();
                $(new_trailer_button).find('.trailer-add-trigger').show();
                var trailer_index = trailers_stored.length;
                $(new_trailer_button).attr('id','content-trailer-item-' + trailer_index);
                trailers_stored.push('');
            });
            /* binding validator class */
            var new_content_form_validator = new Validator(content_validator_seed, '.new-content-form-submit', function(errors, warnings, cleanCallback) {
                if (Object.keys(errors).length > 0) {
                    toastr.error('Please correct the errors in form');
                    cleanCallback();
                } else {
                    /* creating form data to post data to server */
                    var fd = new FormData();
                    fd.append('name', $('#content-name').val().trim());
                    fd.append('slug_url',slugify($('#content-name').val()));
                    var language = $('#content-language').val().trim();
                    if (language != null && language != "") {
                        fd.append('language', language);
                    }

                    var duration = hmsToSeconds($('#content-duration').val());
                    if (duration != null && duration != "") {
                        fd.append('duration', duration);
                    }

                    var seo = $('#content-seo').val().trim();
                    if (seo != null && seo != "") {
                        fd.append('seo', seo);
                    }

                    fd.append('trailers',JSON.stringify(trailer_items));

                    var type = $('#content-type').val();
                    if (type != null && type != "") {
                        fd.append('type', type);
                        console.log('type appended');
                    } else {
                        console.log('type not appended');
                        toastr.error("I think content should have some type");
                        cleanCallback();
                        return;
                    }

                    if (raw_media_id != null && raw_media_id != "") {
                        fd.append('raw_content_id', raw_media_id);
                    }

                    var content_genre = $('#content-genre').val().trim();
                    if (content_genre != null && content_genre != "") {
                        fd.append('genre', JSON.stringify(content_genre.split(',')));
                    }

                    if($('#content-tag-list').val() == ""){
                        fd.append('tags','[]');
                    }
                    else{
                        var tag_string = $('#content-tag-list').val().split(',');
                        var tag_array = [];
                        tag_string.forEach(function(name){
                            if($.inArray(name,tag_name_list) < 0){
                                var trimed = name.trim();
                                tag_array.push({
                                    type: "new",
                                    name: trimed
                                });
                            }
                            else {
                                tag_array.push({
                                    type: "existing",
                                    name: name
                                });
                            }
                        });
                        fd.append('tags',JSON.stringify(tag_array));
                    }

                    fd.append('provider_id',selected_provider);

                    var specifics = {};
                    var starcast = $('#content-star-cast').val().trim();
                    if (starcast != null && starcast != "") {
                        specifics['cast'] = starcast.split(',');
                    }

                    var description = $('#content-description').val().trim();
                    if (description != null && description != "") {
                        fd.append('description',description);
                    }

                    var short_description = $('#content-short-description').val();
                    if (short_description != null && short_description != "") {
                        fd.append('short_description',short_description.trim());
                    }

                    var expiry = $('#content-expiry').val();
                    var cost = $('#content-cost').val();
                    if($('#content-premium').is(':checked')){
                        fd.append('premium',"yes");
                        fd.append('expiry',hmsToSeconds(expiry) * 1000);
                        fd.append('cost',cost);
                    } else {
                        fd.append('premium',"no");
                    }

                    fd.append('specifics', JSON.stringify(specifics));
                    var video_content_type = $('#content-video-type').val();
                    fd.append('video_content_type', video_content_type );
                    fd.append('box_only', $('#content-box-only').is(':checked') ? "yes" : "no");
                    fd.append('featured', $('#content-feature').is(':checked') ? "yes" : "no");
                    fd.append('allow_download', $('#content-allow-download').is(':checked') ? "yes" : "no");

                    if (content_cover_data_url != "" && content_cover_data_url != null) {
                        var blob = dataURItoBlob(content_cover_data_url);
                        fd.append('cover_image', blob);
                        fd.append('cover_image_name', contentCoverFileName);
                    }

                    if (content_poster_data_url != "" && content_poster_data_url != null) {
                        var blob = dataURItoBlob(content_poster_data_url);
                        fd.append('poster', blob);
                        fd.append('poster_name', contentPosterFileName);
                    }
                    if (content_poster_small_data_url != "" && content_poster_small_data_url != null) {
                        var blob = dataURItoBlob(content_poster_small_data_url);
                        fd.append('poster_small', blob);
                        fd.append('poster_small_name', contentPosterSmallFileName);
                    }

                    var status = "incomplete";

                    var essentials = ['name', 'duration','type'];
                    /* If warnings object is empty */
                    if (canBeStaged(warnings, essentials) && content_poster_data_url != null && content_poster_small_data_url != null && tag_string != null && raw_media_id != null) {
                        if(video_content_type == 'shortform'){
                            if(short_description == null || short_description == ''){
                                status = "incomplete";
                            }else if(seo == null || seo == ''){
                                status = "seo_pending";
                            } else {
                                status = "staged";
                            }
                        } else {
                            if(description == null || description == '' || short_description == null || short_description == ''){
                                status = "incomplete";
                            } else if(seo == null || seo == ''){
                                status = "seo_pending";
                            } else {
                                status = "staged";
                            }

                        }
                    }

                    fd.append("status", status);
                    fd.append('user_name', current_active_user['name']);
                    fd.append('user_email', current_active_user['email']);
                    /* posting data to server */
                    $('.new-content-form-submit').attr('disabled', 'disabled').removeClass('pointer');
                    $('.submit-indicator').append($('.m-preloader').addClass('preloader-copy').clone().show());

                    // TODO remove
                    // var xhr = new XMLHttpRequest;
                    // xhr.open('POST', '/', true);
                    // xhr.send(fd);
                     //return;
                    $.ajax({
                        url: BASE_URL + 'content/keep/',
                        method: 'POST',
                        data: fd,
                        processData: false,
                        contentType: false,
                    }).done(function(result) {
                        toastr.success('Content added successully');
                        toastr.info('Redirecting to content display page');
                        setTimeout(
                            function() {
                                page('/content/' + result['storage_id']);
                            }, 2000);

                    }).fail(function(data) {
                        console.log(data);
                        showError(data);
                    }).always(function() {
                        $('.new-content-form-submit').removeAttr('disabled', 'disabled').addClass('pointer');
                        $('.submit-indicator').empty();
                        cleanCallback();
                    });

                }
            }, formValidationViewUpdate)

            new_content_form_validator.fortify();

            $('#content-tag-list').tagsinput({
                typeahead: {
                    source: function (query,process) {
                        $.get(BASE_URL + 'bloodhound/tag',{query: query},function(data){
                            tag_name_list = tag_name_list.concat(data.data);
                            return process(data.data);
                        });
                    },
                    minLength: 3
                }
            });
            $('#content-provider').typeahead({
                source: function(query,process){
                    return $.get(BASE_URL + 'bloodhound/provider',{query: query},function(data){
                        return process(data.data);
                    });
                },
                minLength:3,
                afterSelect: function(item){
                    selected_provider = item.id;
                }
            });

            $('.bootstrap-tagsinput input').attr('placeholder','Choose tags');
            $('.wait-section').hide();
            $('.new-content-form-wrapper').show();
        });

        page('/raw-content/new', function(ctx, next) {
            /*var template = $('#raw-content-form').html();
            var compiled_template = Hogan.compile(template);*/
            var compiled_template = templates['raw-content-form'];


            var media_source_file_object = null;
            var media_source_url = null;
            var media_type = null;
            var procurement_method = null;
            var ALLOWED_FILE_EXTENSIONS = ['torrent', 'mp4', 'mp3']

            $('.page-content').html(compiled_template.render());
            $('#file-explorer-modal').html($('#file-explorer').html());
            $('#file-explorer-modal').find('.spinner-section').append($('.spin').clone().addClass('spin-clone').show());

            $('.s3-explorer-trigger').off().click(function() {
                $('.file-explorer-wait-section').show();
                $('.main-explorer-container').hide();
                $('.file-explorer-preview').hide().find('.audio-preview').hide();
                $('.file-explorer-preview').find('.video-preview').hide();
                $('.s3-explorer').click();
                console.log(BASE_URL + 'util/get-s3-contents/content-repo');
                $.ajax({
                    url: BASE_URL + 'util/get-s3-contents/content-repo',
                    method: 'GET',
                }).done(function(result) {
                    if (!result['content_list'].length) {
                        $('.modal-close').click();
                        toastr.error('No content available in S3 bucket');
                        return;
                    }
                    prepareFileExplorer(result['content_list'], 'content-repo');
                }).fail(function(data) {
                    $('.modal-close').click();
                    toastr.error('Unable to fetch data from S3 bucket. Please try again later');
                })
            });

            $(document).off('click', '.file-play');
            $(document).on('click', '.file-play', function() {
                var type = $(this).attr('data-type');
                var link = $(this).attr('data-link');
                console.log('playing file');
                if (type == 'audio') {
                    $('.file-explorer-preview').find('.audio-preview').show();
                    var audio_player = $('.file-explorer-preview').find('audio');
                    $(audio_player).show().attr('src', link).load();
                    $('.file-explorer-preview').show();
                } else {
                    $('.file-explorer-preview').find('.video-preview').show();
                    var video_player = $('.file-explorer-preview').find('video');
                    $(video_player).show().attr('src', link).load();
                    $('.file-explorer-preview').show();
                }
            });
            $(document).off('click', '.file-preview-close');
            $(document).on('click', '.file-preview-close', function() {
                $('.file-explorer-preview').find('audio').attr('src', '');
                $('.file-explorer-preview').find('video').attr('src', '');
                $('.file-explorer-preview').hide();
            });

            $(document).off('click', '.file-add');
            $(document).on('click', '.file-add', function() {
                var name = $(this).attr('data-name');
                media_source_url = name;
                procurement_method = 'S3';
                $('.media-source-type').html(procurement_method).removeClass('btn-info btn-danger btn-warning').addClass('btn-danger');
                $('.media-source-name').html(name);
                $('#media-source').val('');
                $('.media-source-input').hide();
                $('.media-source-info').show();
                $('.modal-close').click();

            })

            $('#content-type').material_select();
            $('.modal-trigger').off().leanModal();

            $('#media-source').off().on('dragover', function(e) {
                e.preventDefault();
                e = e.originalEvent;
                e.dataTransfer.dropEffect = 'copy';
            }).on('drop', function(e) {
                e.preventDefault();
                e = e.originalEvent;
                var target = e.dataTransfer || e.target,
                    file = target && target.files && target.files[0];
                if (!file) {
                    toastr.error('Unable to load image, please try again');
                    return;
                }
                var file_fragments = file.name.split('.');
                var file_extension = file_fragments[file_fragments.length - 1];

                if (ALLOWED_FILE_EXTENSIONS.indexOf(file_extension) < 0) {
                    toastr.error('File type of ' + file_extension + ' not supported');
                    return;
                }
                media_source_file_object = file;

                if (file_extension == 'torrent') {
                    procurement_method = 'TORRENT';
                    $('.media-source-type').html(procurement_method).removeClass('btn-info btn-danger btn-warning').addClass('btn-warning');
                } else {
                    procurement_method = 'UPLOAD';
                    $('.media-source-type').html(procurement_method).removeClass('btn-info btn-danger btn-warning').addClass('btn-info');
                }

                $('.media-source-name').html(file.name);
                $('#media-source').val('');
                $('.media-source-input').hide();
                $('.media-source-info').show();
            });

            $('#media-source').change(function() {
                var link = $(this).val();
                if (link.startsWith('magnet:?')) {
                    procurement_method = 'MAGNET';
                    media_source_url = link;
                    $('.media-source-type').html(procurement_method).removeClass('btn-info btn-danger btn-warning').addClass('btn-warning');

                } else if (/(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/.test(link)) {
                    procurement_method = 'DOWNLOAD';
                    media_source_url = link;
                    $('.media-source-type').html(procurement_method).removeClass('btn-info btn-danger btn-warning').addClass('btn-success');
                }
                media_source_url = link;
                $('.media-source-name').html(link);
                $('#media-source').val('');
                $('.media-source-input').hide();
                $('.media-source-info').show();
            });

            $('.remove-media-source').click(function() {
                $('#upload-status').hide();
                media_source_file_object = null;
                media_source_url = null;
                procurement_method = null;
                $('.media-source-info').hide();
                $('.media-source-input').val('').show();
            });

            var new_raw_content_form_validator = new Validator(raw_content_seed, '.new-raw-content-form-submit', function(errors, warnings, cleanCallback) {
                var content_type = $('#content-type').val();
                if (Object.keys(errors).length > 0 || procurement_method == null) {
                    console.log(errors);
                    if (procurement_method == null)
                        toastr.error('Please add a media source');
                    else
                        toastr.error('Please correct the errors in form');
                    cleanCallback();
                } else {
                    /* creating form data to post data to server */
                    var fd = new FormData();

                    if (procurement_method == 'UPLOAD') {
                        if (!media_source_file_object) {
                            toastr.error('No file provided. please try replacing file');
                            cleanCallback();
                            return;
                        }
                        /*$.ajax({
                        });*/
                        var file_fragments = media_source_file_object.name.split('.');
                        var file_extension = file_fragments[file_fragments.length - 1];
                        if ((file_extension == 'mp3' && content_type == "video") || (file_extension == 'mp3' && content_type == "video")) {
                            console.log(errors);
                            toastr.error('Content type chosen does not matches with file provided');
                            cleanCallback();
                            return;
                        }
                        fd.append('media_file', media_source_file_object);
                    } else if (procurement_method == 'DOWNLOAD') {
                        if (!media_source_url) {
                            toastr.error('No url provided. please try replacing source url');
                            cleanCallback();
                            return;
                        }
                        fd.append('media_url', media_source_url)
                    } else if (procurement_method == 'TORRENT') {
                        if (!media_source_file_object) {
                            toastr.error('No file provided. please try replacing file');
                            cleanCallback();
                            return;
                        }
                        fd.append('torrent', media_source_file_object);
                    } else if (procurement_method == 'MAGNET') {
                        if (!media_source_url) {
                            toastr.error('No url provided. please try source url');
                            cleanCallback();
                            return;
                        }
                        fd.append('media_url', media_source_url);
                    } else if (procurement_method == 'S3') {
                        fd.append('media_url', media_source_url);
                    }

                    fd.append('procurement_method', procurement_method);
                    fd.append('media_name', $('#raw-content-name').val().trim());
                    fd.append('media_type', content_type);
                    fd.append('user_name', current_active_user['name']);
                    fd.append('user_email', current_active_user['email']);

                    $('.new-raw-content-form-submit').attr('disabled', 'disabled').removeClass('pointer');
                    $('.submit-indicator').append($('.m-preloader').addClass('preloader-copy').clone().show());

                    $.ajax({
                        url: BASE_URL + 'util/procure-content',
                        method: 'POST',
                        data: fd,
                        processData: false,
                        contentType: false,
                        xhr: function() {
                            var xhr = $.ajaxSettings.xhr();
                            var started_at = new Date();
                            xhr.upload.onprogress = function(e){
                                if( e.lengthComputable )
                                {
                                    $('.upload-status-wrapper').show();
                                    // Append progress percentage.
                                    var loaded = e.loaded;
                                    var total = e.total;
                                    var progressValue = Math.round( ( loaded / total ) * 100 );
                                    $('#upload-status').css('width',progressValue + '%');

                                    // Time Remaining
                                    var seconds_elapsed =   ( new Date().getTime() - started_at.getTime() )/1000;
                                    var bytes_per_second =  (seconds_elapsed) ? (loaded/ seconds_elapsed) : 0 ;
                                    var Kbytes_per_second = bytes_per_second / 1000 ;
                                    var remaining_bytes =   total - loaded;
                                    var seconds_remaining = (seconds_elapsed) ? (remaining_bytes / bytes_per_second) : 'calculating' ;
                                    //var bar_html = Math.round(loaded/(1024*1024)) + 'MB / ' + Math.round(total/(1024*1024)) + 'MB ETA(' + Math.round(seconds_remaining) + ' seconds)';
                                    $('#upload-ratio').html(Math.round(loaded/(1024*1024)) + 'MB / ' + Math.round(total/(1024*1024)) + 'MB');
                                    $('#upload-time').html('Time remaining: ' + secondsToHmsKeepingHRS(Math.round(seconds_remaining)));
                                    $('#upload-status').html(progressValue + '%');
                                }
                            }
                            return xhr;
                        }
                    }).done(function(result) {
                        toastr.success('Raw Content added successully');
                        toastr.info('Redirecting to content display page');
                        setTimeout(
                            function() {
                                page('/raw-content/');
                            }, 2000);

                    }).fail(function(data) {
                        console.log(data);
                        showError(data);
                    }).always(function() {
                        $('.new-raw-content-form-submit').removeAttr('disabled', 'disabled').addClass('pointer');
                        $('.submit-indicator').empty();
                        cleanCallback();
                    });
                }
            }, formValidationViewUpdate);

            new_raw_content_form_validator.fortify();
        });

        page('/channel/new', function(ctx, next) {
            /*var template = $('#new-channel-form').html();
            var compiled_template = Hogan.compile(template);*/
            var compiled_template = templates['new-channel-form'];
            $('.page-content').html(compiled_template.render());
            var channelCoverFileName = null;
            var channelPosterFileName = null;
            var channelPosterSmallFileName = null;
            tag_name_list = [];
            var selected_provider = 'pp-provider-1';
            splashFileObject = null;
            channel_cover_data_url = null;
            channel_icon_data_url = null;
            channel_icon_small_data_url = null;

            $('input.length-field').characterCounter();
            /* rendering image upload template */
            $('#cover-add-modal').html(compiled_image_template.render({
                title: 'Add Icon'
            }));
            $('#poster-add-modal').html(compiled_image_template.render({
                title: 'Add Icon'
            }));
            $('#poster-small-add-modal').html(compiled_image_template.render({
                title: 'Add Small Icon'
            }));
            $('#splash-add-modal').html(compiled_video_template.render({
                title: 'Add Splash'
            }));
            /* rendering image upload template */


            $('.modal-trigger').off().leanModal();
            /* initialize js components */

            $('#channel-name').on('input',function(){
                var name = $(this).val();
                var slug = slugify(name);
                if(name != ''){
                    $('.slug-available').show();
                    $('.generated-slug').text(slug);
                } else {
                    $('.slug-available').hide();
                }
            });

            $('#channel-show-title').attr('checked', 'checked');
            $('.page-content').find('.wait-section').empty().append($('.spin').clone().addClass('spin-clone').show());
            $('.page-content').find('.wait-section').show();

            prepareImageUploaderModal('#cover-add-modal', function(poster_src, filename) {
                channelCoverFileName = filename;
                $('.cover-thumbnail').attr('src', poster_src);
                channel_cover_data_url = poster_src;
                $('.new-channel-cover-thumbnail-container').show();
                $('.channel-cover-control').html('Edit cover');
            }, function() {
                channelCoverFileName = null;
                channel_cover_data_url = null;
                $('.cover-thumbnail').attr('src', '');
                $('.new-channel-cover-thumbnail-container').hide();
                $('.channel-cover-control').html('Add cover');
            }, channelCoverDimensions);

            prepareImageUploaderModal('#poster-add-modal', function(poster_src, filename) {
                channelPosterFileName = filename;
                $('.icon-thumbnail').attr('src', poster_src);
                channel_icon_data_url = poster_src;
                $('.new-channel-icon-thumbnail-container').show();
                $('.channel-icon-control').html('Edit icon');
            }, function() {
                channelPosterFileName = null;
                channel_icon_data_url = null;
                $('.icon-thumbnail').attr('src', '');
                $('.new-channel-icon-thumbnail-container').hide();
                $('.channel-icon-control').html('Add icon');
            }, channelPosterDimensions);


            prepareImageUploaderModal('#poster-small-add-modal', function(poster_src, filename) {
                channelPosterSmallFileName = filename;
                $('.icon-small-thumbnail').attr('src', poster_src);
                channel_icon_small_data_url = poster_src;
                $('.new-channel-icon-small-thumbnail-container').show();
                $('.channel-icon-small-control').html('Edit Small icon');
            }, function() {
                channelPosterSmallFileName = null;
                channel_icon_small_data_url = null;
                $('.icon-small-thumbnail').attr('src', '');
                $('.new-channel-icon-small-thumbnail-container').hide();
                $('.channel-icon-small-control').html('Add Small icon');
            }, channelIconDimensions);

            prepareVideoUploaderModal('#splash-add-modal', function(file) {
                splashFileObject = file;
                $('.channel-splash-name').html(file.name).show();
                $('.new-channel-splash').html('Edit splash');
            }, function() {
                splashFileObject = null;
                $('.channel-splash-name').html('').hide();
                $('.new-channel-splash').html('Upload splash');
            })


            /* binding validator class */
            var new_channel_form_validator = new Validator(channel_validator_seed, '.new-channel-form-submit', function(errors, warnings, cleanCallback) {
                if (Object.keys(errors).length > 0) {
                    console.log(errors);
                    toastr.error('Please correct the errors in form');
                    cleanCallback();
                } else {
                    /* creating form data to post data to server */
                    var fd = new FormData();
                    fd.append('name', $('#channel-name').val().trim());
                    fd.append('slug_url', slugify($('#channel-name').val()));
                    fd.append('display_name', $('#channel-display-name').val().trim());
                    var description = $('#channel-description').val().trim();
                    fd.append('description', description);
                    var short_description = $('#channel-short-description').val().trim();
                    fd.append('short_description', short_description);
                    var tagline = $('#channel-tagline').val().trim();
                    fd.append('tagline', tagline);

                    fd.append('highlighted', $('#channel-highlight').is(':checked') ? "yes" : "no");
                    fd.append('is_global', $('#channel-is-global').is(':checked') ? "yes" : "no");
                    fd.append('show_title', $('#channel-show-title').is(':checked') ? "yes" : "no");
                    fd.append('provider_id',selected_provider);

                    if (channel_cover_data_url != "" && channel_cover_data_url != null) {
                        var blob = dataURItoBlob(channel_cover_data_url);
                        fd.append('cover_image', blob);
                        fd.append('cover_image_name', channelCoverFileName);
                    }

                    if (channel_icon_data_url != "" && channel_icon_data_url != null) {
                        var blob = dataURItoBlob(channel_icon_data_url);
                        fd.append('icon', blob);
                        fd.append('icon_name', channelPosterFileName);
                    }

                    if (channel_icon_small_data_url != "" && channel_icon_small_data_url != null) {
                        var blob = dataURItoBlob(channel_icon_small_data_url);
                        fd.append('icon_small', blob);
                        fd.append('icon_small_name', channelPosterFileName);
                    }

                    if (splashFileObject != null && splashFileObject != "") {
                        fd.append('base_splash', splashFileObject);
                        fd.append('base_splash_name', splashFileObject.name);
                    }


                    if($('#channel-tag-list').val() == ""){
                        fd.append('tags','[]');
                    }
                    else{
                        var tag_string = $('#channel-tag-list').val().split(',');
                        var tag_array = [];
                        tag_string.forEach(function(name){
                            if($.inArray(name,tag_name_list)<0){
                                var trimed = name.trim();
                                tag_array.push({
                                    type: "new",
                                    name: trimed
                                });
                            }
                            else {
                                tag_array.push({
                                    type: "existing",
                                    name: name
                                });
                            }
                        });
                        fd.append('tags',JSON.stringify(tag_array));
                    }

                    var status = "incomplete";
                    var essentials = ['name', 'display-name'];
                    /* If warnings object is empty */
                    if (canBeStaged(warnings, essentials) && channel_icon_data_url != null && channel_icon_small_data_url != null && tag_string != null && description!=null && description!='' && short_description!=null && short_description!='') {
                        status = "staged";
                    }



                    fd.append("status", status);
                    fd.append('user_name', current_active_user['name']);
                    fd.append('user_email', current_active_user['email']);
                    /* posting data to server */
                    $('.new-channel-form-submit').attr('disabled', 'disabled').removeClass('pointer');
                    $('.submit-indicator').append($('.m-preloader').addClass('preloader-copy').clone().show());

                    // TODO remove
                    // var xhr = new XMLHttpRequest;
                    // xhr.open('POST', '/', true);
                    // xhr.send(fd);
                    // return;
                    $.ajax({
                        url: BASE_URL + 'channel/keep/',
                        method: 'POST',
                        data: fd,
                        processData: false,
                        contentType: false
                    }).done(function(result) {
                        toastr.success('Channel added successully');
                        toastr.info('Redirecting to channel display page');
                        setTimeout(
                            function() {
                                page('/channel/' + result['storage_id']);
                            }, 2000);
                    }).fail(function(data) {
                        console.log(data);
                        showError(data);
                    }).always(function() {
                        $('.new-channel-form-submit').removeAttr('disabled', 'disabled').addClass('pointer');
                        $('.submit-indicator').empty();
                        cleanCallback();
                    });

                }
            }, formValidationViewUpdate)

            new_channel_form_validator.fortify();

            $('#channel-provider').typeahead({
                source: function(query,process){
                    return $.get(BASE_URL + 'bloodhound/provider',{query: query},function(data){
                        return process(data.data);
                    });
                },
                minLength:3,
                afterSelect: function(item){
                    selected_provider = item.id;
                }
            });

            $('#channel-tag-list').tagsinput({
                typeahead: {
                    source: function (query,process) {
                        $.get(BASE_URL + 'bloodhound/tag',{query: query},function(data){
                            tag_name_list = tag_name_list.concat(data.data);
                            return process(data.data);
                        });
                    },
                    minLength: 3
                }
            });

            $('.bootstrap-tagsinput input').attr('placeholder','Choose tags');
            $('.wait-section').hide();
            $('.new-channel-form-wrapper').show();
        });

        page('/sponsor/new', function(ctx, next) {
            apk_file = null;
            apk_filename = null;
            var sponsorCoverFileName = null;
            var sponsorLogoFileName = null;
            var sponsorImageFileName = null;
            /*var template = $('#new-sponsor-form').html();
            var compiled_template = Hogan.compile(template);*/
            var compiled_template = templates['new-sponsor-form'];
            $('.page-content').html(compiled_template.render());

            sponsorSplashFileObject = null;
            sponsorSplashObjects = {};
            sponsorPrerollObjects = {};
            sponsorImageAds = {};
            sponsor_logo_data_url = null;
            sponsor_cover_data_url = null;
            sponsor_image_data_url = null;
            sponsorPrerollFileObject = null;

            /* setting first splash as null to prevent creation of bogus spash elements */
            sponsorSplashObjects['splash-0'] = null;
            sponsorPrerollObjects['preroll-0'] = null;
            sponsorImageAds['image-ad-0'] = null;

            /* rendering image upload template */
            $('#logo-add-modal').html(compiled_image_template.render({
                title: 'Add logo'
            }));
            $('#cover-add-modal').html(compiled_image_template.render({
                title: 'Add Cover'
            }));
            $('#sponsor-image-add-modal').html(compiled_image_template.render({
                title: 'Add app image'
            }));

            $('#splash-add-modal').html(compiled_video_template.render({
                title: 'Add Splash'
            }));
            $('#preroll-add-modal').html(compiled_video_template.render({
                title: 'Add preroll'
            }));
            $('#image-ad-add-modal').html(compiled_image_template.render({
                title: 'Add Image ad'
            }));

            //updates slug for typed name
            $('#sponsor-slug-url').on('input',function(){
                var url = $(this).val();
                var slug = slugify(url);
                if(url != ''){
                    $.ajax({
                        'url': BASE_URL + 'sponsor/slugify?value=' + slug,
                        'method': 'GET'
                    }).done(function(data){
                        if(data.available){
                            $('.slug-available').show();
                            $('.slug-unavailable').hide();
                            $('.generated-slug').html(slug);
                        } else {
                            $('.slug-unavailable').show();
                            $('.slug-available').hide();
                        }

                    });
                } else {
                    $('.slug-available').hide();
                    $('.slug-unavailable').hide();
                }
            });
            /* initialize js components */

            prepareImageUploaderModal('#cover-add-modal', function(poster_src, filename) {
                sponsorCoverFileName = filename;
                $('.cover-thumbnail').attr('src', poster_src);
                sponsor_cover_data_url = poster_src;
                $('.new-sponsor-cover-thumbnail-container').show();
                $('.sponsor-cover-control').html('Edit cover');
            }, function() {
                sponsorCoverFileName = null;
                sponsor_cover_data_url = null;
                $('.cover-thumbnail').attr('src', '');
                $('.new-sponsor-cover-thumbnail-container').hide();
                $('.sponsor-cover-control').html('Add cover');
            }, sponsorCoverDimensions);

            prepareImageUploaderModal('#logo-add-modal', function(poster_src, filename) {
                sponsorLogoFileName = filename;
                $('.logo-thumbnail').attr('src', poster_src);
                sponsor_logo_data_url = poster_src;
                $('.new-sponsor-logo-thumbnail-container').show();
                $('.sponsor-logo-control').html('Edit logo');
            }, function() {
                sponsorLogoFileName = null;
                sponsor_logo_data_url = null;
                $('.logo-thumbnail').attr('src', '');
                $('.new-sponsor-logo-thumbnail-container').hide();
                $('.sponsor-logo-control').html('Add logo');
            }, sponsorLogoDimensions);

            prepareImageUploaderModal('#sponsor-image-add-modal', function(poster_src, filename) {
                sponsorImageFileName = filename;
                $('.sponsor-image-thumbnail').attr('src', poster_src);
                sponsor_image_data_url = poster_src;
                $('.new-sponsor-image-thumbnail-container').show();
                $('.sponsor-image-control').html('Edit image');
            }, function() {
                sponsorImageFileName = null;
                sponsor_image_data_url = null;
                $('.sponsor-image-thumbnail').attr('src', '');
                $('.new-sponsor-image-thumbnail-container').hide();
                $('.sponsor-image-control').html('Add image');
            }, sponsorImageDimesions);

            /* preparing default splash object */
            prepareVideoUploaderModal('#splash-add-modal', function(file) {
                sponsorSplashObjects['splash-0'] = file;
                $('.sponsor-splash-name').html(file.name).show();
                $('.new-sponsor-splash').html('Edit splash');
            }, function() {
                sponsorSplashObjects['splash-0'] = null;
                $('.sponsor-splash-name').html('').hide();
                $('.new-sponsor-splash').html('Upload splash');
            });

            /* preparing default preroll object */
            prepareVideoUploaderModal('#preroll-add-modal', function(file) {
                sponsorPrerollObjects['preroll-0'] = file;
                $('.sponsor-preroll-name').html(file.name).show();
                $('.new-sponsor-preroll').html('Edit preroll');
            }, function() {
                sponsorPrerollObjects['preroll-0'] = null;
                $('.sponsor-preroll-name').html('').hide();
                $('.new-sponsor-preroll').html('Upload preroll');
            });

            /* preparing default preroll object */
            prepareImageUploaderModal('#image-ad-add-modal', function(image_ad_src, filename) {
                sponsorImageAds['image-ad-0'] = [image_ad_src, filename]
                $('.sponsor-image-ad-name').html(filename).show();
                $('.new-sponsor-image-ad').html('Edit image ad');
            }, function() {
                sponsorImageAds['image-ad-0'] = null;
                $('.sponsor-image-ad-name').hide();
                $('.new-sponsor-image-ad').html('Upload image-ad');
            }, ImageAdDimesions);

            $('.new-sponsor-apk-package').click(function(){
                if(apk_filename == null){
                    $('.new-sponsor-apk-package-input').click();
                    $('.new-sponsor-apk-package-input').on('change',function(){
                        apk_file = $('.new-sponsor-apk-package-input')[0].files[0];
                        apk_filename = $('.new-sponsor-apk-package-input').val().split('\\').pop();

                        if(apk_file != null && apk_file != ''){
                            console.log('file selected');
                            $('.new-sponsor-apk-package-name').html(apk_filename);
                            $('.new-sponsor-apk-package').html('Remove apk package');
                        }
                    });
                }else {
                    apk_filename = null;
                    apk_file = null;
                    $('.new-sponsor-apk-package-input').val('');
                    $('.new-sponsor-apk-package-name').empty();
                    $('.new-sponsor-apk-package').html('Upload apk package');
                }
            });

            $('.splash-add-button').click(
                function() {
                    sponsorAddVideoObjectHandler(sponsorSplashObjects, 'splash')
                });

            $('.preroll-add-button').click(
                function() {
                    sponsorAddVideoObjectHandler(sponsorPrerollObjects, 'preroll')
                });

            /* event handler for  image ads*/
            $('.image-ad-add-button').click(function() {
                for (var ia in sponsorImageAds) {
                    if (sponsorImageAds[ia] == null) {
                        toastr.error('use alredy added element');
                        return;
                    }
                }
                var imageAds = Object.keys(sponsorImageAds).length;
                /* adding new splash element */
                $('#image-ad-add-modal-template').clone().attr('id', 'image-ad-add-modal-' + (imageAds)).insertAfter('#image-ad-add-modal');
                $('#image-ad-add-modal-' + (imageAds)).html(compiled_image_template.render({
                    title: 'Add Image Ad'
                }));

                sponsorImageAds['image-ad-' + imageAds] = null;

                /* attaching event handler */
                (function(image_ad_no) {
                    prepareImageUploaderModal('#image-ad-add-modal-' + image_ad_no, function(image_ad_src, filename) {
                        sponsorImageAds['image-ad-' + image_ad_no] = [image_ad_src, filename]
                        $('.sponsor-image-ad-name-' + image_ad_no).html(filename).show();
                        $('.new-sponsor-image-ad-' + image_ad_no).html('Edit image ad');
                    }, function() {
                        sponsorImageAds['image-ad-' + image_ad_no] = null;
                        $('.sponsor-image-ad-name-' + image_ad_no).hide();
                        $('.new-sponsor-image-ad' + image_ad_no).html('Upload image-ad');
                    }, ImageAdDimesions);
                })(imageAds)

                var new_image_ad_button = $('.sponsor-image-ad-item.base-template').clone().removeClass('base-template');
                new_image_ad_button.find('.new-sponsor-image-ad').attr('href', '#image-ad-add-modal-' + (imageAds)).removeClass('new-sponsor-image-ad')
                    .addClass('new-sponsor-image-ad-' + (imageAds)).html('Upload image ad');
                new_image_ad_button.find('.sponsor-image-ad-name').html('').removeClass('sponsor-image-ad-name').addClass('sponsor-image-ad-name-' + (imageAds))
                new_image_ad_button.insertBefore('.image-ad-add-button');
                //$('.modal-trigger').off().leanModal();
                $(new_image_ad_button).find('.modal-trigger').click();
                $('.modal-trigger').off().leanModal();
            });

            /* rendering image upload template */
            $('.modal-trigger').off().leanModal();


            /* binding validator class */
            var new_sponsor_form_validator = new Validator(sponsor_validator_seed, '.new-sponsor-form-submit', function(errors, warnings, cleanCallback) {

                if (Object.keys(errors).length > 0) {
                    console.log(errors);
                    toastr.error('Please correct the errors in form');
                    cleanCallback();
                } else {
                    /* creating form data to post data to server */
                    var fd = new FormData();
                    var name = $('#sponsor-name').val().trim();
                    fd.append('name', name);

                    var sponsor_url = $('#sponsor-url').val().trim();
                    if (sponsor_url != null && sponsor_url != '') {
                        fd.append('url', sponsor_url)
                    }

                    fd.append('active', $('#sponsor-status').is(':checked') ? "yes" : "no");
                    fd.append('is_global', $('#sponsor-is-global').is(':checked') ? "yes" : "no");
                    var apps = {};
                    apps['apps_download'] = $('#sponsor-apps-download').is(':checked') ? "yes" : "no";

                    var sponsor_app_tagline = $('#sponsor-tagline').val().trim();
                    if (sponsor_app_tagline != null && sponsor_app_tagline != '') {
                        fd.append('tagline', sponsor_app_tagline);
                    }

                    var sponsor_app_package_android = $('#sponsor-package-android').val().trim();
                    if (sponsor_app_package_android != null && sponsor_app_package_android != '') {
                        apps['package_android'] = sponsor_app_package_android;
                    }

                    var sponsor_app_url_android = $('#sponsor-app-url-android').val().trim();
                    if (sponsor_app_url_android != null && sponsor_app_url_android != '') {
                        apps['remote_android'] = sponsor_app_url_android;
                    }

                    var sponsor_app_package_ios = $('#sponsor-package-ios').val().trim();
                    if (sponsor_app_package_ios != null && sponsor_app_package_ios != '') {
                        apps['package_ios'] = sponsor_app_package_ios;
                    }

                    var sponsor_app_url_ios = $('#sponsor-app-url-ios').val().trim();
                    if (sponsor_app_url_ios != null && sponsor_app_url_ios != '') {
                        apps['remote_ios'] = sponsor_app_url_ios;
                    }

                    var sponsor_twitter_link = $('#sponsor-twitter-link').val().trim();
                    if (sponsor_twitter_link != null && sponsor_twitter_link != '') {
                        fd.append('twitter_link', sponsor_twitter_link);
                    }

                    var sponsor_fb_link = $('#sponsor-fb-link').val().trim();
                    if (sponsor_fb_link != null && sponsor_fb_link != '') {
                        fd.append('fb_link', sponsor_fb_link);
                    }

                    var sponsor_slug_url = $('#sponsor-slug-url').val().trim();
                    if (sponsor_slug_url != null && sponsor_slug_url != '') {
                        fd.append('slug_url',slugify(sponsor_slug_url));
                    } else {
                        fd.append('slug_url',slugify(name));
                    }

                    if(apk_file != null && apk_file != ''){
                        fd.append('apk_filename', apk_filename);
                        fd.append('apk_file',apk_file);
                    }

                    var sponsor_desc_large = $('#sponsor-desc-large').val().trim();
                    if (sponsor_desc_large != null && sponsor_desc_large != '') {
                        fd.append('description', sponsor_desc_large);
                    }

                    var sponsor_desc_small = $('#sponsor-desc-small').val().trim();
                    if (sponsor_desc_small != null && sponsor_desc_small != '') {
                        fd.append('description_small', sponsor_desc_small);
                    }

                    fd.append('app_data', JSON.stringify(apps));

                    if (sponsor_logo_data_url != "" && sponsor_logo_data_url != null) {
                        var blob = dataURItoBlob(sponsor_logo_data_url);
                        fd.append('logo', blob);
                        fd.append('logo_name', sponsorLogoFileName);
                    }

                    if (sponsor_cover_data_url != "" && sponsor_cover_data_url != null) {
                        var blob = dataURItoBlob(sponsor_cover_data_url);
                        fd.append('cover_image', blob);
                        fd.append('cover_image_name', sponsorCoverFileName);
                    }

                    if (sponsor_image_data_url != "" && sponsor_image_data_url != null) {
                        var blob = dataURItoBlob(sponsor_image_data_url);
                        fd.append('image', blob);
                        fd.append('image_name', sponsorImageFileName);
                    }

                    /* adding splashes and  prerolls */
                    var splash_names = {}
                    for (var sp in sponsorSplashObjects) {
                        if (sponsorSplashObjects[sp] == null)
                            continue;
                        var n = parseInt(sp.split('-')[1]);
                        fd.append('splash-' + n, sponsorSplashObjects[sp]);
                        splash_names['splash-' + n] = sponsorSplashObjects[sp].name;
                    }

                    var preroll_names = {}
                    for (var pr in sponsorPrerollObjects) {
                        if (sponsorPrerollObjects[pr] == null)
                            continue;
                        var n = parseInt(pr.split('-')[1]);
                        fd.append('preroll-' + n, sponsorPrerollObjects[pr]);
                        preroll_names['preroll-' + n] = sponsorPrerollObjects[pr].name;
                    }

                    var image_ad_names = {};
                    for (var ia in sponsorImageAds) {
                        if (sponsorImageAds[ia] == null)
                            continue;
                        var n = parseInt(ia.split('-')[2]);
                        var blob = dataURItoBlob(sponsorImageAds[ia][0]);
                        fd.append('image_ad-' + n, blob);
                        image_ad_names['image_ad-' + n] = sponsorImageAds[ia][1];
                    }

                    /* addimg image file names */
                    fd.append('image_ad_names', JSON.stringify(image_ad_names));
                    fd.append('preroll_names', JSON.stringify(preroll_names));
                    fd.append('splash_names', JSON.stringify(splash_names));


                    var status = "incomplete";
                    var essentials = ['name', 'sponsor-tagline'];
                    /* If warnings object is empty */
                    if (canBeStaged(warnings, essentials) && sponsor_logo_data_url != null && $('#sponsor-status').is(':checked')) {
                        status = "staged";
                    }

                    fd.append("status", status);
                    fd.append('user_name', current_active_user['name']);
                    fd.append('user_email', current_active_user['email']);
                    /* posting data to server */
                    $('.new-sponsor-form-submit').attr('disabled', 'disabled').removeClass('pointer');
                    $('.submit-indicator').append($('.m-preloader').addClass('preloader-copy').clone().show());

                    // TODO remove
                    //var xhr = new XMLHttpRequest;
                    //xhr.open('POST', BASE_URL + 'sponsor/keep/' , true);
                    //xhr.send(fd);
                    //return;
                    $.ajax({
                        url: BASE_URL + 'sponsor/keep/',
                        method: 'POST',
                        data: fd,
                        processData: false,
                        contentType: false
                    }).done(function(result) {
                        toastr.success('sponsor edited successully');
                        toastr.info('Redirecting to sponsor display page');
                        setTimeout(
                            function() {
                                page('/sponsor/' + result['storage_id']);
                            }, 2000);
                    }).fail(function(data) {
                        console.log(data);
                        showError(data);
                    }).always(function() {
                        $('.new-sponsor-form-submit').removeAttr('disabled', 'disabled').addClass('pointer');
                        $('.submit-indicator').empty();
                        cleanCallback();
                    });
                }
            }, formValidationViewUpdate)

            new_sponsor_form_validator.fortify();
        });

        page('/tag/new', function(ctx, next) {
            var tagIconFileName = null;
            var tagPosterFileName = null;
            var tagCoverFileName = null;
            /*var template = $('#new-tag-form').html();
            var compiled_template = Hogan.compile(template);*/
            var compiled_template = templates['new-tag-form'];
            $('.page-content').html(compiled_template.render());

            $('#icon-add-modal').html(compiled_image_template.render({
                title: 'Add Icon'
            }));
            $('#poster-add-modal').html(compiled_image_template.render({
                title: 'Add Poster'
            }));
            $('#cover-add-modal').html(compiled_image_template.render({
                title: 'Add cover'
            }));
            $('input.length-field').characterCounter();
            $('.modal-trigger').off().leanModal();

            tag_icon_data_url = null;
            tag_poster_data_url = null;
            tag_cover_data_url = null;

            //updates slug for typed name
            $('#tag-name').on('input',function(){
                var name = $(this).val();
                var slug = slugify(name);
                if(name != ''){
                    $('.slug-available').show();
                    $('.generated-slug').text(slug);
                } else {
                    $('.slug-available').hide();
                }
            });

            $('#tag-show-title').attr('checked', 'checked');

            prepareImageUploaderModal('#cover-add-modal', function(cover_src, filename) {
                tagCoverFileName = filename;
                $('.cover-thumbnail').attr('src', cover_src);
                tag_cover_data_url = cover_src;
                $('.new-tag-cover-thumbnail-container').show();
                $('.tag-cover-control').html('Edit cover');
            }, function() {
                tagCoverFileName = null;
                tag_cover_data_url = null;
                $('.cover-thumbnail').attr('src', '');
                $('.new-tag-cover-thumbnail-container').hide();
                $('.tag-cover-control').html('Add cover');
            }, tagCoverDimensions);

            prepareImageUploaderModal('#icon-add-modal', function(icon_src, filename) {
                tagIconFileName = filename;
                $('.icon-thumbnail').attr('src', icon_src);
                tag_icon_data_url = icon_src;
                $('.new-tag-icon-thumbnail-container').show();
                $('.tag-icon-control').html('Edit icon');
            }, function() {
                tagIconFileName = null;
                tag_icon_data_url = null;
                $('.icon-thumbnail').attr('src', '');
                $('.new-tag-icon-thumbnail-container').hide();
                $('.tag-icon-control').html('Add icon');
            }, tagIconDimensions);

            //madal for poster
            prepareImageUploaderModal('#poster-add-modal', function(poster_src, filename) {
                tagPosterFileName = filename;
                $('.poster-thumbnail').attr('src', poster_src);
                tag_poster_data_url = poster_src;
                $('.new-tag-poster-thumbnail-container').show();
                $('.tag-poster-control').html('Edit poster');
            }, function() {
                tagPosterFileName = null;
                tag_poster_data_url = null;
                $('.poster-thumbnail').attr('src', '');
                $('.new-tag-poster-thumbnail-container').hide();
                $('.tag-poster-control').html('Add poster');
            }, tagPosterDimensions);

            var new_tag_form_validator = new Validator(tag_validator_seed, '.new-tag-form-submit', function(errors, warnings, cleanCallback) {

                if (Object.keys(errors).length > 0) {
                    console.log(errors);
                    toastr.error('Please correct the errors in form');
                    cleanCallback();
                } else {
                    var fd = new FormData();
                    fd.append('name', $('#tag-name').val().trim());
                    fd.append('slug_url', slugify($('#tag-name').val()));
                    fd.append('display_name', $('#tag-display-name').val().trim());
                    var description = $('#tag-description').val().trim();
                    fd.append('description', description);
                    var short_description = $('#tag-short-description').val().trim();
                    fd.append('short_description', short_description);
                    var tagline = $('#tag-tagline').val().trim();
                    fd.append('tagline', tagline);
                    var seo = $('#tag-seo').val().trim();
                    fd.append('seo', seo);
                    fd.append('show_title',$('#tag-show-title').is(':checked') ? "yes" : "no");
                    fd.append('collection',$('#tag-collection').is(':checked') ? "yes" : "no");

                    if (tag_cover_data_url != "" && tag_cover_data_url != null) {
                        var blob = dataURItoBlob(tag_cover_data_url);
                        fd.append('cover_image', blob);
                        fd.append('cover_image_name', tagCoverFileName);
                    }

                    if (tag_icon_data_url != "" && tag_icon_data_url != null) {
                        var blobIcon = dataURItoBlob(tag_icon_data_url);
                        fd.append('poster_small', blobIcon);
                        fd.append('poster_small_name', tagIconFileName);
                    }

                    if (tag_poster_data_url != "" && tag_poster_data_url != null) {
                        var blobPoster = dataURItoBlob(tag_poster_data_url);
                        fd.append('poster', blobPoster);
                        fd.append('poster_name', tagPosterFileName);
                    }

                    var status = "incomplete";
                    var essentials = ['name', 'display_name'];
                    /* If warnings object is empty */
                    if (canBeStaged(warnings, essentials) && tag_poster_data_url != null && tag_poster_data_url != "" && description!=null && description!='' && short_description!=null && short_description!='' && seo != null && seo != '') {
                        status = "staged";
                    }

                    fd.append("status", status);
                    fd.append('user_name', current_active_user['name']);
                    fd.append('user_email', current_active_user['email']);

                    $('.new-tag-form-submit').attr('disabled', 'disabled').removeClass('pointer');
                    $('.submit-indicator').append($('.m-preloader').addClass('preloader-copy').clone().show());

                    $.ajax({
                        url: BASE_URL + 'tag/keep/',
                        method: 'POST',
                        data: fd,
                        processData: false,
                        contentType: false
                    }).done(function(result) {
                        toastr.success('Tag added successully');
                        toastr.info('Redirecting to Tag display page');
                        setTimeout(
                            function() {
                                page('/tag/' + result['storage_id']);
                            }, 2000);
                    }).fail(function(data) {
                        console.log(data);
                        showError(data);
                    }).always(function() {
                        $('.new-tag-form-submit').removeAttr('disabled', 'disabled').addClass('pointer');
                        $('.submit-indicator').empty();
                        cleanCallback();
                    });

                }

            }, formValidationViewUpdate)

            new_tag_form_validator.fortify();
        });

        page('/provider/new',function(ctx,next){

            /*var template = $('#new-provider-form').html();
            var compiled_template = Hogan.compile(template);*/
            var compiled_template = templates['new-provider-form'];
            $('.page-content').html(compiled_template.render());

            $('#provider-campaign-start').pickadate({
                format: 'dd-mm-yyyy',
                selectMonths: true, // Creates a dropdown to control month
                selectYears: 15 // Creates a dropdown of 15 years to control year
            });

            $('#provider-campaign-end').pickadate({
                format: 'dd-mm-yyyy',
                selectMonths: true, // Creates a dropdown to control month
                selectYears: 15 // Creates a dropdown of 15 years to control year
            });

            //updates slug for typed name
            $('#provider-slug-url').on('input',function(){
                var url = $(this).val();
                var slug = slugify(url);
                if(url != ''){
                    $.ajax({
                        'url': BASE_URL + 'provider/slugify?value=' + slug,
                        'method': 'GET'
                    }).done(function(data){
                        if(data.available){
                            $('.slug-available').show();
                            $('.slug-unavailable').hide();
                            $('.generated-slug').html(slug);
                        } else {
                            $('.slug-unavailable').show();
                            $('.slug-available').hide();
                        }

                    });
                } else {
                    $('.slug-available').hide();
                    $('.slug-unavailable').hide();
                }
            });

            var new_provider_form_validator = new Validator(provider_validator_seed, '.new-provider-form-submit', function(errors, warnings, cleanCallback) {

                if (Object.keys(errors).length > 0) {
                    console.log(errors);
                    toastr.error('Please correct the errors in form');
                    cleanCallback();
                } else {
                    console.log($('#provider-campaign-start').val());
                    console.log($('#provider-campaign-end').val());
                    var startparts = $('#provider-campaign-start').val().split('-');
                    var endparts = $('#provider-campaign-end').val().split('-');
                    if(new Date(startparts[2],startparts[1]-1,startparts[0]) >= new Date(endparts[2],endparts[1]-1,endparts[0])){
                        toastr.error('please enter a valid date,you know what a valid date is?');
                        $('.new-provider-form-submit').removeAttr('disabled', 'disabled').addClass('pointer');
                        $('.submit-indicator').empty();
                        cleanCallback();
                        return;
                    }
                    var fd = new FormData();
                    var name = $('#provider-name').val().trim();
                    fd.append('name', name);
                    fd.append('display_name', $('#provider-display-name').val().trim());

                    if($('#provider-campaign-start').val() != null && $('#provider-campaign-start').val() != '')
                        fd.append('campaign_start',$('#provider-campaign-start').val());
                    else
                        fd.append('campaign_start', null);


                    if($('#provider-campaign-end').val() != null && $('#provider-campaign-end').val() != '')
                        fd.append('campaign_end', $('#provider-campaign-end').val());
                    else
                        fd.append('campaign_end', null);

                    fd.append('active', $('#provider-active').is(':checked') ? "yes" : "no");
                    console.log($('#provider-campaign-start').val());
                    console.log($('#provider-campaign-end').val());

                    var provider_twitter_link = $('#provider-twitter-link').val().trim();
                    if (provider_twitter_link != null && provider_twitter_link != '') {
                        fd.append('twitter_link', provider_twitter_link);
                    }

                    var provider_fb_link = $('#provider-fb-link').val().trim();
                    if (provider_fb_link != null && provider_fb_link != '') {
                        fd.append('fb_link', provider_fb_link);
                    }

                    var provider_slug_url = $('#provider-slug-url').val().trim();
                    if (provider_slug_url != null && provider_slug_url != '') {
                        fd.append('slug_url',slugify(provider_slug_url));
                    } else {
                        fd.append('slug_url',slugify(name));
                    }

                    var status = "incomplete";
                    var essentials = ['name', 'display_name'];
                    /* If warnings object is empty */
                    if (canBeStaged(warnings, essentials) && $('#provider-campaign-start').val() != '' && $('#provider-campaign-start').val() != null ) {
                        status = "staged";
                    }

                    fd.append("status", status);
                    fd.append('user_name', current_active_user['name']);
                    fd.append('user_email', current_active_user['email']);

                    $('.new-provider-form-submit').attr('disabled', 'disabled').removeClass('pointer');
                    $('.submit-indicator').append($('.m-preloader').addClass('preloader-copy').clone().show());

                    $.ajax({
                        url: BASE_URL + 'provider/keep/',
                        method: 'POST',
                        data: fd,
                        processData: false,
                        contentType: false
                    }).done(function(result) {
                        toastr.success('Provider added successully');
                        toastr.info('Redirecting to Provider display page');
                        setTimeout(
                            function() {
                                page('/provider/' + result['storage_id']);
                            }, 2000);
                    }).fail(function(data) {
                        console.log(data);
                        showError(data);
                    }).always(function() {
                        $('.new-provider-form-submit').removeAttr('disabled', 'disabled').addClass('pointer');
                        $('.submit-indicator').empty();
                        cleanCallback();
                    });

                }

            }, formValidationViewUpdate)

            new_provider_form_validator.fortify();
        });


        page('/content/:id/edit', function(ctx, next) {
            var content_id = ctx.params.id;
            $('.page-content').empty().append($('.spin').clone().addClass('spin-clone').show());
            $('.page-content').append($('.retry-prototype').clone().addClass('content-fetch-retry'));
            fetchData('content/fetch/' + content_id, prepareContentEditForm);
            $('.content-fetch-retry').click(function() {
                fetchData('content/fetch/' + content_id, prepareContentEditForm);
            })
        });
        page('/content/:id', function(ctx, next) {
            var content_id = ctx.params.id;
            $('.page-content').empty().append($('.spin').clone().addClass('spin-clone').show());
            $('.page-content').append($('.retry-prototype').clone().addClass('content-fetch-retry'));
            fetchData('content/fetch/' + content_id, renderContentView);
            $('.content-fetch-retry').click(function() {
                $('.content-fetch-retry').hide();
                $('.spin-clone').show();
                fetchData('content/fetch/' + content_id, renderContentView);
            });
        });

        page('/channel/:id', function(ctx, next) {
            var channel_id = ctx.params.id;
            $('.page-content').empty().append($('.spin').clone().addClass('spin-clone').show());
            $('.page-content').append($('.retry-prototype').clone().addClass('channel-fetch-retry'));
            fetchData('channel/fetch/' + channel_id, renderChannelView);
            $('.channel-fetch-retry').click(function() {
                fetchData('channel/fetch/' + channel_id, renderChannelView);
            })
        });

        page('/tag/:id', function(ctx, next) {
            var tag_id = ctx.params.id;
            $('.page-content').empty().append($('.spin').clone().addClass('spin-clone').show());
            $('.page-content').append($('.retry-prototype').clone().addClass('tag-fetch-retry'));
            fetchData('tag/fetch/' + tag_id, renderTagView);
            $('.tag-fetch-retry').click(function() {
                fetchData('tag/fetch/' + tag_id, renderTagView);
            })
        });

        page('/provider/:id', function(ctx, next) {
            var provider_id = ctx.params.id;
            $('.page-content').empty().append($('.spin').clone().addClass('spin-clone').show());
            $('.page-content').append($('.retry-prototype').clone().addClass('provider-fetch-retry'));
            fetchData('provider/fetch/' + provider_id, renderProviderView);
            $('.provider-fetch-retry').click(function() {
                fetchData('provider/fetch/' + provider_id, renderProviderView);
            })
        });

        page('/sponsor/:id', function(ctx, next) {
            var sponsor_id = ctx.params.id;
            $('.page-content').empty().append($('.spin').clone().addClass('spin-clone').show());
            $('.page-content').append($('.retry-prototype').clone().addClass('sponsor-fetch-retry'));
            fetchData('sponsor/fetch/' + sponsor_id, renderSponsorView);
            $('.sponsor-fetch-retry').click(function() {
                fetchData('sponsor/fetch/' + sponsor_id, renderSponsorView);
            })
        })


        page('/channel/:id/edit', function(ctx, next) {
            var channel_id = ctx.params.id;
            $('.page-content').empty().append($('.spin').clone().addClass('spin-clone').show());
            $('.page-content').append($('.retry-prototype').clone().addClass('channel-fetch-retry'));
            fetchData('channel/fetch/' + channel_id, prepareChannelEditForm);
            $('.channel-fetch-retry').click(function() {
                $('.channel-fetch-retry').hide();
                $('.spin-clone').show();
                fetchData('channel/fetch/' + channel_id, prepareChannelEditForm);
            })
        });

        page('/tag/:id/edit', function(ctx, next) {
            var tag_id = ctx.params.id;
            $('.page-content').empty().append($('.spin').clone().addClass('spin-clone').show());
            $('.page-content').append($('.retry-prototype').clone().addClass('tag-fetch-retry'));
            fetchData('tag/fetch/' + tag_id, prepareTagEditForm);
            $('.tag-fetch-retry').click(function() {
                $('.tag-fetch-retry').hide();
                $('.spin-clone').show();
                fetchData('tag/fetch/' + tag_id, prepareTagEditForm);
            })
        });

        page('/provider/:id/edit', function(ctx, next) {
            var provider_id = ctx.params.id;
            $('.page-content').empty().append($('.spin').clone().addClass('spin-clone').show());
            $('.page-content').append($('.retry-prototype').clone().addClass('provider-fetch-retry'));
            fetchData('provider/fetch/' + provider_id, prepareProviderEditForm);
            $('.provider-fetch-retry').click(function() {
                $('.provider-fetch-retry').hide();
                $('.spin-clone').show();
                fetchData('provider/fetch/' + provider_id, prepareProviderEditForm);
            })
        });

        page('/sponsor/:id/edit', function(ctx, next) {
            var sponsor_id = ctx.params.id;
            $('.page-content').empty().append($('.spin').clone().addClass('spin-clone').show());
            $('.page-content').append($('.retry-prototype').clone().addClass('sponsor-fetch-retry'));
            fetchData('sponsor/fetch/' + sponsor_id, prepareSponsorEditForm);
            $('.sponsor-fetch-retry').click(function() {
                $('.sponsor-fetch-retry').hide();
                $('.spin-clone').show();
                fetchData('sponsor/fetch' + sponsor_id, prepareSponsorEditForm);
            })
        });

        page('/content/', function(ctx, next) {
            /*var template = $('#content-view-template').html();
            var compiled_template = Hogan.compile(template);*/
            var compiled_template = templates['content-view-template'];
            $('.page-content').html(compiled_template.render());
            $('#media-preview-modal').html($('#media-preview-template').html());
            $(document).off('click', '.media-previewer');
            $(document).on('click', '.media-previewer', function() {
                var name = $(this).data('name');
                var link = $(this).data('link');
                $('#media-preview-modal').find('.media-title').html(name);
                var link_fragments = link.split('.');
                var ext = link_fragments[link_fragments.length - 1]
                if (ext.toLowerCase() == 'mp3') {
                    $('#media-preview-modal').find('.video-preview').hide();
                    $('#media-preview-modal').find('.audio-preview').show();
                    $('#media-preview-modal').find('#audio-player').attr('src', link).load();
                    $('#media-preview-modal').find('#audio-player').off().on('error', function() {
                        toastr.error('Unable to preview media. Probably some error occured ');
                        $('.modal-close').click();
                    })
                } else {
                    $('#media-preview-modal').find('.audio-preview').hide();
                    $('#media-preview-modal').find('.video-preview').show();
                    $('#media-preview-modal').find('#video-player').attr('src', link).load();
                    $('#media-preview-modal').find('#video-player').off().on('error', function() {
                        toastr.error('Unable to preview media. Probably some error occured ');
                        $('.modal-close').click();
                    });
                }
                $('.media-preview-trigger').click();
            });


            $('.modal-trigger').off().leanModal();


            $('.content-fetch-retry').click(function() {
                    catchRain(populateContentTable, 'content', '#content-list');
                });
                /* adding loader */
            catchRain(populateContentTable, 'content', '#content-list');
        });

        page('/channel/', function(ctx, next) {
            /*var template = $('#channel-view-template').html();
            var compiled_template = Hogan.compile(template);*/
            var compiled_template = templates['channel-view-template'];
            $('.page-content').html(compiled_template.render());

            $('.channel-fetch-retry').click(function() {
                    catchRain(populateTable, 'channel', '#channel-list');
                })
                /* adding loader */
                // catchChannelRain();
            catchRain(populateTable, 'channel', '#channel-list');
        });

        page('/sponsor/', function(ctx, next) {
            /*var template = $('#sponsor-view-template').html();
            var compiled_template = Hogan.compile(template);*/
            var compiled_template = templates['sponsor-view-template'];
            $('.page-content').html(compiled_template.render());

            $('.sponsor-fetch-retry').click(function() {
                    catchRain(populateTable, 'sponsor', '#sponsor-list');
                })
                /* adding loader */
            catchRain(populateTable, 'sponsor', '#sponsor-list');
        });

        page('/tag/', function(ctx, next) {
            /*var template = $('#tag-view-template').html();
            var compiled_template = Hogan.compile(template);*/
            var compiled_template = templates['tag-view-template'];
            $('.page-content').html(compiled_template.render());

            $('.tag-fetch-retry').click(function() {
                    catchRain(populateTagTable, 'tag', '#tag-list');
                });
            catchRain(populateTagTable, 'tag', '#tag-list');
        });

        page('/provider/', function(ctx, next) {
            /*var template = $('#provider-view-template').html();
            var compiled_template = Hogan.compile(template);*/
            var compiled_template = templates['provider-view-template'];
            $('.page-content').html(compiled_template.render());

            $('.provider-fetch-retry').click(function() {
                catchRain(populateProviderTable, 'provider', '#provider-list');
            });
            catchRain(populateProviderTable, 'provider', '#provider-list');
        });

        page('/raw-content/', function(ctx, next) {
            /*var template = $('#raw-content-view-template').html();
            var compiled_template = Hogan.compile(template);*/
            var compiled_template = templates['raw-content-view-template'];
            $('.page-content').html(compiled_template.render());

            $('#media-preview-modal').html($('#media-preview-template').html());
            $('.modal-trigger').off().leanModal();

            $('.raw-content-fetch-retry').click(function() {
                catchRain(populateRawContentTable, 'raw-content', '#raw-content-list');
            });
            /* adding loader */
            // catchChannelRain();
            catchRain(populateRawContentTable, 'raw-content', '#raw-content-list');

            /* biniding content remove handler */
            $(document).off('click', '.remove-raw-content');
            $(document).on('click', '.remove-raw-content', function() {
                console.log('removing');
                var table = $('#raw-content-list').DataTable();
                var raw_content_id = $(this).data('id');
                var that = this;
                $.ajax({
                    url: BASE_URL + 'raw-content/unhook/' + raw_content_id,
                    method: 'POST'
                }).done(function(data) {
                    toastr.success('Raw content successully deleted ');
                    table.row($(that).parents('tr')).remove().draw();
                }).fail(function() {
                    toastr.error('Opps something weird just happened. But look we are not embarrassed ');
                });
            });

            $(document).off('click', '.raw-media-preview-trigger');
            $(document).on('click', '.raw-media-preview-trigger', function() {
                var name = $(this).data('name');
                var link = $(this).data('link');
                var type = $(this).data('type');
                $('#media-preview-modal').find('.media-title').html(name);
                var link_fragments = link.split('.');
                var ext = link_fragments[link_fragments.length - 1]
                if (type == 'audio') {
                    $('#media-preview-modal').find('.video-preview').hide();
                    $('#media-preview-modal').find('.audio-preview').show();
                    $('#media-preview-modal').find('#audio-player').attr('src', link).load();
                    $('#media-preview-modal').find('#audio-player').off().on('error', function() {
                        toastr.error('Unable to preview media. Probably some error occured ');
                        $('.modal-close').click();
                    })
                } else {
                    $('#media-preview-modal').find('.audio-preview').hide();
                    $('#media-preview-modal').find('.video-preview').show();
                    $('#media-preview-modal').find('#video-player').attr('src', link).load();
                    $('#media-preview-modal').find('#video-player').off().on('error', function() {
                        toastr.error('Unable to preview media. Probably some error occured ');
                        $('.modal-close').click();
                    })
                }

                $('.preview-media').click();

            });

        });

        page('/version/new', function(ctx, next) {
            $.ajaxq.clear('version_related_data');
            last_fetched_version = null;
            last_fetched_version_id = null;
            top_videos = [];
            var template = $('#create-version-view');
            /* clearing selected content */
            // selected_content['data'] = [];

            //var compiled_template = Hogan.compile(template);
            $('.page-content').html(template.html());
            /* Replacing heading */
            sortableInstances = {};
            mainSortableInstance = null;
            topVideoSortableInstance = null;
            mainCollectionSortableInstance = null;

            /* object to hold info about selected channels*/
            insertedChannels = {};
            $('.spinner-section').append($('.spin').clone().addClass('spin-clone').show());
            hideSelectContainers();

            //$('.top-content-select-container').show();
            initializeVersionMakerComponents();
            /* creatint sortable on phone display */
            mainSortableInstance = Sortable.create($('.channel-list')[0], {
                group: "sorting",
                sort: true
            });
            //currently not keeping track of top videos
            /*topVideoSortableInstance = Sortable.create($('.top-videos-list')[0], {
                group: "sorting",
                sort: true
            });*/
            mainFeedSortableInstance = Sortable.create($('.feed-list')[0], {
                group: "sorting",
                sort: true
            });

            mainCollectionSortableInstance = Sortable.create($('.maincollection-list')[0], {
                group: "sorting",
                sort: true
            })
            /* binding click handler to push version */
            $('.version-update-button').click(function() {

                var version_data = getSimulatorData();
                if ('error' in version_data) {
                    return;
                }
                var version_type = $('.version-type').find('select').val();
                if (version_type == null) {
                    toastr.error('Please choose type for this version');
                    return;
                }

                if(!(alreadyInserted['channel'].length) || !(alreadyInserted['feed'].length) || !(alreadyInserted['collection'].length)){
                    if(!(alreadyInserted['channel'].length)){
                        toastr.error('there ane no channels in channel list');
                    } else if(!(alreadyInserted['feed'].length)){
                        toastr.error('i dont think new feed should be empty');
                    } else {
                        toastr.error('there are no collections in collection list');
                    }
                    return;
                }
                var empty_collection = 0;
                $('.maincollection-content-preview-container').not('.prototype').each(function(index){
                    var content_list = $(this).find('.content-list');
                    if($(content_list).children().length == 0){
                        empty_collection++;
                    }
                });

                if(!updateFeedElementCount(null,null,'getstatus')){
                    if (confirm('few feed counts are not met,you still want to push version?')) {
                    } else{
                        return;
                    }
                }

                if(empty_collection > 0){
                    toastr.error('few collection are empty please check');
                    return;
                }

                var empty_channel = 0;
                $('.content-preview-container').not('.prototype').each(function(index){
                    var content_list = $(this).find('.content-list');
                    if($(content_list).children().length == 0){
                        empty_channel++;
                    }
                });
                if(empty_channel > 0){
                    toastr.error('few channels are empty please check');
                    return;
                }

                var version_description = $('#version_description').val();
                if(version_description.length > 100){
                    version_description = version_description.substring(0,100);
                }

                var request_data = {
                    'environment': $('.version-type').find('select').val(),
                    'version_data': version_data,
                    'description': version_description,
                    'user_name': current_active_user['name'],
                    'user_email': current_active_user['email']
                }
                $('.version-update-button').attr('disabled', 'disabled').removeClass('pointer');
                $('.submit-indicator').empty().html($('.m-preloader').addClass('preloader-copy').clone().show());
                //var data_string = JSON.stringify(request_data);
                // TODO remove
                /*var xhr = new XMLHttpRequest;
                xhr.open('POST', '/', true);
                xhr.send('');
                return;*/
                console.log(request_data);
                //last_fetched_version = request_data;
                $.ajax({
                    url: BASE_URL + 'version/keep/',
                    data: JSON.stringify(request_data),
                    method: 'POST',
                    contentType: 'application/json'
                }).done(function(data) {
                    toastr.success('Congratulations! New content version has been uploaded');
                    console.log(data);
                    last_fetched_version = request_data;
                    last_fetched_version_id = data['storage_id'];
                    /* calling file transfer script */
                    /*$.ajax({
                        url: BASE_URL + 'util/get_json',
                        method: 'GET'
                    }).done(function() {
                        toastr.success('version updated succesfully');
                    });*/
                    toastr.info('Redirecting to version display page');
                    setTimeout(
                        function() {
                            page('/version/' + data['storage_id']);
                        }, 5000);
                }).fail(function(data) {
                    last_fetched_version = null;
                    last_fetched_version_id = null;
                    showError(data);
                }).always(function() {
                    $('.version-update-button').removeAttr('disabled').addClass('pointer');
                    $('.submit-indicator').empty()
                })
            });
            $('.retry-version').hide();
            //$('.version-options').hide();
            $('.wait-section').hide();
            $('.version-creator').show();
            $('.version-edit-button').hide();
        });

        page('/version/:id', function(ctx, next) {
            $.ajaxq.clear('version_related_data');
            var version_id = ctx.params.id;
            var template = $('#create-version-view');
            insertedChannels = {};
            $('.page-content').html(template.html());
            $('.page-content').find('.heading').html('Content Version-' + version_id);
            $('.spinner-section').append($('.spin').clone().addClass('spin-clone').show());

            //insert template for content info on content click
            $('#version-content-info-modal').html($('#version-content-info-template').html());

            $('.preview-version-content').off().leanModal();

            $('.content-select-container').hide();
            $('.sponsor-select-container').hide();
            $('.version-update-button').hide();
            $('.version-edit-button').addClass('hyperlink').attr('data-url','/version/'+ version_id + '/edit');

            $('.retry-version').hide();
            $('.version-options').hide();

            $('.wait-text').html('Fetching data for previous version');
            $('.wait-section').show();

            if(last_fetched_version_id == version_id){
                console.log(last_fetched_version);
                toastr.success('Version data fetched ');
                var data = last_fetched_version;
                $('#version_description').val(data['description']);
                renderPreviousContentVersion(data['version_data'], data['environment'],'display',version_id);
            } else{
                $.ajax({
                    url: BASE_URL + 'version/fetch/' + version_id,
                    method: 'GET'
                }).done(function(data) {
                    console.log(data);
                    toastr.success('Version data fetched ');
                    last_fetched_version_id = version_id;
                    last_fetched_version = data.data;
                    $('#version_description').val(data['data']['description']);
                    renderPreviousContentVersion(data['data']['version_data'], data['data']['environment'],'display',version_id);
                }).fail(function(data) {
                    console.log(data);
                    showError(data);
                    $('.wait-section').hide();
                });
            }
            $('.retry-version').data('url','/version/' + version_id);
        });

        page('/version/:id/edit', function(ctx, next) {
            $.ajaxq.clear('version_related_data');
            var version_id = ctx.params.id;
            var template = $('#create-version-view');
            insertedChannels = {};
            $('.page-content').html(template.html());
            $('.page-content').find('.heading').html('Edit Version-' + version_id);
            $('.spinner-section').append($('.spin').clone().addClass('spin-clone').show());
            $('#version-unapproved-element-modal').html($('#version-unapproved-element-modal-template').html());
            $('.version-unapproved-element-modal-click').off().leanModal();
            hideSelectContainers();
            $('.version-update-button').show();

            $('.retry-version').hide();
            $('.version-options').hide();

            $('.wait-text').html('Fetching data for previous version');
            $('.wait-section').show();
            /* fetching previous versioin data */

            if(last_fetched_version != null && last_fetched_version_id == version_id){
                toastr.success('Version data fetched');
                var data = last_fetched_version;
                $('#version_description').val(data['description']);
                renderPreviousContentVersion(data['version_data'], data['environment'], 'edit',version_id);
                makeSimulatorEditable(data['version_data'], data['environment']);
            }else {
                $.ajax({
                    url: BASE_URL + 'version/fetch/' + version_id,
                    method: 'GET'
                }).done(function(data) {
                    last_fetched_version = data.data;
                    last_fetched_version_id = version_id;
                    toastr.success('Version data fetched ');
                    console.log(data['data']['version_description']);
                    $('#version_description').val(data['data']['description']);
                    renderPreviousContentVersion(data['data']['version_data'], data['data']['environment'], 'edit',version_id);
                    makeSimulatorEditable(data['data']['version_data'], data['data']['environment']);
                }).fail(function(data) {
                    console.log(data);
                    showError(data);
                    $('.wait-section').hide();
                }).always(function(){
                    $('.submit-indicator').empty();
                });
            }

            /* binding click handler to push version */
            $('.version-update-button').click(function() {
                var version_data = getSimulatorData();
                if ('error' in version_data) {
                    return;
                }
                var version_type = $('.version-type').find('select').val();
                if (version_type == null) {
                    toastr.error('Please choose type for this version');
                    return;
                }

                if(!(alreadyInserted['channel'].length) || !(alreadyInserted['feed'].length) || !(alreadyInserted['collection'].length)){
                    if(!(alreadyInserted['channel'].length)){
                        toastr.error('there ane no channels in channel list');
                    } else if(!(alreadyInserted['feed'].length)){
                        toastr.error('i dont think feed should be empty');
                    } else {
                        toastr.error('there are no collections in collection list');
                    }
                    return;
                }
                var empty_collection = 0;
                $('.maincollection-content-preview-container').not('.prototype').each(function(index){
                    var content_list = $(this).find('.content-list');
                    if($(content_list).children().length == 0){
                        empty_collection++;
                    }
                });
                if(empty_collection > 0){
                    toastr.error('few collection are empty please check');
                    return;
                }

                var empty_channel = 0;
                $('.content-preview-container').not('.prototype').each(function(index){
                    var content_list = $(this).find('.content-list');
                    if($(content_list).children().length == 0){
                        empty_channel++;
                    }
                });
                if(empty_channel > 0){
                    toastr.error('few channels are empty please check');
                    return;
                }

                if(!updateFeedElementCount(null,null,'getstatus')){
                    if (confirm('few feed counts are not met you still want to push?')) {
                    } else{
                        return;
                    }
                }

                var version_description = $('#version_description').val();
                if(version_description.length > 100){
                    version_description = version_description.substring(0,100);
                }

                var request_data = {
                    'environment': $('.version-type').find('select').val(),
                    'version_data': version_data,
                    'description': version_description,
                    'parent': version_id,
                    'user_name': current_active_user['name'],
                    'user_email': current_active_user['email']
                }
                $('.version-update-button').attr('disabled', 'disabled').removeClass('pointer');
                $('.submit-indicator').empty().html($('.m-preloader').addClass('preloader-copy').clone().show());
                console.log(request_data);
                //return;
                $.ajax({
                    url: BASE_URL + 'version/keep/',
                    data: JSON.stringify(request_data),
                    method: 'POST',
                    contentType: 'application/json'
                }).done(function(data) {
                    toastr.success('Congratulations! New content version has been uploaded');
                    console.log(data);
                    last_fetched_version = request_data;
                    last_fetched_version_id = data['storage_id'];
                    /* calling file transfer script */
                    /*$.ajax({
                        url: BASE_URL + 'util/get_json',
                        method: 'GET'
                    }).done(function() {
                        toastr.success('version updated succesfully');
                    });*/
                    toastr.info('Redirecting to version display page');
                    setTimeout(
                        function() {
                            page('/version/' + data['storage_id']);
                        }, 2000);
                }).fail(function(data) {
                    console.log(data);
                    showError(data);
                }).always(function() {
                    $('.version-update-button').removeAttr('disabled').addClass('pointer');
                    $('.submit-indicator').empty();
                })
            });

        })

        page('/version/', function(ctx, next) {
            /*var template = $('#version-view-template').html();
            var compiled_template = Hogan.compile(template);*/
            var compiled_template = templates['version-view-template'];
            $('.page-content').html(compiled_template.render());
            $('.version-publish-modal').html($('#version-publish-template').html());

            $('.version-fetch-retry').click(function() {
                    catchRain(populateVersionTable, 'version', '#version-list');
                })
                /* adding loader */
            catchRain(populateVersionTable, 'version', '#version-list');

        });


        /* define page routes */
        page('/', function(ctx, next) {
            console.log('initial content');
            renderHome();
        });

        page('*', function(ctx, next) {
                /*var template = $('#page-404').html();
                var compiled_template = Hogan.compile(template);*/
                var compiled_template = templates['page-404'];
                $('.page-content').html(compiled_template.render());
            })
            /* initializing  pagejs router setup */
        page();
        /* TODO remove */
        /*$('.main-container').show();
        $('.logout-button').show();
        $('.signin-button-container').hide();
        $('.loader-container').hide();*/

        $(document).on('guser-changed', function(event, userState) {
            /* TODO : remove*/
            //return;
            if (userState['status']) {
                auth = userState['currentUserState']['auth'];
                current_active_user['email'] = userState['currentUserState']['email'];
                current_active_user['name'] = userState['currentUserState']['user_name'];
                /* checking if user is allowed */
                $.get(window.location.origin + "/util/identify?email=" + userState['currentUserState']['email']).done(function(data) {
                    user_key = data['user_key'];
                    if (data['authorised']) {
                        $('.signin-button-container').hide();
                        console.log('Welcome : ' + userState['currentUserState']['user_name']);
                        /*Todo find a place to show this */
                        //$('.greeting-container').html(getGreeting() + userState['currentUserState']['user_name'] + '!');
                        $('.main-container').show();
                        //renderHome();
                        $('.logout-button').show();
                        /* setting signuout event listener */
                        $('.logout-button').off('click').on('click', function() {
                            cleanLoggedInState(auth);
                        });
                    } else {
                        cleanLoggedInState(auth);
                        toastr.error('Unknown error occured');
                    }
                }).fail(function(data) {
                    cleanLoggedInState(auth);
                    toastr.error('You did\'nt login with valid pressplay email id. Please try again.');
                });
            } else {
                cleanLoggedInState(auth);
            }
            $('.loader-container').hide();
        });
    });


    /* setting up handler for hyperlinks */
    $(document).on('click', '.hyperlink', function() {
        var url = $(this).data('url');
        /* navigating to url of div */
        page(url);
    });


})(document)
