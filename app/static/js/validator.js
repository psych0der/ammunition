'use strict';

/**
 * Form validator class. Accepts dom selectors to apply form validation
 * @param {[type]} config [description]
 */
var Validator = function(config, submit, callback, updateView) {
        this.config = config;
        this.errors = {};
        this.warnings = {};
        this.submitSelector = submit;
        this.callback = callback;
        this.updateView = updateView;
        var filterFunctions = {};

        this.setFilterFunction = function(key, value) {
            filterFunctions[key] = value;
        }

        this.getFilterFunction = function(key, value) {
            return filterFunctions[key]
        }
    }
    /**
     * Routine to return error or warning message based on error_type and message type
     * @param  String   validation_status Result of validation ('error' or 'warning')
     * @param  String   message_key       Error type
     * @param  String   target_key        config object key of target element on which validation is being run [OPTIONAL]
     * @return String                     Message string based on event type and message type
     */
Validator.prototype.getValidationMessage = function(validation_status, message_key, target_key) {
    if (['errors', 'warnings'].indexOf(validation_status) < 0) {
        throw 'Validatr : Invalid valdation status';
    }

    if (['empty', 'invalid'].indexOf(message_key) < 0) {
        throw 'Validatr : Invalid message type';
    }

    var messages = {
        'warnings': {
            'empty': '',
            'invalid': 'Invalid value or format'
        },
        'errors': {
            'empty': '',
            'invalid': 'Invalid value or format'
        }
    }

    var message = '';
    var target_key = target_key || '';

    if (validation_status in this.config[target_key] && message_key in this.config[target_key][validation_status]) {
        message = this.config[target_key][validation_status][message_key];
    } else if (message_key in messages[validation_status]) {
        message = messages[validation_status][message_key];
    } else {
        throw 'Validatr : Invalid error message key'
    }

    /*if (target_key != '') {
        message = target_key + ' : ' + message;
    }*/

    return message;
}


Validator.prototype.fortify = function() {

    var that = this; // saving context of this validator
    /* iterating over keys in config */
    for (var key in that.config) {
        /* creating errors for this key */
        that.errors[key] = [];
        that.warnings[key] = [];
        var selector = '';

        /* creating jquery DOM selector */
        if ('class' in that.config[key]) {
            selector = '.' + that.config[key]['class'];
        } else if ('id' in that.config[key]) {
            selector = '#' + that.config[key]['id'];
        } else {
            throw '[VALIDATR] : No or invalid DOM selector passed';
        }

        var required = ('required' in that.config[key]) ? that.config[key]['required'] : false;

        /* overriding update view function */
        var updateView = that.updateView;
        if ('view_update' in that.config[key] && typeof that.config[key]['view_update'] === 'function')
            updateView = that.config[key]['view_update'];


        /* creating filter function */
        if ('regex' in that.config[key]) {
            that.setFilterFunction(key, (function(that, key, selector, required, updateView) {
                return function(val) {
                    /* emptying previous error and warning */
                    that.errors[key] = '';
                    that.warnings[key] = '';                    
                    /* if field is empty */
                    if (!val.length) {
                        if (required) {
                            var error_message = that.getValidationMessage('errors', 'empty', key);                                                        
                            delete that.warnings[key];
                            that.errors[key] = error_message;
                            updateView(selector, 'error', error_message);
                            return;
                        } else {
                            var warning_message = that.getValidationMessage('warnings', 'empty', key);
                            delete that.errors[key];
                            that.warnings[key] = warning_message;
                            updateView(selector, 'warning', warning_message);
                            return;
                        }
                    }
                    if (that.config[key]['regex'] != '') {
                        if (!(new RegExp(that.config[key]['regex']).test(val))) {
                            var error_message = that.getValidationMessage('errors', 'invalid', key);
                            delete that.warnings[key];
                            that.errors[key] = error_message;
                            updateView(selector, 'error', error_message);
                            return;

                        } else {
                            /* success*/
                            delete that.errors[key];
                            delete that.warnings[key];
                            updateView(selector, 'valid');
                        }

                    } else {
                        /* success*/
                        delete that.errors[key];
                        delete that.warnings[key];
                        updateView(selector, 'valid');

                    }
                }
            })(that, key, selector, required, updateView));

        } else if ('custom_validator' in that.config[key] && typeof that.config[key]['custom_validator'] === 'function') {
            that.setFilterFunction(key, (function(that, key, selector, required, updateView) {
                return function(val) {                  
                    var result = that.config[key]['custom_validator'](val, required);
                    if (!('status' in result)) {
                        throw 'Validatr : status key not found in result';
                    }
                    if (result['status'] == 'error') {
                        delete that.warnings[key];
                        that.errors[key] = result['message'];
                        updateView(selector, 'error', result['message']);
                    } else if (result['status'] == 'warning') {
                        delete that.errors[key];
                        that.warnings[key] = result['message'];
                        updateView(selector, 'warning', result['message']);
                    } else if (result['status'] == 'success') {
                        delete that.errors[key];
                        delete that.warnings[key];
                        updateView(selector, 'valid');
                    }
                }
            })(that, key, selector, required, updateView));
        }

        /* binding event handler */
        $(selector).on('change', (function(key) {
            return function() {
                var val = $(this).val();
                var filterFunction = that.getFilterFunction(key);
                filterFunction(val);
            }
        }(key)));
    }
    /* attaching submit handler on submit button */
    $(that.submitSelector).click(function() {
        /* iterating over elements and validation each one of them */
        if($(this).attr('executing') == 'true'){
            toastr.error("please wait while your request is being processed");
            return;
        }

        $(this).attr('executing','true');
        for (var key in that.config) {
            var selector = '';
            /* creating jquery DOM selector */
            if ('class' in that.config[key]) {
                selector = '.' + that.config[key]['class'];
            } else if ('id' in that.config[key]) {
                selector = '#' + that.config[key]['id'];
            } else {
                throw '[VALIDATR] : No or invalid DOM selector passed';
            }
            var val = $(selector).val();
            var filterFunction = that.getFilterFunction(key);
            /* calling validation function */
            filterFunction(val);
        }

        /* calling the success click callback */
        that.callback(that.errors, that.warnings,function(){
            $(that.submitSelector).attr('executing','false');
        });
    });

}
