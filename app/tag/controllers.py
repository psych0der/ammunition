"""
    Controller to dictate working of __URL__/tag/ and all associated APIs.
"""
from flask import Blueprint, request, g
from app.commons.CORS import crossdomain
from app.commons.Toolkit import respond, get_timestamp
import models
import app.commons.modules as modules
from werkzeug.datastructures import CombinedMultiDict, MultiDict

# Setting up access to this controller under '<OUR_AWESOME_DOMAIN>/tag/*'
api = Blueprint('tag', __name__, url_prefix='/tag')

@api.route('/', methods=['POST', 'GET'])
#@crossdomain(origin='*')
def base():
    """
        The base method for this controller does not need to do anything.
        A list of methods is possible, but might make us vulnerable.
        So we just do something fun for the time being.
    """
    response = {
        'message': "We don't make coffee. How about an XKCD instead?",
        'alternative': 'https://c.xkcd.com/random/comic/',
        'status': "It's complicated",
        'http_status': 418
    }

    # Returning with the joke 418 status code, because why not
    return respond(response)

@api.route('/fetch/<tag_id>', methods=['GET'])
#@crossdomain(origin='*')
def fetch(tag_id):
    response = modules.tag.fetch(tag_id, g.incoming)
    return respond(response)

@api.route('/keep/', methods=['POST'])
@api.route('/keep/<tag_id>', methods=['POST'])
#@crossdomain(origin='*')
def keep(tag_id=None):
    response = modules.tag.keep(tag_id, g.incoming, g.incoming_files)
    return respond(response)

@api.route('/rain', methods=['GET'])
#@crossdomain(origin='*')
def rain():
    response = modules.tag.rain(g.incoming)
    return respond(response)

@api.route('/datatable-rain', methods=['GET'])
#@crossdomain(origin='*')
def datatable_rain():
    response = modules.tag.datatable_rain(g.incoming)
    return respond(response)


@api.route('/datarain/<tag_id>', methods=['GET'])
#@crossdomain(origin='*')
def data_rain(tag_id):
    version_id =  request.args.get('version_id')
    response = modules.tag.data_rain(tag_id, version_id)
    return respond(response)

@api.route('/batch_fetch',methods=['GET'])
def batch_fetch():
    id_csv = request.args.get('id')
    field_csv = request.args.get('field')
    response = modules.collection.batch_fetch(field_csv, id_csv)
    return respond(response)

@api.route('/slugify',methods=['GET'])
def check_slug():
    slug_url = request.args.get('value')
    response = modules.collection.check_slug(slug_url)
    return respond(response)
