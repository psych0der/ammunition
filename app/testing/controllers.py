"""
    Controller to dictate working of __URL__/testing/ and all associated APIs and pages.
"""
from flask import Blueprint, render_template, request,g
from app.commons.CORS import crossdomain
from app.commons.Toolkit import respond, get_timestamp
from app.commons.resources import redis_cli, putio_client
import app.commons.modules as modules
from werkzeug import secure_filename
import models
import redis
import putio
import app.commons.modules as modules
import config
import pathlib
import os

# Setting up access to this controller under '<OUR_AWESOME_DOMAIN>/testing/*'
test_api = Blueprint('test_api', __name__, url_prefix='/testing')

@test_api.route('/<template>', methods=['POST', 'GET'])
#@crossdomain(origin='*')
def template_renderer(template=None):
    """
        Test view to render template name provided as path 
    """

    if template is not None:
        template_name = template + '.html'
        path = pathlib.Path(
            config.BASE_DIR + '/app/templates/' + template_name)
        if path.is_file():
            return render_template(template_name)
        else:
            response = {
                'message': "Invalid template name provided. Please try again with valid template name",
                'alternative': 'https://c.xkcd.com/random/comic/',
                'status': "It's complicated",
                'http_status': 418
            }
        # Returning with the joke 418 status code, because why not
        return respond(response)
    else:
        response = {
            'message': "No template provided. Please try again with valid template name",
            'alternative': 'https://c.xkcd.com/random/comic/',
            'status': "It's complicated",
            'http_status': 418
        }
        # Returning with the joke 418 status code, because why not
        return respond(response)


@test_api.route('/download', methods=['POST'])
#@crossdomain(origin='*')
def download_content():
    """
        Content download test view
    """        
    content_url = g.incoming.get('content_url', None)
    content_type = g.incoming.get('content_type', None)
    print g.incoming_files
    if content_type == None :
        return respond({'message' : 'Missing parameters', 'status': 'Nasty', 'http_status' : 419})

    if content_type == 'video':
        if content_url == None:
            return respond({'message' : 'Missing parameters', 'status': 'Nasty', 'http_status' : 419})

        resp = putio_client.Transfer.add_url(content_url, callback_url='http://test-ammo.pressplaytv.in/hooks/putio')
        # converting class to dictionary
        attrs = vars(resp)    
        redis_cli.set('CONTENT_STATUS', 'transfer_started')
        return respond({'message' : 'transfer started', 'status': 'wow', 'putio_resp': attrs, 'http_status' : 200})

    if content_type == 'torrent':        
        torrent_file = request.files['torrent'] if 'torrent' in g.incoming_files else None
        if torrent_file :
            filename = secure_filename(torrent_file.filename)
            torrent_file.save(os.path.join(config.TORRENT_FILE_DIR, filename))
            magneturi = modules.torrent.getMagnetLink(os.path.join(config.TORRENT_FILE_DIR, filename))

            if not magneturi:
                return respond({'message' : 'Please send a valid torrent file', 'status': 'failed', 'http_status' : 419})

            resp = putio_client.Transfer.add_url(magneturi, callback_url='http://test-ammo.pressplaytv.in/hooks/putio')
            attrs = vars(resp)    
            return respond({'message' : 'file stored', 'status': 'wow' ,'putio_resp': attrs,'http_status' : 200})
        else:
            return respond({'message' : 'Please send a valid torrent file', 'status': 'failed', 'http_status' : 419})




