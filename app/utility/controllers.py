"""
    Controller to dictate working of __URL__/util/ and all associated APIs.
"""
from flask import Blueprint, request, g
from app.commons.CORS import crossdomain
from app.commons.Toolkit import respond, get_timestamp
import models
import app.commons.modules as modules
from werkzeug.datastructures import CombinedMultiDict, MultiDict


# Setting up access to this controller under '<OUR_AWESOME_DOMAIN>/util/*'
api = Blueprint('utility', __name__, url_prefix='/util')

@api.route('/', methods=['POST', 'GET'])
#@crossdomain(origin='*')
def base():
    """
        The base method for this controller does not need to do anything.
        A list of methods is possible, but might make us vulnerable.
        So we just do something fun for the time being.
    """
    request_timestamp = get_timestamp()
    response = {
        'message': "We don't make coffee. How about an XKCD instead?",
        'alternative': 'https://c.xkcd.com/random/comic/',
        'status': "It's complicated",
        'http_status': 418
    }

    # Returning with the joke 418 status code, because why not
    return respond(response)

@api.route('/identify', methods=['GET'])
#@crossdomain(origin='*')
def identify():
    response = modules.identify.identify(g.incoming)
    return respond(response)

@api.route('/get_json', methods=['GET'])
#@crossdomain(origin='*')
def get_json():
    response = modules.export_json.handle(g.incoming)
    return respond(response)


@api.route('/fetch_net_image',methods=['POST'])
#@crossdomain(origin='*')
def fetch_net_image():
    response=modules.img_fetch_online.fetch_img(request.form['imageurl'])
    return respond(response)


@api.route('/app_notification', methods=['POST'])
#@crossdomain(origin='*')
def app_notification():
    response = modules.notification.to_all(g.incoming)
    return respond(response)

@api.route('/get-s3-contents/<bucket_name>', methods=['GET'])
def fetch(bucket_name):
    response = modules.upload_s3.get_bucket_contents(bucket_name)
    return respond(response)

@api.route('/get-raw-contents/<content_type>', methods=['GET'])
def get_raw_contents(content_type):
    response = modules.content.get_raw_content_list(content_type)
    return respond(response)

@api.route('/procure-content', methods=['POST'])
def procure_content():
    '''
        Procures content by various methods viz. Downloading from youtube,facebook and other media websites,
        Downloading from a torrent, saving content uploaded with to this endpoint.
        Post procurement, content is transferred to S3
    '''
    response = modules.content.procure(g.incoming, g.incoming_files)
    return respond(response)

#@api.route('/downloadvideo', methods=['POST'])
#def download_video():
#    response=modules.img_fetch_online.downloadvideo(request.form['filename'])
#    return respond(response)
@api.route('/box-version/latest',methods=['GET'])
@api.route('/box-version/<version_id>',methods=['GET'])
def get_card_info(version_id=None):
    response=modules.card_info.get_status()
    return respond(response)

@api.route('/box-version/update', methods=['GET'])
def update_rsync_status():
    row_id = request.args.get('row_id')
    response = modules.card_info.update_status(row_id)
    return respond(response)
