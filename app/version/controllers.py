"""
    Controller to dictate working of __URL__/version/ and all associated APIs.
"""
from flask import Blueprint, request, g
from app.commons.CORS import crossdomain
from app.commons.Toolkit import respond, get_timestamp
import models
import app.commons.modules as modules
from werkzeug.datastructures import CombinedMultiDict, MultiDict
# Setting up access to this controller under '<OUR_AWESOME_DOMAIN>/version/*'
api = Blueprint('version', __name__, url_prefix='/version')

@api.route('/', methods=['POST', 'GET'])
def base():
    """
        The base method for this controller does not need to do anything.
        A list of methods is possible, but might make us vulnerable.
        So we just do something fun for the time being.
    """
    response = {
        'message': "We don't make coffee. How about an XKCD instead?",
        'alternative': 'https://c.xkcd.com/random/comic/',
        'status': "It's complicated",
        'http_status': 418
    }

    # Returning with the joke 418 status code, because why not
    return respond(response)

@api.route('/fetch/', methods=['GET'])
@api.route('/fetch/<version_id>', methods=['GET'])
def fetch(version_id=None):
    response = modules.version.fetch(version_id, g.incoming)
    return respond(response)

@api.route('/fetch/live', methods=['GET'])
def latest_version_id():
    response = modules.version.latest_version_id()
    return respond(response)

@api.route('/keep/', methods=['POST'])
@api.route('/keep/<version_id>', methods=['POST'])
def keep(version_id=None):
    response = modules.version.keep(version_id, g.incoming, g.incoming_files)
    return respond(response)

@api.route('/rain', methods=['GET'])
def rain():
    response = modules.version.rain(g.incoming)
    return respond(response)

@api.route('/datatable-rain', methods=['GET'])
def datatable_rain():
    response = modules.version.datatable_rain(g.incoming)
    return respond(response)

@api.route('/publish/<version_id>', methods=['POST'])
def publish(version_id):
    response = modules.version.publish(version_id)
    return respond(response)

@api.route('/test-publish/<version_id>', methods=['POST'])
def publish_test(version_id,test=True):
    response = modules.version.publish(version_id,True)
    return respond(response)

@api.route('/network/live', methods=['GET'])
def network():
    response = {'http_status':200,
                'status':'success'}
    return respond(response)
