#!/bin/bash

set -e

VERSION_ID=$1
ZIP_LOCATION=$2
BASE_DIR=/home/ubuntu
DEFAULT_FOLDER_PATH="$BASE_DIR"/boxworks
mkdir -p "$DEFAULT_FOLDER_PATH"
echo $HOME

if [ ! -f "$DEFAULT_FOLDER_PATH"/initiator.lock ];then
    touch "$DEFAULT_FOLDER_PATH"/initiator.lock
    DEFAULT_ZIP_PATH="$BASE_DIR"/www/ammunition
    mkdir -p "$DEFAULT_FOLDER_PATH"/json "$DEFAULT_FOLDER_PATH"/others "$DEFAULT_FOLDER_PATH/others_json"
    echo " " > "$DEFAULT_FOLDER_PATH"/initiator.log

    # Write json
    echo "Getting version data..." >> "$DEFAULT_FOLDER_PATH"/initiator.log
    echo "$DEFAULT_FOLDER_PATH"/json/output.json
    echo $VERSION_ID
    python "$DEFAULT_ZIP_PATH"/scripts/getVersionDump.py $VERSION_ID "$DEFAULT_FOLDER_PATH"/json/output.json

    # Convert the json to box format
    echo "Downloading version assets..." >> "$DEFAULT_FOLDER_PATH"/initiator.log
    python "$DEFAULT_ZIP_PATH"/scripts/jsonparser.py -s "$DEFAULT_FOLDER_PATH"/json/output.json -d "$DEFAULT_FOLDER_PATH"/json/new_box.json -m "$DEFAULT_FOLDER_PATH"/others/medialinks.csv -a "$DEFAULT_FOLDER_PATH"/appurl.txt -l "$DEFAULT_FOLDER_PATH"/downloadfiles -A

    # JSON
    OLD_VALUE=`echo $(jq -r '.content_version' "$DEFAULT_FOLDER_PATH"/others_json/loopin.json)`
    LOOPIN_JSON=`cat "$DEFAULT_FOLDER_PATH"/others_json/loopin.json`
    echo $LOOPIN_JSON | sed -e "s/$OLD_VALUE/$VERSION_ID/g" > "$DEFAULT_FOLDER_PATH"/others_json/loopin.json

    PREV_VALUE=`echo $(jq -r '.content_version' "$DEFAULT_FOLDER_PATH"/others_json/pp-content.json)`
    PP_JSON=`cat "$DEFAULT_FOLDER_PATH"/others_json/pp-content.json`
    echo $PP_JSON | sed -e "s/$OLD_VALUE/$VERSION_ID/g" > "$DEFAULT_FOLDER_PATH"/others_json/pp-content.json

    # Download apks
    mkdir -p "$DEFAULT_FOLDER_PATH"/apps
    echo " "
    echo "Downloading apps... " >> "$DEFAULT_FOLDER_PATH"/initiator.log
    #python "$DEFAULT_ZIP_PATH"/scripts/download_apk.py

    echo "Creating complete_content.zip..." >> "$DEFAULT_FOLDER_PATH"/initiator.log

    # Create zip file for content

    cd "$DEFAULT_FOLDER_PATH"/
    zip -r $ZIP_LOCATION/complete_content downloadfiles apps json others_json
    cd
    echo " "
    echo "Removing extra content..." >> "$DEFAULT_FOLDER_PATH"/initiator.log
    echo " "
    rm -rf "$DEFAULT_FOLDER_PATH"/downloadfiles "$DEFAULT_FOLDER_PATH"/apps "$DEFAULT_FOLDER_PATH"/aws_content "$DEFAULT_FOLDER_PATH"/json others_json

    echo "zip successful" >> "$DEFAULT_FOLDER_PATH"/initiator.log
    rm -rf "$DEFAULT_FOLDER_PATH"/initiator.lock
fi
