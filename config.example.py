import os

# Define if the application is running in debug mode
# Expected value = Boolean
DEBUG = True

# Manual configuration for testing environment
# Expected value = Boolean
TEST_ENV = False

# Define port to run the application on
# Expected value = Integer
PORT = 8080

#======================================================================
BASE_DIR = os.path.abspath(os.path.dirname(__file__))
#======================================================================

# Defining PostgreSQL credentials
POSTGRES_CREDS = {
    'USER': 'root',
    'PASSWORD': 'groot',
    'HOST': '127.0.0.1',
    'DATABASE': 'bestdb',
    'ENCODING': 'UTF8'
}

MONGO_CREDS = {
    'USER': 'beaconadmin',
    'PASSWORD': 'TubbySingh',
    'HOST': '52.76.23.63',
    'DATABASE': 'beacon'
    'PORT':'27017'
}

MYSQL_CREDS = {
    'USER': 'root',
    'PASSWORD': 'groot',
    'HOST': '127.0.0.1',
    'DATABASE': 'bestdb',
    'ENCODING': 'UTF8'
}

# Specify default encoding for the application
DEFAULT_ENCODING = 'utf-8'

# Application threads. A common general assumption is
# using 2 per available processor cores - to handle
# incoming requests using one and performing background
# operations using the other.
THREADS_PER_PAGE = 2

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED = True

# Use a secure, unique and absolutely secret key for
# signing the data.
CSRF_SESSION_KEY = "18b49521e895ace6ae5277a87a2ac0f5"

# Secret key for signing cookies
SECRET_KEY = "18b49521e895ace6ae5277a87a2ac0f5"

# AWS Credentials
AWS_CREDS = {
    'KEY': 'OneKey',
    'SECRET': 'NoSecrets'
}

# REDIS CREDENTIAL
REDIS_PASSWORD = 'XXXXXXXXXX'
REDIS_HOST = 'localhost'
REDIS_PORT = '6379'


# PUTIO credentials
PUTIO_CLIENT_ID = "XXXX"
PUTIO_CLIENT_SECRET = "XXXXXXXXXXXX"
PUTIO_OAUTH_TOKEN = "XXXXXXXXXXXXXX"

# Brightcove credentials
BRIGHTCOVE_CLIENT_ID = "XXXXXXXXXXXXXXXXXX"
BRIGHTCOVE_SECRET = "XXXXXXXXXXXXXXXXXXXXX"
BRIGHTCOVE_PUB_ID = "XXXXXXXXXXXX"

#S3 Bucket
S3_CONTENT_RESOURCES_BUCKET = 'AmazingBucket'
BRIGHTCOVE_SRC_BUCKET = 'some us bucket'
RAW_CONTENT_BUCKET = 'pp-vid-testing'
CONTENT_REPO_BUCKET = 'pp-content-providers'

# GCM Auth
GCM_AUTH = "_______________"

# Media settings
MEDIA_DOWNLOAD_LOCATION = '/this/location/'

# LOGS Location
LOGS_DIR = '/this/location/'

TORRENT_FILE_DIR = '/this/location/'

TMP = '/Users/mayankbhola/Projects/playground/tmp/'


# server settings
API_PROTOCOL = 'http'
API_SERVER_NAME = 'dl.prspl.ay'


#DIRECTORY FOR FETCHING media in LOCAL DIRECTORY
SRC_MEDIA_BASE_URL='http://pratush.dev.pressplaytv.in/static/test123/'

#DIRECTORY TO STORE TEMPORARY FILES
FILE_PATH='/some/path/'

# FILE LOCATION for local_link attribute in Content
LOCAL_FILE_PATH='/home/pratush/ammunition/app/static/test123/'

CELERY_BROKER = 'amqp://ubuntu:test@52.77.57.155/host'
CELERY_QUEUE = 'awesome one'

MEDIA_TMP_LOCATION='______'

IMAGE_RESOURCE_BUCKET='______'

HIDDEN_CHANNELS = ['pp-channel-00']
