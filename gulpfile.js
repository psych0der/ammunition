'use strict';

/**
 * Crystal : Builder for Beam
 *     ______ .______     ____    ____  _______.___________.    ___       __      
 *    /      ||   _  \    \   \  /   / /       |           |   /   \     |  |     
 *   |  ,----'|  |_)  |    \   \/   / |   (----`---|  |----`  /  ^  \    |  |     
 *   |  |     |      /      \_    _/   \   \       |  |      /  /_\  \   |  |     
 *   |  `----.|  |\  \----.   |  | .----)   |      |  |     /  _____  \  |  `----.
 *    \______|| _| `._____|   |__| |_______/       |__|    /__/     \__\ |_______|
 *
 */

/* COPY THIS FILE AND RENAME TO GULPFILE.JS */
/* PLEASE ENSURE BEAM/STATIC/BUILD DIR EXISTS WITH SCRIPTS AND STYLES DIRECTORY BELOW IT*/

/* importing required modules */
var gulp = require('gulp'),
    gutil = require('gulp-util'),
    clean = require('gulp-clean'),
    del = require('del'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    notify = require('gulp-notify'),
    rename = require('gulp-rename'),
    moment = require('moment'),
    uglifycss = require('gulp-uglifycss'),
    program = require('commander'),
    hoganCompiler = require('gulp-hogan-precompile'),
    declare = require('gulp-declare'),
    Q = require('q'),
    static_map = require('./static-mapper');


require('gulp-help')(gulp, {
    description: 'Help listing.'
});

program
    .usage('./crystal <options>')
    .version('0.0.1')
    .option('-d, --debug', 'run in debug mode')
    .parse(process.argv);

var debug = program.debug;

gulp.task('clear-static-map-cache' ,function(){
  gutil.log('Clearing require cache');
  for(var key in require.cache){
    var key_frags = key.split('/');
    if (key_frags[key_frags.length-1] == 'static-mapper.js') {
        delete require.cache[key]
        static_map = require('./app/lib/static-mapper');
        break;
    }
  }
});

/* Clean task to build directory */
gulp.task('clean', function() {
    gutil.log('Cleaning build directory');
    return del([
        'app/static/build/styles/home.min.css',
        'app/static/build/scripts/home.min.js',
    ]);
});

/*compile all template files to one js file*/
gulp.task('compile-templates', function() {
    gulp.src('app/templates/hogan_templates/*.html')
      .pipe(hoganCompiler())
      .pipe(declare({
        namespace: 'templates',
        noRedeclare: true
      }))
      .pipe(concat('templates.js'))
      .pipe(gulp.dest('app/static/js/'));
});

gulp.task('copy-fonts', function() {
   gulp.src('./app/static/css/Bootstrap-3.3.5/fonts/**/*')
   .pipe(gulp.dest('./app/static/build/styles/Bootstrap-3.3.5/fonts/'));
});


gulp.task('build-js', function() {
    var deferrds = [];
    var build_profiles = getBuildProfiles();
    build_profiles.forEach(function(bp) {
        var q = Q.defer();
        deferrds.push(q.promise)
        var compressed_name = getCompressedResourceName(bp)
        gulp.src(getScripts(bp).map(function(js) {
                return 'app/static/js/' + js + '.js'
            }))
            .pipe(concat(compressed_name+ ".js"))
            .on('error', function(error) {
                notify('Error: '+error, 'error');
            })
            /*.pipe(uglify())
            .on('error', function(error) {
                notify(error, 'error')
            })*/
            .pipe(gulp.dest('app/static/build/scripts'))
            .on('end', function() {
                q.resolve()
                notify('Uglified js for ' + compressed_name + ' (' + moment().format('MMM Do h:mm:ss A') + ')')
            })
    });
    return Q.all(deferrds)
});

gulp.task('build-css', function() {
    var deferrds = [];
    var build_profiles = getBuildProfiles();
    build_profiles.forEach(function(bp) {
        var compressed_name = getCompressedResourceName(bp);
        var q = Q.defer();
        deferrds.push(q.promise)
        gulp.src(getStyleSheets(bp).map(function(css) {
                return 'app/static/css/' + css + '.css'
            }))
            .pipe(concat(compressed_name + '.css'))
            .on('error', function(error) {
                notify(error)
            })
            .pipe(uglifycss())
            .on('error', function(erorr) {
                notify(error)
            })
            .pipe(gulp.dest('app/static/build/styles'))
            .on('end', function() {
                q.resolve()
                gutil.log('Concatanated stylesheet for ' + compressed_name + ' (' + moment().format('MMM Do h:mm:ss A') + ')')
                notify('Concatanated stylesheet for ' + compressed_name + ' (' + moment().format('MMM Do h:mm:ss A') + ')')
            })
    });
    return Q.all(deferrds)
});

gulp.task('deploy',['build-css','build-js'],function(){
    gutil.log('Ready for deployment');
});


/* default task for gulp */
gulp.task('default', [ 'clean' , 'compile-templates' ,'build-css', 'build-js' , 'copy-fonts']);


//function
function getStyleSheets(key){
    var cs;
    if(key in static_map['custom_stylesheets']){
        cs = static_map['custom_stylesheets'][key];
    } else {
        cs = []
    }
    return static_map['essential_stylesheets'].concat(cs);
}

function getScripts(key){
    var cs;
    if(key in static_map['custom_scripts']){
        cs = static_map['custom_scripts'][key];
    } else {
        cs = []
    }
    return static_map['essential_scripts'].concat(cs);
}

function getCompressedResourceName(key){
    if(key in static_map['compressed_names']){
        return static_map['compressed_names'][key];
    }
    return '';
}

function getBuildProfiles(key){
    if(static_map['build_profiles'] == null || static_map['build_profiles'].length == 0)
        return Object.keys(static_map['compressed_names']);
    else
        return static_map['build_profiles'];
}