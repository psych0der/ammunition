#!/usr/bin/python

from __future__ import unicode_literals
import os
import sys
import threading
import youtube_dl
import json
import traceback
import glob
import logging
import logging.handlers
import signal
import gevent
import traceback
from celery import Celery
from subprocess import Popen, PIPE, call
from boto.s3.connection import S3Connection, Location , Bucket, Key
import shutil
import time
# patch_all()

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                             os.pardir))
import wrappers
from app.commons.modules import upload_s3 as fileops
from app.commons.modules import brightcove
from app.commons.modules.mongoDataDump import fireUpMongo

# from app.commons.modules import content

import config
from app.commons.resources import redis_cli, putio_client
from models.Content import Content
from models.Raw_content_registry import Raw_content_registry

app = Celery('celery_tasks', broker=config.CELERY_BROKER)

LOG_FILENAME = config.LOGS_DIR + 'video_download_worker.out'
ALLOWED_FILE_EXTENSIONS = [
    '.mp4',
    '.avi',
    '.wmv',
    '.flv',
    '.mpg',
    '.mov',
    '.ogv',
    '.mp3',
    '.mkv',
]

# Set up a specific logger with our desired output level

my_logger = logging.getLogger('MyLogger')
my_logger.setLevel(logging.DEBUG)

# Add the log message handler to the logger
class MyLogger(object):

    '''
        Logger object for youtube dl
    '''

    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        pass



handler = logging.handlers.RotatingFileHandler(LOG_FILENAME,maxBytes=200000, backupCount=5)
my_logger.addHandler(handler)

# flattens a directory and moves all sub dirs and files recursively to destination folder
def flatten_dir(source_dir, destination_dir):
    for (root, dirs, files) in os.walk(source_dir):
        for filename in files:
            source_path = os.path.join(root, filename)
            destination_path = os.path.join(destination_dir, filename)
            print 'Copying {} to {}'.format(source_path,
                                            destination_path)
            shutil.copy(source_path, destination_path)


@app.task
def download_video(raw_content_id,media_url,media_type,media_name):
    db = wrappers.Postgres.init()
    print media_url
    print raw_content_id
    db_cursor = wrappers.Postgres.get_cursor(db)
    youtube_dl_options = {'logger': MyLogger(),
                          'outtmpl': config.MEDIA_DOWNLOAD_LOCATION
                          + 'pp-raw-content-' + str(raw_content_id)
                          + '.%(ext)s'}
    ext = 'mp4'

    if media_type == 'audio':
        ext = 'mp3'
        youtube_dl_options['format'] = 'bestaudio/best'
        youtube_dl_options['postprocessors'] = \
            [{'key': 'FFmpegExtractAudio', 'preferredcodec': 'mp3',
              'preferredquality': '192'}]
        print 'patching options for audio'
    else:
        youtube_dl_options['format'] = 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best'
        youtube_dl_options['postprocessors'] = [{'key': 'FFmpegVideoConvertor', 'preferedformat': 'mp4'}]


    my_logger.info('Job recieved : Download ' + media_name)
    print 'Content type: ' + media_type

    with youtube_dl.YoutubeDL(youtube_dl_options) as ydl:
        try:
            ydl.download([media_url])
            print media_name + ' downloaded'
            my_logger.info('Download complete for ' + media_name)
            print 'transcoding for faster html5 startup'


            (conversion_status, stderr) = \
                convert_vid(os.path.join(config.MEDIA_DOWNLOAD_LOCATION,
                                         'pp-raw-content-' +
                                         str(raw_content_id)
                                         + '.' + ext),
                            os.path.join(config.MEDIA_DOWNLOAD_LOCATION,
                                         'pp-raw-content-' +
                                         str(raw_content_id)
                                         + '-convert.' + ext))

            if conversion_status :
              shutil.move(os.path.join(config.MEDIA_DOWNLOAD_LOCATION,
                                       'pp-raw-content-' +
                                       str(raw_content_id)
                                       + '-convert.' + ext),
                                       os.path.join(config.MEDIA_DOWNLOAD_LOCATION,
                                                    'pp-raw-content-' +
                                                    str(raw_content_id)
                                                    + '.' + ext))

            else :
                print 'Error : ' + stderr
                my_logger.error('Error downloading ' + media_name
                                + ' : ' + stderr)
                db_cursor.execute('UPDATE raw_content_registry SET status = %(status)s, \
                    remarks = %(remarks)s WHERE id = %(id)s', {'status': 'error','remarks': 'Error in converting video','id': raw_content_id})
                db.commit()
                print 'aborting'
                return

            print 'transfering from : ' \
                + os.path.join(config.MEDIA_DOWNLOAD_LOCATION,
                               'pp-raw-content-' + str(raw_content_id)
                               + '.' + ext)
            remote_link = \
                fileops.transfer_s3(os.path.join(config.MEDIA_DOWNLOAD_LOCATION,
                                                 'pp-raw-content-'
                                                 + str(raw_content_id) + '.' + ext),
                                    target_bucket=config.RAW_CONTENT_BUCKET)
            print 'remote link : ' + remote_link
            db_cursor.execute('UPDATE raw_content_registry SET status = %(status)s, remote_link = %(remote_link)s \
                WHERE id = %(id)s', {'status': 'complete', 'id': raw_content_id,'remote_link': remote_link})
            db.commit()

            # transfer to s3 and remove

            os.remove(os.path.join(config.MEDIA_DOWNLOAD_LOCATION,
                                   'pp-raw-content-' +
                                   str(raw_content_id) + '.'
                                   + ext))
            print 'transfered to s3'
            my_logger.info(raw_content_id + ' : Transfered to s3')
        except Exception, e:

            print 'Error : ' + str(e)
            my_logger.error('Error downloading ' + media_name + ' : '+ str(e))
            db_cursor.execute('UPDATE raw_content_registry SET status = %(status)s, \
                remarks = %(remarks)s WHERE id = %(id)s', {'status': 'error',\
                'remarks': 'Error in downloading : '+ str(e)[:50], 'id': raw_content_id})
            db.commit()


@app.task
def fetch_from_putio(raw_content_id, remote_id, media_name):
    db = wrappers.Postgres.init()
    db_cursor = wrappers.Postgres.get_cursor(db, 'dict')
    content_dir_location = config.TMP + str(raw_content_id)

    if not os.path.exists(content_dir_location):
        os.makedirs(content_dir_location)
    transfer_id = remote_id

    # preparing putio file for download

    try:
        putio_transfer_object = \
            vars(putio_client.Transfer.get(int(transfer_id)))
        putio_file_id = putio_transfer_object['file_id']
        putio_file = putio_client.File.get(putio_file_id)
        putio_file_info = vars(putio_file)
        print 'putio transfer started'
        my_logger.info('Transfer started for ' + str(raw_content_id))
        putio_file.download(dest=content_dir_location,
                            delete_after_download=True)
        if putio_file_info['content_type'] == 'application/x-directory':
            print 'downloaded a directory'
            print 'flattening it ...'

            # bad practice ---

            flatten_dir(os.path.join(content_dir_location,
                                     putio_file_info['name']), content_dir_location)

            # shutil.move(os.path.join(content_dir_location,putio_file_info['name'])+'/*',
            # content_dir_location+'/')

            shutil.rmtree(os.path.join(content_dir_location,
                                       putio_file_info['name']))
        print 'putio transfer completed'
        my_logger.info('Transfer complete for ' + str(raw_content_id))

        # iterating overfiles in location

        extracted_content_count = 0
        for file in os.listdir(content_dir_location):
            if '.' not in file:
                os.remove(os.path.join(content_dir_location, file))
                continue
            else:
                (basename, ext) = os.path.splitext(file)
                if ext.lower() not in ALLOWED_FILE_EXTENSIONS:
                    os.remove(os.path.join(content_dir_location, file))
                    continue
                else:
                    if ext not in ['.mp3']:
                        print 'converting ' + file
                        if ext == '.mp4':
                            (conversion_status, stderr) = convert_vid(os.path.join(content_dir_location,file),\
                                os.path.join(content_dir_location,basename + '-convert.mp4'))
                        else:
                            (conversion_status, stderr) = convert_vid(os.path.join(content_dir_location,file),\
                                os.path.join(content_dir_location,basename + '.mp4'))
                        if conversion_status:
                            print 'media converted .....'
                            if ext == '.mp4':
                                 shutil.move(os.path.join(content_dir_location,basename + '-convert.mp4'),\
                                         os.path.join(content_dir_location,basename + '.mp4'))

                            extracted_content_count += 1
                            media_type = 'video'
                            db_cursor.execute('INSERT INTO raw_content_registry (name, status, source, \
                                media_type) VALUES (%(name)s, %(status)s, %(source)s, %(type)s ) returning id', {
                                'name': media_name + '-'
                                + str(extracted_content_count),
                                'status': 'complete',
                                'source': 'MAGNET',
                                'type': media_type,
                            })
                            db.commit()

                            new_raw_content_id = db_cursor.fetchone()[0]

                            # move converted media file

                            shutil.move(os.path.join(content_dir_location,
                                                     basename + '.mp4'),
                                        os.path.join(config.MEDIA_DOWNLOAD_LOCATION,
                                                     'pp-raw-content-'
                                                     + str(new_raw_content_id) + '.mp4'))
                            remote_link = \
                                fileops.transfer_s3(os.path.join(config.MEDIA_DOWNLOAD_LOCATION,
                                                                 'pp-raw-content-'
                                                                 +
                                                                 str(new_raw_content_id) +
                                                                 '.mp4'
                                                                 ),
                                                    target_bucket=config.RAW_CONTENT_BUCKET)
                            print 'remote link : ' + remote_link
                            db_cursor.execute('UPDATE raw_content_registry SET remote_link =  %(remote_link)s \
                                WHERE id = %(id)s', {'id': new_raw_content_id,'remote_link': remote_link})
                            db.commit()

                            # transfer to s3 and remove

                            my_logger.info(raw_content_id
                                           + ': transfered to S3')
                            if ext != '.mp4':
                                os.remove(os.path.join(content_dir_location,
                                                       file))
                        else:
                            print 'couldn\'t convert media'
                            print 'error: ' + str(stderr)
                            os.remove(os.path.join(content_dir_location,
                                                   file))
                    else:

                        extracted_content_count += 1
                        if ext == '.mp3':
                            media_type = 'audio'
                        else:
                            media_type = 'video'
                        db_cursor.execute('INSERT INTO raw_content_registry (name, status, source, media_type)\
                         VALUES (%(name)s, %(status)s, %(source)s, %(type)s) returning id', {
                            'name': media_name + '-'
                            + str(extracted_content_count),
                            'status': 'complete',
                            'source': 'MAGNET',
                            'type': media_type,
                        })
                        db.commit()
                        new_raw_content_id = db_cursor.fetchone()[0]

                        shutil.move(os.path.join(content_dir_location,
                                                 file),
                                    os.path.join(config.MEDIA_DOWNLOAD_LOCATION,
                                                 'pp-raw-content-'
                                                 + str(new_raw_content_id) + ext))

                        # transfer to s3 and remove

                        remote_link = \
                            fileops.transfer_s3(os.path.join(config.MEDIA_DOWNLOAD_LOCATION,
                                                             'pp-raw-content-'
                                                             + str(new_raw_content_id) + ext),
                                                target_bucket=config.RAW_CONTENT_BUCKET)
                        print 'remote link : ' + remote_link
                        print 'transfered to S3'
                        db_cursor.execute('UPDATE raw_content_registry SET remote_link =  %(remote_link)s \
                            WHERE id = %(id)s', {'id': new_raw_content_id,'remote_link': remote_link})
                        db.commit()

                        my_logger.info(raw_content_id
                                       + ': transfered to S3')
                        os.remove(os.path.join(config.MEDIA_DOWNLOAD_LOCATION,
                                               'pp-raw-content-'
                                               + str(new_raw_content_id) + ext))

        if extracted_content_count == 0:
            print 'couldn\'t extract media'
            my_logger.error(' couldn\'t extract media for '
                            + str(raw_content_id))
            db_cursor.execute('UPDATE raw_content_registry SET status = %(status)s , remarks = %(remarks)s \
                WHERE id = %(id)s', {'status': 'error','remarks': "could'nt extract any content", 'id': raw_content_id})
            db.commit()
        else:
            db_cursor.execute(
                'DELETE FROM raw_content_registry WHERE id = %(id)s', {'id': raw_content_id})
            db.commit()
    except Exception, e:

        print 'Error in transfer object ' + str(e)
        traceback.print_exc()
        db_cursor.execute('UPDATE raw_content_registry SET status = %(status)s , remarks = %(remarks)s \
            WHERE id = %(id)s', {'status': 'error','remarks': 'Error in transfer object '+ str(e)[:50], 'id': raw_content_id})
        db.commit()
        my_logger.error(' ' + str(e))

    # removing directory from tmp

    shutil.rmtree(content_dir_location)


def convert_vid(input_file, output_file):
    my_logger.info(' input file ' + input_file)
    my_logger.info(' output file ' + output_file)
    print 'converting vids'
    ffmpeg_args = [
        'ffmpeg',
        '-i',
        input_file,
        '-c:v',
        'libx264',
        '-movflags',
        '+faststart',
        '-strict',
        'experimental',
        '-c:a',
        'aac',
        '-crf',
        '26',
        '-b:v',
        '800k',
        '-b:a',
        '128k',
        output_file,
    ]
    process = Popen(ffmpeg_args, stdout=PIPE, stderr=PIPE)
    (stdout, stderr) = process.communicate()
    print 'conversion complete'
    return (process.returncode == 0, stderr)


@app.task
def video_converter(input_file, output_file, *args,**kwargs):
    db = wrappers.Postgres.init()
    print 'convertin media...'

    my_logger.info(input_file)
    my_logger.info(output_file)


    if 'raw_content_id' in kwargs:
        raw_content_id = kwargs['raw_content_id']
    else:
        raw_content_id = None

    if 'target_bucket' in kwargs:
        target_bucket = kwargs['raw_content_id']
    else:
        target_bucket = config.RAW_CONTENT_BUCKET


    if raw_content_id != None:
        my_logger.info('Media conversion started for ..'
                    + str(raw_content_id))
    else:
        my_logger.info('Media conversion started for .. '+input_file)

    db_cursor = wrappers.Postgres.get_cursor(db, 'dict')
    basename, ext = os.path.splitext(input_file)
    if ext == '.mp4':
        output_basename, output_ext = os.path.splitext(output_file)
        (conversion_status, stderr) = convert_vid(input_file, output_basename+'-convert'+output_ext)
    else:
        (conversion_status, stderr) = convert_vid(input_file, output_file)


    if conversion_status:
        print 'media converted'

        if output_ext == '.mp4':
            shutil.move(output_basename+'-convert'+output_ext, output_file)

        remote_link = fileops.transfer_s3(output_file, target_bucket=target_bucket)

        if raw_content_id != None:
            my_logger.info('Media conversion completed for ..'
                           + str(raw_content_id))
            db_cursor.execute("UPDATE raw_content_registry SET status = %(status)s , remote_link = %(remote_link)s WHERE id = %(id)s",\
                {'status': 'complete', 'id': raw_content_id, 'remote_link': remote_link})

            db.commit()

        else:
            my_logger.info('Media conversion completed for ..'
                           + input_file)

        os.remove(input_file)
        os.remove(output_file)


    else:
        print 'status : ' + str(conversion_status)
        print 'couldn\'t convert media'
        my_logger.error('Error in media conversion for ..'
                        + str(raw_content_id))
        my_logger.error('error : ' + str(stderr))
        print 'error: ' + str(stderr)

        if raw_content_id != None:
            db_cursor.execute('UPDATE raw_content_registry SET status = %(status)s , remarks = %(remarks)s \
                WHERE id = %(id)s', {'status': 'error','remarks': 'unable to convert to mp4','id': raw_content_id})
            db.commit()

    # removing input file
    db.close()


@app.task
def transfer_from_s3(raw_content_id, media_url, type):
    db = wrappers.Postgres.init()
    db_cursor = wrappers.Postgres.get_cursor(db, 'dict')

    # transferring directly to s3 in case of audio
    if type == 'audio':
        result = fileops.copy_file(
        config.CONTENT_REPO_BUCKET+'/'+media_url, config.RAW_CONTENT_BUCKET+'/pp-raw-content-'+str(raw_content_id)+ext)
        if result['status'] == 'success':
            link = result['new_key'].generate_url(
                expires_in=0, query_auth=False)
            cursor.execute("UPDATE raw_content_registry SET status = %(status)s , remote_link = %(link)s WHERE id = %(id)s",
                           {'status': 'complete', 'id': raw_content_id, 'link': link})
        else:
            cursor.execute("UPDATE raw_content_registry SET status = %(status)s, remarks = %(remarks)s  WHERE id = %(id)s",
                           {'status': 'error', 'id': raw_content_id, 'remarks': str(result['message'])[:50]})


        db.commit()
        db.close()
    else:
        result = fileops.manage_download(media_url, config.CONTENT_REPO_BUCKET)
        if result['status'] == 'failed':
            cursor.execute("UPDATE raw_content_registry SET status = %(status)s, remarks = %(remarks)s  WHERE id = %(id)s",
                           {'status': 'error', 'id': raw_content_id, 'remarks': 'Unable to transfer content from s3 bucket'})
            my_logger.error('unable to transfer raw content from s3 with url : '+str(media_url))
            db.commit()
        else:
            print 'media downloaded ....'
            my_logger.info('Video downloaded from s3')
            saved_file = result['path']
            basename, ext = os.path.splitext(saved_file)

            if ext == '.mp4':
                (conversion_status, stderr) = convert_vid(saved_file,os.path.join(config.TMP,basename + '-convert.mp4'))
            else:
                (conversion_status, stderr) = convert_vid(saved_file,os.path.join(config.TMP,'pp-raw-content-'+ str(raw_content_id) + '.mp4'))
            if conversion_status:
                print 'media converted .....'
                my_logger.info('Video download complete for '+raw_content_id)
                if ext == '.mp4':
                     shutil.move(os.path.join(config.TMP,basename + '-convert.mp4'),\
                             os.path.join(config.TMP,'pp-raw-content-'+ str(raw_content_id) + '.mp4'))

                remote_link = fileops.transfer_s3(os.path.join(config.TMP,'pp-raw-content-'+ str(raw_content_id) + '.mp4'), target_bucket=config.RAW_CONTENT_BUCKET)
                print 'remote link : ' + remote_link
                db_cursor.execute('UPDATE raw_content_registry SET status = %(status)s, remote_link = %(remote_link)s \
                    WHERE id = %(id)s', {'status': 'complete', 'id': raw_content_id,'remote_link': remote_link})
                db.commit()
                db.close()

                os.remove(os.path.join(config.TMP,'pp-raw-content-'+ str(raw_content_id) + '.mp4'))
                print 'transfered to s3'
                my_logger.info(raw_content_id + ' : Transfered to s3')

            else:
                print 'status : ' + conversion_status
                print 'couldn\'t convert media'
                my_logger.error('Error in media conversion for ..'
                                + str(raw_content_id))
                my_logger.error('error : ' + str(stderr))
                print 'error: ' + str(stderr)
                db_cursor.execute('UPDATE raw_content_registry SET status = %(status)s , remarks = %(remarks)s \
                    WHERE id = %(id)s', {'status': 'error','remarks': 'unable to convert to mp4','id': raw_content_id})
                db.commit()
                db.close()

@app.task
def content_ingestor(content_id, old_raw_content, new_raw_content):
    db = wrappers.Postgres.init()

    if old_raw_content == new_raw_content:
        db.close()
        return {'status': 'failed','message': 'old and new raw content id is same'}

    if old_raw_content == 'None':
        old_raw_content = None
    if new_raw_content == 'None':
        new_raw_content = None

    a_content = Content(content_id=content_id)

    if old_raw_content == None:
        print 'no old raw content'

        a_raw_content = Raw_content_registry(raw_content_id=new_raw_content)
        a_raw_content.used = 'yes'

        conn = S3Connection(config.AWS_CREDS['KEY'], config.AWS_CREDS['SECRET'])
        bucket = conn.get_bucket(config.RAW_CONTENT_BUCKET)
        ext = ('mp4' if a_raw_content.media_type == 'video' else 'mp3')
        key = bucket.new_key('pp-raw-content-'+str(new_raw_content)+'.'+ext)

        link = key.generate_url(expires_in=0,query_auth=False)
        a_content.local_link = link

        if a_content.type != 'video':
            print 'ignoring non video types'
            a_content.save()
            a_raw_content.save()
            db.close()
            return {'status': 'success'}

        suffix = ''
        if hasattr(config,'TEST_ENV'):
            if config.TEST_ENV == True:
                suffix = '-test'
        rem_id = a_content.remote_id
        if rem_id == None:
            v = brightcove.create_video(content_id+suffix)
            print v
            if 'id' not in v:
                a_content.error = 'unable to get brightcove remote id'
                a_content.last_failed_upload_attempt = int(time.time() * 1000)
                a_content.save()
                db.close()
                return {'status': 'failed','message': 'unable to fetch brightcove id'}

            a_content.remote_id = v['id']
            rem_id = v['id']


        dest = config.RAW_CONTENT_BUCKET+'/pp-raw-content-'+new_raw_content+'.'+ext
        print 'pushing video to brightcove from : '+str(dest)
        response = brightcove.submit_pbi(rem_id, dest)
        if 'id' not in response:
            a_content.error = 'video ingestion failed'
            a_content.last_failed_upload_attempt = int(time.time() * 1000)
            a_raw_content.save()
            a_content.save()
            db.close()
            return {'status': 'failed','message': 'video ingestion failed.'}

        print 'video saved to brigtcove'

        a_content.save()
        a_raw_content.save()
        db.close()
        return {'status': 'success'}

    elif new_raw_content == None:
        print 'new raw content is null'
        a_raw_content = Raw_content_registry(raw_content_id=old_raw_content)
        a_raw_content.used = 'no'
        a_content.remote_id = None
        a_content.save()
        a_raw_content.save()
        db.close()
        return {'status': 'success'}
    else:
        print 'replace raw content'

        raw_old_obj = Raw_content_registry(raw_content_id=str(old_raw_content))
        raw_new_obj = Raw_content_registry(raw_content_id=str(new_raw_content))
        raw_new_obj.used = 'yes'
        raw_old_obj.used = 'no'

        ext = ('mp4' if raw_new_obj.media_type == 'video' else 'mp3')
        dest = config.RAW_CONTENT_BUCKET+'/pp-raw-content-'+new_raw_content+'.'+ext
        conn = S3Connection(config.AWS_CREDS['KEY'], config.AWS_CREDS['SECRET'])
        bucket = conn.get_bucket(config.RAW_CONTENT_BUCKET)
        key = bucket.new_key('pp-raw-content-'+str(new_raw_content)+'.'+ext)

        link = key.generate_url(expires_in=0,query_auth=False)
        a_content.local_link = link


        if a_content.type != 'video':
            print 'ignoring non video types'
            raw_new_obj.save()
            raw_old_obj.save()
            a_content.save()
            db.close()
            return {'status': 'success'}

        suffix = ''
        if hasattr(config,'TEST_ENV'):
            if config.TEST_ENV == True:
                suffix = '-test'

        rem_id = a_content.remote_id
        if rem_id == None:
            v = brightcove.create_video(content_id+suffix)
            print v

            if 'id' not in v:
                a_content.error = 'unable to get brightcove remote id'
                a_content.last_failed_upload_attempt = int(time.time() * 1000)
                a_content.save()
                raw_old_obj.save()
                raw_new_obj.save()
                db.close()
                return {'status': 'failed','message': 'unable to fetch brightcove id'}

            a_content.remote_id = v['id']
            rem_id = v['id']


        response = brightcove.submit_pbi(rem_id, dest)
        if 'id' not in response:
            a_content.error = 'video ingestion failed'
            a_content.last_failed_upload_attempt = int(time.time() * 1000)
            raw_old_obj.save()
            raw_new_obj.save()
            a_content.save()
            db.close()
            return {'status': 'failed','message': 'video ingestion failed.'}


        raw_new_obj.save()
        raw_old_obj.save()
        a_content.save()
        db.close()
        return {'status': 'success'}


@app.task
def fire_up_mongo():
    fireUpMongo()

@app.task
def download_initiator(version_id,save_path):
    if os.path.isfile(config.INITIATOR_LOCK) is False:
        print "initiating box version!!!", version_id, save_path
        db = wrappers.Postgres.init()
        cursor = wrappers.Postgres.get_cursor(db)

        try:
            a_query="INSERT INTO version_box (version_id,zip_location,status,error) VALUES (%(version_id)s,%(zip_location)s,%(status)s,%(error)s) returning id"
            params={'version_id':int(version_id),'zip_location':save_path,'status':'pending','error':'no'}
            cursor.execute(a_query,params)
            result = cursor.fetchone()
            row_id = result[0]
            db.commit()
        except Exception, e:
            print e

        cmd = ["sudo","bash",config.DOWNLOAD_BOX_INITIATOR_LOCATION,version_id,save_path,'>>','/home/ubuntu/desi.log']
        cmdstr = " ".join(cmd)
        print "command ---- > ", cmdstr
        p = Popen(cmdstr, shell=True, stdout = PIPE,stderr=PIPE,stdin=PIPE)
        out, err = p.communicate()
        print "command chal gaya"
        if p.returncode==0:
            infile = open(config.INITIATOR_LOG)
            linelist = infile.readlines()
            lastline = linelist[-1].replace('\n','')

            if lastline == 'zip successful' and os.path.isfile(save_path+'complete_content.zip') is True:
                params={'status':'success','error':'no','id': str(row_id)}
            else:
                params={'status':'failed','error':'yes','id': str(row_id)}

            try:
                a_query="UPDATE version_box set status=%(status)s,error=%(error)s where id=%(id)s"
                cursor.execute(a_query,params)
                db.commit()
            except Exception, e:
                print e

        else:
            print err
            print "return code galat hai", p.returncode
            try:
                a_query="UPDATE version_box set status=%(status)s,error=%(error)s where id=%(id)s"
                params={'status':'failed','error':'yes','id': str(row_id)}
                cursor.execute(a_query,params)
                db.commit()
            except Exception,e:
                db.commit()
                return "error"
        db.close()
        return out
    else:
        print "version already in progress"
        return False

@app.task
def trailer_ingestor(content_id, raw_content_list):
    db = wrappers.Postgres.init()
    cursor = wrappers.Postgres.get_cursor(db)
    content_id = str(content_id).split('-')[-1]
    if raw_content_list is not None and len(raw_content_list) > 0:
        print raw_content_list
        raw_content_list = list(set(raw_content_list))
        return_list = []
        for i, raw_content_id in enumerate(raw_content_list):
            try:
                raw_content_id = str(raw_content_id)
                a_raw_content = Raw_content_registry(raw_content_id=raw_content_id)

                if a_raw_content.used == 'yes':
                    query = "DELETE from trailers_map where content_id=%s and raw_content_id=%s" % (content_id, raw_content_id)
                    print "not attaching trailer %s, already used somewhere else!" % (raw_content_id)
                    cursor.execute(query)
                    db.commit()
                    continue

                insert_object = {'content_id': int(content_id),
                                 'raw_content_id': int(raw_content_id),
                                 'local_link': None}

                return_object = {'status': 'success',
                                 'error': None,
                                 'remote_id': None,
                                 'raw_content_id': raw_content_id,
                                 'content_id': content_id,
                                 'local_link': None}

                a_raw_content.used = 'yes'

                conn = S3Connection(config.AWS_CREDS['KEY'], config.AWS_CREDS['SECRET'])
                bucket = conn.get_bucket(config.RAW_CONTENT_BUCKET)
                ext = ('mp4' if a_raw_content.media_type == 'video' else 'mp3')
                key = bucket.new_key('pp-raw-content-'+str(raw_content_id)+'.'+ext)

                insert_object['local_link'] = key.generate_url(expires_in=0,query_auth=False)
                return_object['local_link'] = insert_object['local_link']
                updateQuery = "UPDATE trailers_map set local_link=%(local_link)s where content_id=%(content_id)s and raw_content_id=%(raw_content_id)s"
                cursor.execute(updateQuery, insert_object)
                db.commit()

                filename = 'pp-content-' + content_id + '-trailer-' + str(i)

                suffix = ''
                if hasattr(config,'TEST_ENV'):
                    if config.TEST_ENV == True:
                        suffix = '-test'
                v = brightcove.create_video(filename+suffix)
                print v

                if 'id' not in v:
                    return_object['error'] = 'unable to get brightcove remote id'
                    return_object['status'] = 'failed'
                else:
                    rem_id = v['id']
                    insert_object['remote_id'] = rem_id
                    updateQuery = "UPDATE trailers_map set remote_id=%(remote_id)s where content_id=%(content_id)s and raw_content_id=%(raw_content_id)s"
                    cursor.execute(updateQuery, {'content_id': int(content_id),
                                                 'remote_id': str(rem_id),
                                                 'raw_content_id': int(raw_content_id)})
                    db.commit()
                    dest = config.RAW_CONTENT_BUCKET+'/pp-raw-content-'+raw_content_id+'.'+ext
                    print 'pushing trailer to brightcove from : '+str(dest)
                    response = brightcove.submit_pbi(rem_id, dest, callback_type='trailer')
                    if 'id' not in response:
                        return_object['error'] = 'trailer ingestion failed'
                        return_object['status'] = 'failed'

                a_raw_content.save()
                return_list.append(return_object)

            except Exception, e:
                db.commit()
                print 'trailer ingetion error ----> ', e
                continue
        return return_list
