import wrappers
import json
import logging
import logging.handlers
from app.commons.resources import my_logger
import datetime
import traceback

class Channel:
    _token_id=-1
    _unique_error=False
    _exists = False
    __TRACKED_FILEDS = ["name", "display_name", "icon", "status","icon_small","provider_id","date_created","date_modified", "slug_url", "cover_image", "description", "short_description","tagline","show_title","is_global"]

    def __init__(self, channel_id):
        db = wrappers.Postgres.init()
        cursor = wrappers.Postgres.get_cursor(db)
        fields = ', '.join(self.__TRACKED_FILEDS)
        row_id = channel_id.split('-')[-1]
        self.channel_id = 'pp-channel-' + str(row_id)
        self.id = self.channel_id
        a_query = "SELECT " + fields + " FROM channels WHERE id = %(row_id)s"
        print a_query
        cursor.execute(a_query, {"row_id": row_id})
        result = cursor.fetchone()
        db.close()
        if result == None:
            return

        self._exists = True
        index = 0
        for a_field in self.__TRACKED_FILEDS:
            value = result[index]
            setattr(self, a_field, value)
            index += 1

    def consume(self, data):
        for a_field in self.__TRACKED_FILEDS:
            if a_field not in data:
                try:
                    value = getattr(self, a_field)
                except AttributeError:
                    #if a_field=='tags' or a_field=='sponsors':
                    #    value=[]
                    #else:
                    value = None
            else:
                value = data[a_field]
                if str(value) == "null":
                    value = None
                #if a_field == 'tags':
                #    value=data[a_field].split(',')

                #if a_field == 'tags' and value is not None:
                #    value=data[a_field].split(',')

                #if a_field == 'tags' and '' in value :
                 #   value=[]

            setattr(self, a_field, value)

    def export(self, to_database=False):
        data_dict = {}
        attributes = [a for a in dir(self) if not a.startswith('_') and not callable(getattr(self,a))]
        for an_attribute in attributes:
            data_dict[an_attribute] = getattr(self, an_attribute)

        if to_database:
            data_dict['id'] = self.channel_id.split('-')[-1]
            #data_dict['sponsors']=json.dumps(self.sponsors)
        if self._exists:
            data_dict['channel_id'] = self.channel_id
        return data_dict

    def save(self, create_new=False):
        db = wrappers.Postgres.init()
        cursor = wrappers.Postgres.get_cursor(db)
        if self._exists == False:
            create_new = True

        if create_new == True:
            a_query = """
                INSERT INTO channels
                (""" + ', '.join(self.__TRACKED_FILEDS) +  """)
                VALUES ( %(""" + ')s, %('.join(self.__TRACKED_FILEDS) + """)s )
                RETURNING *
            """

        else:
            update_params = wrappers.Postgres.get_update_params(self.__TRACKED_FILEDS)
            a_query = " UPDATE channels SET " + update_params + " WHERE id=%(id)s"
            item_id = self.id


        try:
            cursor.execute(a_query, self.export(to_database=True))


            if create_new:
                item_id = 'pp-channel-' + str(cursor.fetchone()[0])
                self.id=int(item_id.split('-')[-1])
                self.channel_id=item_id
        except Exception as ex:
            db_e = wrappers.Postgres.init()
            cursor_e = wrappers.Postgres.get_cursor(db_e)
            cursor_e.execute("SELECT * FROM error_token")
            res=cursor_e.fetchone()
            trace = traceback.format_exc()
            time=str(datetime.datetime.now())
            timestamp= time.split('.')[0]
            my_logger.error('Token ID : '+str(res[0])+" Channel Model Exception ")
            my_logger.error(trace)
            cursor_e.execute("UPDATE error_token SET last_used=%(last_used)s",{'last_used':res[0]+1})
            db_e.commit()
            db_e.close()
            xx = ex
            if xx.pgcode == 23505 or xx.pgcode == '23505':
                self._unique_error = True
            item_id = None
            self._token_id=res[0]

        #if item_id:
       # 	chann_id=item_id.split('-')[-1]
        #	tags_map.keep(chann_id,'channel',self.tags)   #mapping after successful storage
        if item_id!=None:
            self._exists=True
        db.commit()
        db.close()
        return item_id
