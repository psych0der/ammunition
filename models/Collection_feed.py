import wrappers
from app.commons.resources import my_logger
import datetime

class Collection_feed:
    _token_id=-1
    _exists = False
    __TRACKED_FILEDS = ["tag_id","row","col","diff"]

    def export(self, to_database=False):
        data_dict = {}
        attributes = [a for a in dir(self) if not a.startswith('_') and not callable(getattr(self,a))]
        for an_attribute in attributes:
            data_dict[an_attribute] = getattr(self, an_attribute)

        #if to_database:
        #    data_dict['item_id']=int(data_dict['item_id'].split('-')[-1])

        return data_dict

    def save(self, create_new=False):



        a_query = """
            INSERT INTO collection_feed
            (""" + ', '.join(self.__TRACKED_FILEDS) +  """)
            VALUES ( %(""" + ')s, %('.join(self.__TRACKED_FILEDS) + """)s )
            RETURNING *
        """
        #else:
        #    update_params = wrappers.Postgres.get_update_params(self.__TRACKED_FILEDS)
        #    a_query = " UPDATE home_feed SET " + update_params + " WHERE id=%(id)s"
        #    item_id = self.id

        db = wrappers.Postgres.init()
        cursor = wrappers.Postgres.get_cursor(db)
        try:
            cursor.execute(a_query, self.export(to_database=True))
            #if create_new:
            item_id = 'pp-collection-feed-' + str(cursor.fetchone()[0])
        except Exception ,e:
            db_e = wrappers.Postgres.init()
            cursor_e = wrappers.Postgres.get_cursor(db_e)
            cursor_e.execute("SELECT * FROM error_token")
            res=cursor_e.fetchone()
            print str(e)
            time=str(datetime.datetime.now())
            timestamp=time.split('.')[0]
            my_logger.error('Token ID : '+str(res[0])+" Collection Feed Model Exception "+str(e))
            cursor_e.execute("UPDATE error_token SET last_used=%(last_used)s",{'last_used':res[0]+1})
            db_e.commit()
            db_e.close()
            item_id = None
            self._token_id=res[0]

        db.commit()
        db.close()
        return item_id
