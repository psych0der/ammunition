import wrappers
import datetime
import json
from app.commons.modules import tags_map
from app.commons.resources import my_logger
import datetime
import traceback

class Content:
    _token_id=-1
    _unique_error = False
    _exists = False
    __TRACKED_FIELDS = ["name", "poster", "language", "box_only",
                        "duration", "type", "status", "genre", "specifics",
                        "video_url", "featured","remote_id","local_link","digital_master_uploaded","media_processed",
                        'raw_content_id','poster_small','provider_id','live','description','premium','expiry','cost','seo',
                        'date_created','date_modified','allow_download', 'video_content_type', 'slug_url', 'cover_image', 'short_description']

    def initialize_result(self, result):
        self._exists = True
        index = 0
        for a_field in self.__TRACKED_FIELDS:
            value = result[index]
            #if a_field == "status" :
                #print "VALUE FOR STATUS IS "+str(value)
            #    value = result[index].seconds

            setattr(self, a_field, value)
            index += 1

        if type(self.specifics) == dict and 'old_poster' in self.specifics:
            self.poster = 'https://dfs346ba3glnp.cloudfront.net/' + \
                self.specifics['old_poster']
        if type(self.specifics) == dict and 'old_video_id' in self.specifics:
            self.video_url = 'https://www.youtube.com/watch?v=' + \
                self.specifics['old_video_id']

    def __init__(self, content_id, precalresult=None):
        if precalresult == None:
            db = wrappers.Postgres.init()
            cursor = wrappers.Postgres.get_cursor(db)
            fields = ', '.join(self.__TRACKED_FIELDS)
            row_id = content_id.split('-')[-1]
            self.content_id = 'pp-content-' + str(row_id)
            self.id = self.content_id
            #a_query = "SELECT " + fields + \
            #    ", (SELECT channel_id from channel_content_mapping where id = ( SELECT max(id) from  channel_content_mapping where content_id = c.id)) FROM content c WHERE id = %(row_id)s"
            a_query = "SELECT "+fields+" FROM content where id = %(row_id)s"
            cursor.execute(a_query, {"row_id": row_id})
            result = cursor.fetchone()
            db.close()
            if result == None:
                return
            self.initialize_result(result=result)
        else:
            row_id = content_id.split('-')[-1]
            self.content_id = 'pp-content-' + str(row_id)
            self.id = self.content_id
            ctr=0
            for a in precalresult:
                if int(a[-1])==int(row_id):
                    break
                else:
                    ctr=ctr+1
            self.initialize_result(result=precalresult[ctr])

    def consume(self, data):
        for a_field in self.__TRACKED_FIELDS:
            if a_field not in data:
                try:
                    value = getattr(self, a_field)
                except AttributeError:
                    if a_field=='tags':
                        value=[]
                    elif a_field=='live':
                        value='no'
                    elif a_field=='provider_id':
                        value=1
                    elif a_field in ['allow_download', 'show_description']:
                        value='no'
                    else:
                        value = None

            else:
                value = data[a_field]

                if str(value) == "null":
                    value = None

                if a_field == "provider_id" and value==None:
                    value=1

                if a_field in ["allow_download", "show_description"] and value==None:
                    value='no'

                if a_field =="duration" and value=="":
                    value=None
                    print 'duration none------------'

                if a_field == "duration" and value is not None:
                    value = datetime.timedelta(seconds=int(data[a_field]))
                    print "duration***",value
                    print "duration***",data[a_field]

                if a_field =="expiry" and value=="":
                    value=None
                    print "expiry"

                if a_field=="cost" and value=="":
                    value=None
                    print "cost"

                if a_field == 'tags' and value is not None:
                    value=data[a_field].split(',')

                if a_field == 'tags' and value=='' :
                    value=[]

                if a_field == 'type' and value=='':
                    value=None

                if a_field=='seo' and value=='':
                    value=None
            setattr(self, a_field, value)

    def export(self, to_database=False):
        data_dict = {}
        attributes = [a for a in dir(self) if not a.startswith('_') and not callable(getattr(self,a))]
        for an_attribute in attributes:
            data_dict[an_attribute] = getattr(self, an_attribute)

        if to_database == True:
            if type(data_dict['specifics']) not in [dict, type(None)]:
                data_dict['specifics'] = json.loads(data_dict['specifics'])
            data_dict['specifics'] = wrappers.Postgres.typecast_json(data_dict['specifics'])
            data_dict['id'] = self.content_id.split('-')[-1]
            if type(data_dict['genre']) not in [list, type(None)]:
                data_dict['genre'] = json.loads(data_dict['genre'])

        elif data_dict['duration'] is not None:
            data_dict['duration'] = data_dict['duration'].seconds

        if self._exists:
            data_dict['content_id'] = self.content_id
        # if to_database==True:
        #     print data_dict
        return data_dict

    def save(self, create_new=False):
        if self._exists == False:
            create_new = True

        if create_new == True:
            a_query = """
                INSERT INTO content
                (""" + ', '.join(self.__TRACKED_FIELDS) +  """)
                VALUES ( %(""" + ')s, %('.join(self.__TRACKED_FIELDS) + """)s )
                RETURNING *
            """
        else:
            update_params = wrappers.Postgres.get_update_params(self.__TRACKED_FIELDS)
            a_query = " UPDATE content SET " + update_params + " WHERE id=%(id)s"
            item_id = self.id
            print "###updating"+str(item_id)

        db = wrappers.Postgres.init()
        cursor = wrappers.Postgres.get_cursor(db)

        try:
            cursor.execute(a_query, self.export(to_database=True))

            if create_new:
                item_id = 'pp-content-' + str(cursor.fetchone()[0])
                self.id=int(item_id.split('-')[-1])
                self.content_id=item_id

        except Exception as ex:
            db_e = wrappers.Postgres.init()
            cursor_e = wrappers.Postgres.get_cursor(db_e)
            cursor_e.execute("SELECT * FROM error_token")
            res=cursor_e.fetchone()
            trace = traceback.format_exc()
            time=str(datetime.datetime.now())
            timestamp=time.split('.')[0]
            my_logger.error('Token ID : '+str(res[0])+" Content Model Exception ")
            my_logger.error(trace)
            cursor_e.execute("UPDATE error_token SET last_used=%(last_used)s",{'last_used':res[0]+1})
            db_e.commit()
            db_e.close()
            item_id = None
            xx = ex
            if xx.pgcode == 23505 or xx.pgcode == '23505':
                self._unique_error = True
            self._token_id=res[0]
        #if item_id:
         #   cont_id=item_id.split('-')[-1]
         #   tags_map.keep(cont_id,'content',self.tags)   #mapping after successful storage
        if item_id!=None:
            self._exists=True
        db.commit()
        db.close()
        return item_id
