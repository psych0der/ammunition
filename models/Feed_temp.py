import wrappers
from app.commons.resources import my_logger
import datetime

class Feed_temp:
    _exists = False
    __TRACKED_FILEDS = ["item_id", "item_type","row","col"]

    def export(self, to_database=False):
        data_dict = {}
        attributes = [a for a in dir(self) if not a.startswith('_') and not callable(getattr(self,a))]
        for an_attribute in attributes:
            data_dict[an_attribute] = getattr(self, an_attribute)

        if to_database:
            data_dict['item_id']=int(data_dict['item_id'].split('-')[-1])


        return data_dict

    def save(self, create_new=False):

        a_query = """
            INSERT INTO home_feed_temp
            (""" + ', '.join(self.__TRACKED_FILEDS) +  """)
            VALUES ( %(""" + ')s, %('.join(self.__TRACKED_FILEDS) + """)s )
            RETURNING *
        """


        db = wrappers.Postgres.init()
        cursor = wrappers.Postgres.get_cursor(db)
        try:
            cursor.execute(a_query, self.export(to_database=True))
            #if create_new:
            item_id = 'pp-feed-' + str(cursor.fetchone()[0])
        except Exception ,e:
            print str(e)
            time=str(datetime.datetime.now())
            timestamp=time.split('.')[0]
            my_logger.error(str(timestamp)+"Home Feed Model Exception"+str(e))
            item_id = None

        db.commit()
        db.close()
        return item_id
