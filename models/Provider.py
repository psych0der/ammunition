import wrappers
import json
import traceback
import logging
import logging.handlers
from app.commons.resources import my_logger
import datetime
import traceback

class Provider:
    _token_id=-1
    _unique_error=False
    _exists = False
    __TRACKED_FILEDS = ["name", "campaign_start","campaign_end","active","status","date_created","date_modified",
    "slug_url", "cover_image","twitter_link","fb_link"]

    def __init__(self, provider_id):
        db = wrappers.Postgres.init()
        cursor = wrappers.Postgres.get_cursor(db)
        fields = ', '.join(self.__TRACKED_FILEDS)
        row_id = provider_id.split('-')[-1]
        self.provider_id = 'pp-provider-' + str(row_id)
        self.id = self.provider_id
        a_query = "SELECT " + fields + " FROM provider WHERE id = %(row_id)s"
        cursor.execute(a_query, {"row_id": row_id})
        result = cursor.fetchone()
        db.close()
        if result == None:
            return

        self._exists = True
        index = 0
        for a_field in self.__TRACKED_FILEDS:
            #print a_field
            value = result[index]
            #print a_field+str(type(value))
            setattr(self, a_field, value)   #storing attributes of the object of Content Provider
            index += 1

    def consume(self, data):
        for a_field in self.__TRACKED_FILEDS:
            if a_field not in data:
                try:
                    value = getattr(self, a_field)
                except AttributeError:
                    #if a_field =='preroll' or a_field=='splash' or a_field=='image_ads':
                    #    value=[]
                    #else:
                    value = None
                    if a_field=='status':
                        value='incomplete'
                    if a_field=='active':
                        value='no'
            else:
                value = data[a_field]
                if str(value) == "null":
                    value = None
                if a_field=='active' and value==None:
                    value='no'

            setattr(self, a_field, value)

    def export(self, to_database=False):
        data_dict = {}
        attributes = [a for a in dir(self) if not a.startswith('_') and not callable(getattr(self,a))]
        for an_attribute in attributes:
            data_dict[an_attribute] = getattr(self, an_attribute)
        if data_dict['campaign_end'] is not None:
            data_dict['campaign_end']=str(data_dict['campaign_end'])
        if data_dict['campaign_start'] is not None:
            data_dict['campaign_start']=str(data_dict['campaign_start'])
        if to_database:
            data_dict['id'] = self.provider_id.split('-')[-1]

        if self._exists:
            data_dict['provider_id'] = self.provider_id



        #print data_dict
        return data_dict

    def save(self, create_new=False):
        if self._exists == False:
            create_new = True

        if create_new == True:
            a_query = """
                INSERT INTO provider
                (""" + ', '.join(self.__TRACKED_FILEDS) +  """)
                VALUES ( %(""" + ')s, %('.join(self.__TRACKED_FILEDS) + """)s )
                RETURNING *
            """

        else:
            update_params = wrappers.Postgres.get_update_params(self.__TRACKED_FILEDS)
            a_query = " UPDATE provider SET " + update_params + " WHERE id=%(id)s"
            item_id = self.id

        db = wrappers.Postgres.init()
        cursor = wrappers.Postgres.get_cursor(db)
        try:

            cursor.execute(a_query, self.export(to_database=True))
            if create_new:
                item_id = 'pp-provider-' + str(cursor.fetchone()[0])
        except Exception as ex:
            db_e = wrappers.Postgres.init()
            cursor_e = wrappers.Postgres.get_cursor(db_e)
            cursor_e.execute("SELECT * FROM error_token")
            res=cursor_e.fetchone()
            trace = traceback.format_exc()
            time=str(datetime.datetime.now())
            timestamp= time.split('.')[0]
            my_logger.error('Token ID : '+str(res[0])+" Provider Model Exception ")
            my_logger.error(trace)
            cursor_e.execute("UPDATE error_token SET last_used=%(last_used)s",{'last_used':res[0]+1})
            db_e.commit()
            db_e.close()
            xx = ex
            if xx.pgcode == 23505 or xx.pgcode == '23505':
                self._unique_error = True
            item_id = None
            self._token_id=res[0]

        #if item_id:
            #ads_map.keep(item_id,self.export(),create_new)   #mapping after successful storage
        db.commit()
        db.close()
        return item_id
