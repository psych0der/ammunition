import wrappers
import json
import traceback
from app.commons.resources import my_logger
import datetime

class Raw_content_registry:
    _token_id=-1
    _exists = False
    __TRACKED_FILEDS = ["name", "status", "media_type", "source", "remote_id", "remarks", "remote_link","used","date_created","source_link"]

    def __init__(self, raw_content_id):
        db = wrappers.Postgres.init()
        cursor = wrappers.Postgres.get_cursor(db)
        fields = ', '.join(self.__TRACKED_FILEDS)
        row_id = raw_content_id.split('-')[-1]
        self.raw_content_id = 'pp-raw-content-' + str(row_id)
        self.id = self.raw_content_id
        a_query = "SELECT " + fields + " FROM raw_content_registry WHERE id = %(row_id)s"
        cursor.execute(a_query, {"row_id": row_id})
        result = cursor.fetchone()
        db.close()
        if result == None:
            return

        self._exists = True
        index = 0
        for a_field in self.__TRACKED_FILEDS:
            #print a_field
            value = result[index]
            #print a_field+str(type(value))
            setattr(self, a_field, value)   #storing attributes of the object of Sponsor
            index += 1

    def consume(self, data):
        for a_field in self.__TRACKED_FILEDS:
            if a_field not in data:
                try:
                    value = getattr(self, a_field)
                except AttributeError:
                    if a_field != 'used':
                        value = None
                    else:
                        value='no'
            else:
                value = data[a_field]
                if str(value) == "null":
                    value = None
                if a_field == 'used' and value== None :
                    value='no'


            setattr(self, a_field, value)

    def export(self, to_database=False):
        data_dict = {}
        attributes = [a for a in dir(self) if not a.startswith('_') and not callable(getattr(self,a))]
        for an_attribute in attributes:
            data_dict[an_attribute] = getattr(self, an_attribute)

        if to_database:
            data_dict['id'] = self.raw_content_id.split('-')[-1]

        if self._exists:
            data_dict['raw_content_id'] = self.raw_content_id

        #print data_dict
        return data_dict

    def save(self, create_new=False):
        if self._exists == False:
            create_new = True

        if create_new == True:
            a_query = """
                INSERT INTO raw_content_registry
                (""" + ', '.join(self.__TRACKED_FILEDS) +  """)
                VALUES ( %(""" + ')s, %('.join(self.__TRACKED_FILEDS) + """)s )
                RETURNING *
            """
            #print "insert query "+a_query
        else:
            update_params = wrappers.Postgres.get_update_params(self.__TRACKED_FILEDS)
            a_query = " UPDATE raw_content_registry SET " + update_params + " WHERE id=%(id)s"
            item_id = self.id

        db = wrappers.Postgres.init()
        cursor = wrappers.Postgres.get_cursor(db)
        try:

            cursor.execute(a_query, self.export(to_database=True))
            if create_new:
                item_id = 'pp-raw-content-' + str(cursor.fetchone()[0])
        except Exception, e:
            db_e = wrappers.Postgres.init()
            cursor_e = wrappers.Postgres.get_cursor(db_e)
            cursor_e.execute("SELECT * FROM error_token")
            res=cursor_e.fetchone()
            print str(e)
            time=str(datetime.datetime.now())
            timestamp= time.split('.')[0]
            my_logger.error('Token ID : '+str(res[0])+" Raw Content Model Exception "+str(e))
            cursor_e.execute("UPDATE error_token SET last_used=%(last_used)s",{'last_used':res[0]+1})
            db_e.commit()
            db_e.close()
            item_id = None
            self._token_id=res[0]

        db.commit()
        db.close()
        return item_id

    def delete(self):
        #delete from config
        a_query="DELETE FROM raw_content_registry where id=%(id)s"
        db=wrappers.Postgres.init()
        cursor=wrappers.Postgres.get_cursor(db)
        item_id = self.id
        try:
            cursor.execute(a_query,{'id':int(self.id.split('-')[-1])})
        except Exception, e:
            print "could not delete"+str(e)
            time=str(datetime.datetime.now())
            timestamp=time.split('.')[0]
            my_logger.error(str(timestamp)+"Raw Content Model Exception"+str(e))
            traceback.print_exc()
            item_id = None
        db.commit()
        db.close
        return item_id
