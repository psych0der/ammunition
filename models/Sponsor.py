import wrappers
import json
import traceback
from app.commons.modules import ads_map
import logging
import logging.handlers
from app.commons.resources import my_logger
import datetime
import traceback


class Sponsor:
    _token_id=-1
    _unique_error = False
    _exists = False
    __TRACKED_FILEDS = ["name", "logo", "image", "description", "description_small", "tagline", "active", "app_data",
    "url", "status","app_package","app_package_ios","play_store_url","ios_store_url","apk","date_created","date_modified",
    "slug_url","cover_image", "twitter_link", "fb_link","is_global"]


    def __init__(self, sponsor_id):
        db = wrappers.Postgres.init()
        cursor = wrappers.Postgres.get_cursor(db)
        fields = ', '.join(self.__TRACKED_FILEDS)
        row_id = sponsor_id.split('-')[-1]
        self.sponsor_id = 'pp-sponsor-' + str(row_id)
        self.id = self.sponsor_id
        a_query = "SELECT " + fields + " FROM sponsors WHERE id = %(row_id)s"
        cursor.execute(a_query, {"row_id": row_id})
        result = cursor.fetchone()
        db.close()
        if result == None:
            return

        self._exists = True
        index = 0
        for a_field in self.__TRACKED_FILEDS:
            #print a_field
            value = result[index]
            #print a_field+str(type(value))
            setattr(self, a_field, value)   #storing attributes of the object of Sponsor
            index += 1

    def consume(self, data):
        for a_field in self.__TRACKED_FILEDS:
            if a_field not in data:
                try:
                    value = getattr(self, a_field)
                except AttributeError:
                    #if a_field =='preroll' or a_field=='splash' or a_field=='image_ads':
                    #    value=[]
                    #else:
                    value = None
            else:
                value = data[a_field]
                if str(value) == "null":
                    value = None

            setattr(self, a_field, value)

    def export(self, to_database=False):
        data_dict = {}
        attributes = [a for a in dir(self) if not a.startswith('_') and not callable(getattr(self,a))]
        for an_attribute in attributes:
            data_dict[an_attribute] = getattr(self, an_attribute)

        if to_database:
            if type(data_dict['app_data']) not in [dict, type(None)]:
                data_dict['app_data'] = json.loads(data_dict['app_data'])
            data_dict['app_data'] = wrappers.Postgres.typecast_json(data_dict['app_data'])
            data_dict['id'] = self.sponsor_id.split('-')[-1]
            #data_dict['preroll']=json.dumps(data_dict['preroll'])   #converting to string
            #data_dict['splash']=json.dumps(data_dict['splash'])
            #data_dict['image_ads']=json.dumps(data_dict['image_ads'])

        if self._exists:
            data_dict['sponsor_id'] = self.sponsor_id

        return data_dict

    def save(self, create_new=False):
        if self._exists == False:
            create_new = True

        if create_new == True:
            a_query = """
                INSERT INTO sponsors
                (""" + ', '.join(self.__TRACKED_FILEDS) +  """)
                VALUES ( %(""" + ')s, %('.join(self.__TRACKED_FILEDS) + """)s )
                RETURNING *
            """

        else:
            update_params = wrappers.Postgres.get_update_params(self.__TRACKED_FILEDS)
            a_query = " UPDATE sponsors SET " + update_params + " WHERE id=%(id)s"
            item_id = self.id

        db = wrappers.Postgres.init()
        cursor = wrappers.Postgres.get_cursor(db)
        try:

            cursor.execute(a_query, self.export(to_database=True))
            if create_new:
                item_id = 'pp-sponsor-' + str(cursor.fetchone()[0])
                self.id=int(item_id.split('-')[-1])
                self.sponsor_id=item_id
        except Exception as ex:
            db_e = wrappers.Postgres.init()
            cursor_e = wrappers.Postgres.get_cursor(db_e)
            cursor_e.execute("SELECT * FROM error_token")
            res=cursor_e.fetchone()
            trace = traceback.format_exc()
            time=str(datetime.datetime.now())
            timestamp= time.split('.')[0]
            my_logger.error('Token ID : '+str(res[0])+" Sponsor Model Exception ")
            my_logger.error(trace)
            cursor_e.execute("UPDATE error_token SET last_used=%(last_used)s",{'last_used':res[0]+1})
            db_e.commit()
            db_e.close()
            xx = ex
            if xx.pgcode == 23505 or xx.pgcode == '23505':
                self._unique_error = True
            item_id = None
            self._token_id=res[0]


        #if item_id:
            #ads_map.keep(item_id,self.export(),create_new)   #mapping after successful storage
        if item_id!=None:
            self._exists=True
        db.commit()
        db.close()
        return item_id
