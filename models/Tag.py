import wrappers
import logging
import logging.handlers
from app.commons.resources import my_logger
import datetime

class Tag:
    _token_id=-1
    _exists = False
    __TRACKED_FILEDS = ["name","display_name","poster","poster_small","collection","status","ready","date_created","date_modified","description","slug_url","cover_image","short_description","tagline","show_title","seo"]

    def __init__(self, tag_id):
        db = wrappers.Postgres.init()
        cursor = wrappers.Postgres.get_cursor(db)
        fields = ', '.join(self.__TRACKED_FILEDS)
        row_id = tag_id.split('-')[-1]
        self.tag_id = 'pp-tag-' + str(row_id)
        self.id = self.tag_id
        a_query = "SELECT " + fields + " FROM tags WHERE id = %(row_id)s"
        cursor.execute(a_query, {"row_id": row_id})
        result = cursor.fetchone()
        db.close()
        if result == None:
            return

        self._exists = True
        index = 0
        for a_field in self.__TRACKED_FILEDS:
            value = result[index]
            setattr(self, a_field, value)
            index += 1

    def consume(self, data):
        for a_field in self.__TRACKED_FILEDS:
            if a_field not in data:
                try:
                    value = getattr(self, a_field)
                except AttributeError:
                    value = None
            else:
                value = data[a_field]
                if str(value) == "null":
                    value = None

            setattr(self, a_field, value)

    def export(self, to_database=False):
        data_dict = {}
        attributes = [a for a in dir(self) if not a.startswith('_') and not callable(getattr(self,a))]
        for an_attribute in attributes:
            data_dict[an_attribute] = getattr(self, an_attribute)

        if to_database:
            data_dict['id'] = self.tag_id.split('-')[-1]

        if self._exists:
            data_dict['tag_id'] = self.tag_id
        return data_dict

    def save(self, create_new=False):
        if self._exists == False:
            create_new = True

        if create_new == True:
            a_query = """
                INSERT INTO tags
                (""" + ', '.join(self.__TRACKED_FILEDS) +  """)
                VALUES ( %(""" + ')s, %('.join(self.__TRACKED_FILEDS) + """)s )
                RETURNING *
            """
        else:
            update_params = wrappers.Postgres.get_update_params(self.__TRACKED_FILEDS)
            a_query = " UPDATE tags SET " + update_params + " WHERE id=%(id)s"
            item_id = self.id
        db = wrappers.Postgres.init()
        cursor = wrappers.Postgres.get_cursor(db)
        try:
            cursor.execute(a_query, self.export(to_database=True))
            if create_new:
                item_id = 'pp-tag-' + str(cursor.fetchone()[0])
        except Exception, e :
            db_e = wrappers.Postgres.init()
            cursor_e = wrappers.Postgres.get_cursor(db_e)
            cursor_e.execute("SELECT * FROM error_token")
            res=cursor_e.fetchone()
            time=str(datetime.datetime.now())
            timestamp=time.split('.')[0]
            my_logger.error('Token ID : '+str(res[0])+" Tag Model Exception "+str(e))
            print "Exception Tag"+str(e)
            cursor_e.execute("UPDATE error_token SET last_used=%(last_used)s",{'last_used':res[0]+1})
            db_e.commit()
            db_e.close()
            item_id = None
            self._token_id=res[0]

        db.commit()
        db.close()
        return item_id
