import wrappers
import json
import traceback
from app.commons.resources import my_logger
import datetime
class Version_store:
    _token_id=-1
    _exists = False
    __TRACKED_FILEDS = ['environment','version_data','live','parent','publish_date','date_created','description']

    def __init__(self, version_id):
        db = wrappers.Postgres.init()
        cursor = wrappers.Postgres.get_cursor(db)
        fields = ', '.join(self.__TRACKED_FILEDS)
        row_id = version_id.split('-')[-1]
        self.version_id = 'version-id-' + str(row_id)
        self.id = self.version_id
        a_query = "SELECT " + fields + " FROM version_store WHERE id = %(row_id)s"
        cursor.execute(a_query, {"row_id": row_id})
        result = cursor.fetchone()
        db.close()
        if result == None:
            return

        self._exists = True
        index = 0
        for a_field in self.__TRACKED_FILEDS:
            #print a_field
            value = result[index]
            #print a_field+str(type(value))
            setattr(self, a_field, value)   #storing attributes of the object of Sponsor
            index += 1

    def consume(self, data):
        for a_field in self.__TRACKED_FILEDS:
            if a_field not in data:
                try:
                    value = getattr(self, a_field)
                except AttributeError:
                    value = None
            else:
                value = data[a_field]
                if str(value) == "null" or str(value)=="":
                    value = None

            setattr(self, a_field, value)

    def export(self, to_database=False):
        data_dict = {}
        attributes = [a for a in dir(self) if not a.startswith('_') and not callable(getattr(self,a))]
        for an_attribute in attributes:
            data_dict[an_attribute] = getattr(self, an_attribute)

        if to_database: 
            data_dict['id'] = self.version_id.split('-')[-1]
            data_dict['version_data']=json.dumps(data_dict['version_data'])
            
        if self._exists:
            data_dict['version_id'] = self.version_id
        


        #print data_dict
        return data_dict

    def save(self, create_new=False):
        if self._exists == False:
            create_new = True

        if create_new == True:
            a_query = """
                INSERT INTO version_store
                (""" + ', '.join(self.__TRACKED_FILEDS) +  """)
                VALUES ( %(""" + ')s, %('.join(self.__TRACKED_FILEDS) + """)s )
                RETURNING *
            """
            #print "insert query "+a_query
        else:
            update_params = wrappers.Postgres.get_update_params(self.__TRACKED_FILEDS)
            a_query = " UPDATE version_store SET " + update_params + " WHERE id=%(id)s"
            item_id = self.id

        db = wrappers.Postgres.init()
        cursor = wrappers.Postgres.get_cursor(db)
        try:

            cursor.execute(a_query, self.export(to_database=True))
            if create_new:
                item_id = 'pp-version-' + str(cursor.fetchone()[2])
        except Exception, e:
            db_e = wrappers.Postgres.init()
            cursor_e = wrappers.Postgres.get_cursor(db_e)
            cursor_e.execute("SELECT * FROM error_token")
            res=cursor_e.fetchone()
            print "exception Version store"+str(e)
            time=str(datetime.datetime.now())
            timestamp= time.split('.')[0]
            my_logger.error('Token ID : '+str(res[0])+" Version Model Exception "+str(e))
            cursor_e.execute("UPDATE error_token SET last_used=%(last_used)s",{'last_used':res[0]+1})
            db_e.commit()
            db_e.close()
            item_id = None
            self._token_id=res[0]

        db.commit()
        db.close()
        return item_id
