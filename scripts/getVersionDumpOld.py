import psycopg2
import psycopg2.extras
import json
import sys
import os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)),os.pardir))
from config import POSTGRES_CREDS as PG
import time
version_num = sys.argv[1]
newfilepath = sys.argv[2]

def fetchDict(query):
    cursor.execute(query)
    records = cursor.fetchall()
    dict_result = []
    for row in records:
        dict_result.append(dict(row))
    return dict_result

def getLatestVersion():
    versionQuery = "SELECT version_data, id FROM version_store WHERE id=" + str(version_num)
    records = fetchDict(versionQuery)
    version_info = records[0]
    GLOBAL_OBJECT['version_id'] = version_info['id']
    VERSION_DATA = version_info['version_data']

    for channel_feed_obj in VERSION_DATA['channel-feed']:
        for content_obj in channel_feed_obj['content']:
            if content_obj['type']=='image_ad':
                a_query="SELECT id,resource_location from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s and ad_id=%(ad_id)s"
                cursor.execute(a_query,{'sponsor_id':int(content_obj['id'].split('-')[-2]),'ad_type':'image_ad','ad_id':int(content_obj['id'].split('-')[-1])})
                res=cursor.fetchone()
                if res != None:
                    content_obj['ad_id']='pp-image-ad-'+content_obj['id'].split('-')[-2]+'-'+str(res[0])

        if 'preroll' in channel_feed_obj['sellable_entities']:
            for sellable_obj in channel_feed_obj['sellable_entities']['preroll']:
                    a_query="SELECT id,resource_location from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s and ad_id=%(ad_id)s"
                    cursor.execute(a_query,{'sponsor_id':int(sellable_obj['preroll_id'].split('-')[-2]),'ad_type':'preroll','ad_id':int(sellable_obj['preroll_id'].split('-')[-1])})
                    res=cursor.fetchone()
                    if res!=None:
                        sellable_obj['ad_id']='pp-pre-roll-'+sellable_obj['preroll_id'].split('-')[-2]+'-'+str(res[0])

        if 'splash' in channel_feed_obj['sellable_entities']:
            for sellable_obj in channel_feed_obj['sellable_entities']['splash']:
                    a_query="SELECT id,resource_location from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s and ad_id=%(ad_id)s"
                    cursor.execute(a_query,{'sponsor_id':int(sellable_obj['splash_id'].split('-')[-2]),'ad_type':'splash','ad_id':int(sellable_obj['splash_id'].split('-')[-1])})
                    res=cursor.fetchone()
                    if res!=None:
                        sellable_obj['ad_id']='pp-splash-'+sellable_obj['splash_id'].split('-')[-2]+'-'+str(res[0])

    for channel_feed_obj in VERSION_DATA['home-feed']:
        for content_obj in channel_feed_obj:
            if content_obj['type']=='image_ad':
                a_query="SELECT id,resource_location from ads_map where sponsor_id=%(sponsor_id)s and ad_type=%(ad_type)s and ad_id=%(ad_id)s"
                cursor.execute(a_query,{'sponsor_id':int(content_obj['id'].split('-')[-2]),'ad_type':'image_ad','ad_id':int(content_obj['id'].split('-')[-1])})
                res=cursor.fetchone()
                if res != None:
                    content_obj['ad_id']='pp-image-ad-'+content_obj['id'].split('-')[-2]+'-'+str(res[0])

    return VERSION_DATA

def dumpLatestVersion():
    outfile = open('latestdump.json', 'w')
    outfile.write(json.dumps(VERSION_DATA))
    outfile.close()

def getChannelInfo(channel_id):
    channelQuery = "SELECT ch.display_name, ch.icon, ch.icon_small, ch.id FROM channels ch WHERE ch.id = %s" % str(channel_id)
    results = fetchDict(channelQuery)
    channel_info = results[0]
    channel_object = {
    'ppId': "pp-channel-" + str(channel_info['id']),
    'type': "channel",
    'name': channel_info['display_name'],
    'poster': channel_info['icon'],
    'posterSmall': channel_info['icon_small'],
    'status': True
    }
    ALL_CHANNEL[channel_object['ppId']] = {}
    ALL_CHANNEL[channel_object['ppId']]['itemData'] = channel_object
    ALL_CHANNEL[channel_object['ppId']]['itemContents'] = []
    return channel_object

def getContentInfo(content_id):
    contentQuery = "SELECT c.name, c.raw_content_id, c.local_link, c.poster, c.poster_small, c.id, c.remote_id, c.type, c.description,c.premium,c.expiry,c.cost,c.seo, p.name AS provider_name FROM content AS c LEFT JOIN provider AS p ON c.provider_id=p.id WHERE c.id = %s" % str(content_id)
    results = fetchDict(contentQuery)
    content_info = results[0]
    content_object = {
        'ppId': "pp-content-" + str(content_id),
        'name': content_info['name'],
        'poster': content_info['poster'],
        'posterSmall': content_info['poster_small'],
        'remoteId': content_info['remote_id'],
        'rawId': content_info['raw_content_id'],
        'localUrl': content_info['local_link'],
        'type': content_info['type'],
        'status': True,
        'description': content_info['description'],
        'providerName': content_info['provider_name'],
        'isPremium': content_info['premium'],
        'expiry': content_info['expiry'],
        'cost': content_info['cost']
        }
    ALL_CONTENT[content_object['ppId']] = {}
    ALL_CONTENT[content_object['ppId']]['itemData'] = content_object
    CONTENT_POOL.append(content_id)
    # ALL_CONTENT[content_object['ppId']]['itemContents'] = getRelatedContent(content_id)
    return content_object


def getSponsorInfo(sponsor_id):
    sponsorQuery = "SELECT id, name, logo, image, description, description_small, tagline, url, app_package,app_package_ios,ios_store_url,play_store_url FROM sponsors WHERE id = %s" % str(sponsor_id)
    results = fetchDict(sponsorQuery)
    sponsor_info = results[0]
    sponsor_object = {
                            "status": True,
                            "description": sponsor_info['description'],
                            "image": sponsor_info['image'],
                            "logo": sponsor_info['logo'],
                            "ppId": "pp-sponsor-" + str(sponsor_info['id']),
                            "descriptionSmall": sponsor_info['description_small'],
                            "name": sponsor_info['name'],
                            "url": sponsor_info['url'],
                            "tagline": sponsor_info['tagline'],
                            "packageName": sponsor_info['app_package'],
                            "type": "sponsor",
                            "iosPackageName": sponsor_info['app_package_ios'],
                            "playStoreUrl": sponsor_info['play_store_url'],
                            "iosStoreUrl": sponsor_info['ios_store_url']
                        }
    return sponsor_object

def getImageAd(ad_id):
    temp_id = ad_id.split('-')[-1]
    adQuery = "SELECT am.name,am.sponsor_id, am.resource_location, am.id FROM ads_map am WHERE am.id = %s" % str(temp_id)
    results = fetchDict(adQuery)
    adInfo = results[0]
    ad_object = {
                "status": True,
                "name": adInfo['name'],
                "poster": adInfo['resource_location'],
                "sponsorInfo": getSponsorInfo(adInfo['sponsor_id']),
                "ppId": ad_id,
                "type": "image_ad",
            }
    return ad_object

def getPreRoll(ad_id):
    temp_id = ad_id.split('-')[-1]
    adQuery = "SELECT am.name,am.sponsor_id, am.resource_location, am.id FROM ads_map am WHERE am.id = %s" % str(temp_id)
    results = fetchDict(adQuery)
    adInfo = results[0]
    ad_object = {
                "status": True,
                "name": adInfo['name'],
                "playbackUrl": adInfo['resource_location'],
                "sponsorId": "pp-sponsor-" + str(adInfo['sponsor_id']),
                "ppId": ad_id,
                "type": "pre_roll"
            }
    return ad_object

def getSplash(ad_id):
    temp_id = ad_id.split('-')[-1]
    adQuery = "SELECT am.name,am.sponsor_id, am.resource_location, am.id FROM ads_map am WHERE am.id = %s" % str(temp_id)
    results = fetchDict(adQuery)
    adInfo = results[0]
    ad_object = {
                "status": True,
                "name": adInfo['name'],
                "playbackUrl": adInfo['resource_location'],
                "sponsorId": "pp-sponsor-" + str(adInfo['sponsor_id']),
                "ppId": ad_id,
                "type": "splash"
            }
    return ad_object


def getCollectionInfo(collection_id):
    collectionQuery = "SELECT display_name, poster, poster_small, id FROM tags WHERE id = %s" % str(collection_id)
    results = fetchDict(collectionQuery)
    collection_info = results[0]
    collection_object = {
                'ppId': "pp-collection-" + str(collection_id),
                'name': collection_info['display_name'],
                'poster': collection_info['poster'],
                'posterSmall': collection_info['poster_small'],
                'type': "collection",
                'status': True
              }
    ALL_COLLECTION[collection_object['ppId']] = {}
    ALL_COLLECTION[collection_object['ppId']]['itemData'] = collection_object
    ALL_COLLECTION[collection_object['ppId']]['itemContents'] = []
    return collection_object


def getHomeFeed():
    GLOBAL_OBJECT['feeds']['home'] = []
    home_feed = VERSION_DATA['home-feed']
    for aRow in home_feed:
        temp_list = []
        for counter, item in enumerate(aRow) :
            item_id = item['id'].split('-')[-1]
            item_type = item['type']
            item_info = {}
            if item_type == "channel":
                item_info = getChannelInfo(item_id)
            elif item_type == "content":
                item_info = getContentInfo(item_id)
            elif item_type == "collection":
                item_info = getCollectionInfo(item_id)
            elif item_type == "image_ad":
                item_info = getImageAd(item['ad_id'])
            temp_list.append(item_info)
            if counter == len(aRow)-1:
                GLOBAL_OBJECT['feeds']['home'].append(temp_list)


def getCollectionFeed():
    GLOBAL_OBJECT['feeds']['collection'] = []
    collection_feed = VERSION_DATA['collection-feed']
    for collection in collection_feed:
        collection_id = collection['collection_id']
        collection_id_new = "pp-collection-" + str(collection_id.split('-')[-1])
        collection_object = {}
        if collection_id in ALL_COLLECTION:
            collection_object = ALL_COLLECTION[collection_id]['itemData']
        else:
            collection_object = getCollectionInfo(collection_id.split('-')[-1])
        GLOBAL_OBJECT['feeds']['collection'].append([collection_object])

        for item in collection['data']:
            item_id = item['id']
            item_object = {}
            if item['type'] == 'content':
                if item_id in ALL_CONTENT:
                    item_object = ALL_CONTENT[item_id]['itemData']
                else:
                    item_object = getContentInfo(item_id.split('-')[-1])
            elif item['type'] == 'channel':
                if item_id in ALL_CHANNEL:
                    item_object = ALL_CHANNEL[item_id]['itemData']
                else:
                    item_object = getChannelInfo(item_id.split('-')[-1])
            ALL_COLLECTION[collection_id_new]['itemContents'].append([item_object])


def getChannelFeed():
    GLOBAL_OBJECT['feeds']['channel'] = []
    channel_feed = VERSION_DATA['channel-feed']
    temp_list = []
    for counter, channel in enumerate(channel_feed):
        channel_id = channel['channel_id']
        channel_object = {}
        if channel_id in ALL_CHANNEL:
            channel_object = ALL_CHANNEL[channel_id]['itemData']
        else:
            channel_object = getChannelInfo(channel_id.split('-')[-1])

        if len(temp_list) == 2:
            GLOBAL_OBJECT['feeds']['channel'].append(temp_list)
            temp_list = []

        if channel_id not in ['pp-channel-72', 'pp-channel-79']:
            temp_list.append(channel_object)

        if counter == len(channel_feed)-1:
            if len(temp_list) > 0:
                GLOBAL_OBJECT['feeds']['channel'].append(temp_list)

        for content in channel['content']:
            content_id = content['id'].split('-')[-1]
            content_object = {}
            if content['type'] == 'content':
                content_object = getContentInfo(content_id)
            elif content['type'] == 'image_ad':
                content_object = getImageAd(content['ad_id'])
            ALL_CHANNEL[channel_id]['itemContents'].append([content_object])

        if len(channel['sellable_entities']) > 0:

            if 'top-bar-sponsor' in channel['sellable_entities']:
                sponsor_id = channel['sellable_entities']['top-bar-sponsor'].split('-')[-1]
                ALL_CHANNEL[channel_id]['itemData']['topBarSponsor'] = getSponsorInfo(sponsor_id)

            if 'preroll' in channel['sellable_entities']:
                ALL_CHANNEL[channel_id]['itemData']['preRollPool'] = []
                for preroll in channel['sellable_entities']['preroll']:
                    preroll_object = getPreRoll(preroll['ad_id'])
                    ALL_CHANNEL[channel_id]['itemData']['preRollPool'].append(preroll_object)

            if 'splash' in channel['sellable_entities']:
                ALL_CHANNEL[channel_id]['itemData']['splashPool'] = []
                for splash in channel['sellable_entities']['splash']:
                    splash_object = getSplash(splash['ad_id'])
                    ALL_CHANNEL[channel_id]['itemData']['splashPool'].append(splash_object)


def getRelatedContent(content_id):
    relatedQuery = "SELECT c.name, c.raw_content_id, c.local_link, c.poster, c.poster_small, c.id, c.remote_id, c.type, c.description, c.premium, c.expiry, c.cost, c.seo,  p.name AS provider_name FROM content AS c, provider AS p WHERE c.provider_id = p.id AND c.id IN (SELECT c.id FROM tags_map tm LEFT JOIN content c ON tm.item_id = c.id WHERE tm.item_type = 'content' AND c.id IN (%s) AND c.id != %s AND tm.tag_id IN (SELECT tag_id FROM tags_map WHERE item_id = %s AND item_type='content')) AND c.status='complete'" % ( ", ".join(map(str, CONTENT_POOL)), str(content_id), str(content_id))
    results = fetchDict(relatedQuery)
    content_list = []
    for content_info in results:
        content_object = {
            'ppId': "pp-content-" + str(content_info['id']),
            'name': content_info['name'],
            'poster': content_info['poster'],
            'posterSmall': content_info['poster_small'],
            'remoteId': content_info['remote_id'],
            'rawId': content_info['raw_content_id'],
            'localUrl': content_info['local_link'],
            'type': content_info['type'],
            'status': True,
            'description': content_info['description'],
            'providerName': content_info['provider_name'],
            'isPremium': content_info['premium'],
            'expiry': content_info['expiry'],
            'cost': content_info['cost']
            }
        content_list.append([content_object])
    return content_list


def attachRelatedContent():
    for content_id in CONTENT_POOL:
        ALL_CONTENT["pp-content-" + str(content_id)]['itemContents'] = getRelatedContent(content_id)


def getVersionDump():
    global cursor
    global VERSION_DATA
    global GLOBAL_OBJECT
    global CONTENT_POOL
    global ALL_CONTENT
    global ALL_COLLECTION
    global ALL_CHANNEL
    global newfilepath

    VERSION_DATA = {}
    GLOBAL_OBJECT = {}
    GLOBAL_OBJECT['feeds'] = {}
    CONTENT_POOL = []
    ALL_CONTENT = {}
    ALL_COLLECTION = {}
    ALL_CHANNEL = {}

    try:
        PG_CONN = "host='%s' dbname='%s' user='%s' password='%s'" % (PG['HOST'], PG['DATABASE'], PG['USER'], PG['PASSWORD'])
        conn = psycopg2.connect(PG_CONN)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        VERSION_DATA = getLatestVersion()
        getHomeFeed()
        getCollectionFeed()
        getChannelFeed()
        CONTENT_POOL = list(set(CONTENT_POOL))
        attachRelatedContent()

        GLOBAL_OBJECT['channel'] = ALL_CHANNEL
        GLOBAL_OBJECT['collection'] = ALL_COLLECTION
        GLOBAL_OBJECT['content'] = ALL_CONTENT

        newfilepath = str(newfilepath)
        outfile = open(newfilepath, 'w')
        outfile.write(json.dumps(GLOBAL_OBJECT, indent=4))
        outfile.close()

        return True

    except Exception, e:
        print e
        return False

getVersionDump()