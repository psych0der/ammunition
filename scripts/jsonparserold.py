# how to use:
# python jsonparser.py <arguments>
# argument types:
# -s <source.json>          required
# -d <destination.json>     optional
# -m <raw_content_link.txt> optional
# -a <app_links.txt>        optional
# -E <entity type/key>      optional
# -D <entity id>            optional
# -x cancel download        optional
# -A download all           optional
# -l download location      optional    (default downloadfiles)

import requests
import json
import os
import argparse
import sys
import urllib

POSTER_STATIC = "http://pressplaytv.in/static/posters/"
STREAM_STATIC = "http://pressplaytv.in/stream/"
DOWNLOAD_STATIC = "http://pressplaytv.in/download/"
DOWNLOAD_STATIC_APP = "http://pressplaytv.in/download-app/"
UNCHAINED_STATIC = "http://pressplaytv.in/stream/unchained/"

parser = argparse.ArgumentParser()

parser.add_argument('-s', dest='source_file',
                    help='Source json')
parser.add_argument('-d', dest='dest_file',
                    help='destination json')
parser.add_argument('-m', dest='media_file',
                    help='raw media links file location')
parser.add_argument('-a', dest='applinks_file',
                    help='Source json')
parser.add_argument('-E', dest='entity_type',
                    help='Source json')
parser.add_argument('-D', dest='download_string',
                    help='Source json')
parser.add_argument('-x', action='store_false', default=True, dest='no_download',
                    help='No Download Flag')
parser.add_argument('-A', action='store_true', default=False, dest='download_all',
                    help='Download all flag')
parser.add_argument('-l',  dest='download_location',
                    help='Download files location')

arguments = parser.parse_args()

download_location = 'downloadfiles'

if arguments.source_file is None:
    print "please provide source file"
    sys.exit(1)

if arguments.download_location is not None:
    download_location = arguments.download_location

if not os.path.exists(download_location):
    os.makedirs(download_location)

with open(arguments.source_file, 'r') as infile:
    data = infile.read()

readjson = json.loads(data)

feeds = readjson['feeds']
key_list = ['channel', 'content', 'collection']
localurl = {}
download_object = {}

# channel_list = set()
# content_list = set()
# sponsor_list = set()
app_links = set()

for itemkey in key_list:
    for key, value in readjson[itemkey].iteritems():
        if key not in download_object:
            download_object[key] = {}
        tempdata = value['itemData']

        url = tempdata['posterSmall']
        download_object[key]['posterSmall'] = url
        # channel_list.add(url)
        local_filename = url.split('/')[-1]
        value['itemData']['posterSmall'] = POSTER_STATIC+local_filename

        url = tempdata['poster']
        download_object[key]['poster'] = url
        # channel_list.add(url)
        local_filename = url.split('/')[-1]
        value['itemData']['poster'] = POSTER_STATIC+local_filename

        if tempdata['type'] == 'video':
            url = tempdata['localUrl']
            download_object[key]['localUrl'] = url
            content_id = tempdata['ppId']
            localurl[content_id] = url
            tempurl = url.split('/')[-1].split('.')
            # content_list.add(url)
            value['itemData']['localUrl'] = STREAM_STATIC+str(content_id)+"/"+str(tempurl[1])
            value['itemData']['downloadUrl'] = DOWNLOAD_STATIC+str(content_id)+"/"+str(tempurl[1])


        if 'preRollPool' in tempdata:
            for k, extra in enumerate(tempdata['preRollPool']):
                sponsor_id = tempdata['preRollPool'][k]['sponsorId']
                if sponsor_id not in download_object:
                    download_object[sponsor_id] = {}
                url = tempdata['preRollPool'][k]['playbackUrl']
                download_object[sponsor_id]['playbackUrl'] = url
                # sponsor_list.add(url)
                tempurl = url.split('/')[-1].split('.')
                prerollname = tempurl[0]
                for y in range(1, len(tempurl)-2):
                    prerollname += '.'+tempurl[y]
                value['itemData']['preRollPool'][k]['playbackUrl'] = UNCHAINED_STATIC+str(prerollname)+"/"+str(tempurl[-1])


        if 'topBarSponsor' in tempdata:
            sponsor_id = tempdata['topBarSponsor']['ppId']
            if sponsor_id not in download_object:
                download_object[sponsor_id] = {}
            url = tempdata['topBarSponsor']['logo']
            download_object[sponsor_id]['logo'] = url
            # sponsor_list.add(url)
            local_filename = url.split('/')[-1]
            value['itemData']['topBarSponsor']['logo'] = POSTER_STATIC+local_filename
            if 'playStoreUrl' in tempdata['topBarSponsor'] and tempdata['topBarSponsor']['playStoreUrl'] != None and len(tempdata['topBarSponsor']['playStoreUrl']) > 1:
                    app_links.add(tempdata['topBarSponsor']['playStoreUrl'])
            # if 'iosStoreUrl' in tempdata['topBarSponsor'] and tempdata['topBarSponsor']['iosStoreUrl'] != None and len(tempdata['topBarSponsor']['iosStoreUrl']) > 1:
            #         app_links.add(tempdata['topBarSponsor']['iosStoreUrl'])
            # print value['itemData']['topBarSponsor']['logo']

            url = tempdata['topBarSponsor']['image']
            download_object[sponsor_id]['image'] = url
            # sponsor_list.add(url)
            local_filename = url.split('/')[-1]
            value['itemData']['topBarSponsor']['image'] = POSTER_STATIC+local_filename

            if value['itemData']['topBarSponsor']['packageName'] is None:
                value['itemData']['topBarSponsor']['packageName'] = ''

            if value['itemData']['topBarSponsor']['packageName'] != '':
                value['itemData']['topBarSponsor']['downloadUrl'] = DOWNLOAD_STATIC_APP+value['itemData']['topBarSponsor']['ppId']+'/ANDROID'

            # print value['itemData']['topBarSponsor']['image']

        try:
            if value['itemContents'] is not None and len(value['itemContents']) > 0:
                for i, content in enumerate(value['itemContents']):
                    for j, faltudata in enumerate(content):

                        temp_id = faltudata['ppId']
                        if temp_id not in download_object:
                            download_object[temp_id] = {}

                        if faltudata['type'] in ['channel', 'video', 'collection']:
                            url = faltudata['poster']
                            download_object[temp_id]['poster'] = url
                            # content_list.add(url)
                            local_filename = url.split('/')[-1]
                            value['itemContents'][i][j]['poster'] = POSTER_STATIC+local_filename

                            url = faltudata['posterSmall']
                            download_object[temp_id]['posterSmall'] = url
                            # content_list.add(url)
                            local_filename = url.split('/')[-1]
                            value['itemContents'][i][j]['posterSmall'] = POSTER_STATIC+local_filename

                        if faltudata['type'] not in ['channel', 'image_ad']:
                            url = faltudata['localUrl']
                            content_id = faltudata['ppId']
                            download_object[temp_id]['localUrl'] = url
                            # content_list.add(url)
                            localurl[content_id] = url
                            tempurl = url.split('/')[-1].split('.')
                            value['itemContents'][i][j]['localUrl'] = STREAM_STATIC+str(content_id)+"/"+str(tempurl[1])
                            value['itemContents'][i][j]['downloadUrl'] = DOWNLOAD_STATIC+str(content_id)+"/"+str(tempurl[1])

                        elif faltudata['type'] == 'image_ad':
                            url = faltudata['poster']
                            temp_id2 = faltudata['sponsorInfo']['ppId']
                            if temp_id2 not in download_object:
                                download_object[temp_id2] = {}
                            download_object[temp_id2]['poster'] = url
                            # sponsor_list.add(url)
                            local_filename = url.split('/')[-1]
                            value['itemContents'][i][j]['poster'] = POSTER_STATIC+local_filename
                            value['itemContents'][i][j]['sponsorInfo']['downloadUrl'] = DOWNLOAD_STATIC_APP+value['itemContents'][i][j]['sponsorInfo']['ppId']+'/ANDROID'

                            url = faltudata['sponsorInfo']['logo']
                            download_object[temp_id2]['logo'] = url

                            # sponsor_list.add(url)
                            local_filename = url.split('/')[-1]
                            value['itemContents'][i][j]['sponsorInfo']['logo'] = POSTER_STATIC+url.split('/')[-1]

                            url = faltudata['sponsorInfo']['image']
                            # sponsor_list.add(url)
                            local_filename = url.split('/')[-1]
                            value['itemContents'][i][j]['sponsorInfo']['image'] = POSTER_STATIC+local_filename

                        elif faltudata['type'] == 'channel':
                            if 'preRollPool' in faltudata:
                                for k, extra in enumerate(faltudata['preRollPool']):
                                    sponsor_id = faltudata['preRollPool'][k]['ppId']
                                    if sponsor_id not in download_object:
                                        download_object[sponsor_id] = {}
                                    url = faltudata['preRollPool'][k]['playbackUrl']
                                    download_object[sponsor_id]['playbackUrl'] = url
                                    # sponsor_list.add(url)
                                    tempurl = url.split('/')[-1].split('.')
                                    local_filename = tempurl[0]
                                    for y in range(1, len(tempurl)-2):
                                        local_filename += tempurl[y]
                                    value['itemContents'][i][j]['preRollPool'][k]['playbackUrl'] = UNCHAINED_STATIC+str(local_filename)+"/"+str(tempurl[-1])

                            if 'topBarSponsor' in faltudata:
                                sponsor_id = faltudata['topBarSponsor']['ppId']
                                if sponsor_id not in download_object:
                                    download_object[sponsor_id] = {}
                                download_object[sponsor_id]['logo'] = url
                                url = faltudata['topBarSponsor']['logo']
                                # sponsor_list.add(url)
                                faltudata['topBarSponsor']['logo'] = POSTER_STATIC+url.split('/')[-1]

                                url = faltudata['topBarSponsor']['image']
                                download_object[sponsor_id]['image'] = url
                                # sponsor_list.add(url)
                                local_filename = url.split('/')[-1]
                                value['itemContents'][i][j]['topBarSponsor']['image'] = POSTER_STATIC+local_filename

                                if faltudata['topBarSponsor']['packageName'] is None:
                                    value['itemContents'][i][j]['topBarSponsor'] = ''

                                if faltudata['topBarSponsor']['packageName'] != '':
                                    value['itemContents'][i][j]['topBarSponsor'] = DOWNLOAD_STATIC_APP+faltudata['topBarSponsor']['ppId']+'/ANDROID'

        except TypeError, e:
            print e
            continue

for newkey, value in readjson['feeds'].iteritems():
    for i, item in enumerate(value):
        for j, faltudata in enumerate(item):
            if faltudata['type'] in ['channel', 'video', 'collection']:
                temp_id = faltudata['ppId']
                if temp_id not in download_object:
                    download_object[temp_id] = {}
                url = faltudata['poster']
                download_object[temp_id]['poster'] = url
                # channel_list.add(url)
                local_filename = url.split('/')[-1]
                value[i][j]['poster'] = POSTER_STATIC+local_filename

                url = faltudata['posterSmall']
                download_object[temp_id]['posterSmall'] = url
                # channel_list.add(url)
                local_filename = url.split('/')[-1]
                value[i][j]['posterSmall'] = POSTER_STATIC+local_filename

            if faltudata['type'] != 'image_ad' and faltudata['type'] != 'channel':
                if 'localUrl' in faltudata:
                    url = faltudata['localUrl']
                    content_id = faltudata['ppId']
                    download_object[temp_id]['localUrl'] = url
                    # content_list.add(url)
                    localurl[content_id] = url
                    tempurl = url.split('/')[-1].split('.')
                    value[i][j]['localUrl'] = STREAM_STATIC+str(content_id)+"/"+str(tempurl[1])
                    value[i][j]['downloadUrl'] = DOWNLOAD_STATIC+str(content_id)+"/"+str(tempurl[1])

            elif faltudata['type'] == 'image_ad':
                temp_id = faltudata['sponsorInfo']['ppId']
                if temp_id not in download_object:
                    download_object[temp_id] = {}
                url = faltudata['poster']
                download_object[temp_id]['poster'] = url
                # sponsor_list.add(url)
                local_filename = url.split('/')[-1]
                value[i][j]['poster'] = POSTER_STATIC+local_filename

                url = faltudata['sponsorInfo']['logo']
                download_object[temp_id]['logo'] = url
                # sponsor_list.add(url)

                value[i][j]['sponsorInfo']['downloadUrl']=DOWNLOAD_STATIC_APP+faltudata['sponsorInfo']['ppId']+'/ANDROID'
                local_filename = url.split('/')[-1]
                value[i][j]['sponsorInfo']['logo'] = POSTER_STATIC+local_filename

                url = faltudata['sponsorInfo']['image']
                download_object[temp_id]['image'] = url
                # sponsor_list.add(url)
                local_filename = url.split('/')[-1]
                value[i][j]['sponsorInfo']['image'] = POSTER_STATIC+local_filename

            elif faltudata['type'] == 'channel':
                if 'preRollPool' in faltudata:
                    for k, extra in enumerate(faltudata['preRollPool']):
                        url = faltudata['preRollPool'][k]['playbackUrl']
                        # sponsor_list.add(url)
                        tempurl = url.split('/')[-1].split('.')
                        prerollname = tempurl[0]
                        for y in range(1, len(tempurl)-2):
                            prerollname += '.'+tempurl[y]
                        value[i][j]['preRollPool'][k]['playbackUrl'] = UNCHAINED_STATIC+str(prerollname)+"/"+str(tempurl[-1])

                if 'topBarSponsor' in faltudata:
                    if 'playStoreUrl' in faltudata['topBarSponsor'] and faltudata['topBarSponsor']['playStoreUrl'] != None and len(faltudata['topBarSponsor']['playStoreUrl']) > 1:
                            app_links.add(faltudata['topBarSponsor']['playStoreUrl'])
                    # if 'iosStoreUrl' in faltudata['topBarSponsor'] and faltudata['topBarSponsor']['iosStoreUrl'] != None and len(faltudata['topBarSponsor']['iosStoreUrl']) > 1:
                    #         app_links.add(faltudata['topBarSponsor']['iosStoreUrl'])
                    url = faltudata['topBarSponsor']['logo']
                    # sponsor_list.add(url)
                    local_filename = url.split('/')[-1]
                    value[i][j]['topBarSponsor']['logo'] = POSTER_STATIC+local_filename

                    url = faltudata['topBarSponsor']['image']
                    # sponsor_list.add(url)
                    local_filename = url.split('/')[-1]
                    value[i][j]['topBarSponsor']['image'] = POSTER_STATIC+local_filename

                    if faltudata['topBarSponsor']['packageName'] is None:
                        value[i][j]['topBarSponsor']['downloadUrl'] = ''

                    if faltudata['topBarSponsor']['packageName'] != '':
                        value[i][j]['topBarSponsor']['downloadUrl'] = DOWNLOAD_STATIC_APP+faltudata['topBarSponsor']['ppId']+'/ANDROID'

# for saving new json
if arguments.dest_file is not None:
    with open(arguments.dest_file, 'w') as data_file:
        data_file.write(json.dumps(readjson, indent=4))
    data_file.close()
    print 'Converted json:\t', arguments.dest_file

if arguments.media_file is not None:
    outfile2 = open(arguments.media_file, 'w')
    for key, value in localurl.iteritems():
        split_name = str(value).split('/')[-1].split('.')
        raw_id = split_name[0]
        extension = split_name[1]
        old_name = "%s.%s" % (raw_id, extension)
        new_name = "%s.%s" % (key, extension)
        outfile2.write('%s,%s,%s\n' % (old_name, new_name, value))

    outfile2.close()
    print 'Media Url File:\t', arguments.media_file
    print 'Content Items:\t', len(localurl)

if arguments.applinks_file is not None:
    appfile = open(arguments.applinks_file, 'w')
    for applink in app_links:
        appfile.write(applink+'\n')
    appfile.close()
    print 'App Links File:\t', arguments.applinks_file

error_file = open('errorlinks.txt', 'w')

def download_file(url, directory, renamed=None):
    local_filename = url.split('/')[-1]
    local_filename = urllib.unquote(local_filename).decode('utf8').split('?')[0]
    message = 'Downloading... ' + local_filename
    if renamed is not None:
        local_filename = renamed
        message += ' as ' + local_filename
    filepath = '%s/%s' % (directory, local_filename)
    if os.path.isfile(filepath):
        print 'File already exists...' + local_filename
    else:
        print message
        try:
            r = requests.get(url, stream=True, timeout=300)
            with open(filepath, 'wb') as f:
                for chunk in r.iter_content(chunk_size=1024):
                    if chunk:
                        f.write(chunk)
            f.close()
        except:
            print 'ERROR: ', url
            error_file.write(url + '\n')
    return True

# master_list = list(channel_list) + list(content_list) + list(sponsor_list)
# master_list = list(set(master_list))
# print download_object
# if arguments.no_download is False:
#     sys.exit(0)

# for downloading files
if arguments.download_all is True:
    for key, value in download_object.iteritems():
        try:
            for key2, value2 in value.iteritems():
                try:
                    if key2 != 'localUrl':
                        download_file(value2, download_location)
                except Exception, e:
                    print e
                    continue
        except Exception, e:
            print e
            continue
    print "All Downloaded...!"
    # sys.exit(0)

if arguments.download_string is not None and arguments.download_string in download_object:
    if arguments.entity_type is not None:
        renamed = None
        if arguments.entity_type in download_object[arguments.download_string]:
            if arguments.entity_type == 'localUrl':
                tempextension = download_object[arguments.download_string][arguments.entity_type].split('/')[-1].split('.')[-1]
                renamed = arguments.download_string + '.' + tempextension
            download_file(download_object[arguments.download_string][arguments.entity_type],download_location, renamed)
        else:
            print "Couldn't Find Key!"
    else:
        for key in download_object[arguments.download_string]:
            print download_object[arguments.download_string]
            renamed = None
            if key == 'localUrl':
                tempextension = download_object[arguments.download_string][key].split('/')[-1].split('.')[-1]
                renamed = arguments.download_string + '.' + tempextension
            download_file(download_object[arguments.download_string][key],download_location, renamed)
