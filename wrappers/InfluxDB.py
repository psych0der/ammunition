from influxdb import InfluxDBClient
import config

def init():
    return InfluxDBClient(config.INFLUX_DB['HOST'], config.INFLUX_DB['PORT'], config.INFLUX_DB['USER'], config.INFLUX_DB['PASSWORD'], config.INFLUX_DB['DB'])
